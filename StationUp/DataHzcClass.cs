using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.Win32;
using System.Security.Permissions;
using System.IO;
namespace StationUp
{
    /// <summary>
    /// 主类 核子秤类
    /// </summary>
    class DataHzcClass : DataOption
    {

        #region 数据初始化
        string strAddress = null, strPass = null;
        OleDbConnection hzcConn = new OleDbConnection();
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int hzcDevScale = 0,nTest=0;
            string hzcDevName = null,strComm=null;
            nUnitID=base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("核子秤GetInitPara函数错误号为：800201");
                return -1;
            }
            try
            {
                OleDbDataReader hzcReader = GetHzcDynData(AddressMachine, PassMachine);
                if (hzcReader == null)
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                while (hzcReader.Read())
                {

                    hzcDevName = Convert.ToString(hzcReader["scname"]);
                    hzcDevScale = Convert.ToInt32(hzcReader["scale"]);

                    //if (strCheckDev.Substring(hzcDevScale, 1) == "1")//选中的不传输
                     //  continue;
                    hzcDevScale = nUnitID * 100 + hzcDevScale;//计算编号
                    nTest = SelectData(2, nUnitID, strName, hzcDevScale, hzcDevName);//第三个参数暂时不用 通过秤名字和矿区ID号判断
                    if (nTest == -1)//
                        return -3;
                    if (nTest == 1)//有数据
                    {
                        strComm = "update  s_device set DeviceName='" + hzcDevName + "' where DeviceID=" + hzcDevScale + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }

                    if (nTest == 0)//无数据
                    {
                        
                        strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + hzcDevScale + ",'" + hzcDevName + "'," + nUnitID + ",1,1,1,2)";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }
                    //***************************************
                   
                }
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DataHzcClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
        #endregion

        #region 从access中获取核子秤的动态与班产量信息
        /// <summary>
        /// 得到核子秤数据库的连接
        /// </summary>
        /// <param name="strPass"></param>
        /// <returns></returns>
        private OleDbConnection GetHzcConnection(string strAddress,string strPass)
        {
            
            if (hzcConn.State == ConnectionState.Open)
            {
                return hzcConn;
            }
            string strPath = "\\\\";
            if (strAddress != "127.0.0.1")
            {
                strPath = strAddress;
                strPath += "\\db1.mdb";
            }
            else
            {
                strPath = @"C:\KJH-N核子秤监控系统XP\Data\db1.mdb";
            }
            string strHzcConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+strPath+";Persist Security Info=False;Jet OLEDB:Database Password="+strPass+"";
            
            try
            {
                hzcConn.ConnectionString=strHzcConn;
                if (hzcConn.State == ConnectionState.Closed)
                    hzcConn.Open();
                return hzcConn;
            }
            catch(Exception ex)
            {
                this.WriteLog("GetHzcConnection 函数 " + ex.ToString());
                return null;
            }
        }
        
        /// <summary>
        /// 得到核子秤的动态数据
        /// </summary>
        /// <param name="hzcAddress">核子秤地址</param>
        /// <param name="hzcPass">密码</param>
        /// <returns>数据集</returns>
        private OleDbDataReader GetHzcDynData(string hzcAddress,string hzcPass)
        {
            try
            {
                string strComm = "select * from dym";
                OleDbCommand hzcComm = new OleDbCommand(strComm, GetHzcConnection(hzcAddress,hzcPass));
                OleDbDataReader hzcReader = hzcComm.ExecuteReader();
                return hzcReader;
            }
            catch(Exception ex)
            {
                this.WriteLog("GetHzcDynData 函数 " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 得到核子秤的Scname数据
        /// </summary>
        /// <param name="hzcAddress">核子秤地址</param>
        /// <param name="hzcPass">密码</param>
        /// <returns>数据集</returns>
        private float GetHzcScnameData(string hzcAddress, string hzcPass,int nDev)
        {
            try
            {
                float  hzcTotal=0,hzcOneTotal = 0, hzcTwoTotal = 0, hzcThreeTotal = 0, hzcFourTotal = 0;
                string strComm = "select scale,scname,af,f1,f2,f3,f4 from scname where scale="+nDev+"";
                OleDbCommand hzcComm = new OleDbCommand(strComm, GetHzcConnection(hzcAddress, hzcPass));
                OleDbDataReader hzcReader = hzcComm.ExecuteReader();
                while (hzcReader.Read())
                {
                    if (hzcReader["f1"].GetType().Name == "DBNull")
                        hzcOneTotal = 0;
                    else
                    {
                        hzcOneTotal = Convert.ToSingle(hzcReader["f1"]);
                        hzcOneTotal = hzcOneTotal < 0 ? 0 : hzcOneTotal;
                    }

                    if (hzcReader["f2"].GetType().Name == "DBNull")
                        hzcTwoTotal = 0;
                    else
                    {
                        hzcTwoTotal = Convert.ToSingle(hzcReader["f2"]);
                        hzcTwoTotal = hzcTwoTotal < 0 ? 0 : hzcTwoTotal;
                    }

                    if (hzcReader["f3"].GetType().Name == "DBNull")
                        hzcThreeTotal = 0;
                    else
                    {
                        hzcThreeTotal = Convert.ToSingle(hzcReader["f3"]);
                        hzcThreeTotal = hzcThreeTotal < 0 ? 0 : hzcThreeTotal;
                    }

                    if (hzcReader["f4"].GetType().Name == "DBNull")
                        hzcFourTotal = 0;
                    else
                    {
                        hzcFourTotal = Convert.ToSingle(hzcReader["f4"]);
                        hzcFourTotal = hzcFourTotal < 0 ? 0 : hzcFourTotal;
                    }


                    
                }
                hzcTotal = hzcOneTotal + hzcTwoTotal + hzcThreeTotal + hzcFourTotal;
                return hzcTotal;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHzcScnameData 函数 " + ex.ToString());
                return 800201;
            }
        }
        /// <summary>
        /// 得到核子秤的班产量信息
        /// </summary>
        /// <param name="strOldTime"></param>
        /// <returns></returns>
        private OleDbDataReader GetHzcOutput(string strOldTime, string hzcAddress, string hzcPass)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                DateTime newDT=DateTime.Now.AddDays(1);
                string strComm = "SELECT * FROM dataout WHERE date > #" + oldDT.ToString("yyyy-MM-dd") + "# order by date";
              //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                OleDbCommand hzcComm = new OleDbCommand(strComm,GetHzcConnection(hzcAddress,hzcPass));
                OleDbDataReader hzcReader = hzcComm.ExecuteReader();
                hzcComm.Dispose();
                return hzcReader;
            }
            catch(Exception ex)
            {
                this.WriteLog("GetHzcOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion
       
        #region 核子秤数据写入到服务器
        /// <summary>
        /// 向服务器中写动态数据
        /// </summary>
        /// <param name="hzcID">矿区编号</param>
        /// <returns>错误编号</returns>
        public int HZCWriteDataDyn(int nUnitID,string hzcAddress,string hzcPass,string strCheckDev,System.Windows.Forms.ListBox lb)
        {
            try
            {
                string strComm = null, hzcDevName = null, hzcState = null; ;
                int hzcDevScale=0;
                float hzcSpeed = 0, hzcBurthen = 0, hzcFlow = 0, hzcFta = 0, hzcFma = 0,hzcSignel=0,hzcZero=0;
     
                float hzcChangeValue = 0;
                hzcChangeValue = this.GetChangeValue();
                OleDbDataReader hzcReader= GetHzcDynData(hzcAddress,hzcPass);
                if (hzcReader == null)
                    return 800204;
               
                SqlConnection conn = GetConnection(lb);
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                while (hzcReader.Read())
                {
                        hzcDevScale = Convert.ToInt32(hzcReader["scale"]);
                        //if (strCheckDev.Substring(hzcDevScale, 1) == "1")//选中的不传输
                         //   continue;
                        
                        hzcDevName = Convert.ToString(hzcReader["scname"]);
                        hzcSignel = Convert.ToSingle(hzcReader["fsg"]);
                        hzcZero = Convert.ToSingle(hzcReader["fzo"]);
                        string strTemp = hzcReader["ssp"].ToString();
                        hzcState = strTemp;//增加核子秤状态
                        if(IsNumber(strTemp))
                            hzcSpeed = Convert.ToSingle(hzcReader["ssp"]);
                        else
                            hzcSpeed=0;
                        hzcBurthen = Convert.ToSingle(hzcReader["fld"]);
                        hzcFlow = Convert.ToSingle(hzcReader["ffw"]);
                        this.WriteLog("之前的数值" + Convert.ToSingle(hzcReader["fta"]).ToString());
                        if (strCheckDev.Substring(8, 1) == "1")
                        {
                            hzcFta = Convert.ToSingle(hzcReader["fta"]) * hzcChangeValue;//系数
                            hzcFma = Convert.ToSingle(hzcReader["fma"]) * hzcChangeValue;//系数
                            this.WriteLog("hzfta与hzcfma系数加过后的数值为"+ hzcFta.ToString() + "与" + hzcFma.ToString());
                        }
                        else
                        {
                            hzcFta = Convert.ToSingle(hzcReader["fta"]);//系数
                            hzcFma = Convert.ToSingle(hzcReader["fma"]);//系数
                            this.WriteLog("无系数" + strCheckDev);
                        }
                        float hzcTotal =0;//= GetHzcScnameData(hzcAddress, hzcPass, hzcDevScale);
                        if (hzcTotal < 0)//出现错误
                            hzcTotal = 0;
                        hzcTotal = hzcTotal + hzcFta;
                        if (strCheckDev.Substring(10, 1) == "1")
                            hzcTotal = Convert.ToSingle(hzcReader["ftd"]);//日产
                        else
                            hzcTotal = Convert.ToSingle(hzcReader["kf"]);//日产
                        //**************************更新dyn表****************************
                        hzcDevScale=nUnitID * 100 + hzcDevScale;//核子秤的编号
                        int nTest = SelectData(0, nUnitID, null, hzcDevScale, hzcDevName);//    
                        if(nTest==-1)
                            return 800205;
                        if (nTest==1)
                            strComm = "update s_JLDyn set CurDT='" + DateTime.Now.ToString() + "',Speed=" + hzcSpeed + ",signal="+hzcSignel+",zero="+hzcZero+",Burthen=" + hzcBurthen + ",Flow=" + hzcFlow + ",ClassOP=" + hzcFta + ",DayOP="+ hzcTotal+",MonthOP=" + hzcFma + ",Remark='"+hzcState+"' where DeviceID=" + hzcDevScale + "";
                        if(nTest==0)
                            strComm = "insert into s_JLDyn (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + hzcDevScale + ",'" + DateTime.Now.ToString() + "'," + hzcSignel + " ," + hzcSpeed + "," + hzcBurthen + "," + hzcFlow + ","+hzcZero+",0,0," + hzcFta + ","+hzcTotal+"," + hzcFma + ",0,'"+hzcState+"') ";
                        this.WriteLog("HZCWriteDataDyn 函数"+strComm);
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        //*************************添加到历史表****************************************
                        strComm = "insert into s_JLHis (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + hzcDevScale + ",'" + DateTime.Now.ToString() + "'," + hzcSignel + " ," + hzcSpeed + "," + hzcBurthen + "," + hzcFlow + ","+hzcZero+",0,0," + hzcFta + ","+hzcTotal+"," + hzcFma + ",0,'"+hzcState+"') ";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                }
                if(conn.State==ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                sqlComm.Dispose();
                hzcReader.Close();
                hzcReader.Dispose();
                return 0;
        }
            catch(Exception ex)
            {
                this.WriteLog("HZCWriteDataDyn 函数 " + ex.ToString());
                return 800206;
            }

        }
        /// <summary>
        /// 向服务器写核子秤的班产量
        /// </summary>
        /// <param name="hzcID">矿区编号</param>
        /// <returns></returns>
        public int HZCWriteDataOutput(int nUnitID, string strAddress, string strPass, int nHour, int nMin, string strCheckDev,System.Windows.Forms.ListBox lb)
        {
            try
            {
                string strComm = null;
                float hzcOut1 = 0, hzcOut2 = 0, hzcOut3 = 0, hzcOut4 = 0, hzcTotal = 0;
                int hzcDevScale=0,nTestData=0;

                string strCurDT = null,strDeviceName=null;
                string[] strInfo=new string[2];
                float hzcChangeValue = 0;
               
                hzcChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(0);//得到时间 以及是否上传过
               
                bool bHzcHour=(DateTime.Now.Hour == nHour);
                bool bHzcMin=(DateTime.Now.Minute== nMin);
                //新桥的产量
                
                //if (bHzcHour && bHzcMin)
                //{
                    this.WriteLog("HZCWriteDataOutput 函数 正在读取数据--");
                    OleDbDataReader hzcReader = GetHzcOutput(strInfo[0],strAddress,strPass);
                    SqlConnection conn = GetConnection(lb);
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.Connection = conn;
                    while (hzcReader.Read())
                    {
                        
                        this.WriteLog("HZCWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                        hzcDevScale = Convert.ToInt32(hzcReader["scale"]);
                        strDeviceName = hzcReader["scname"].GetType().Name=="DBNull"?null:hzcReader["scname"].ToString();
                        //if (strCheckDev.Substring(hzcDevScale, 1) == "1")//选中的不传输
                        //    continue;
                        hzcDevScale = nUnitID * 100 + hzcDevScale;
                        strCurDT = hzcReader["date"].ToString();
                        nTestData = this.SelectData(6, nUnitID, null, hzcDevScale, strCurDT);//监测数据库中是否有数据
                       
                        if (nTestData == 1)
                            continue;
                        if (hzcReader["output1"].GetType().Name == "DBNull")
                            hzcOut1 = 0;
                        else
                        {
                            if (strCheckDev.Substring(9, 1) == "1")//若果选中乘系数
                               hzcOut1 = Convert.ToSingle(hzcReader["output1"]) * hzcChangeValue;
                            else
                                hzcOut1 = Convert.ToSingle(hzcReader["output1"]);
                        }
                        if (hzcReader["output2"].GetType().Name == "DBNull")
                            hzcOut2 = 0;
                        else
                        {
                            if (strCheckDev.Substring(9, 1) == "1")//若果选中乘系数
                                hzcOut2 = Convert.ToSingle(hzcReader["output2"]) * hzcChangeValue;
                            else
                                hzcOut2 = Convert.ToSingle(hzcReader["output2"]);
                        }
                        if (hzcReader["output3"].GetType().Name == "DBNull")
                            hzcOut3 = 0;
                        else
                        {
                            if (strCheckDev.Substring(9, 1) == "1")//若果选中乘系数
                                hzcOut3 = Convert.ToSingle(hzcReader["output3"]) * hzcChangeValue;
                            else
                                hzcOut3 = Convert.ToSingle(hzcReader["output3"]);
                        }
                        if (hzcReader["output4"].GetType().Name == "DBNull")
                            hzcOut4 = 0;
                        else
                        {
                            if (strCheckDev.Substring(9, 1) == "1")//若果选中乘系数
                                hzcOut4 = Convert.ToSingle(hzcReader["output4"]) * hzcChangeValue;
                            else
                                hzcOut4 = Convert.ToSingle(hzcReader["output4"]);
                        }
                        hzcTotal = hzcOut1 + hzcOut2 + hzcOut3 + hzcOut4;
                        strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + hzcDevScale + ",'" + strCurDT + "'," + hzcOut1 + "," + hzcOut2 + "," + hzcOut3 + "," + hzcOut4 + "," + hzcTotal + ") ";
                        
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.SetBanDataInfo(0, strCurDT);//保存时间到注册表
                        lb.Items.Add("核子秤数据正在更新 " +strDeviceName+" "+ strCurDT.ToString() + "班产量数据,传输时间 " + DateTime.Now.ToString());
                    }
                    
                    sqlComm.Dispose();
                    if(conn.State==ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                    hzcReader.Close();
                    hzcReader.Dispose();
                   
                    this.WriteLog("核子秤数据正在更新 " + strCurDT.ToString() +"班产量数据");
               // }
                
                return 0;
            }
            catch(Exception ex)
            {
                this.WriteLog("HZCWriteDataOutput 函数 " + ex.ToString());
                return 800207;
            }
        }
        #endregion

    

    }
}
