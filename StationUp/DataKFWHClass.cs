using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using Microsoft.Win32;
using System.Security.Permissions;
using System.IO;
namespace StationUp
{
    /// <summary>
    /// 主类 开封万惠/清华激光秤类
    /// </summary>
    class DataKFWHClass : DataOption
    {

        #region 数据初始化
        string strAddress = null, strPass = null,strUser=null;
        SqlConnection kfwhConn = new SqlConnection();
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        public string UserMaching
        {
            get { return strUser; }
            set { strUser = value; }
        }
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int kfwhDevScale = 0, nTest = 0;
            string kfwhDevName = null, strComm = null;
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("开封万惠/清华激光秤皮带秤GetInitPara函数错误号为：-1");
                return -1;
            }
            try
            {
                SqlDataReader kfwhReader = GetKFWHDynData();
                if (kfwhReader == null)
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                while (kfwhReader.Read())
                {

                    kfwhDevName = "皮带秤";//Convert.ToString(kfwhReader["scname"]);
                    kfwhDevScale = Convert.ToInt32(kfwhReader["scale"]);

                    //if (strCheckDev.Substring(kfwhDevScale, 1) == "1")//选中的不传输
                    //  continue;
                    kfwhDevScale = nUnitID * 100 + kfwhDevScale;//计算编号
                    nTest = SelectData(2, nUnitID, strName, kfwhDevScale, kfwhDevName);//第三个参数暂时不用 通过秤名字和矿区ID号判断
                    if (nTest == -1)//
                        return -3;
                    if (nTest == 1)//有数据
                    {
                        strComm = "update  s_device set DeviceName='" + kfwhDevName + "' where DeviceID=" + kfwhDevScale + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }

                    if (nTest == 0)//无数据
                    {

                        strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + kfwhDevScale + ",'" + kfwhDevName + "'," + nUnitID + ",1,1,1,2)";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }
                    //***************************************

                }
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DatakfwhClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
        #endregion

        #region 获取开封万惠/清华激光秤的动态与班产量信息
        /// <summary>
        /// 得到开封万惠/清华激光秤数据库的连接
        /// </summary>
        /// <param name="strPass"></param>
        /// <returns></returns>
        private SqlConnection GetkfwhConnection()
        {

            string strConnSql = "data source=" + AddressMachine + ";initial catalog=whhzc;user id=" + UserMaching + ";password=" + PassMachine + "";
            try
            {
                SqlConnection sqlConn = new SqlConnection(strConnSql);
                if (sqlConn.State == ConnectionState.Closed)
                    sqlConn.Open();
                return sqlConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("开封万惠/清华激光秤DataOption下的GetConnection 函数" + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// 得到开封万惠/清华激光秤的动态数据
        /// </summary>
        /// <param name="kfwhAddress">开封万惠/清华激光秤地址</param>
        /// <param name="kfwhPass">密码</param>
        /// <returns>数据集</returns>
        private SqlDataReader GetKFWHDynData()
        {
            try
            {
                string strComm = "select * from jldym";
                SqlCommand kfwhComm = new SqlCommand(strComm, GetkfwhConnection());
                SqlDataReader kfwhReader = kfwhComm.ExecuteReader();
                return kfwhReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetKFWHDynData 函数 " + ex.ToString());
                return null;
            }
        }
       
        /// <summary>
        /// 得到开封万惠/清华激光秤的班产量信息
        /// </summary>
        /// <param name="strOldTime"></param>
        /// <returns></returns>
        private SqlDataReader GetkfwhOutput(string strOldTime)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                DateTime newDT = DateTime.Now.AddDays(1);
                string strComm = "SELECT * FROM jldataout WHERE date > '" + oldDT.ToString("yyyy-MM-dd") + "' order by date";
                //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                SqlCommand kfwhComm = new SqlCommand(strComm, GetkfwhConnection());
                SqlDataReader kfwhReader = kfwhComm.ExecuteReader();
                kfwhComm.Dispose();
                return kfwhReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetkfwhOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 开封万惠/清华激光秤数据写入到服务器
        /// <summary>
        /// 向服务器中写动态数据
        /// </summary>
        /// <param name="kfwhID">矿区编号</param>
        /// <returns>错误编号</returns>
        public int kfwhWriteDataDyn(int nUnitID, string strCheckDev)
        {
            try
            {
                string strComm = null, kfwhDevName = null, kfwhState = null,strCurDT=null; ;
                int kfwhDevScale = 0;
                float kfwhSpeed = 0, kfwhBurthen = 0, kfwhFlow = 0, kfwhFta = 0, kfwhDay=0,kfwhFma = 0, kfwhSignel = 0, kfwhZero = 0;

                float kfwhChangeValue = 0;
                kfwhChangeValue = this.GetChangeValue();
                SqlDataReader kfwhReader = GetKFWHDynData();
                if (kfwhReader == null)
                    return 801104;

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                while (kfwhReader.Read())
                {
                    kfwhDevScale = Convert.ToInt32(kfwhReader["scale"]);
                    //if (strCheckDev.Substring(kfwhDevScale, 1) == "1")//选中的不传输
                    //   continue;

                    kfwhDevName = "皮带秤";//Convert.ToString(kfwhReader["scname"]);
                    strCurDT = kfwhReader["curdt"].ToString();
                    kfwhSignel = Convert.ToSingle(kfwhReader["fsg"]);
                    kfwhZero = Convert.ToSingle(kfwhReader["fzo"]);
                    string strTemp = kfwhReader["ssp"].ToString();
                    kfwhState = strTemp;//增加开封万惠/清华激光秤状态
                    if (IsNumber(strTemp))
                        kfwhSpeed = Convert.ToSingle(kfwhReader["ssp"]);
                    else
                        kfwhSpeed = 0;

                    if (kfwhSpeed > 0)//状态
                    {
                        kfwhState = "开机";
                    }
                    else
                    {
                        kfwhSpeed = 0;
                        kfwhState = "停机";
                    }

                    kfwhBurthen = Convert.ToSingle(kfwhReader["fld"]);
                    kfwhFlow = Convert.ToSingle(kfwhReader["ffw"]);
                    this.WriteLog("之前的数值" + Convert.ToSingle(kfwhReader["fta"]).ToString());
                 
                        kfwhFta = Convert.ToSingle(kfwhReader["fta"]);//系数
                        kfwhFma = Convert.ToSingle(kfwhReader["fma"]);//系数
                        this.WriteLog("无系数" + strCheckDev);
                  
                   
                    //**************************更新dyn表****************************
                    kfwhDevScale = nUnitID * 100 + kfwhDevScale;//开封万惠/清华激光秤的编号
                    int nTest = SelectData(0, nUnitID, null, kfwhDevScale, kfwhDevName);//    
                    if (nTest == -1)
                        return 801105;
                    if (nTest == 1)
                        strComm = "update s_JLDyn set CurDT='" + strCurDT + "',Speed=" + kfwhSpeed + ",signal=" + kfwhSignel + ",zero=" + kfwhZero + ",Burthen=" + kfwhBurthen + ",Flow=" + kfwhFlow + ",ClassOP=" + kfwhFta + ",DayOP=" + kfwhDay + ",MonthOP=" + kfwhFma + ",Remark='" + kfwhState + "' where DeviceID=" + kfwhDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_JLDyn (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + kfwhDevScale + ",'" + strCurDT + "'," + kfwhSignel + " ," + kfwhSpeed + "," + kfwhBurthen + "," + kfwhFlow + "," + kfwhZero + ",0,0," + kfwhFta + "," + kfwhDay + "," + kfwhFma + ",0,'" + kfwhState + "') ";
                    this.WriteLog("kfwhWriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    //*************************添加到历史表****************************************
                    strComm = "insert into s_JLHis (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + kfwhDevScale + ",'" + DateTime.Now.ToString() + "'," + kfwhSignel + " ," + kfwhSpeed + "," + kfwhBurthen + "," + kfwhFlow + "," + kfwhZero + ",0,0," + kfwhFta + "," + kfwhDay + "," + kfwhFma + ",0,'" + kfwhState + "') ";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                sqlComm.Dispose();
                kfwhReader.Close();
                kfwhReader.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("kfwhWriteDataDyn 函数 " + ex.ToString());
                return 801106;
            }

        }
        /// <summary>
        /// 向服务器写开封万惠/清华激光秤的班产量
        /// </summary>
        /// <param name="kfwhID">矿区编号</param>
        /// <returns></returns>
        public int kfwhWriteDataOutput(int nUnitID, string strCheckDev)
        {
            try
            {
                string strComm = null;
                float kfwhOut1 = 0, kfwhOut2 = 0, kfwhOut3 = 0, kfwhOut4 = 0, kfwhTotal = 0;
                int kfwhDevScale = 0, nTestData = 0;

                string strCurDT = null;
                string[] strInfo = new string[2];
                float kfwhChangeValue = 0;

                kfwhChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(10);//得到时间 以及是否上传过

               

                //if (bkfwhHour && bkfwhMin)
                //{
           
                SqlDataReader kfwhReader = GetkfwhOutput(strInfo[0]);
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                while (kfwhReader.Read())
                {

                    
                    kfwhDevScale = Convert.ToInt32(kfwhReader["scale"]);
                    //if (strCheckDev.Substring(kfwhDevScale, 1) == "1")//选中的不传输
                    //    continue;
                    if (kfwhDevScale == 0)
                    {
                        this.WriteLog("kfwhWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                        kfwhDevScale = nUnitID * 100 + kfwhDevScale;
                        strCurDT = kfwhReader["date"].ToString();
                        nTestData = this.SelectData(6, nUnitID, null, kfwhDevScale, strCurDT);//监测数据库中是否有数据

                        if (nTestData == 1)
                            continue;
                        if (kfwhReader["output1"].GetType().Name == "DBNull")
                            kfwhOut1 = 0;
                        else
                        {
                            kfwhOut1 = Convert.ToSingle(kfwhReader["output1"]);
                        }
                        if (kfwhReader["output2"].GetType().Name == "DBNull")
                            kfwhOut2 = 0;
                        else
                        {
                            kfwhOut2 = Convert.ToSingle(kfwhReader["output2"]);
                        }
                        if (kfwhReader["output3"].GetType().Name == "DBNull")
                            kfwhOut3 = 0;
                        else
                        {
                            kfwhOut3 = Convert.ToSingle(kfwhReader["output3"]);
                        }
                        if (kfwhReader["output4"].GetType().Name == "DBNull")
                            kfwhOut4 = 0;
                        else
                        {
                            kfwhOut4 = Convert.ToSingle(kfwhReader["output4"]);
                        }
                        kfwhTotal = kfwhOut1 + kfwhOut2 + kfwhOut3 + kfwhOut4;
                        strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + kfwhDevScale + ",'" + strCurDT + "'," + kfwhOut1 + "," + kfwhOut2 + "," + kfwhOut3 + "," + kfwhOut4 + "," + kfwhTotal + ") ";

                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.WriteLog("kfwhWriteDataOutput 函数 正在上传  " + strCurDT + "  班产数据");
                        this.SetBanDataInfo(10, strCurDT);//保存时间到注册表
                    }
                }

                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                kfwhReader.Close();
                kfwhReader.Dispose();
                //lb.Items.Add("开封万惠/清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("开封万惠/清华激光秤数据正在发送班产量数据 ");
                // }

                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("kfwhWriteDataOutput 函数 " + ex.ToString());
                return 801107;
            }
        }
        #endregion



    }
}
