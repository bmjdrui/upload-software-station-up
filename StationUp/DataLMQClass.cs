using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using MySql.Data;
using MySql.Data.Common;
namespace StationUp
{
    class DataLMQClass : DataOption
    {
        #region 数据初始化
        string strAddress = null, strPass = null,strUser="root";
        /// <summary>
        /// 电子秤数据库地址
        /// </summary>
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        /// <summary>
        /// 电子秤密码
        /// </summary>
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        public string UserMachine
        {
            get { return strUser; }
            set { strUser = value; }
        }
        /// <summary>
        /// 拉姆齐电子称初始化
        /// </summary>
        /// <param name="nUnitID">编号</param>
        /// <param name="strName"></param>
        /// <param name="strCheckDev"></param>
        /// <returns></returns>
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {

            int dzcDevScale = 0, nTest = 0;
            string dzcDevName = null, strComm = null;
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("拉姆齐电子秤GetInitPara函数错误号为： 90090--无法从集团公司中心数据库获取单位编号，请联系系统管理员");
                return -1;
            }
            try
            {
                MySqlDataReader dzcReader = GetDzcDynData();
                if (dzcReader == null)
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                while(dzcReader.Read())
                {
                                       
                    dzcDevScale = Convert.ToInt32(dzcReader[0])-1;//序号,修正为从0开始
                    if (strCheckDev.Substring(dzcDevScale, 1) != "1")
                        continue;
                    dzcDevName = "皮带秤" + dzcDevScale.ToString();//设备名称 矿原煤吊挂称;称4:矿原煤架空称
                    dzcDevScale = nUnitID * 100 + dzcDevScale;//计算编号
                    nTest = SelectData(2, nUnitID, strName, dzcDevScale, dzcDevName);//第三个参数暂时不用 通过矿区ID号判断
                    if (nTest == -1)//
                    {
                        this.WriteLog("读取SQL SERVER数据库错误 错误号:92");
                        return -3;
                    }

                    if (nTest == 1)//有数据
                    {
                        strComm = "update  s_device set DeviceName='" + dzcDevName + "' where DeviceID=" + dzcDevScale + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }

                    if (nTest == 0)//无数据
                    {

                        strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + dzcDevScale + ",'" + dzcDevName + "'," + nUnitID + ",1,1,1,2)";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }
                    //***************************************
                }
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("拉姆齐DataLMQClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
        #endregion

        #region 拉姆齐电子称数据初始化
        /// <summary>
        /// 得到拉姆齐MySql电子称数据库的链接
        /// </summary>
        /// <param name="strAddressMachine">地址</param>
        /// <param name="strPassMachine">密码</param>
        /// <returns>链接对象</returns>
        private MySqlConnection GetDzcDynConnection()
        {
            try
            {
                
                //mysql数据库连接需要加入字符编码设置Charset=gb2312 
                string constr = null;
                MySqlConnection mycn = new MySqlConnection();
                constr = "User Id=" + UserMachine + ";Host=" + AddressMachine + ";Database=pdc;password=" + PassMachine + ";Charset=gb2312";
                mycn.ConnectionString = constr;
                if (mycn.State == ConnectionState.Open)
                {
                    mycn.Close();
                }
                if(mycn.State==ConnectionState.Closed)
                    mycn.Open();
                return mycn;
               
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// 获取拉姆齐电子称的流量数据
        /// </summary>
        /// <param name="strAddressMachine">地址</param>
        /// <param name="strPassMachine">密码</param>
        /// <returns>数据对象</returns>
        private MySqlDataReader GetDzcDynData()
        {
            try
            {
                
                string strComm = "select * from ssdat order by 序号";//ssdat
                MySqlCommand dzcComm = new MySqlCommand(strComm, GetDzcDynConnection());
                MySqlDataReader dzcReader = dzcComm.ExecuteReader();
                dzcComm.Dispose();
                return dzcReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("拉姆齐GetDzcDynData 函数 " + ex.ToString());
                return null;
            }
        }
        // <summary>
        /// 得到拉姆齐电子秤的班产量信息
        /// </summary>
        /// <param name="strOldTime">时间</param>
        /// <returns>MySqlDataReader数据集</returns>
        private MySqlDataReader GetDzcOutput(string strOldTime)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                DateTime newDT = DateTime.Now.AddDays(1);
                string strComm = "SELECT * FROM blj WHERE 日期>= '" + oldDT.ToString("yyyy-MM-dd") + "' order by 日期,时间";
                //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                this.WriteLog("GetDzcOutput 函数   " + strComm);
                MySqlCommand dzcComm = new MySqlCommand(strComm, GetDzcDynConnection());
                MySqlDataReader dzcReader = dzcComm.ExecuteReader();
                dzcComm.Dispose();
                return dzcReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetDzcOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 拉姆齐电子秤数据写入到服务器
        /// <summary>
        /// 向服务器中写动态数据
        /// </summary>
        /// <param name="dzcID">矿区编号</param>
        /// <returns>错误编号</returns>
        public int LMQDzcWriteDataDyn(int nUnitID,string strCheckDev)
        {
            try
            {
                string strComm = null, dzcDevName = null, dzcState = null,strCurDt=null ;
                int dzcDevScale = 0;
                float dzcSpeed = 0, dzcBurthen = 0, dzcFlow = 0, dzcFta = 0, dzcDay = 0, dzcFma = 0, dzcSignel = 0;
                float dzcChangeValue = 0;
                int sendScale = 1;
                dzcChangeValue = 1f;//this.GetChangeValue();
               
                MySqlDataReader dzcReader = GetDzcDynData();
                if (dzcReader == null)
                {
                    this.WriteLog("读取拉姆齐电子秤数据库错误 错误号:94");
                    return 90094;
                }
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                while(dzcReader.Read())
                {

                        sendScale = Convert.ToInt16(dzcReader[0])-1;//序号

                        if (strCheckDev.Substring(sendScale, 1) != "1")
                            continue;
                        dzcDevScale = sendScale;
                        dzcDevName = Convert.ToString(dzcReader[2]);//设备名称
                        dzcSignel = 0;//信号
                       
                        strCurDt = dzcReader[1].ToString();//时间
                        float fSpeed = Convert.ToSingle(dzcReader[4]);//速度
                        if (fSpeed > 0)//状态
                        {
                            dzcSpeed = fSpeed;
                            dzcState = "开机";
                        }
                        else
                        {
                            dzcSpeed = 0;
                            dzcState = "停机";
                        }
                        dzcFta = Convert.ToSingle(dzcReader[6]);//负荷
                        dzcFlow = Convert.ToSingle(dzcReader[5]);//流量
                      

                        dzcFta = Convert.ToSingle(dzcReader[8]);//当前班产
                        dzcFma = Convert.ToSingle(dzcReader[3]);//主累计
                        dzcDay = Convert.ToSingle(dzcReader[9]);//日产                


                        //**************************更新dyn表****************************
                        dzcDevScale = nUnitID * 100 + dzcDevScale;//电子秤的编号
                        int nTest = SelectData(0, nUnitID, null, dzcDevScale, dzcDevName);//    
                        if (nTest == -1)
                            return 90095;
                        if (nTest == 1)
                            strComm = "update s_JLDyn set CurDT='" + strCurDt + "',Speed=" + dzcSpeed + ",Burthen=" + dzcBurthen + ",Flow=" + dzcFlow + ",ClassOP=" + dzcFta + ",DayOP=" + dzcDay + ",MonthOP=" + dzcFma + ",Remark='" + dzcState + "' where DeviceID=" + dzcDevScale + "";
                        if (nTest == 0)
                            strComm = "insert into s_JLDyn (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + dzcDevScale + ",'" + DateTime.Now.ToString() + "'," + dzcSignel + " ," + dzcSpeed + "," + dzcBurthen + "," + dzcFlow + ",0,0,0," + dzcFta + "," + dzcDay + "," + dzcFma + ",0,'" + dzcState + "') ";
                        this.WriteLog("dzcWriteDataDyn 函数" + strComm);
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        //*************************添加到历史表****************************************
                        strComm = "insert into s_JLHis (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + dzcDevScale + ",'" + DateTime.Now.ToString() + "'," + dzcSignel + " ," + dzcSpeed + "," + dzcBurthen + "," + dzcFlow + ",0,0,0," + dzcFta + "," + dzcDay + "," + dzcFma + ",0,'" + dzcState + "') ";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        
                    
                }
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                sqlComm.Dispose();
                dzcReader.Close();
                dzcReader.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("拉姆齐dzcWriteDataDyn 函数 " + ex.ToString());
                return 90096;
            }

        }
        /// <summary>
        /// 向服务器写拉姆齐电子秤的班产量
        /// </summary>
        /// <param name="dzcID">矿区编号</param>
        /// <returns></returns>
        public int LMQDzcWriteDataOutput(int nUnitID, int nHour, int nMin, string strCheckDev)
        {
            try
            {
                string strComm = null;
                float dzcOut1 = 0, dzcTotal = 0;
                int dzcDevScale = 0, nTestData = 0;
                float[,] dzcOutList = new float[8, 5];//班产
                int[] nScaleList = new int[8];//秤号
                string[] dtList = new string[8];//日期
                int nScale = 0, nBanID = 0;//秤号 班次

                string strCurDT = null;
                string[] strInfo = new string[2];
                
                int sendScale = 0;
                
                strInfo = this.GetBanDataInfo(8);//得到时间 以及是否上传过

                bool bdzcHour = (DateTime.Now.Hour == nHour);
                bool bdzcMin = (DateTime.Now.Minute == nMin);
                MySqlDataReader dzcReader = null;
                SqlConnection conn=null;
                SqlCommand sqlComm=null;
                int len = strCheckDev.Length;
                for (int n = 0; n < len - 1; n++)
                {
                    if (strCheckDev.Substring(n, 1) != "1")
                        continue;
                    dzcReader = GetDzcOutput(strInfo[0]);
                    if (dzcReader == null)//有错误
                    {
                        this.WriteLog("读取拉姆齐电子秤数据库错误 错误号:97");
                        return 90098;
                    }
                    
                    if (!dzcReader.HasRows)//无数据
                       return 90099;
                    conn= GetConnection();
                    sqlComm= new SqlCommand();
                    sqlComm.Connection = conn;
                                       
                    #region  获取三班班产
                    while (dzcReader.Read())
                    {
                        this.WriteLog("拉姆齐dzcWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                        nScale = n;//每台秤
                        sendScale = n+4;//每台秤的产量放在 每一列中
                        dzcDevScale = nUnitID * 100 + nScale;
                        
                        if (strCurDT != dzcReader[0].ToString())
                        {

                            for (int i = 0; i < 8; i++)
                            {
                                for (int m = 0; m < 5; m++)
                                    dzcOutList[i, m] = 0;
                            }
                        }
                        
                        strCurDT = dzcReader[0].ToString();//日期字段，由于mysql汉子编码问题不能用名字标识
                        dtList[nScale] = strCurDT;
                        string strBan= dzcReader[1].ToString();//班次
                        if((strBan=="0班")||(strBan=="夜班"))
                        {
                            nBanID = 1;
                        }
                        if ((strBan == "8班")||(strBan=="早班")||(strBan=="白班"))
                        {
                            nBanID = 2;
                        }
                        if ((strBan == "4班")||(strBan=="中班")||(strBan=="晚班"))
                        {
                            nBanID = 3;
                        }
                        //**************************************
                        //统一格式化

                        //**************************************

                        if (dzcReader[sendScale].GetType().Name == "DBNull")
                            dzcOut1 = 0f;
                        else
                        {
                            dzcOut1 = Convert.ToSingle(dzcReader[sendScale]);//班产
                         }
                        dzcOutList[nScale, nBanID] = dzcOut1;//赋值班产量给数组
                        //***************************************
                        nTestData = this.SelectData(6, nUnitID, null, dzcDevScale, strCurDT);//监测数据库中是否有数据
                        if (nTestData == 1)//更新数据
                        {
                            dzcTotal = dzcOutList[nScale, 1] + dzcOutList[nScale, 2] + dzcOutList[nScale, 3] + dzcOutList[nScale, 4];
                            strComm = "update s_JLOUT set Class1OP=" + dzcOutList[nScale, 1] + ",Class2OP =" + dzcOutList[nScale, 2] + ",Class3OP=" + dzcOutList[nScale, 3] + ",Class4OP=" + dzcOutList[nScale, 4] + ",Total=" + dzcTotal + " where DeviceID=" + dzcDevScale + " and curDt='" + strCurDT + "'";
                            sqlComm.CommandText = strComm;
                            sqlComm.ExecuteNonQuery();
                            this.SetBanDataInfo(8, dtList[nScale]);//保存时间到注册表

                        }
                        else//插入数据
                        {
                            dzcTotal = dzcOutList[nScale, 1] + dzcOutList[nScale, 2] + dzcOutList[nScale, 3] + dzcOutList[nScale, 4];
                            strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + dzcDevScale + ",'" + dtList[nScale] + "'," + dzcOutList[nScale, 1] + "," + dzcOutList[nScale, 2] + "," + dzcOutList[nScale, 3] + "," + dzcOutList[nScale, 4] + "," + dzcTotal + ") ";
                            sqlComm.CommandText = strComm;
                            sqlComm.ExecuteNonQuery();
                            this.SetBanDataInfo(8, dtList[nScale]);//保存时间到注册表
                        }

                        //***************************************
                       
                    }
                        #endregion
                    sqlComm.Dispose();
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                    dzcReader.Close();
                    dzcReader.Dispose();
                }
                   
                //****************************************************
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                dzcReader.Close();
                dzcReader.Dispose();
                //lb.Items.Add("拉姆齐电子秤数据正在发送班产量数据 ");
                this.WriteLog("拉姆齐电子秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                // }

                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("拉姆齐DZCWriteDataOutput 函数 " + ex.ToString());
                return 90100;
            }
        }
        #endregion

    }

}
