using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace StationUp
{
    public partial class FormPass : Form
    {
        bool bModify = false;
        public FormPass()
        {
            InitializeComponent();
        }
        public bool SetModify
        {
            set { bModify = value; }
            get { return bModify; }

        }

        private void FormPass_Load(object sender, EventArgs e)
        {
            textPass.Clear();
        }
        /// <summary>
        /// �˳�
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (textPass.Text.Trim() == "scalepassme")
                bModify = true;
            else
                bModify = false;
            this.Close();
        }

        private void FormPass_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }

        private void FormPass_FormClosing(object sender, FormClosingEventArgs e)
        {
               
        }
    }
}