using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using Microsoft.Win32;
using System.Security.Permissions;
using System.IO;
using System.Net.Sockets;
using System.Net;
namespace StationUp
{
    /// <summary>
    ///清华激光秤 
    /// </summary>
    class DatajgcClass : DataOption
    {

        #region 数据初始化
        string strAddress = null, strPass = null, strUser = null;
        SqlConnection jgcConn = new SqlConnection();
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        public string UserMaching
        {
            get { return strUser; }
            set { strUser = value; }
        }
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int jgcDevScale = 0, nTest = 0;
            string jgcDevName = null, strComm = null;
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("清华激光秤皮带秤GetInitPara函数错误号为：-1");
                return -1;
            }
            try
            {
               
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                jgcDevName = "皮带秤";//Convert.ToString(jgcReader["scname"]);
                jgcDevScale = 0;
                                   
                jgcDevScale = nUnitID * 100 + jgcDevScale;//计算编号
                nTest = SelectData(2, nUnitID, strName, jgcDevScale, jgcDevName);//第三个参数暂时不用 通过秤名字和矿区ID号判断
                if (nTest == -1)//
                    return -3;
                if (nTest == 1)//有数据
                {
                    strComm = "update  s_device set DeviceName='" + jgcDevName + "' where DeviceID=" + jgcDevScale + "";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }

                if (nTest == 0)//无数据
                {
                    strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + jgcDevScale + ",'" + jgcDevName + "'," + nUnitID + ",1,1,1,2)";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                //***************************************

                
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DatajgcClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
        #endregion

        #region 获取清华激光秤的动态与班产量信息
        /// <summary>
        /// 得到清华激光秤数据库的连接
        /// </summary>
        /// <param name="strPass"></param>
        /// <returns></returns>
        private SqlConnection GetjgcConnection()
        {

            string strConnSql = "data source=" + AddressMachine + ";initial catalog=whhzc;user id=" + UserMaching + ";password=" + PassMachine + "";
            try
            {
                SqlConnection sqlConn = new SqlConnection(strConnSql);
                if (sqlConn.State == ConnectionState.Closed)
                    sqlConn.Open();
                return sqlConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("清华激光秤DataOption下的GetConnection 函数" + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// 得到清华激光秤的动态数据
        /// </summary>
        /// <param name="jgcAddress">清华激光秤地址</param>
        /// <param name="jgcPass">密码</param>
        /// <returns>数据集</returns>
        private string [] GetjgcDynData(string strStation, string strPort)
        {
            string[] strArray = new string[3];
            char[] charArray = new Char[] { ',' };
            double[] avgInfo = new double[2];
            string strName = strStation;
           
            TcpClient tcpClient = new TcpClient();
            try
            {
                tcpClient.Connect(IPAddress.Parse(strStation), int.Parse(strPort.Trim()));
                NetworkStream ns = tcpClient.GetStream();
                byte[] data = new Byte[256];
                Int32 bytes = ns.Read(data, 0, data.Length);
                string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                if (responseData!=null)
                {
                    responseData = responseData.Replace("\r\n", " ").Trim();
                    if (responseData == "ERROR")
                        return null;
                    if (responseData == "NOT")
                    {
                        strArray[0] = "NOT";
                        
                    }
                    else
                    {
                        strArray = responseData.Split(charArray);
                       
                    }
                
                    this.WriteLog("清华激光秤网络连接函数GetjgcDynData得到内容:  " + responseData);

                }

                ns.Close();
                tcpClient.Close();
                this.WriteLog("清华激光秤网络连接函数GetjgcDynData" + strName);
                

                /* 增加小时灰分 分钟灰分
                minHui=Convert.ToDouble(strArray[1]);
                avgInfo = GetSqlServerAvg(nUnitID);
                if (avgInfo != null)
                {
                    double tempHui = (avgInfo[0] + minHui) / 2;//分钟灰分
                    strArray[2] = tempHui.ToString();
                    tempHui = (avgInfo[1] + minHui) / 2;//小时灰分
                    strArray[3] = tempHui.ToString();
                }
                else
                {
                    strArray[2] = minHui.ToString();
                    strArray[3] = minHui.ToString();
                }
                */

                return strArray;
            }
            catch (Exception ex)
            {
                tcpClient.Close();
                this.WriteLog("清华激光秤网络连接函数GetjgcDynData" + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// 得到清华激光秤的班产量信息
        /// </summary>
        /// <param name="strOldTime"></param>
        /// <returns></returns>
        private string [] GetjgcOutput(string strStation, string strPort,string strDT)
        {
            string[] strArray = new string[3];
            char[] charArray = new Char[] { ',' };
            double[] avgInfo = new double[2];
            string strName = strStation;
            
            TcpClient tcpClient = new TcpClient();
            try
            {

                tcpClient.Connect(IPAddress.Parse(strStation), int.Parse(strPort.Trim()));
                NetworkStream ns = tcpClient.GetStream();
                byte[] data = new Byte[256];
                Int32 bytes = ns.Read(data, 0, data.Length);
                string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                if (responseData == " on \r\n")
                {
                    byte[] dt = System.Text.Encoding.ASCII.GetBytes(strDT);
                    ns.Write(dt, 0, strDT.Length);
                    System.Threading.Thread.Sleep(3000);
                    bytes = ns.Read(data, 0, data.Length);
                    responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    responseData = responseData.Replace("\r\n", " ").Trim();
                    this.WriteLog("清华灰分仪网络连接函数GethfyCReadDataDyn得到内容:  " + responseData + "传入的时间为:  " + strDT);

                }

                ns.Close();
                tcpClient.Close();
                this.WriteLog("清华灰分仪网络连接函数GethfyCReadDataDyn" + strName);
                if (responseData == "ERROR")
                    return null;
                if (responseData == "NOT")
                {
                    strArray[0] = strDT;
                    strArray[1] = "NOT";
                }
                else
                {
                    strArray = responseData.Split(charArray);
                    strArray[0] = strArray[0].Replace("\r\n", " ");

                }

                /* 增加小时灰分 分钟灰分
                minHui=Convert.ToDouble(strArray[1]);
                avgInfo = GetSqlServerAvg(nUnitID);
                if (avgInfo != null)
                {
                    double tempHui = (avgInfo[0] + minHui) / 2;//分钟灰分
                    strArray[2] = tempHui.ToString();
                    tempHui = (avgInfo[1] + minHui) / 2;//小时灰分
                    strArray[3] = tempHui.ToString();
                }
                else
                {
                    strArray[2] = minHui.ToString();
                    strArray[3] = minHui.ToString();
                }
                */

                return strArray;
            }
            catch (Exception ex)
            {
                tcpClient.Close();
                this.WriteLog("清华灰分仪网络连接函数GethfyCReadDataDyn" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 清华激光秤数据写入到服务器
        /// <summary>
        /// 向服务器中写动态数据
        /// </summary>
        /// <param name="jgcID">矿区编号</param>
        /// <returns>错误编号</returns>
        public int jgcWriteDataDyn(int nUnitID, string strStation ,string strPort,string strCheckDev)
        {
            try
            {
                string strComm = null, jgcDevName = null, jgcState = null, strCurDT = null; ;
                int jgcDevScale = 0;
                float jgcSpeed = 0, jgcBurthen = 0, jgcFlow = 0, jgcFta = 0, jgcDay = 0, jgcFma = 0, jgcSignel = 0, jgcZero = 0;

                float jgcChangeValue = 0;
                jgcChangeValue = this.GetChangeValue();
                string [] jgcReader = GetjgcDynData(strStation,strPort);
                if (jgcReader == null)
                    return 801204;

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                if (jgcReader[0]!="NOT" || jgcReader[0]!="ERROR")
                {
                    jgcDevScale = Convert.ToInt32(jgcReader[0]);
                    //if (strCheckDev.Substring(jgcDevScale, 1) == "1")//选中的不传输
                    //   continue;

                    jgcDevName = "皮带秤";//Convert.ToString(jgcReader["scname"]);
                    strCurDT = jgcReader[1].ToString();
                    jgcSignel = 0;
                    jgcZero = 0;
                    string strTemp = jgcReader[2].ToString();
                    jgcState = strTemp;//增加清华激光秤状态
                    if (IsNumber(strTemp))
                        jgcSpeed = Convert.ToSingle(jgcReader[2]);
                    else
                        jgcSpeed = 0;

                    if (jgcSpeed > 0)//状态
                    {
                        jgcState = "开机";
                    }
                    else
                    {
                        jgcSpeed = 0;
                        jgcState = "停机";
                    }

                    jgcBurthen = 0;
                    jgcFlow = Convert.ToSingle(jgcReader[4]);
                    jgcFta = Convert.ToSingle(jgcReader[5]);//班产
                    jgcDay = Convert.ToSingle(jgcReader[6]);//日产
                    jgcFma = Convert.ToSingle(jgcReader[7]);//月累计
                    
                    this.WriteLog("无系数" + strCheckDev);


                    //**************************更新dyn表****************************
                    jgcDevScale = nUnitID * 100 + jgcDevScale;//清华激光秤的编号
                    int nTest = SelectData(0, nUnitID, null, jgcDevScale, jgcDevName);//    
                    if (nTest == -1)
                        return 801205;
                    if (nTest == 1)
                        strComm = "update s_JLDyn set CurDT='" + strCurDT + "',Speed=" + jgcSpeed + ",signal=" + jgcSignel + ",zero=" + jgcZero + ",Burthen=" + jgcBurthen + ",Flow=" + jgcFlow + ",ClassOP=" + jgcFta + ",DayOP=" + jgcDay + ",MonthOP=" + jgcFma + ",Remark='" + jgcState + "' where DeviceID=" + jgcDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_JLDyn (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + jgcDevScale + ",'" + strCurDT + "'," + jgcSignel + " ," + jgcSpeed + "," + jgcBurthen + "," + jgcFlow + "," + jgcZero + ",0,0," + jgcFta + "," + jgcDay + "," + jgcFma + ",0,'" + jgcState + "') ";
                    this.WriteLog("jgcWriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    //*************************添加到历史表****************************************
                    strComm = "insert into s_JLHis (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + jgcDevScale + ",'" + DateTime.Now.ToString() + "'," + jgcSignel + " ," + jgcSpeed + "," + jgcBurthen + "," + jgcFlow + "," + jgcZero + ",0,0," + jgcFta + "," + jgcDay + "," + jgcFma + ",0,'" + jgcState + "') ";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                sqlComm.Dispose();
               
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("jgcWriteDataDyn 函数 " + ex.ToString());
                return 801206;
            }

        }
        /// <summary>
        /// 向服务器写清华激光秤的班产量
        /// </summary>
        /// <param name="jgcID">矿区编号</param>
        /// <returns></returns>
        public int jgcWriteDataOutput(int nUnitID, string strStation,string strPort,string strCheckDev)
        {
            try
            {
                string strComm = null;
                float jgcOut1 = 0, jgcOut2 = 0, jgcOut3 = 0, jgcOut4 = 0, jgcTotal = 0;
                int jgcDevScale = 0, nTestData = 0;

                string strCurDT = null;
                string[] strInfo = new string[2];
                float jgcChangeValue = 0;

                jgcChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(12);//得到时间 以及是否上传过
                DateTime saveDT = Convert.ToDateTime(strInfo[0]);
                saveDT = Convert.ToDateTime(saveDT.ToString("yyyy-MM-DD")); 
                DateTime nowDT = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                while (saveDT <= nowDT)//当网络连接跨天时
                {
                    string[] jgcReader = GetjgcOutput(strStation,strPort,saveDT.ToString());
                    if (jgcReader == null)
                        return 8001208;
                    SqlConnection conn = GetConnection();
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.Connection = conn;
                    if (jgcReader[0] != "NOT" || jgcReader[0] != "ERROR")
                    {
                        jgcDevScale = Convert.ToInt32(jgcReader[0]);
                        if (jgcDevScale == 0)
                        {
                            this.WriteLog("jgcWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                            jgcDevScale = nUnitID * 100 + jgcDevScale;
                            strCurDT = jgcReader[2].ToString();
                            nTestData = this.SelectData(6, nUnitID, null, jgcDevScale, strCurDT);//监测数据库中是否有数据

                            if (nTestData == 1)
                                continue;
                            if (jgcReader[3].GetType().Name == "DBNull")
                                jgcOut1 = 0;
                            else
                            {
                                jgcOut1 = Convert.ToSingle(jgcReader[3]);
                            }
                            if (jgcReader[4].GetType().Name == "DBNull")
                                jgcOut2 = 0;
                            else
                            {
                                jgcOut2 = Convert.ToSingle(jgcReader[4]);
                            }
                            if (jgcReader[5].GetType().Name == "DBNull")
                                jgcOut3 = 0;
                            else
                            {
                                jgcOut3 = Convert.ToSingle(jgcReader[5]);
                            }
                            if (jgcReader[6].GetType().Name == "DBNull")
                                jgcOut4 = 0;
                            else
                            {
                                jgcOut4 = Convert.ToSingle(jgcReader[6]);
                            }
                            jgcTotal = jgcOut1 + jgcOut2 + jgcOut3 + jgcOut4;
                            strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + jgcDevScale + ",'" + strCurDT + "'," + jgcOut1 + "," + jgcOut2 + "," + jgcOut3 + "," + jgcOut4 + "," + jgcTotal + ") ";

                            sqlComm.CommandText = strComm;
                            sqlComm.ExecuteNonQuery();
                            this.WriteLog("jgcWriteDataOutput 函数 正在上传  " + strCurDT + "  班产数据");
                            this.SetBanDataInfo(12, strCurDT);//保存时间到注册表
                            saveDT=saveDT.AddDays(1);
                        }
                    }

                    sqlComm.Dispose();
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                    
                    //lb.Items.Add("清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                    this.WriteLog("清华激光秤数据正在发送班产量数据 ");
                    // }
                }

                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("jgcWriteDataOutput 函数 " + ex.ToString());
                return 801207;
            }
        }
        #endregion



    }
}
