using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Media;

namespace StationUp
{
    public static class AlertSound
    {
        private static string _soundLocation = Application.StartupPath + "\\AlertSound\\ALARM1.WAV";
        private static SoundPlayer _play = new SoundPlayer();
        
       
        /// <summary>
        /// 播放报警声音
        /// </summary>
        /// <param name="looping">指定是否循环播放</param>
        public  static void Alert(bool looping)
        {
            _play.SoundLocation = _soundLocation;
            _play.LoadAsync();
            if(looping)
            {
                _play.PlayLooping();
            }
            else
            {
                _play .Play();
            }
           
        }

        public static void Stop()
        {
            _play.Stop();
        }
    }
}
