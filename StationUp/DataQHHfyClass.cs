using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Sockets;
namespace StationUp
{
    /// <summary>
    /// 清华灰分仪
    /// </summary>
    class DataQHHfyClass :DataOption
    {
        #region 数据初始化
        string strAddress = null, strPass = null;

        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        /// <summary>
        /// 2000C型灰分仪数据初始化
        /// </summary>
        /// <param name="nUnitID"></param>
        /// <param name="strName"></param>
        /// <param name="strCheckDev"></param>
        /// <returns></returns>
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int nTest = 0, nCurUnitID = 0;
            string strComm = null, strhfyName = null;
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);
            this.WriteLog("QHHfy   nUnitId=" + nUnitID.ToString());
            if (nUnitID < 0)
            {
                this.WriteLog("清华灰分仪GetInitPara无法获取单位编号：90");
                return -1;
            }
            try
            {
                strhfyName = strName + "灰分仪";
                nTest = SelectData(4, nUnitID, null, nUnitID * 100 + 50, null);//第三个参数暂时不用 通过秤nUnitID*100+50判断
                this.WriteLog("Hfyc nTest=" + nTest.ToString() + "  " + "nUnitId=" + nUnitID.ToString());
                if (nTest == -1)//
                    return -2;
                if (nTest == 1)//有数据
                { }

                if (nTest == 0)//无数据
                {
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.Connection = GetConnection();
                    nCurUnitID = nUnitID * 100 + 50;
                    strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + nCurUnitID + ",'" + strhfyName + "'," + nUnitID + ",2,3,1,2)";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    if (sqlComm.Connection.State == ConnectionState.Open)
                        sqlComm.Connection.Close();
                    sqlComm.Connection.Dispose();
                }
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DataHfyCClass下的GetInitPara函数" + ex.Message);
                return -4;
            }
        }
        #endregion

        #region 得到2000C型灰分仪的数据
        /// <summary>
        /// 得到清华型灰分仪的数据
        /// </summary>
        /// <param name="strStation">中心站</param>
        /// <returns>当前数据信息</returns>
        private string[] GethfyCReadDataDyn(string strStation,string strPort,int nUnitID,string strDT)
        {
            string[] strArray = new string[3];
            char[] charArray = new Char[] { ',' };
            double[] avgInfo = new double[2];
            string strName = strStation;
            double minHui = 0, tenHui = 0,hourHui = 0;
             TcpClient tcpClient = new TcpClient();
            try
            {
                
                tcpClient.Connect(IPAddress.Parse(strStation),int.Parse(strPort.Trim()));
                NetworkStream ns = tcpClient.GetStream();
                byte []data = new Byte[256];
                Int32 bytes = ns.Read(data, 0, data.Length);
                string responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                if (responseData == " on \r\n")
                {
                    byte[] dt = System.Text.Encoding.ASCII.GetBytes(strDT);
                    ns.Write(dt, 0,strDT.Length);
                    System.Threading.Thread.Sleep(3000);
                    bytes = ns.Read(data, 0, data.Length);
                    responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    responseData = responseData.Replace("\r\n", " ").Trim();
                    this.WriteLog("清华灰分仪网络连接函数GethfyCReadDataDyn得到内容:  " + responseData+"传入的时间为:  "+strDT);
                      
                }
                             
                ns.Close();
                tcpClient.Close();
                this.WriteLog("清华灰分仪网络连接函数GethfyCReadDataDyn" + strName);
                if (responseData == "ERROR")
                    return null;
                if (responseData == "NOT")
                {
                    strArray[0] = strDT;
                    strArray[1] = "0";
                }
                else
                {
                    strArray = responseData.Split(charArray);
                    strArray[0] = strArray[0].Replace("\r\n", " ");
                    
                }

                /* 增加小时灰分 分钟灰分
                minHui=Convert.ToDouble(strArray[1]);
                avgInfo = GetSqlServerAvg(nUnitID);
                if (avgInfo != null)
                {
                    double tempHui = (avgInfo[0] + minHui) / 2;//分钟灰分
                    strArray[2] = tempHui.ToString();
                    tempHui = (avgInfo[1] + minHui) / 2;//小时灰分
                    strArray[3] = tempHui.ToString();
                }
                else
                {
                    strArray[2] = minHui.ToString();
                    strArray[3] = minHui.ToString();
                }
                */
            
                return strArray;
            }
            catch (Exception ex)
            {
                tcpClient.Close();
                this.WriteLog("清华灰分仪网络连接函数GethfyCReadDataDyn" + ex.ToString());
                return null;
            }

        }
        /// <summary>
        /// 在Sql Server数据库中获取
        /// </summary>
        /// <returns></returns>
        private double [] GetSqlServerAvg(int nUnitID)
        {
            try
            {
                DateTime dt = DateTime.Now.AddMinutes(-10);
                double[] avgInfo = new double[2];
                SqlConnection sqlConn = new SqlConnection();
                sqlConn = this.GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = sqlConn;
                string strSql = "select avg(hui) from hfyout where (hui<>0) and (unitid=" + nUnitID + ")";
                strSql += "and (curdt>'" + dt + "')";
                sqlComm.CommandText = strSql;
                avgInfo[0] = Convert.ToDouble(sqlComm.ExecuteScalar());

                dt = DateTime.Now.AddMinutes(-60);
                sqlComm.CommandText = strSql;
                avgInfo[1] = Convert.ToDouble(sqlComm.ExecuteScalar());
                sqlComm.Dispose();
                sqlConn.Close();
                sqlConn.Dispose();
                return avgInfo;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }
       
        #endregion

        #region 将数据写入sql server服务器
        /// <summary>
        /// 清华灰分仪写入动态数据
        /// </summary>
        /// <param name="nUnitID">单位编号</param>
        /// <param name="strStation">灰分仪IP</param>
        /// <param name="strPort">通信端口</param>
        /// <param name="strCheck">其他</param>
        /// <returns></returns>
        public int HFYQHWriteDataDyn(int nUnitID, string strStation, string strPort,string strCheck)
        {
         
            int nTest = 0;
            string[] strInfo;
            string strCurDt = null, strComm = null;
            try
            {
                string strDT = DateTime.Now.ToString("yyyy-MM-dd HH:mm:00");
                this.WriteLog("清华灰分仪时间获取测试   " + strDT);
                strInfo = GethfyCReadDataDyn(strStation,strPort,nUnitID,strDT);
                float nhfyCMin = 0, nhfyCTenMin = 0, nhfyCHour = 0, nhfyCDay = 0, nhfyCLNR = 0, nhfyCFlow = 0;
                if (strInfo == null)
                    return 801203;
                
                strCurDt =strInfo[0];//DateTime.Now.ToString("yyyy-MM-dd") + " " + nHour.ToString() + ":" + nMin.ToString() + ":" + "00";
                
                nhfyCMin = Convert.ToSingle(strInfo[1]);//分钟灰分  十分钟灰分 小时灰分 天灰分 都归为分钟灰分
                nhfyCTenMin = nhfyCMin;//Convert.ToSingle(strInfo[2]);
                nhfyCHour = nhfyCMin;//Convert.ToSingle(strInfo[3]);

                nhfyCDay = nhfyCMin;
                nhfyCLNR = 0;// Convert.ToSingle(strInfo[2]);//权重因子
                nhfyCFlow = 0; //Convert.ToSingle(strInfo[10]);
                SqlConnection sqlConn = new SqlConnection();
                sqlConn = this.GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = sqlConn;
                int nCurUnitID = nUnitID * 100 + 50;
                nTest = SelectData(3, nUnitID, null, nCurUnitID, null);//采用第一个参数验证
                if (nTest == -1)
                    return 801204;
                if (nTest == 0)
                {
                    strComm = "insert into s_HFYDyn (UnitID,DeviceID,CurDT,CsSignal,AmSignal,LNR,Cs,Am,Slope,Intercept,hui,minutehui,Tenhui,hourhui,banhui,dayhui) Values(" + nUnitID + "," + nCurUnitID + ",'" + strCurDt + "',0,0,0,0,0,0,0," + nhfyCMin + "," + nhfyCMin + " ," + nhfyCTenMin + "," + nhfyCHour + ",0," + nhfyCDay + ")";
                }
                if (nTest == 1)
                {
                    strComm = "update s_HFYDyn set CurDT='" + strCurDt + "',CsSignal=0,AmSignal=0,LNR=0,Cs=0,Am=0,Slope=0,Intercept=0,hui=" + nhfyCMin + ",minutehui=" + nhfyCMin + ",Tenhui=" + nhfyCTenMin + ",hourhui=" + nhfyCHour + ",Dayhui=" + nhfyCDay + " where DeviceID=" + nCurUnitID + "";
                }
                this.WriteLog("HFYCWriteDataDyn 函数---" + strComm);
                sqlComm.CommandText = strComm;
                sqlComm.ExecuteNonQuery();
                sqlComm.Dispose();
                sqlConn.Close();
                sqlConn.Dispose();
                this.WriteLog("清华灰分仪HFYCWriteDataDyn 函数---写入完成");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("HFYCWriteDataDyn 函数 " + ex.ToString());
                return 801205;
            }

        }
        /// <summary>
        /// 读取清华灰分仪的数据信息
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="strStation">中心站地址</param>
        /// <returns></returns>
        public int HFYQHWriteDataHis(int nUnitID, string strStation, string strPort, System.Windows.Forms.ListBox lb)
        {
            float nhfyCMin, nhfyCTen, nhfyCHour, nhfyCDay, nhfyCMeasure, nhfyCDTBetween = 0;
           
            string[] strInfo;
            string[] strDT;
            string strComm = null, strCurDt = null;
            int nTestData = 0;
            int count = 0;
            try
            {
                int nCurUnitID = nUnitID * 100 + 50;
                int nTime = 0;

               
                strDT = this.GetBanDataInfo(9);//得到时间 以及是否上传过 参数1 为灰分仪
                if (strDT== null)
                {
                    return 801208;
                }
                DateTime saveDT = Convert.ToDateTime(strDT[0]);
                saveDT = Convert.ToDateTime(saveDT.ToString("yyyy-MM-dd HH:mm:00"));
                DateTime nowDt = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:00"));
                this.WriteLog("清华灰分仪得到保存的日期时间为:" + saveDT);
                while (saveDT <=nowDt)//当网络连接跨天时
                {
                    strInfo=GethfyCReadDataDyn(strStation, strPort, nUnitID,saveDT.ToString());
                    if(strInfo==null)
                        return 801208;
                    nTestData = SelectData(5, nUnitID, null, nCurUnitID, strInfo[0].ToString());//测试与数据库是否匹配
                    this.WriteLog("清华灰分仪HFYCWriteDataHis 函数--测试数据库服务器是否有当前数据" + nTestData.ToString());
                    if (nTestData == 1)
                    {
                        saveDT = saveDT.AddMinutes(1);
                        continue;
                    }
                    strCurDt = strInfo[0];
                    nhfyCMin = Convert.ToSingle(strInfo[1]);
                    nhfyCTen = nhfyCMin;// Convert.ToSingle(strHfyInfo[2]);
                    nhfyCHour = nhfyCMin; //Convert.ToSingle(strHfyInfo[3]);
                   
                    nhfyCDay = 0;

                    nhfyCMeasure = Convert.ToSingle(strInfo[2]);//权重因子
                    nhfyCDTBetween = 0; 

                    strComm = "insert into s_HFYOut (UnitID,DeviceID,CurDT,hui,minutehui,Tenhui,hourhui,banhui,dayhui,Measure,DTBetween) Values(" + nUnitID + "," + nCurUnitID + ",'" + strCurDt + "'," + nhfyCMin + "," + nhfyCMin + "," + nhfyCTen + "," + nhfyCHour + ",0," + nhfyCDay + "," + nhfyCMeasure + "," + nhfyCDTBetween + ")";
                    lb.Items.Add("清华灰分仪正在上传  " + strCurDt + "  的分钟灰分("+nhfyCMin.ToString()+","+nhfyCMeasure.ToString()+")");
                    this.WriteLog("清华灰分仪HFYCWriteDataHis 函数" + strComm);
                    SqlCommand sqlComm = new SqlCommand();
                    SqlConnection sqlConn = new SqlConnection();
                    sqlConn = this.GetConnection();
                    sqlComm.Connection = sqlConn;
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    sqlConn.Close();
                    sqlConn.Dispose();
                    saveDT=saveDT.AddMinutes(1);
                    this.SetBanDataInfo(9, saveDT.ToString());//清华灰分仪保存时间到注册表
                    count++;
                    if (count > 12)//连续传输12个数值后 跳出
                    {
                        string strInfoMessage = "清华灰分仪需要补传数据过多,需要暂时跳出循环给其他设备,下一分钟继续传输";
                        lb.Items.Add(strInfoMessage);
                        this.WriteLog(strInfoMessage);
                        break;
                    }
               }   
               return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("清华灰分仪HFYCWriteDataHis 函数" + ex.ToString());
                return 801207;
            }

        }
        #endregion

    }
}

