using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过下列属性集
// 控制。更改这些属性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("StationUp")]
[assembly: AssemblyDescription("StationUp上传软件")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("scale")]
[assembly: AssemblyProduct("StationUp")]
[assembly: AssemblyCopyright("版权所有 (C) scale 2009")]
[assembly: AssemblyTrademark("scale")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 使此程序集中的类型
// 对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 属性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("1936aa49-5d55-44d0-92ac-6388e0a123d1")]

// 程序集的版本信息由下面四个值组成:
//
//      主版本
//      次版本 
//      内部版本号
//      修订号
//
[assembly: AssemblyVersion("2.2404.3.0")]
[assembly: AssemblyFileVersion("2.2404.3.0")]

[assembly: AssemblyInformationalVersion("0.2404.3.0")]