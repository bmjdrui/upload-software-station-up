using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
namespace StationUp
{
    /// <summary>
    /// 2000C型灰分仪
    /// </summary>
    class DataHfyCClass : DataOption
    {
        #region 数据初始化
        string strAddress = null, strPass = null;

        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        /// <summary>
        /// 2000C型灰分仪数据初始化
        /// </summary>
        /// <param name="nUnitID"></param>
        /// <param name="strName"></param>
        /// <param name="strCheckDev"></param>
        /// <returns></returns>
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int nTest = 0, nCurUnitID = 0;
            string strComm = null, strhfyName = null;
            nUnitID=base.GetInitPara(nUnitID, strName, strCheckDev);
            this.WriteLog("Hfyc   nUnitId=" + nUnitID.ToString());
            if (nUnitID < 0)
            {
                this.WriteLog("灰分仪C型GetInitPara无法获取单位编号：90");
                return -1;
            }
            try
            {
                strhfyName = strName + "灰分仪";
                nTest = SelectData(4, nUnitID, null, nUnitID*100+50, null);//第三个参数暂时不用 通过秤nUnitID*100+50判断
                this.WriteLog("Hfyc nTest=" + nTest.ToString()+"  "+"nUnitId="+nUnitID.ToString());
                if (nTest == -1)//
                    return -2;
                if (nTest == 1)//有数据
                { }

                if (nTest == 0)//无数据
                {
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.Connection = GetConnection();
                    nCurUnitID = nUnitID * 100 + 50;
                    strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + nCurUnitID + ",'" + strhfyName + "'," + nUnitID + ",2,2,1,2)";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    if (sqlComm.Connection.State == ConnectionState.Open)
                        sqlComm.Connection.Close();
                    sqlComm.Connection.Dispose();
                }
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DataHfyCClass下的GetInitPara函数"+ex.Message);
                return -4;
            }
        }
        #endregion

        #region 得到2000C型灰分仪的数据
        /// <summary>
        /// 得到2000C型灰分仪的数据《尚德》
        /// </summary>
        /// <param name="strStation">中心站</param>
        /// <returns>当前数据信息</returns>
        private string [] GethfyCReadDataDyn(string strStation)
        {
            string strLine;
            string[] strArray=null;
            char[] charArray = new Char[] { ',' };
             
            string strName= strStation;
            strName+=@"\Ash Data";
            strName += "\\"+DateTime.Now.ToString("yyyy")+"年";
            strName += "\\" + DateTime.Now.ToString("MM") + "月";
            strName += "\\" + DateTime.Now.ToString("dd") + ".txt";
            this.WriteLog("GethfyCReadDataDyn 函数" + strName);
            // strName = @"D:\工业控制\永城联网\永煤新桥洗煤厂\2009年\02月\01.txt";
            
            try
            {
                FileStream aFile = new FileStream(strName, FileMode.Open,FileAccess.Read,FileShare.Read);
                StreamReader sr = new StreamReader(aFile);

                strLine = sr.ReadLine();
                while (strLine != null)
                {
                    strArray = strLine.Split(charArray);
                    /*for (int x = 0; x <= strArray.GetUpperBound(0); x++)
                    {
                       a=strArray[x].Trim();
                    }*/
                    strLine = sr.ReadLine();
                }
                sr.Close();
                return strArray;
            }
            catch (IOException ex)
            {
                this.WriteLog("GethfyCReadDataDyn 函数" + ex.ToString());
                return null;
            }

        }
        /// <summary>
        /// 得到2000C型数据
        /// </summary>
        /// <param name="strStation">中心站</param>
        /// <param name="startTime">时间</param>
        /// <returns>得到的数据</returns>
        private string[] GetHFYCHisData(string strStation, DateTime startTime)
        {
            string strLine;
            string[] strArray = null;
            char[] charArray = new Char[] { ',' };

            string strName = strStation;
            strName+=@"\Ash Data";
            strName += "\\" + startTime.ToString("yyyy") + "年";
            strName += "\\" + startTime.ToString("MM") + "月";
            strName += "\\" + startTime.ToString("dd") + ".txt";
            this.WriteLog("GetHFYCHisData 函数" + strName);
            //strName = @"D:\工业控制\永城联网\永煤新桥洗煤厂\2009年\02月\01.txt";
            string a = null;
            try
            {
                int nCurNode = 0;
                DateTime dt = Convert.ToDateTime(startTime);
                int nNode = dt.Hour * 60 + dt.Minute;
                FileStream aFile = new FileStream(strName, FileMode.Open, FileAccess.Read, FileShare.Read);
                if (aFile == null)
                    return null;
                StreamReader sr = new StreamReader(aFile);

                strLine = sr.ReadLine();
                while (strLine != null)
                {

                    strArray = strLine.Split(charArray);
                    if (!this.IsNumber(strArray[0]))
                    {
                        strLine = sr.ReadLine();
                        continue;
                    }
                    nCurNode = Convert.ToInt32(strArray[0]);
                    if (nCurNode == nNode)
                        break;
                    for (int x = 0; x <= strArray.GetUpperBound(0); x++)
                    {
                        a = strArray[x].Trim();
                    }
                    strLine = sr.ReadLine();
                }
                sr.Close();
                return strArray;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHFYCHisData 函数" + ex.ToString());
                return null;
            }
        }
        #endregion

        #region 将数据写入sql server服务器
        /// <summary>
        /// 2000C型写入到数据库
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="strStation">站名</param>
        /// <returns></returns>
        public int HFYCWriteDataDyn(int nUnitID, string strStation,string  strCheck)
        {
            //分钟灰分,十分灰分,小时灰分,

            int nTest = 0,nTime=0;
            string[] strInfo;
            string strCurDt=null,strComm=null;
            try
            {
                strInfo = GethfyCReadDataDyn(strStation);
                float nhfyCMin = 0, nhfyCTenMin = 0, nhfyCHour = 0,nhfyCDay=0, nhfyCLNR = 0, nhfyCFlow=0;
                if (strInfo == null)
                    return 800603;
                nTime = Convert.ToInt32(strInfo[0]);
                int nHour = nTime / 60; //小时
                int nMin = nTime % 60;//分钟
                strCurDt = DateTime.Now.ToString("yyyy-MM-dd") + " " + nHour.ToString() + ":" + nMin.ToString() + ":" + "00";

                nhfyCMin = Convert.ToSingle(strInfo[1]);
                nhfyCTenMin = Convert.ToSingle(strInfo[2]);
                nhfyCHour = Convert.ToSingle(strInfo[3]);
                if (strCheck.Substring(0, 1) == "1")
                    nhfyCDay = Convert.ToSingle(strInfo[27]);//日灰分 27
                else
                    nhfyCDay = Convert.ToSingle(strInfo[24]);//日灰分 24 开封测控23Convert.ToSingle(strInfo[23])
                
                nhfyCLNR = Convert.ToSingle(strInfo[6]);
                nhfyCFlow = Convert.ToSingle(strInfo[10]);
                SqlConnection sqlConn = new SqlConnection();
                sqlConn = this.GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = sqlConn;
                int nCurUnitID = nUnitID * 100 + 50;
                nTest = SelectData(3, nUnitID, null, nCurUnitID, null);//采用第一个参数验证
                if (nTest == -1)
                    return 800604;
                if (nTest == 0)
                {

                    strComm = "insert into s_HFYDyn (UnitID,DeviceID,CurDT,CsSignal,AmSignal,LNR,Cs,Am,Slope,Intercept,hui,minutehui,Tenhui,hourhui,banhui,dayhui) Values(" + nUnitID + "," + nCurUnitID + ",'" + strCurDt + "',0,0,0,0,0,0,0," + nhfyCMin + "," + nhfyCMin + " ," + nhfyCTenMin + "," + nhfyCHour + ",0," + nhfyCDay + ")";
                    
                   
                }
                if (nTest == 1)
                {
                    strComm = "update s_HFYDyn set CurDT='" + strCurDt + "',CsSignal=0,AmSignal=0,LNR=0,Cs=0,Am=0,Slope=0,Intercept=0,hui=" + nhfyCMin + ",minutehui=" + nhfyCMin + ",Tenhui=" + nhfyCTenMin + ",hourhui=" + nhfyCHour + ",Dayhui=" + nhfyCDay + " where DeviceID=" + nCurUnitID + "";
                }
                this.WriteLog("HFYCWriteDataDyn 函数---" + strComm);
                sqlComm.CommandText = strComm;
                sqlComm.ExecuteNonQuery();
                sqlComm.Dispose();
                sqlConn.Close();
                sqlConn.Dispose();
                this.WriteLog("HFYCWriteDataDyn 函数---写入完成");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("HFYCWriteDataDyn 函数 " + ex.ToString());
                return 800605;
            }
            
        }
        /// <summary>
        /// 读取2000C型灰分仪的数据信息
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="strStation">中心站地址</param>
        /// <returns></returns>
        public int HFYCWriteDataHis(int nUnitID, string strStation, string strCheck, string strCheckDev, System.Windows.Forms.ListBox lb)
        {
            float nhfyCMin, nhfyCTen, nhfyCHour, nhfyCDay,nhfyCMeasure, nhfyCDTBetween = 0;
            string[] strDtInfo = new string[2];
            string[] strHfyInfo;
            string strComm = null,strCurDt=null;
            int nTestData = 0;
            try
            {
                int nCurUnitID = nUnitID * 100 + 50;
                int nTime = 0;
                strDtInfo = this.GetBanDataInfo(2);//得到时间 以及是否上传过 参数2 为2000C灰分仪
                DateTime saveDT = Convert.ToDateTime(strDtInfo[0]);
                DateTime nowDt = DateTime.Now;
                //***************实现跨天读取数据*********************************************       
                bool bYmd=saveDT.ToString("yyyy-MM-dd") ==nowDt.ToString("yyyy-MM-dd");
                bool bHM = saveDT.ToString("HH:mm") == nowDt.ToString("HH:mm");
                while (!bYmd || !bHM)
                {
                    this.WriteLog("HFYCWriteDataHis--显示 函数" + saveDT.ToString("yyyy-MM-dd HH:mm"));
                    saveDT = saveDT.AddMinutes(1);
                    this.WriteLog("HFYCWriteDataHis--加一分钟 函数" + saveDT.ToString("yyyy-MM-dd HH:mm"));
                    bYmd = saveDT.ToString("yyyy-MM-dd") == nowDt.ToString("yyyy-MM-dd");
                    bHM = saveDT.ToString("HH:mm") == nowDt.ToString("HH:mm");
                    strHfyInfo = GetHFYCHisData(strStation, saveDT);
                    nTime = Convert.ToInt32(strHfyInfo[0]);
                    int nHour = nTime / 60; //小时
                    int nMin = nTime % 60;//分钟
                    if (strHfyInfo == null)
                        return 800606;
                  //  strCurDt = DateTime.Now.ToString("yyyy-MM-dd") + " " + nHour.ToString() + ":" + nMin.ToString() + ":" + "00";
                    strCurDt = saveDT.ToString("yyyy-MM-dd") + " " + nHour.ToString() + ":" + nMin.ToString() + ":" + "00";
                    lb.Items.Add("2000C型灰分仪正在检测并补传 " + saveDT.ToString("yyyy-MM-dd HH:mm") + " 数据");
                    nTestData = SelectData(5, nUnitID, null, nCurUnitID, strCurDt.ToString());//测试与数据库是否匹配
                    this.WriteLog("HFYCWriteDataHis 函数--测试数据库服务器是否有当前数据" + nTestData.ToString());
                    if (nTestData == 1)
                    {
                        this.SetBanDataInfo(2, saveDT.ToString("yyyy-MM-dd HH:mm"));//保存参数信息到注册表2000C
                        continue;
                    }
                        
                    nhfyCMin = Convert.ToSingle(strHfyInfo[1]);
                    nhfyCTen = Convert.ToSingle(strHfyInfo[2]);
                    nhfyCHour = Convert.ToSingle(strHfyInfo[3]);
                    if(strCheck.Substring(0,1)=="1")
                        nhfyCDay = Convert.ToSingle(strHfyInfo[27]);//日灰分 27
                    else
                        nhfyCDay = Convert.ToSingle(strHfyInfo[24]);//日灰分 24  开封测控23Convert.ToSingle(strHfyInfo[23])

                    nhfyCMeasure = Convert.ToSingle(strHfyInfo[10]);//流量
                    nhfyCDTBetween = Convert.ToSingle(strHfyInfo[13]);//时长

                    strComm = "insert into s_HFYOut (UnitID,DeviceID,CurDT,hui,minutehui,Tenhui,hourhui,banhui,dayhui,Measure,DTBetween) Values(" + nUnitID + "," + nCurUnitID + ",'" + strCurDt + "'," + nhfyCMin + "," + nhfyCMin + "," + nhfyCTen + "," + nhfyCHour + ",0," + nhfyCDay + "," + nhfyCMeasure + "," + nhfyCDTBetween + ")";
                    this.WriteLog("HFYCWriteDataHis 函数" + strComm);
                    SqlCommand sqlComm = new SqlCommand();
                    SqlConnection sqlConn = new SqlConnection();
                    sqlConn = this.GetConnection();
                    sqlComm.Connection = sqlConn;
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    sqlConn.Close();
                    sqlConn.Dispose();
                    this.SetBanDataInfo(2, strCurDt);//保存参数信息到注册表2000C 对应参数为2
                    //nTimeCount++;
                }
                return 0;
            }
             catch(Exception ex)
             {
                 this.WriteLog("HFYCWriteDataHis 函数"+ex.ToString());
                 return 800607;
              }

          }
        #endregion

      }
}
