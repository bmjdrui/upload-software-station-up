using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Data.OleDb;
using System.Data.Odbc;
namespace StationUp
{
    /// <summary>
    /// 新疆地磅运销系统
    /// </summary>
    class DataCarXJClass:DataOption
    {
        #region 数据初始化
        string strAddress = null, strPass = null, strUser = null;
        OleDbConnection carPjConn = new OleDbConnection();
        SqlConnection carxjConn = new SqlConnection();
        OleDbConnection carZWConn = new OleDbConnection();
        OleDbConnection carZTConn = new OleDbConnection();
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        public string UserMaching
        {
            get { return strUser; }
            set { strUser = value; }
        }
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int carxjDevScale = 0, nTest = 0;
            string carxjDevName = null, strComm = null;
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("新疆汽车衡GetInitPara函数错误号为：-1");
                return -1;
            }
            try
            {
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();               
                carxjDevName = "地磅";//Convert.ToString(carxjReader["scname"]);
                carxjDevScale = 0;
                carxjDevScale = nUnitID * 100 +60+ carxjDevScale;//计算编号
                nTest = SelectData(2, nUnitID, strName, carxjDevScale, carxjDevName);//第三个参数暂时不用 通过秤名字和矿区ID号判断
                if (nTest == -1)//
                    return -3;
                if (nTest == 1)//有数据
                {
                    strComm = "update  s_device set DeviceName='" + carxjDevName + "' where DeviceID=" + carxjDevScale + "";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }

                if (nTest == 0)//无数据  插入数据s_device 地磅类型
                {
                    //计量类型为4
                    strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + carxjDevScale + ",'" + carxjDevName + "'," + nUnitID + ",1,1,1,4)";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                //***************************************

                
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DatacarxjClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
        #endregion
       
        #region 获取龟兹矿业地磅数据
        
        //得到龟兹矿业地磅数据连接  数据库类型 sql server  数据库名czbase1 
        private SqlConnection GetCarxjConnection()
        {

            string strConnSql = "data source=" + AddressMachine + ";initial catalog=czbase1;user id=" + UserMaching + ";password=" + PassMachine + "";
            try
            {
                SqlConnection sqlConn = new SqlConnection(strConnSql);
                if (sqlConn.State == ConnectionState.Closed)
                    sqlConn.Open();
                return sqlConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("新疆地磅DataOption下的GetConnection 函数" + ex.ToString());
                return null;
            }
        }
               
        //得到龟兹矿业地磅数据
        private SqlDataReader GetCarxjOutput(string strOldTime)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                string strComm = "SELECT * FROM weight_data WHERE weighed_time > '" + oldDT.ToString("yyyy-MM-dd HH:mm:ss") + "' order by weighed_time";
                SqlCommand carXjComm = new SqlCommand(strComm, GetCarxjConnection());
                SqlDataReader carXjReader = carXjComm.ExecuteReader();
                carXjComm.Dispose();
                return carXjReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetCarxjOutput 函数" + ex.ToString());
                return null;
            }

        }
     
        #endregion 

        #region 获取潘津矿地磅数据  
        //获取潘津数据库连接 数据库类型access 数据库名称：weight.dll 默认密码:www.54321.com
        private OleDbConnection GetPjCarConnection()
        {
            string strPath = null;
            if (carPjConn.State == ConnectionState.Open)
            {
                return carPjConn;
            }

            if (strAddress != "127.0.0.1")
            {
                strPath = strAddress;
                strPath += "\\Weight.dll";
            }
            else
            {
                strPath = @"D:\产量监控联网\新疆能源化工\实施工作\12年软件\Weight.dll";
            }
           
            //string strPath = @"D:\产量监控联网\新疆能源化工\实施工作\12年软件\Weight.dll";
            //string strPass = "www.54321.com";
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";

            carPjConn.ConnectionString = strConn;
            if (carPjConn.State == ConnectionState.Closed)
                carPjConn.Open();
            return carPjConn;

        }
        //潘津矿获取数据
        private OleDbDataReader GetCarPJOutput(string strOldTime,string strPath,string strPass)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                string strComm = "SELECT * FROM t_standard WHERE F_NetTime > '" + oldDT.ToString("yyyy-MM-dd HH:mm:ss") + "' order by f_nettime";
                OleDbCommand carXjComm = new OleDbCommand(strComm,GetPjCarConnection());
                OleDbDataReader carXjReader = carXjComm.ExecuteReader();
                carXjComm.Dispose();
                return carXjReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("潘津矿GetCarxjOutput 函数" + ex.ToString());
                return null;
            }

        }
            
        #endregion

        #region 获取众维地磅数据
        //获取众维数据库连接 数据库类型access 数据库名称：data.mdb 默认密码:KeliElectric
        private OleDbConnection GetZWCarConnection()
        {
            string strPath = null;
            if (carZWConn.State == ConnectionState.Open)
            {
                return carZWConn;
            }

            if (strAddress != "127.0.0.1")
            {
                strPath = strAddress;
                strPath += "\\data.mdb";
            }
            else
            {
                strPath = @"D:\产量监控联网\新疆能源化工\实施工作\12年软件\data.mdb";
            }

            //string strPath = @"D:\产量监控联网\新疆能源化工\实施工作\12年软件\Weight.dll";
            //string strPass = "www.54321.com";
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";

            carZWConn.ConnectionString = strConn;
            if (carZWConn.State == ConnectionState.Closed)
                carZWConn.Open();
            return carZWConn;

        }
        //众维矿获取数据
        private OleDbDataReader GetCarZWOutput(string strOldTime)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                string strComm = "SELECT * FROM trade WHERE grosstime > #" + oldDT.ToString("yyyy-MM-dd HH:mm:ss") + "# order by grosstime";
                OleDbCommand carXjComm = new OleDbCommand(strComm, GetZWCarConnection());
                OleDbDataReader carXjReader = carXjComm.ExecuteReader();
                carXjComm.Dispose();
                return carXjReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("众维矿GetCarxjOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 获取众泰地磅数据
        //获取众泰数据库连接 数据库类型access 数据库名称：locdb.mdb 默认密码:aaaaa
        private OleDbConnection GetZTCarConnection()
        {
            string strPath = null;
            if (carZTConn.State == ConnectionState.Open)
            {
                return carZTConn;
            }

            if (strAddress != "127.0.0.1")
            {
                strPath = strAddress;
                strPath += "\\locdb.mdb";
            }
            else
            {
                strPath = @"D:\产量监控联网\新疆能源化工\实施工作\12年软件\locdb.mdb";
            }

            //string strPath = @"D:\产量监控联网\新疆能源化工\实施工作\12年软件\Weight.dll";
            //string strPass = "www.54321.com";
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";

            carZTConn.ConnectionString = strConn;
            if (carZTConn.State == ConnectionState.Closed)
                carZTConn.Open();
            return carZTConn;

        }
        //众泰矿获取数据
        private OleDbDataReader GetCarZTOutput(string strOldTime)
        {
            try
            {
                DateTime oldDT =DateTime.Now.AddDays(-1);
                //string strComm = "SELECT * FROM dict_czjl WHERE 称重时间2 > '" + oldDT.ToString("yyyy-MM-dd HH:mm:ss") + "' order by 称重时间2";
                string strComm = "SELECT * FROM dict_czjl WHERE (称重日期 >= '" + oldDT.ToString("yyyy-MM-dd") + "') and (是否完成='是') order by 称重时间2";
                this.WriteLog("众泰矿GetCarxjOutput 函数" + strComm);
                OleDbCommand carXjComm = new OleDbCommand(strComm, GetZTCarConnection());
                OleDbDataReader carXjReader = carXjComm.ExecuteReader();
                carXjComm.Dispose();
                return carXjReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("众泰矿GetCarxjOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 获取音西地磅数据
        private OdbcConnection GetYXCarConnection()
        {
            string strPath = null;
            OdbcConnection YXconn = new OdbcConnection("DSN=scale1;charset=iso_1");
            YXconn.Open();
            return YXconn;

        }
        //音西矿获取数据
        private OdbcDataReader GetCarXYOutput(string strOldTime)
        {
            try
            {
                Int32 id = Convert.ToInt32(strOldTime);
                //string strComm = "SELECT * FROM sjb_out WHERE (rq2 > '" + rq.ToString("yyyy-MM-dd") + "') and (sj2>'"+sj.ToString("HH:mm:ss")+"')order by bh";
                string strComm = "select * from sjb_out where bh>" + id + "";
                OdbcCommand carXjComm = new OdbcCommand(strComm, GetYXCarConnection());
                OdbcDataReader carXjReader = carXjComm.ExecuteReader();
                carXjComm.Dispose();
                return carXjReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("音西矿GetCarxjOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 获取恒泰地磅数据
        private OleDbConnection GetCarHTConnection()
        {

           // string strConnSql = "data source=" + AddressMachine + ";initial catalog=czbase1;user id=" + UserMaching + ";password=" + PassMachine + "";
            string strConnSql = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=mdrec;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=GBGKNKGKTTAL80K;Use Encryption for Data=False;Tag with column collation when possible=False";
            try
            {
                OleDbConnection sqlConn = new OleDbConnection(strConnSql);
                if (sqlConn.State == ConnectionState.Closed)
                    sqlConn.Open();
                return sqlConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("新疆地磅DataOption下的GetConnection 函数" + ex.ToString());
                return null;
            }
        }

        //得到恒泰矿业地磅数据
        private OleDbDataReader GetCarHTOutput(string strIndex)
        {
            try
            {
                Int32 index = Convert.ToInt32(strIndex);
                string strComm = "SELECT * FROM v_maindb WHERE id >"+index+" order by id";
                OleDbCommand carXjComm = new OleDbCommand(strComm, GetCarHTConnection());
                OleDbDataReader carXjReader = carXjComm.ExecuteReader();
                carXjComm.Dispose();
                return carXjReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetCarHTOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 潘津矿向服务器写入数据

        //潘津矿
        public int CarPJWriteDataOutput(int nUnitID, int nDevScale,string strPath,string strPass)
        {
            try
            {
                bool bUpdate = false;
                string strComm = null;
                int carXJDevScale = 0,nTestData = 0;

                string strCurDT = null, strCarID = null,strPerson=null,coalType=null;
                string[] strInfo = new string[2];
                float carXJChangeValue = 0, grossWeight = 0, tareWeight = 0, netWeight = 0;
                carXJChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(12);//得到时间 以及是否上传过
                           
                
                OleDbDataReader carPjReader = GetCarPJOutput(strInfo[0],strPath,strPass);

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                carXJDevScale = nUnitID * 100 + 60 + nDevScale;
                while (carPjReader.Read())
                {
                    bUpdate = true;
                    this.WriteLog("carPJWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                   

                    strCurDT = carPjReader["f_nettime"].ToString();//时间
                    strCarID = carPjReader["f_carno"].ToString();//车号
                    grossWeight = Convert.ToSingle(carPjReader["f_gross"]);//毛重
                    tareWeight = Convert.ToSingle(carPjReader["f_tare"]);//皮重
                    netWeight = Convert.ToSingle(carPjReader["f_net"]);//净重
                    coalType = carPjReader["F_ProName"].ToString();//产品名称
                    nTestData = this.SelectData(7, nUnitID, null, carXJDevScale, strCurDT);//监测数据库中是否有数据
                    if (nTestData == 1)
                        continue;
                    strComm = "insert into s_CarsHis (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coalType) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'"+coalType+"') ";

                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    this.WriteLog("carPJWriteDataOutput 函数 正在上传  " + strCurDT + "  地磅数据");
                    this.SetBanDataInfo(12, strCurDT);//保存时间到注册表
                    bUpdate = true;
                    
                }
                //**************************更新s_cardyn表****************************
                if (bUpdate)
                {
                    int nTest = SelectData(8, nUnitID, null, carXJDevScale, "0");//    
                    if (nTest == -1)
                        return 8001405;
                    if (nTest == 1)
                        strComm = "update s_carsDyn set CurDT='" + strCurDT + "',carid='" + strCarID + "',GrossWeight=" + grossWeight + ",TareWeight=" + tareWeight + ",netWeight=" + netWeight + " where DeviceID=" + carXJDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_carsdyn (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coalType) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'"+coalType+"') ";

                    this.WriteLog("潘津矿WriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    bUpdate = false;
                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                carPjReader.Close();
                carPjReader.Dispose();
                //lb.Items.Add("开封万惠/清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("新疆潘津地磅正在发送数据 ");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("潘津矿carPJWriteDataOutput 函数 " + ex.ToString());
                return 801407;
            }
        }
        #endregion

        #region 龟兹矿向服务器写入数据
        public int CarQCWriteDataOutput(int nUnitID, int nDevScale, string strPath, string strPass)
        {
            try
            {
                string strComm = null;
                int carXJDevScale = 0, nTestData = 0;

                string strCurDT = null, strCarID = null, strPerson = null;
                string[] strInfo = new string[2];
                float carXJChangeValue = 0, grossWeight = 0, tareWeight = 0, netWeight = 0;
                carXJChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(12);//得到时间 以及是否上传过


                SqlDataReader carQCReader = GetCarxjOutput(strInfo[0]);

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                carXJDevScale = nUnitID * 100 + 60 + nDevScale;
                while (carQCReader.Read())
                {

                    this.WriteLog("carQCWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                    strCurDT = carQCReader["weighed_time"].ToString();//时间
                    strCarID = carQCReader["car_id"].ToString();//车号
                    grossWeight = Convert.ToSingle(carQCReader["gross_weight"]);//毛重
                    tareWeight = Convert.ToSingle(carQCReader["tare_weight"]);//皮重
                    netWeight = Convert.ToSingle(carQCReader["net_weight"]);//净重

                    nTestData = this.SelectData(7, nUnitID, null, carXJDevScale, strCurDT);//监测数据库中是否有数据
                    if (nTestData == 1)
                        continue;
                    strComm = "insert into s_CarsHis (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ") ";

                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    this.WriteLog("carQCWriteDataOutput 函数 正在上传  " + strCurDT + "  地磅数据");
                    this.SetBanDataInfo(12, strCurDT);//保存时间到注册表

                }
                //**************************更新s_cardyn表****************************

                int nTest = SelectData(8, nUnitID, null, 0, "0");//    
                if (nTest == -1)
                    return 8001405;
                if (nTest == 1)
                    strComm = "update s_carsDyn set DeviceID=" + carXJDevScale + ", CurDT='" + strCurDT + "',carid=" + strCarID + ",GrossWeight=" + grossWeight + ",TareWeight=" + tareWeight + ",netWeight=" + netWeight + "";
                if (nTest == 0)
                    strComm = "insert into s_carsdyn (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ") ";

                this.WriteLog("龟兹矿WriteDataDyn 函数" + strComm);
                sqlComm.CommandText = strComm;
                sqlComm.ExecuteNonQuery();

                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                carQCReader.Close();
                carQCReader.Dispose();
                //lb.Items.Add("开封万惠/清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("新疆龟兹地磅正在发送数据 ");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("龟兹矿carPJWriteDataOutput 函数 " + ex.ToString());
                return 801407;
            }
        }
        #endregion

        #region 众维矿向服务器写入数据
        //众维地磅写入服务器
        public int CarZWWriteDataOutput(int nUnitID, int nDevScale, string strPath, string strPass)
        {
            try
            {
                bool bUpdate = false;
                string strComm = null;
                int carXJDevScale = 0, nTestData = 0;
                string rq = null;
                string strCurDT = null, strCarID = null, strPerson = null,coalType=null;
                string[] strInfo = new string[2];
                float carXJChangeValue = 0, grossWeight = 0, tareWeight = 0, netWeight = 0;
                carXJChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(12);//得到时间 以及是否上传过


                OleDbDataReader carZWReader = GetCarZWOutput(strInfo[0]);

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                carXJDevScale = nUnitID * 100 + 60 + nDevScale;
                while (carZWReader.Read())
                {

                    this.WriteLog("carZWWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                    bUpdate = true;
                   
                    strCurDT = carZWReader["grosstime"].ToString();//时间
                    strCarID = carZWReader["truckno"].ToString();//车号
                    if (strCurDT == "")
                    {
                        bUpdate = false;
                        continue;
                    }
                    if ((strCurDT == "1900-01-01") || (strCurDT == "1900-1-1"))
                    {
                        bUpdate = false;
                        continue;
                    }
                    grossWeight = (Convert.ToSingle(carZWReader["gross"]))/1000;//毛重
                    tareWeight = (Convert.ToSingle(carZWReader["tare"]))/1000;//皮重
                    netWeight = (Convert.ToSingle(carZWReader["net"]))/1000;//净重
                    coalType = carZWReader["product"].ToString();//煤种名称

                    nTestData = this.SelectData(7, nUnitID, null, carXJDevScale, strCurDT);//监测数据库中是否有数据
                    if (nTestData == 1)
                        continue;
                    strComm = "insert into s_CarsHis (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coaltype) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'"+coalType+"') ";

                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    this.WriteLog("carZWWriteDataOutput 函数 正在上传  " + strCurDT + "  地磅数据");
                    this.SetBanDataInfo(12, strCurDT);//保存时间到注册表
                    bUpdate = true;

                }
                //**************************更新s_cardyn表****************************
                if (bUpdate)
                {
                    int nTest = SelectData(8, nUnitID, null, carXJDevScale, "0");//    
                    if (nTest == -1)
                        return 8001405;
                    if (nTest == 1)
                        strComm = "update s_carsDyn set DeviceID=" + carXJDevScale + ", CurDT='" + strCurDT + "',carid='" + strCarID + "',GrossWeight=" + grossWeight + ",TareWeight=" + tareWeight + ",netWeight=" + netWeight + ",CoalType='" + coalType + "' where deviceid=" + carXJDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_carsdyn (DeviceID,CurDT,CarID,GrossWeight,TareWeight,netWeight,CoalType) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'" + coalType + "') ";

                    this.WriteLog("众维矿WriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();

                    sqlComm.Dispose();
                }
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                carZWReader.Close();
                carZWReader.Dispose();
                //lb.Items.Add("开封万惠/清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("新疆众维地磅正在发送数据 ");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("众维矿carPJWriteDataOutput 函数 " + ex.ToString());
                return 801407;
            }
        }
        #endregion

        #region 众泰向服务器写入数据
        //众泰地磅写入服务器
        public int CarZTWriteDataOutput(int nUnitID, int nDevScale, string strPath, string strPass)
        {
            try
            {
                bool bUpdate = false;
                string strComm = null;
                int carXJDevScale = 0, nTestData = 0;

                string strCurDT = null, strCarID = null, strPerson = null, coalType = null;
                string[] strInfo = new string[2];
                float carXJChangeValue = 0, grossWeight = 0, tareWeight = 0, netWeight = 0;
                carXJChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(12);//得到时间 以及是否上传过


                OleDbDataReader carZTReader = GetCarZTOutput(strInfo[0]);

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                carXJDevScale = nUnitID * 100 + 60 + nDevScale;
                while (carZTReader.Read())
                {
                    
                   // this.WriteLog("carZTWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());


                    strCurDT = carZTReader["称重时间2"].ToString();//时间
                    strCarID = carZTReader["车号"].ToString();//车号
                    grossWeight = (Convert.ToSingle(carZTReader["毛重"]))/1000;//毛重
                    tareWeight = (Convert.ToSingle(carZTReader["皮重"]))/1000;//皮重
                    netWeight = (Convert.ToSingle(carZTReader["净重"]))/1000;//净重
                    coalType = carZTReader["品名"].ToString();//煤种名称

                    nTestData = this.SelectData(7, nUnitID, null, carXJDevScale, strCurDT);//监测数据库中是否有数据
                    if (nTestData == 1)
                        continue;
                    strComm = "insert into s_CarsHis (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coaltype) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'" + coalType + "') ";

                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    this.WriteLog("carZTWriteDataOutput 函数 正在上传  " + strCurDT + "  地磅数据");
                   
                    this.SetBanDataInfo(12, strCurDT);//保存时间到注册表
                    bUpdate = true;

                }
                //**************************更新s_cardyn表****************************
                if ((bUpdate) && (strCurDT!=""))//有数据更新
                {
                    int nTest = SelectData(8, nUnitID, null, carXJDevScale, "0");//    
                    if (nTest == -1)
                        return 8001405;
                    if (nTest == 1) 
                        strComm = "update s_carsDyn set CurDT='" + strCurDT + "',carid='" + strCarID + "',GrossWeight=" + grossWeight + ",TareWeight=" + tareWeight + ",netWeight=" + netWeight + ",CoalType='" + coalType + "' where DeviceID=" + carXJDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_carsdyn (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coalType) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'" + coalType + "') ";

                    this.WriteLog("众泰焦化WriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    bUpdate = false;
                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                carZTReader.Close();
                carZTReader.Dispose();
                //lb.Items.Add("开封万惠/清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("新疆众泰地磅正在发送数据 ");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("众泰矿carZTWriteDataOutput 函数 " + ex.ToString());
                return 801407;
            }
        }
        #endregion

        #region 音西向服务器写入数据
        //音西地磅写入服务器
        public int CarYXWriteDataOutput(int nUnitID, int nDevScale, string strPath, string strPass)
        {
            try
            {
                bool bUpdate = false;
                string strComm = null;
                int carXJDevScale = 0, nTestData = 0;
                Int32 carID = 0;
                string strCurDT = null, strCarID = null, strPerson = null, coalType = null;
                string[] strInfo = new string[2];
                float carXJChangeValue = 0, grossWeight = 0, tareWeight = 0, netWeight = 0;
                carXJChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(13);//得到序号 以及是否上传过
                string dtRQ=null,dtSJ=null,rq=null,sj=null;

                OdbcDataReader carYXReader = GetCarXYOutput(strInfo[0]);

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                carXJDevScale = nUnitID * 100 + 60 + nDevScale;
                while (carYXReader.Read())
                {
                    
                    this.WriteLog("carXYWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                    bUpdate = true;
                    rq = carYXReader["rq2"].ToString();//时间
                    sj = carYXReader["sj2"].ToString();
                   this.WriteLog("carYXWritedatetime 函数日期格式" + rq);
                     if (rq == "")
                    {
                        this.WriteLog("rq为空" + rq+"  "+strCurDT);
                       continue;
                    }
                     if ((rq == "1900-1-1") || (rq == "1900-01-01"))
                     {
                         this.WriteLog("rq日期" + rq);
                         continue;
                     }
                    dtRQ = (Convert.ToDateTime(rq)).ToString("yyyy-MM-dd");
                    dtSJ = (Convert.ToDateTime(sj)).ToString("HH:mm:ss");
                    strCurDT = dtRQ+" "+dtSJ;//时间
                   
                    carID = Convert.ToInt32(carYXReader["bh"]);
                    string strTempCarID = carYXReader["ce_hao"].ToString();//车号
                    byte[] temp1 = Encoding.GetEncoding("Windows-1252").GetBytes(strTempCarID);
                    strCarID = Encoding.GetEncoding("gb2312").GetString(temp1);
                    grossWeight = (Convert.ToSingle(carYXReader["mao_z"]))/1000;//毛重
                    tareWeight = (Convert.ToSingle(carYXReader["pi_z"]))/1000;//皮重
                    netWeight = (Convert.ToSingle(carYXReader["jin_z"]))/1000;//净重
                    string strCoalType = carYXReader["pin_min"].ToString();//煤种名称
                    byte[] temp2 = Encoding.GetEncoding("Windows-1252").GetBytes(strCoalType);
                    coalType = Encoding.GetEncoding("gb2312").GetString(temp2);

                    nTestData = this.SelectData(7, nUnitID, null, carXJDevScale, strCurDT);//监测数据库中是否有数据
                    if (nTestData == 1)
                        continue;
                    strComm = "insert into s_CarsHis (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coaltype) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'" + coalType + "') ";
                    this.WriteLog("carYXWritedatetime 函数日期格式" + strCurDT+"字符串"+strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    this.WriteLog("carYXWriteDataOutput 函数 正在上传  " + strCurDT + "  地磅数据");
                    this.SetBanDataInfo(12, carID.ToString());//保存序号到注册表
                    bUpdate = true;

                }
                //**************************更新s_cardyn表****************************
                this.WriteLog("bUpdate值为" + bUpdate.ToString()+"日期"+strCurDT+"tareweight"+tareWeight.ToString());
                if ((bUpdate)&& (strCurDT!="")&&(tareWeight!=0))//有数据更新
                {
                    int nTest = SelectData(8, nUnitID, null, carXJDevScale, "0");//    
                    if (nTest == -1)
                        return 8001405;
                   
                    if (nTest == 1)
                        strComm = "update s_carsDyn set CurDT='" + strCurDT + "',carid='" + strCarID + "',GrossWeight=" + grossWeight + ",TareWeight=" + tareWeight + ",netWeight=" + netWeight + ",CoalType='" + coalType + "' where DeviceID=" + carXJDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_carsdyn (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coalType) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'" + coalType + "') ";

                    this.WriteLog("音西WriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    
                    bUpdate = false;
                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                carYXReader.Close();
                carYXReader.Dispose();
                //lb.Items.Add("开封万惠/清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("新疆音西矿地磅正在发送数据 ");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("音西矿carYXWriteDataOutput 函数 " + ex.ToString());
                return 801407;
            }
        }
        #endregion


        #region 恒泰向服务器写入数据
        public int CarHTWriteDataOutput(int nUnitID, int nDevScale, string strPath, string strPass)
        {
            try
            {
                bool bUpdate = false;
                string strComm = null;
                int carXJDevScale = 0, nTestData = 0;
                Int32 carID = 0;
                string carsIndex="0";
                string strCurDT = null, strCarID = null, strPerson = null, coalType = null;
                string[] strInfo = new string[2];
                float carXJChangeValue = 0, grossWeight = 0, tareWeight = 0, netWeight = 0;
                carXJChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(13);//得到时间 以及是否上传过


                OleDbDataReader carHTReader = GetCarHTOutput(strInfo[0]);

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                carXJDevScale = nUnitID * 100 + 60 + nDevScale;
                while (carHTReader.Read())
                {

                    this.WriteLog("carHTWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                    bUpdate = true;
                    carsIndex = carHTReader["id"].ToString();
                    strCurDT = carHTReader["datetime"].ToString();//时间
                    strCarID = carHTReader["chehao"].ToString();//车号
                    grossWeight = Convert.ToSingle(carHTReader["mz"]);//毛重
                    tareWeight = Convert.ToSingle(carHTReader["pz"]);//皮重
                    netWeight = Convert.ToSingle(carHTReader["jz"]);//净重
                    coalType = carHTReader["chw"].ToString();//煤种名称
                    

                    nTestData = this.SelectData(7, nUnitID, null, carXJDevScale, strCurDT);//监测数据库中是否有数据
                    if (nTestData == 1)
                        continue;
                    strComm = "insert into s_CarsHis (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coaltype) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'" + coalType + "') ";

                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    this.WriteLog("carHTWriteDataOutput 函数 正在上传  " + strCurDT + "  地磅数据");
                    this.SetBanDataInfo(13, carsIndex);//保存序号到注册表
                    bUpdate=true;
                }
                //**************************更新s_cardyn表****************************
                if (bUpdate)//有数据更新
                {
                    int nTest = SelectData(8, nUnitID, null, carXJDevScale, "0");//    
                    if (nTest == -1)
                        return 8001405;
                    if (nTest == 1)
                        strComm = "update s_carsDyn set CurDT='" + strCurDT + "',carid='" + strCarID + "',GrossWeight=" + grossWeight + ",TareWeight=" + tareWeight + ",netWeight=" + netWeight + ",CoalType='" + coalType + "' where DeviceID=" + carXJDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_carsdyn (DeviceID,CurDT,CarID,OperatePerson,GrossWeight,TareWeight,netWeight,coalType) Values(" + carXJDevScale + ",'" + strCurDT + "','" + strCarID + "','" + strPerson + "'," + grossWeight + "," + tareWeight + "," + netWeight + ",'" + coalType + "') ";

                    this.WriteLog("恒泰WriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    bUpdate = false;
                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                carHTReader.Close();
                carHTReader.Dispose();
                //lb.Items.Add("开封万惠/清华激光秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("新疆恒泰矿地磅正在发送数据 ");
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("恒泰矿carYXWriteDataOutput 函数 " + ex.ToString());
                return 801407;
            }
        }
        #endregion
    }
}
