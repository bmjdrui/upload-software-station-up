﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Threading;
using System.Configuration;
using Microsoft.Win32;
namespace StationUp
{
    public partial class FormMain : Form
    {
        string strSoftPath = @"C:\Program Files\scale\SetupScaleUp\";
        string[] strServerInfo = new string[10];//服务器设置
        string[] strHZCInfo = new string[10];//核子秤
        string[] strDZCInfo = new string[10];//电子秤
        string[] strSCLInfo = new string[10];//SCL监控系统
        string[] strHAInfo = new string[10];//灰分A型
        string[] strHCInfo = new string[10];//灰分C型
        string[] strHfyFB100Info = new string[10];//防爆灰分仪
        string[] strSFYInfo = new string[10];//东方测控水分仪
        string[] strHMInfo = new string[10];//海明

        string[] strLMQInfo = new string[10];//拉姆齐
        string[] strQhfyInfo = new string[10];//清华灰分仪
        string[] strQsfyInfo = new string[10];//清华水分仪
        string[] strQjgcInfo = new string[10];//清华激光秤
        string[] strKFwhInfo = new string[10];//开封万惠/清华激光秤
        string[] strXJcarInfo = new string[10];//新疆地磅
        string[] strHNInfo = new string[10];//淮南煤质
        string strCheckDev = "1111111111";
        string strHCCheckDev = "000000000";//HC灰分仪
        string strLMQCheckDev = null;
        string strCarXjDev = null;
        string strSclCheckDev = null;//SCL监控平台remark字段
        string strMessage = null;
        int timeCount = 1;
        bool bNetState = true;//网络状态写s_statehis表
        static object objLock = new object();//锁定对象
        DataOption dataOp = new DataOption();
        DataHzcClass hzcClass = new DataHzcClass();//
        DataHfyAClass hfyAClass = new DataHfyAClass();
        DataHfyCClass hfyCClass = new DataHfyCClass();
        DataDzcClass dzcClass = new DataDzcClass();
        DataHfyFB100Class hfyFB100Class = new DataHfyFB100Class();
        DataSCLClass sclClass = new DataSCLClass();
        DataHMClass hmClass = new DataHMClass();
        DataSHYClass shfClass = new DataSHYClass();
        DataLMQClass lmqClass = new DataLMQClass();
        DataQHHfyClass qhHfyClass = new DataQHHfyClass();
        DataKFWHClass kfwhClass = new DataKFWHClass();
        DatajgcClass jgcClass = new DatajgcClass();
        DataCarXJClass carxjClass = new DataCarXJClass();
        DataHNClass hnClass = new DataHNClass();
        FormPass fp = new FormPass();
        System.Timers.Timer time = new System.Timers.Timer();
        DateTime time1;
        DateTime time2;
        TimeSpan time3;
        Thread mainThread;
        #region 窗体设置
        public FormMain()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;

        }


        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //初始化控件禁用
                tabControl1.Enabled = false;
                textBoxAddress.Enabled = false;
                textBoxUser.Enabled = false;
                textBoxPass.Enabled = false;
                textBoxID.Enabled = false;
                textBoxName.Enabled = false;

                string strHzcCheckDev = null;
                string strHfyCCheckDev = "000000000";//C型灰分仪201109版本软件
                string strLMQDev = null;//拉姆齐电子秤版本区分
                string strSclDev = null;//scl监控平台版本区分
                string strXJcarCheckDev = null;
                if (checkDev0.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                if (checkDev1.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                if (checkDev2.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                if (checkDev3.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                if (checkDev4.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                if (checkDev5.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                if (checkDev6.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                if (checkDev7.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                //*********动态系数********
                if (checkDev8.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                //*********班产系数********
                if (checkDev9.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                //*********Ftd/kf 选中为ftd********
                if (checkDev10.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";
                //*********备用********
                if (checkDev11.Checked)
                    strHzcCheckDev += "1";
                else
                    strHzcCheckDev += "0";

                //201109版本灰分仪28个字段版本
                if (checkHCCheck0.Checked)
                    strHfyCCheckDev = "1";
                else
                    strHfyCCheckDev = "0";

                //Scl监控系统版本
                if (checkBoxSclVer.Checked)
                    strSclDev += "1";
                else
                    strSclDev += "0";

                //拉姆齐版本
                strLMQCheckDev = null;
                if (checkBox18.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                if (checkBox17.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                if (checkBox16.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                if (checkBox15.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                if (checkBox14.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                if (checkBox13.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                if (checkBox12.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                if (checkBox11.Checked)
                    strLMQCheckDev += "1";
                else
                    strLMQCheckDev += "0";
                //新疆地磅
                strCarXjDev = null;
                if (checkCar1.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";

                if (checkCar2.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";

                if (checkCar3.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";

                if (checkCar4.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";

                if (checkCar5.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";

                if (checkCar6.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";

                if (checkCar7.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";

                if (checkCar8.Checked)
                    strCarXjDev += "1";
                else
                    strCarXjDev += "0";


                //*************************************
                //淮南煤质
                WriteXmlMachine("HNSet.xml", textHNStation.Text.Trim(), textHNUser.Text.Trim(), textHNPass.Text.Trim(), textHNHour.Text.Trim(), textHNMin.Text.Trim(), textHNDay.Text.Trim(), textHNDataPass.Text.Trim(), checkHNUP.Checked.ToString(), null);

                //新疆地磅
                WriteXmlMachine("XJcarSet.xml", textXJcarStation.Text.Trim(), textXJcarUser.Text.Trim(), textXJcarPass.Text.Trim(), textXJcarHour.Text.Trim(), textXJcarMin.Text.Trim(), textXJcarBan.Text.Trim(), textXJcarDataPass.Text.Trim(), checkXJcarUP.Checked.ToString(), strCarXjDev);
                //清华水分仪
                WriteXmlMachine("QsfySet.xml", textQsfyStation.Text.Trim(), textQsfyUser.Text.Trim(), textQsfyPass.Text.Trim(), textQsfyHour.Text.Trim(), textQsfyMin.Text.Trim(), textQsfyBan.Text.Trim(), textQsfyDataPass.Text.Trim(), checkQsfyUP.Checked.ToString(), null);
                //清华激光秤
                WriteXmlMachine("QjgcSet.xml", textQjgcStation.Text.Trim(), textQjgcUser.Text.Trim(), textQjgcPass.Text.Trim(), textQjgcHour.Text.Trim(), textQjgcMin.Text.Trim(), textQjgcBan.Text.Trim(), textQjgcDataPass.Text.Trim(), checkQjgcUP.Checked.ToString(), null);
                //开封万惠/清华激光秤
                WriteXmlMachine("KFwhSet.xml", textKFwhStation.Text.Trim(), textKFwhUser.Text.Trim(), textKFwhPass.Text.Trim(), textKFwhHour.Text.Trim(), textKFwhMin.Text.Trim(), textKFwhBan.Text.Trim(), textKFwhDataPass.Text.Trim(), checkKfwhUP.Checked.ToString(), null);
                //拉姆齐
                WriteXmlMachine("LMQSet.xml", textLMQStation.Text.Trim(), textLMQUser.Text.Trim(), textLMQPass.Text.Trim(), textLMQHour.Text.Trim(), textLMQMin.Text.Trim(), textLMQBan.Text.Trim(), textLMQDataPass.Text.Trim(), checkLMQUP.Checked.ToString(), strLMQCheckDev);
                //清华灰分仪
                WriteXmlMachine("QhfySet.xml", textQhfyStation.Text.Trim(), textQhfyUser.Text.Trim(), textQhfyPass.Text.Trim(), textQhfyHour.Text.Trim(), textQhfyMin.Text.Trim(), textQhfyBan.Text.Trim(), textQhfyDataPass.Text.Trim(), checkQhfyUP.Checked.ToString(), null);

                //核子秤
                WriteXmlMachine("HZCSet.xml", textHZStation.Text.Trim(), textHZUser.Text.Trim(), textHZPass.Text.Trim(), textHZHour.Text.Trim(), textHZMin.Text.Trim(), textHZClass.Text.Trim(), textHZDataPass.Text.Trim(), checkHZUp.Checked.ToString(), strHzcCheckDev);
                //电子秤
                WriteXmlMachine("DZCSet.xml", textDZStation.Text.Trim(), textDZUser.Text.Trim(), textDZPass.Text.Trim(), textDZHour.Text.Trim(), textDZMin.Text.Trim(), textDZClass.Text.Trim(), textDZDataPass.Text.Trim(), checkDZUp.Checked.ToString(), null);
                //SCL监控系统
                WriteXmlMachine("SCLSet.xml", textSCLStation.Text.Trim(), textSCLUser.Text.Trim(), textSCLPass.Text.Trim(), textSCLHour.Text.Trim(), textSCLMin.Text.Trim(), textSCLClass.Text.Trim(), textSCLDataPass.Text.Trim(), checkSCLUp.Checked.ToString(), strSclDev);
                //2000A灰分仪
                WriteXmlMachine("HASet.xml", textHAStation.Text.Trim(), textHAUser.Text.Trim(), textHAPass.Text.Trim(), textHAHour.Text.Trim(), textHAMin.Text.Trim(), textHAClass.Text.Trim(), textHADataPass.Text.Trim(), checkHAUp.Checked.ToString(), null);
                //2000C灰分仪
                WriteXmlMachine("HCSet.xml", textHCStation.Text.Trim(), textHCUser.Text.Trim(), textHCPass.Text.Trim(), textHCHour.Text.Trim(), textHCMin.Text.Trim(), textHCClass.Text.Trim(), textHCDataPass.Text.Trim(), checkHCUp.Checked.ToString(), strHfyCCheckDev);
                //防爆灰分仪
                WriteXmlMachine("HFYFB100Set.xml", textHfyFB100Station.Text.Trim(), textHfyFB100User.Text.Trim(), textHfyFB100Pass.Text.Trim(), textHfyFB100Hour.Text.Trim(), textHfyFB100Min.Text.Trim(), textHfyFB100Class.Text.Trim(), textHfyFB100DataPass.Text.Trim(), checkHfyFB100Up.Checked.ToString(), null);
                //合肥海明
                WriteXmlMachine("HMSet.xml", textHMStation.Text.Trim(), textHMUser.Text.Trim(), textHMPass.Text.Trim(), textHMHour.Text.Trim(), textHMMin.Text.Trim(), textHMClass.Text.Trim(), textHMDataPass.Text.Trim(), checkHMUp.Checked.ToString(), null);
                //东方水分仪
                WriteXmlMachine("SFYSet.xml", textSFYStation.Text.Trim(), textSFYUser.Text.Trim(), textSFYPass.Text.Trim(), textSFYHour.Text.Trim(), textSFYMin.Text.Trim(), textSFYBan.Text.Trim(), textSHYPassWord.Text.Trim(), checkSFYUP.Checked.ToString(), null);
                //服务器
                WriteXmlServer(textBoxAddress.Text.Trim(), textBoxUser.Text.Trim(), textBoxPass.Text.Trim(), textBoxName.Text.Trim(), textBoxID.Text.Trim());
                //保存矿名到服务器
                DataOption dataOp = new DataOption();
                dataOp.ServerAddress = textBoxAddress.Text.Trim();
                dataOp.SqlName = textBoxUser.Text.Trim();
                dataOp.SqlPass = textBoxPass.Text.Trim();
                dataOp.GetInitPara(Convert.ToInt32(textBoxID.Text.Trim()), textBoxName.Text.Trim(), null);
                return;
            }
            catch
            {
                return;
            }
        }
        /// <summary>
        /// 读取地址信息
        /// </summary>
        /// <param name="strIpAddress">地址</param>
        /// <param name="strUser">用户</param>
        /// <param name="strPass">密码</param>
        /// <param name="strPort">端口</param>
        /// <param name="strInterval">刷新时间</param>
        private void WriteXmlServer(string strIpAddress, string strUser, string strPass, string strName, string strID)
        {
            string strPath = strSoftPath + "ServerSet.xml";
            XmlTextWriter xmlwriter = new XmlTextWriter(strPath, System.Text.Encoding.UTF8);
            xmlwriter.Formatting = Formatting.Indented;
            xmlwriter.WriteStartElement("Set");
            xmlwriter.WriteComment("数据库服务器的设置信息");
            xmlwriter.WriteElementString("IpAddress", strIpAddress);
            xmlwriter.WriteElementString("User", strUser);
            xmlwriter.WriteElementString("Pass", strPass);
            xmlwriter.WriteElementString("Name", strName);
            xmlwriter.WriteElementString("ID", strID);
            xmlwriter.WriteEndElement();
            xmlwriter.Close();
        }
        private void WriteXmlMachine(string fileName, string strIpAddress, string strUser, string strPass, string strHour, string strMin, string strClass, string strDataPass, string strUp, string strRemark)
        {
            try
            {
                string strPath = strSoftPath + fileName;
                XmlTextWriter xmlwriter = new XmlTextWriter(fileName, System.Text.Encoding.UTF8);//,System.Text.Encoding.UTF8
                xmlwriter.Formatting = Formatting.Indented;
                xmlwriter.WriteStartElement("Set");
                xmlwriter.WriteComment("设备设置信息");
                xmlwriter.WriteElementString("Station", strIpAddress);
                xmlwriter.WriteElementString("User", strUser);
                xmlwriter.WriteElementString("Pass", strPass);
                xmlwriter.WriteElementString("Hour", strHour);
                xmlwriter.WriteElementString("Min", strMin);
                xmlwriter.WriteElementString("Class", strClass);
                xmlwriter.WriteElementString("DataPass", strDataPass);
                xmlwriter.WriteElementString("Up", strUp);
                xmlwriter.WriteElementString("Remark", strRemark);
                xmlwriter.WriteEndElement();
                xmlwriter.Close();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        /// <summary>
        /// 读取数据库设置信息
        /// </summary>
        /// <returns></returns>
        private string[] ReadConnectionInfo()
        {
            int i = 0;
            string[] strCurrValue = new string[5];
            try
            {
                string strPath = strSoftPath + "ServerSet.xml";
                XmlTextReader xmlreader = new XmlTextReader(strPath);
                while (xmlreader.Read())
                {
                    if (xmlreader.NodeType == XmlNodeType.Text)
                    {
                        strCurrValue[i] = xmlreader.Value;
                        i++;
                    }
                }
                xmlreader.Close();
                return strCurrValue;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private string[] ReadXmlInfo(string fileName)
        {
            int i = 0;
            string[] strCurrValue = new string[10];
            try
            {
                string strPath = strSoftPath + fileName;
                XmlTextReader xmlreader = new XmlTextReader(strPath);
                while (xmlreader.Read())
                {
                    if (xmlreader.NodeType == XmlNodeType.Text)
                    {
                        strCurrValue[i] = xmlreader.Value;
                        i++;
                    }
                }
                xmlreader.Close();

                dataOp.WriteLog(fileName + "读取xml数据正常");
                return strCurrValue;
            }
            catch (Exception e)
            {
                dataOp.WriteLog("正在读取XML数据出现错误" + e.Message);
                return null;
            }
        }
        /// <summary>
        /// 窗体初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            //lbShowInfo.Items.Add("执行了新改");
            //RegistryKey key = Registry.LocalMachine.OpenSubKey("Software", true);
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE", true);
            if (key == null)
            {
                key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE");
            }
            //strSoftPath = key.GetValue("上传软件", @"C:\Program Files\scale\SetupScaleUp\").ToString();
            strSoftPath = AppDomain.CurrentDomain.BaseDirectory;
            int indexStr = strSoftPath.IndexOf("StationUp.exe");
            if (indexStr != -1)
                strSoftPath = strSoftPath.Substring(0, indexStr);
            #region 控件赋值
            //初始化控件禁用
            tabControl1.Enabled = false;
            textBoxAddress.Enabled = false;
            textBoxUser.Enabled = false;
            textBoxPass.Enabled = false;
            textBoxID.Enabled = false;
            textBoxName.Enabled = false;
            //sql Server数据库
            strServerInfo = ReadConnectionInfo();
            if (strServerInfo != null)
            {
                textBoxAddress.Text = strServerInfo[0];
                textBoxUser.Text = strServerInfo[1];
                textBoxPass.Text = strServerInfo[2];
                textBoxName.Text = strServerInfo[3];
                textBoxID.Text = strServerInfo[4];

                /*sc.Address = strServerInfo[0];
                sc.Name = strServerInfo[1];
                sc.Pass = strServerInfo[2];*/
            }
            else
            {
                lbShowInfo.Items.Add("本地配置文件读取不正确，请检查");
                return;
            }
            //核子秤
            strHZCInfo = ReadXmlInfo("HZCSet.xml");
            if (strHZCInfo != null)
            {
                textHZStation.Text = strHZCInfo[0];
                textHZUser.Text = strHZCInfo[1];
                textHZPass.Text = strHZCInfo[2];
                textHZHour.Text = strHZCInfo[3];
                textHZMin.Text = strHZCInfo[4];
                textHZClass.Text = strHZCInfo[5];
                textHZDataPass.Text = strHZCInfo[6];
                if (strHZCInfo[7] == "True")
                    checkHZUp.Checked = true;
                else
                    checkHZUp.Checked = false;
                strCheckDev = strHZCInfo[8];
                string strCheck = strCheckDev.Substring(0, 1);
                if (strCheck == "1")
                    checkDev0.Checked = true;
                else
                    checkDev0.Checked = false;
                strCheck = strCheckDev.Substring(1, 1);
                if (strCheck == "1")
                    checkDev1.Checked = true;
                else
                    checkDev1.Checked = false;
                strCheck = strCheckDev.Substring(2, 1);
                if (strCheck == "1")
                    checkDev2.Checked = true;
                else
                    checkDev2.Checked = false;
                strCheck = strCheckDev.Substring(3, 1);
                if (strCheck == "1")
                    checkDev3.Checked = true;
                else
                    checkDev3.Checked = false;
                strCheck = strCheckDev.Substring(4, 1);
                if (strCheck == "1")
                    checkDev4.Checked = true;
                else
                    checkDev4.Checked = false;
                strCheck = strCheckDev.Substring(5, 1);
                if (strCheck == "1")
                    checkDev5.Checked = true;
                else
                    checkDev5.Checked = false;
                strCheck = strCheckDev.Substring(6, 1);
                if (strCheck == "1")
                    checkDev6.Checked = true;
                else
                    checkDev6.Checked = false;
                strCheck = strCheckDev.Substring(7, 1);
                if (strCheck == "1")
                    checkDev7.Checked = true;
                else
                    checkDev7.Checked = false;
                //*************************************
                strCheck = strCheckDev.Substring(8, 1);
                if (strCheck == "1")
                    checkDev8.Checked = true;
                else
                    checkDev8.Checked = false;
                strCheck = strCheckDev.Substring(9, 1);
                if (strCheck == "1")
                    checkDev9.Checked = true;
                else
                    checkDev9.Checked = false;
                strCheck = strCheckDev.Substring(10, 1);
                if (strCheck == "1")
                    checkDev10.Checked = true;
                else
                    checkDev10.Checked = false;
                strCheck = strCheckDev.Substring(11, 1);
                if (strCheck == "1")
                    checkDev11.Checked = true;
                else
                    checkDev11.Checked = false;

            }
            //电子秤
            strDZCInfo = ReadXmlInfo("DZCSet.xml");
            if (strDZCInfo != null)
            {
                textDZStation.Text = strDZCInfo[0];
                textDZUser.Text = strDZCInfo[1];
                textDZPass.Text = strDZCInfo[2];
                textDZHour.Text = strDZCInfo[3];
                textDZMin.Text = strDZCInfo[4];
                textDZClass.Text = strDZCInfo[5];
                textDZDataPass.Text = strDZCInfo[6];
                if (strDZCInfo[7] == "True")
                    checkDZUp.Checked = true;
                else
                    checkDZUp.Checked = false;

            }
            //SCL监控系统
            strSCLInfo = ReadXmlInfo("SCLSet.xml");
            if (strSCLInfo != null)
            {
                textSCLStation.Text = strSCLInfo[0];
                textSCLUser.Text = strSCLInfo[1];
                textSCLPass.Text = strSCLInfo[2];
                textSCLHour.Text = strSCLInfo[3];
                textSCLMin.Text = strSCLInfo[4];
                textSCLClass.Text = strSCLInfo[5];
                textSCLDataPass.Text = strSCLInfo[6];
                if (strSCLInfo[7] == "True")
                    checkSCLUp.Checked = true;
                else
                    checkSCLUp.Checked = false;
                strSclCheckDev = strSCLInfo[8];
                string strSCLCheck = strSclCheckDev.Substring(0, 1);
                if (strSCLCheck == "1")
                    checkBoxSclVer.Checked = true;
                else
                    checkBoxSclVer.Checked = false;

            }
            //2000A灰分仪
            strHAInfo = ReadXmlInfo("HASet.xml");
            if (strHAInfo != null)
            {
                textHAStation.Text = strHAInfo[0];
                textHAUser.Text = strHAInfo[1];
                textHAPass.Text = strHAInfo[2];
                textHAHour.Text = strHAInfo[3];
                textHAMin.Text = strHAInfo[4];
                textHAClass.Text = strHAInfo[5];
                textHADataPass.Text = strHAInfo[6];
                if (strHAInfo[7] == "True")
                    checkHAUp.Checked = true;
                else
                    checkHAUp.Checked = false;

            }
            //2000C灰分仪
            strHCInfo = ReadXmlInfo("HCSet.xml");
            if (strHCInfo != null)
            {
                textHCStation.Text = strHCInfo[0];
                textHCUser.Text = strHCInfo[1];
                textHCPass.Text = strHCInfo[2];
                textHCHour.Text = strHCInfo[3];
                textHCMin.Text = strHCInfo[4];
                textHCClass.Text = strHCInfo[5];
                textHCDataPass.Text = strHCInfo[6];
                if (strHCInfo[7] == "True")
                    checkHCUp.Checked = true;
                else
                    checkHCUp.Checked = false;
                strHCCheckDev = strHCInfo[8];
                string strHCCheck = strHCCheckDev.Substring(0, 1);
                if (strHCCheck == "1")
                    checkHCCheck0.Checked = true;
                else
                    checkHCCheck0.Checked = false;

            }
            //防爆灰分仪
            strHfyFB100Info = ReadXmlInfo("HfyFB100Set.xml");
            if (strHfyFB100Info != null)
            {

                textHfyFB100Station.Text = strHfyFB100Info[0];
                textHfyFB100User.Text = strHfyFB100Info[1];
                textHfyFB100Pass.Text = strHfyFB100Info[2];
                textHfyFB100Hour.Text = strHfyFB100Info[3];
                textHfyFB100Min.Text = strHfyFB100Info[4];
                textHfyFB100Class.Text = strHfyFB100Info[5];
                textHfyFB100DataPass.Text = strHfyFB100Info[6];
                if (strHfyFB100Info[7] == "True")
                    checkHfyFB100Up.Checked = true;
                else
                    checkHfyFB100Up.Checked = false;

            }
            //合肥海明核子秤 
            strHMInfo = ReadXmlInfo("HMSet.xml");
            if (strHMInfo != null)
            {
                textHMStation.Text = strHMInfo[0];
                textHMUser.Text = strHMInfo[1];
                textHMPass.Text = strHMInfo[2];
                textHMHour.Text = strHMInfo[3];
                textHMMin.Text = strHMInfo[4];
                textHMClass.Text = strHMInfo[5];
                textHMDataPass.Text = strHMInfo[6];
                if (strHMInfo[7] == "True")
                    checkHMUp.Checked = true;
                else
                    checkHMUp.Checked = false;

            }
            //东方水分仪
            strSFYInfo = ReadXmlInfo("SFYSet.xml");
            if (strSFYInfo != null)
            {

                textSFYStation.Text = strSFYInfo[0];
                textSFYUser.Text = strSFYInfo[1];
                textSFYPass.Text = strSFYInfo[2];
                textSFYHour.Text = strSFYInfo[3];
                textSFYMin.Text = strSFYInfo[4];
                textSFYBan.Text = strSFYInfo[5];
                textSHYPassWord.Text = strSFYInfo[6];
                if (strSFYInfo[7] == "True")
                    checkSFYUP.Checked = true;
                else
                    checkSFYUP.Checked = false;

            }
            //拉姆齐
            strLMQInfo = ReadXmlInfo("LMQSet.xml");
            if (strLMQInfo != null)
            {
                textLMQStation.Text = strLMQInfo[0];
                textLMQUser.Text = strLMQInfo[1];
                textLMQPass.Text = strLMQInfo[2];
                textLMQHour.Text = strLMQInfo[3];
                textLMQMin.Text = strLMQInfo[4];
                textLMQBan.Text = strLMQInfo[5];
                textLMQDataPass.Text = strLMQInfo[6];
                if (strLMQInfo[7] == "True")
                {
                    checkLMQUP.Checked = true;
                }
                else
                {
                    checkLMQUP.Checked = false;
                }
                strLMQCheckDev = strLMQInfo[8];
                string strLMQCheck = strLMQCheckDev.Substring(0, 1);
                if (strLMQCheck == "1")
                    checkBox18.Checked = true;
                else
                    checkBox18.Checked = false;
                if (strLMQCheckDev.Substring(1, 1) == "1")
                    checkBox17.Checked = true;
                else
                    checkBox17.Checked = false;
                if (strLMQCheckDev.Substring(2, 1) == "1")
                    checkBox16.Checked = true;
                else
                    checkBox16.Checked = false;
                if (strLMQCheckDev.Substring(3, 1) == "1")
                    checkBox15.Checked = true;
                else
                    checkBox15.Checked = false;
                if (strLMQCheckDev.Substring(4, 1) == "1")
                    checkBox14.Checked = true;
                else
                    checkBox14.Checked = false;
                if (strLMQCheckDev.Substring(5, 1) == "1")
                    checkBox13.Checked = true;
                else
                    checkBox13.Checked = false;
                if (strLMQCheckDev.Substring(6, 1) == "1")
                    checkBox12.Checked = true;
                else
                    checkBox12.Checked = false;
                if (strLMQCheckDev.Substring(7, 1) == "1")
                    checkBox11.Checked = true;
                else
                    checkBox11.Checked = false;
            }
            //清华灰分仪
            strQhfyInfo = ReadXmlInfo("QhfySet.xml");
            if (strQhfyInfo != null)
            {
                textQhfyStation.Text = strQhfyInfo[0];
                textQhfyUser.Text = strQhfyInfo[1];
                textQhfyPass.Text = strQhfyInfo[2];
                textQhfyHour.Text = strQhfyInfo[3];
                textQhfyMin.Text = strQhfyInfo[4];
                textQhfyBan.Text = strQhfyInfo[5];
                textQhfyPass.Text = strQhfyInfo[6];
                if (strQhfyInfo[7] == "True")
                {
                    checkQhfyUP.Checked = true;
                }
                else
                {
                    checkQhfyUP.Checked = false;
                }
            }
            //清华水分仪
            strQsfyInfo = ReadXmlInfo("QsfySet.xml");
            if (strQsfyInfo != null)
            {
                textQsfyStation.Text = strQsfyInfo[0];
                textQsfyUser.Text = strQsfyInfo[1];
                textQsfyPass.Text = strQsfyInfo[2];
                textQsfyHour.Text = strQsfyInfo[3];
                textQsfyMin.Text = strQsfyInfo[4];
                textQsfyBan.Text = strQsfyInfo[5];
                textQsfyPass.Text = strQsfyInfo[6];
                if (strQsfyInfo[7] == "True")
                {
                    checkQsfyUP.Checked = true;
                }
                else
                {
                    checkQsfyUP.Checked = false;
                }
            }
            //清华激光秤
            strQjgcInfo = ReadXmlInfo("QjgcSet.xml");
            if (strQjgcInfo != null)
            {
                textQjgcStation.Text = strQjgcInfo[0];
                textQjgcUser.Text = strQjgcInfo[1];
                textQjgcPass.Text = strQjgcInfo[2];
                textQjgcHour.Text = strQjgcInfo[3];
                textQjgcMin.Text = strQjgcInfo[4];
                textQjgcBan.Text = strQjgcInfo[5];
                textQjgcPass.Text = strQjgcInfo[6];
                if (strQjgcInfo[7] == "True")
                {
                    checkQjgcUP.Checked = true;
                }
                else
                {
                    checkQjgcUP.Checked = false;
                }
            }
            //开封万惠/清华激光秤
            strKFwhInfo = ReadXmlInfo("KFwhSet.xml");
            if (strKFwhInfo != null)
            {
                textKFwhStation.Text = strKFwhInfo[0];
                textKFwhUser.Text = strKFwhInfo[1];
                textKFwhPass.Text = strKFwhInfo[2];
                textKFwhHour.Text = strKFwhInfo[3];
                textKFwhMin.Text = strKFwhInfo[4];
                textKFwhBan.Text = strKFwhInfo[5];
                textKFwhDataPass.Text = strKFwhInfo[6];
                if (strKFwhInfo[7] == "True")
                {
                    checkKfwhUP.Checked = true;
                }
                else
                {
                    checkKfwhUP.Checked = false;
                }
            }
            //新疆地磅
            strXJcarInfo = ReadXmlInfo("XJcarSet.xml");
            if (strXJcarInfo != null)
            {
                textXJcarStation.Text = strXJcarInfo[0];
                textXJcarUser.Text = strXJcarInfo[1];
                textXJcarPass.Text = strXJcarInfo[2];
                textXJcarHour.Text = strXJcarInfo[3];
                textXJcarMin.Text = strXJcarInfo[4];
                textXJcarBan.Text = strXJcarInfo[5];
                textXJcarDataPass.Text = strXJcarInfo[6];
                if (strXJcarInfo[7] == "True")
                {
                    checkXJcarUP.Checked = true;
                }
                else
                {
                    checkXJcarUP.Checked = false;
                }
                strCarXjDev = strXJcarInfo[8];
                if (strCarXjDev.Substring(0, 1) == "1")
                    checkCar1.Checked = true;
                else
                    checkCar1.Checked = false;
                if (strCarXjDev.Substring(1, 1) == "1")
                    checkCar2.Checked = true;
                else
                    checkCar2.Checked = false;
                if (strCarXjDev.Substring(2, 1) == "1")
                    checkCar3.Checked = true;
                else
                    checkCar3.Checked = false;
                if (strCarXjDev.Substring(3, 1) == "1")
                    checkCar4.Checked = true;
                else
                    checkCar4.Checked = false;
                if (strCarXjDev.Substring(4, 1) == "1")
                    checkCar5.Checked = true;
                else
                    checkCar5.Checked = false;
                if (strCarXjDev.Substring(5, 1) == "1")
                    checkCar6.Checked = true;
                else
                    checkCar6.Checked = false;
                if (strCarXjDev.Substring(6, 1) == "1")
                    checkCar7.Checked = true;
                else
                    checkCar7.Checked = false;
                if (strCarXjDev.Substring(7, 1) == "1")
                    checkCar8.Checked = true;
                else
                    checkCar8.Checked = false;





            }
            //淮南煤质
            strHNInfo = ReadXmlInfo("HNSet.xml");
            if (strHNInfo != null)
            {
                textHNStation.Text = strHNInfo[0];
                textHNUser.Text = strHNInfo[1];
                textHNPass.Text = strHNInfo[2];
                textHNHour.Text = strHNInfo[3];
                textHNMin.Text = strHNInfo[4];
                textHNDay.Text = strHNInfo[5];
                textHNDataPass.Text = strHNInfo[6];
                if (strHNInfo[7] == "True")
                {
                    checkHNUP.Checked = true;
                }
                else
                {
                    checkHNUP.Checked = false;
                }
            }
            int nID = Convert.ToInt32(textBoxID.Text.Trim());
            //核子秤数据库的密码 地址
            hzcClass.AddressMachine = textHZStation.Text.Trim();
            hzcClass.PassMachine = textHZPass.Text.Trim();
            hzcClass.ServerAddress = textBoxAddress.Text.Trim();
            hzcClass.SqlName = textBoxUser.Text.Trim();
            hzcClass.SqlPass = textBoxPass.Text.Trim();
            //电子秤密码 地址
            dzcClass.ServerAddress = textBoxAddress.Text.Trim();
            dzcClass.SqlName = textBoxUser.Text.Trim();
            dzcClass.SqlPass = textBoxPass.Text.Trim();
            dzcClass.AddressMachine = textDZStation.Text.Trim();
            dzcClass.PassMachine = textDZPass.Text.Trim();
            //SCL监控系统
            sclClass.AddressMachine = textSCLStation.Text.Trim();
            sclClass.PassMachine = textSCLPass.Text.Trim();
            sclClass.ServerAddress = textBoxAddress.Text.Trim();
            sclClass.SqlName = textBoxUser.Text.Trim();
            sclClass.SqlPass = textBoxPass.Text.Trim();
            //灰分仪2000A
            hfyAClass.AddressMachine = textHAStation.Text.Trim();
            hfyAClass.PassMachine = textHAPass.Text.Trim();
            hfyAClass.ServerAddress = textBoxAddress.Text.Trim();
            hfyAClass.SqlName = textBoxUser.Text.Trim();
            hfyAClass.SqlPass = textBoxPass.Text.Trim();
            //灰分仪2000C
            hfyCClass.AddressMachine = textHCStation.Text.Trim();
            hfyCClass.PassMachine = textHCPass.Text.Trim();

            hfyCClass.ServerAddress = textBoxAddress.Text.Trim();
            hfyCClass.SqlName = textBoxUser.Text.Trim();
            hfyCClass.SqlPass = textBoxPass.Text.Trim();
            //防爆灰分仪
            hfyFB100Class.AddressMachine = textHfyFB100Station.Text.Trim();
            hfyFB100Class.PassMachine = textHfyFB100Pass.Text.Trim();
            hfyFB100Class.ServerAddress = textBoxAddress.Text.Trim();
            hfyFB100Class.SqlName = textBoxUser.Text.Trim();
            hfyFB100Class.SqlPass = textBoxPass.Text.Trim();
            //海明核子秤
            hmClass.AddressMachine = textHMStation.Text.Trim();
            hmClass.PassMachine = textHMPass.Text.Trim();
            hmClass.ServerAddress = textBoxAddress.Text.Trim();
            hmClass.SqlName = textBoxUser.Text.Trim();
            hmClass.SqlPass = textBoxPass.Text.Trim();
            //东方水分仪

            shfClass.AddressMachine = textSFYStation.Text.Trim();
            shfClass.PassMachine = textSFYPass.Text.Trim();
            shfClass.ServerAddress = textBoxAddress.Text.Trim();
            shfClass.SqlName = textBoxUser.Text.Trim();
            shfClass.SqlPass = textBoxPass.Text.Trim();

            //拉姆齐
            lmqClass.AddressMachine = textLMQStation.Text.Trim();//mySql地址
            lmqClass.PassMachine = textLMQPass.Text.Trim();//mysql密码
            lmqClass.UserMachine = textLMQUser.Text.Trim();//mysql用户
            lmqClass.ServerAddress = textBoxAddress.Text.Trim();//Sql server地址
            lmqClass.SqlName = textBoxUser.Text.Trim();//Sql server用户
            lmqClass.SqlPass = textBoxPass.Text.Trim();//Sql server密码
            //清华灰分仪
            qhHfyClass.AddressMachine = textQhfyStation.Text.Trim();//清华地址
            qhHfyClass.PassMachine = textQhfyPass.Text.Trim();//密码
            //qhHfyClass. = textQhfyUser.Text.Trim();//用户
            qhHfyClass.ServerAddress = textBoxAddress.Text.Trim();//Sql server地址
            qhHfyClass.SqlName = textBoxUser.Text.Trim();//Sql server用户
            qhHfyClass.SqlPass = textBoxPass.Text.Trim();//Sql server密码
            //开封万惠/清华激光秤
            kfwhClass.AddressMachine = textKFwhStation.Text.Trim();
            kfwhClass.UserMaching = textKFwhUser.Text.Trim();
            kfwhClass.PassMachine = textKFwhPass.Text.Trim();
            kfwhClass.ServerAddress = textBoxAddress.Text.Trim();
            kfwhClass.SqlName = textBoxUser.Text.Trim();
            kfwhClass.SqlPass = textBoxPass.Text.Trim();
            //清华激光秤
            jgcClass.AddressMachine = textQjgcStation.Text.Trim();
            jgcClass.UserMaching = textQjgcUser.Text.Trim();
            jgcClass.PassMachine = textQjgcPass.Text.Trim();
            jgcClass.ServerAddress = textBoxUser.Text.Trim();
            jgcClass.SqlName = textBoxName.Text.Trim();
            jgcClass.SqlPass = textBoxPass.Text.Trim();
            //新疆地磅
            carxjClass.AddressMachine = textXJcarStation.Text.Trim();
            carxjClass.UserMaching = textXJcarUser.Text.Trim();
            carxjClass.PassMachine = textXJcarPass.Text.Trim();
            carxjClass.ServerAddress = textBoxAddress.Text.Trim();
            carxjClass.SqlName = textBoxUser.Text.Trim();
            carxjClass.SqlPass = textBoxPass.Text.Trim();
            //淮南煤质
            hnClass.AddressMachine = textHNStation.Text.Trim();
            hnClass.UserMaching = textHNUser.Text.Trim();
            hnClass.PassMachine = textHNPass.Text.Trim();
            hnClass.Day = Convert.ToInt32(textHNDay.Text.Trim());
            hnClass.ServerAddress = textBoxAddress.Text.Trim();
            hnClass.SqlName = textBoxUser.Text.Trim();
            hnClass.SqlPass = textBoxPass.Text.Trim();

            dataOp.ServerAddress = textBoxAddress.Text.Trim();
            dataOp.SqlName = textBoxUser.Text.Trim();
            dataOp.SqlPass = textBoxPass.Text.Trim();
#endregion
            //注册表建立
            RegistryKey newkey = key.CreateSubKey("KJH-UP");// 增加一个子键
            //随机启动
           // RegistryKey root = key.OpenSubKey("Microsoft\\Windows\\CurrentVersion\\Run", true); ;
            RegistryKey root = key.OpenSubKey(@"Microsoft\Windows\CurrentVersion\Run", true);
            if (root == null)
            {
                root = key.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run");
            }
            root.SetValue("上传软件", System.AppDomain.CurrentDomain.BaseDirectory + "StationUp.exe");
            dataOp.WriteLog("写入注册表程序自动启动");
            SqlConnection SqlTestConn = dataOp.GetConnection();
            if (SqlTestConn != null)
            {
                int nReturn = 0;
                lbShowInfo.Items.Add("数据库连接正常，开始接受数据--" + DateTime.Now.ToString());
                dataOp.WriteLog("数据库连接正常，开始接受数据 ");
                //对不同设备进行初始化
                if (checkHZUp.Checked)
                    nReturn = hzcClass.GetInitPara(nID, textBoxName.Text.Trim(), strCheckDev);
                if (checkDZUp.Checked)
                    nReturn = dzcClass.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkHCUp.Checked)
                    nReturn = hfyCClass.GetInitPara(nID, textBoxName.Text.Trim(), strCheckDev);
                if (checkHAUp.Checked)
                    nReturn = hfyAClass.GetInitPara(nID, textBoxName.Text.Trim(), strCheckDev);
                if (checkHfyFB100Up.Checked)
                    nReturn = hfyFB100Class.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkSCLUp.Checked)
                    nReturn = sclClass.GetInitPara(nID, textBoxName.Text.Trim(), strSclCheckDev);
                if (checkHMUp.Checked)
                    nReturn = hmClass.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkSFYUP.Checked)
                    nReturn = shfClass.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkLMQUP.Checked)
                    nReturn = lmqClass.GetInitPara(nID, textBoxName.Text.Trim(), strLMQCheckDev);
                if (checkQhfyUP.Checked)
                    nReturn = qhHfyClass.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkKfwhUP.Checked)
                    nReturn = kfwhClass.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkQjgcUP.Checked)
                    nReturn = jgcClass.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkXJcarUP.Checked)
                    nReturn = carxjClass.GetInitPara(nID, textBoxName.Text.Trim(), null);
                if (checkHNUP.Checked)//淮南煤质
                {
                    //hnClass.WriteOracleStateHis("正常", "煤质上传软件启动");
                }
                // nReturn = hnClass.GetInitPara(nID, textBoxName.Text.Trim(), null);

                if (nReturn < 0)
                {
                    if (nReturn == -1)
                    {
                        lbShowInfo.Items.Add("数据库连接正常,但该单位未获得授权,请联系数据库管理员" + DateTime.Now.ToString());
                        dataOp.WriteLog("数据库连接正常,但该单位未获得授权,请联系数据库管理员--" + nReturn.ToString() + ")");
                    }

                }
                else
                {
                    textBoxID.Text = nReturn.ToString();//矿区编号
                    if (DataOption.UnitName != "测试")//判断矿井的名字在服务器上有没有 如果有就赋值
                        textBoxName.Text = DataOption.UnitName;
                    //获取ID成功即可保存
                    WriteXmlServer(textBoxAddress.Text.Trim(), textBoxUser.Text.Trim(), textBoxPass.Text.Trim(), textBoxName.Text.Trim(), textBoxID.Text.Trim());

                    lbShowInfo.Items.Add("数据库连接正常，初始化参数成功--" + DateTime.Now.ToString());
                    dataOp.WriteLog(DateTime.Now.ToString() + "--数据库连接正常，初始化参数成功" + nReturn.ToString());

                }


            }
            else
            {
                lbShowInfo.Items.Add("数据库无法连接，请检查--" + DateTime.Now.ToString());
                dataOp.WriteLog("数据库无法连接，请检查 ");
            }
            if (SqlTestConn != null)
            {
                if (!checkHNUP.Checked)//淮南煤质不需要更新s_statehis表
                {
                    dataOp.WriteNetState(Convert.ToInt16(textBoxID.Text.Trim()), "正常", "上传软件启动");//写网络状态
                }
                if (checkHNUP.Checked)
                {
                    hnClass.WriteOracleStateUnit("正常");
                    hnClass.WriteOracleStateHis("正常", "上传软件启动");
                }
                if (SqlTestConn.State == ConnectionState.Open)
                    SqlTestConn.Close();
                SqlTestConn.Dispose();
            }


            ////启动计时器 
            //time.Elapsed += new System.Timers.ElapsedEventHandler(time_Elapsed);
            //time.Interval = 60000;
            //time.Start();
            time1 = DateTime.Now;
            time2 = DateTime.Now;
            
            mainThread = new Thread(new ThreadStart(ThreadRun));
            mainThread.Name = "mainThread";
            mainThread.Start();

        }
        #endregion
        /// <summary>
        /// 循环发送数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void time_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {


        }


        private void WorkList_Update(object sender, EventArgs e)
        {
            lbShowInfo.Items.Add(strMessage);
            lbShowInfo.Update();
        }

        private void btnModfiy_Click(object sender, EventArgs e)
        {

            fp.ShowDialog();
            if (fp.SetModify)
            {
                //控件启用
                tabControl1.Enabled = true;
                textBoxAddress.Enabled = true;
                textBoxUser.Enabled = true;
                textBoxPass.Enabled = true;
                textBoxID.Enabled = true;
                textBoxName.Enabled = true;
            }

        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainThread.Abort();//线程退出
        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            fp.ShowDialog();
            if (fp.SetModify)
            {

                if (!checkHNUP.Checked)
                {
                    dataOp.WriteNetState(Convert.ToInt16(textBoxID.Text.Trim()), "断开", "上传软件退出");//写网络状态
                }
                if (checkHNUP.Checked)
                {
                    hnClass.WriteOracleStateHis("断开", "煤质上传软件退出");
                    hnClass.WriteOracleStateUnit("断开");
                }

                e.Cancel = false;
            }
            else
                e.Cancel = true;
            //fp.Close();
        }

        private void btnSclChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialogScl = new OpenFileDialog();
            openFileDialogScl.FileName = "*.mdb";
            openFileDialogScl.Filter = "mdb files (*.mdb)|*.mdb|All files (*.*)|*.*";
            openFileDialogScl.ShowDialog();
            if (openFileDialogScl.ShowDialog() == DialogResult.OK)
            {
                string strPath = System.IO.Path.GetDirectoryName(openFileDialogScl.FileName.ToString()).ToString().Trim();
                textSCLStation.Text = strPath;
            }



        }

        private void btnHzcChange_Click(object sender, EventArgs e)
        {

        }

        private void checkAlertSound_CheckedChanged(object sender, EventArgs e)
        {
           // AlertSound.Stop();
        }
        /// <summary>
        /// 关闭声音
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlert_Click(object sender, EventArgs e)
        {
            //AlertSound.Stop();//关闭声音
        }
        /// <summary>
        /// 测试网络状态
        /// </summary>
        private void NetState()
        {
            if (dataOp.GetConnection() != null)
            {
                return;
            }
            else
            {
                if (!checkAlertSound.Checked)
                {
                    AlertSound.Alert(true);
                }

                return;
            }

        }
        /// <summary>
        /// 线程函数
        /// </summary>
        private void ThreadRun()
        {
            Int32 tShow = 0;
            while (true)
            {
                tShow++;
                time3 = time2 - time1;
                int t = time3.Minutes;
                System.Diagnostics.Debug.Print(time2.ToString() + "   t=" + t.ToString());
                if (tShow == 6)
                {
                    strMessage = "即将开始传输数据1分钟倒计时----" + t.ToString() + "----" + DateTime.Now.ToString();
                    lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                    tShow = 0;
                }
                if (t >= 1)//每10分钟一次
                {
                    DataMain();//主函数
                    time1 = DateTime.Now;
                }
                time2 = DateTime.Now;
                Thread.Sleep(10000);
            }
            System.Diagnostics.Debug.Print("thread exit" + DateTime.Now.ToString());
        }
        /// <summary>
        /// 主函数
        /// </summary>
        private void DataMain()
        {
            lock (objLock)
            {
                System.Diagnostics.Debug.Print("DataMain");

                System.Diagnostics.Debug.Print("start DataMain" + DateTime.Now.ToString());
                int nState = 9999;
                string strCheck = null;
                string[] strObjArray = { "hzcClass", "hfyAClass", "hfyCClass", "dzcClass", "hfyFB100Class", "sclClass", "hmClass", "shfClass", "lmqClass", "qhHfyClass", "kfwhClass", "carxjClass", "hnClass" };
                int nUnitID = int.Parse(textBoxID.Text.ToString());
                if (nUnitID == -1)//如果id为-1不传输,
                    return;
                if (lbShowInfo.Items.Count > 200)
                    lbShowInfo.Items.Clear();

                try
                {
                    //foreach (string s in strObjArray)
                    //{
                    //    System.Threading.Monitor.Enter(s);
                    //}

                    if (checkHZUp.Checked)//核子秤
                    {

                        nState = hzcClass.HZCWriteDataDyn(nUnitID, textHZStation.Text.Trim().ToString(), textHZDataPass.Text.Trim().ToString(), strCheckDev, lbShowInfo);
                        strMessage = "核子秤数据正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        hzcClass.WriteLog("核子秤数据正在发送动态数据 ID--" + nState.ToString());
                        nState = hzcClass.HZCWriteDataOutput(int.Parse(textBoxID.Text.Trim()), textHZStation.Text.Trim(), textHZDataPass.Text.Trim(), int.Parse(textHZHour.Text.Trim()), int.Parse(textHZMin.Text.ToString()), strCheckDev, lbShowInfo);

                    }
                    if (checkHAUp.Checked)//2000A灰分仪
                    {
                        nState = hfyAClass.HFYWriteDataDyn(nUnitID, textHAStation.Text.Trim(), textHADataPass.Text.Trim());
                        strMessage = "2000A型灰分仪正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        hfyAClass.WriteLog("2000A型灰分仪正在发送动态数据 ID--" + nState.ToString());
                        nState = hfyAClass.HFYWriteDataHis(int.Parse(textBoxID.Text.Trim()), textHAStation.Text.Trim(), textHADataPass.Text.Trim());
                        strMessage = "2000A型灰分仪正在发送历史数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        hfyAClass.WriteLog("2000A型灰分仪正在发送历史数据 ID--" + nState.ToString());
                    }
                    if (checkHCUp.Checked)//2000C灰分仪
                    {
                        if (checkHCCheck0.Checked)
                            strCheck = "1";
                        else
                            strCheck = "0";
                        hfyCClass.WriteLog("2000C灰分仪" + strCheck.ToString());
                        nState = hfyCClass.HFYCWriteDataDyn(nUnitID, textHCStation.Text.Trim(), strCheck);
                        strMessage = "2000C型灰分仪正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));

                        hfyCClass.WriteLog("2000C型灰分仪正在发送动态数据 ID--" + nState.ToString());
                        nState = hfyCClass.HFYCWriteDataHis(nUnitID, textHCStation.Text.Trim(), strCheck, null, lbShowInfo);
                        strMessage = "2000C型灰分仪正在发送历史数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        hfyCClass.WriteLog("2000C型灰分仪正在发送历史数据 ID--" + nState.ToString());
                    }
                    if (checkDZUp.Checked)//KJD-N电子称
                    {
                        nState = dzcClass.DzcWriteDataDyn(nUnitID, textDZStation.Text.Trim(), textDZDataPass.Text.Trim(), null);
                        strMessage = "电子秤数据正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        dzcClass.WriteLog("电子秤数据正在发送动态数据 ID--" + nState.ToString());
                        nState = dzcClass.DzcWriteDataOutput(int.Parse(textBoxID.Text.Trim()), textDZStation.Text.Trim(), textDZDataPass.Text.Trim(), int.Parse(textDZHour.Text.Trim()), int.Parse(textDZMin.Text.ToString()), null, lbShowInfo);
                    }
                    if (checkHfyFB100Up.Checked)//防爆灰分仪
                    {
                        nState = hfyFB100Class.HFYFB100WriteDataDyn(nUnitID, textHfyFB100Station.Text.Trim(), textHfyFB100DataPass.Text.Trim());
                        strMessage = "防爆灰分仪正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        hfyFB100Class.WriteLog("防爆灰分仪正在发送动态数据 ID--" + nState.ToString());
                        nState = hfyFB100Class.HFYFB100WriteDataHis(nUnitID, textHfyFB100Station.Text.Trim(), textHfyFB100DataPass.Text.Trim());
                        strMessage = "防爆灰分仪正在发送历史数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        hfyFB100Class.WriteLog("防爆灰分仪正在发送历史数据 ID--" + nState.ToString());
                    }
                    if (checkSCLUp.Checked)//SCL监控系统
                    {
                        nState = sclClass.SCLWriteDataDyn(nUnitID, textSCLStation.Text.Trim().ToString(), textSCLDataPass.Text.Trim().ToString(), strSclCheckDev);
                        strMessage = "SCL监控系统正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        sclClass.WriteLog("SCL监控系统正在发送动态数据 ID--" + nState.ToString());
                        nState = sclClass.SCLWriteDataOutput(int.Parse(textBoxID.Text.Trim()), textSCLStation.Text.Trim(), textSCLDataPass.Text.Trim(), int.Parse(textSCLHour.Text.Trim()), int.Parse(textSCLMin.Text.ToString()), strSclCheckDev, lbShowInfo);
                    }
                    if (checkHMUp.Checked)//合肥海明核子秤
                    {
                        nState = hmClass.HMWriteDataDyn(nUnitID, textHMStation.Text.Trim().ToString(), textHMDataPass.Text.Trim().ToString(), null);
                        strMessage = "合肥海明系统正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        hmClass.WriteLog("合肥海明正在发送动态数据 ID--" + nState.ToString());
                        nState = hmClass.HMWriteDataOutput(int.Parse(textBoxID.Text.Trim()), textHMStation.Text.Trim(), textHMDataPass.Text.Trim(), int.Parse(textHMHour.Text.Trim()), int.Parse(textHMMin.Text.ToString()), null, lbShowInfo);
                    }
                    if (checkSFYUP.Checked)//水分仪
                    {
                        nState = shfClass.SFYWriteDataDyn(nUnitID, textSFYStation.Text, textSHYPassWord.Text);
                        strMessage = "东方水分仪正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        shfClass.WriteLog("东方水分仪正在发送动态数据 ID--" + nState.ToString());
                        nState = shfClass.SFYWriteDataHis(nUnitID, textSFYStation.Text.Trim(), textSHYPassWord.Text.Trim());
                        strMessage = "东方水分仪正在发送历史数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        shfClass.WriteLog("东方水分仪正在发送历史数据 ID--" + nState.ToString());
                    }
                    if (checkLMQUP.Checked)//拉姆齐电子秤
                    {
                        nState = lmqClass.LMQDzcWriteDataDyn(nUnitID, strLMQCheckDev);
                        strMessage = "拉姆齐电子秤正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        lmqClass.WriteLog("拉姆齐电子秤正在发送动态数据 ID--" + nState.ToString());
                        nState = lmqClass.LMQDzcWriteDataOutput(nUnitID, int.Parse(textLMQHour.Text.Trim()), int.Parse(textLMQMin.Text.Trim()), strLMQCheckDev);
                        if (nState == 0)
                        {
                            strMessage = "拉姆齐电子秤正在更新班产量 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                            lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                            lmqClass.WriteLog("拉姆齐电子秤正在更新班产量ID--" + nState.ToString());
                        }
                    }

                    if (checkQhfyUP.Checked)//清华灰分仪
                    {
                        //参数为单位号，灰分仪iP ,通信端口
                        nState = qhHfyClass.HFYQHWriteDataDyn(nUnitID, textQhfyStation.Text.Trim(), textQhfyUser.Text, null);
                        strMessage = "清华灰分仪正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        qhHfyClass.WriteLog("清华灰分仪正在发送动态及历史数据 ID--" + nState.ToString());
                        nState = qhHfyClass.HFYQHWriteDataHis(nUnitID, textQhfyStation.Text.Trim(), textQhfyUser.Text.Trim(), lbShowInfo);
                        strMessage = "清华分仪正在发送历史数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        qhHfyClass.WriteLog("清华分仪正在发送历史数据 ID--" + nState.ToString());

                    }
                    if (checkKfwhUP.Checked)//开封万惠/清华激光秤
                    {
                        nState = kfwhClass.kfwhWriteDataDyn(nUnitID, strCheckDev);
                        strMessage = "开封万惠/清华激光秤正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        kfwhClass.WriteLog("开封万惠/清华激光秤正在发送动态数据 ID--" + nState.ToString());
                        nState = kfwhClass.kfwhWriteDataOutput(int.Parse(textBoxID.Text.Trim()), strCheckDev);
                        kfwhClass.WriteLog("开封万惠/清华激光秤正在发送历史数据 ID--" + nState.ToString());
                        strMessage = "开封万惠/清华激光秤正在发送班产数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));

                    }
                    if (checkQjgcUP.Checked)//清华激光秤 暂时不用
                    {
                        nState = jgcClass.jgcWriteDataDyn(nUnitID, textQjgcStation.Text.Trim(), textQjgcUser.Text.ToString(), null);
                        strMessage = "清华激光秤正在发送动态数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                        kfwhClass.WriteLog("清华激光秤正在发送动态数据 ID--" + nState.ToString());
                        nState = jgcClass.jgcWriteDataOutput(nUnitID, textQjgcStation.Text.Trim(), textQjgcUser.Text.ToString(), null);
                        kfwhClass.WriteLog("清华激光秤正在发送历史数据 ID--" + nState.ToString());
                        strMessage = "清华激光秤正在发送班产数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                    }
                    if (checkXJcarUP.Checked)//新疆地磅
                    {
                        if (checkCar1.Checked)//潘津地磅
                            nState = carxjClass.CarPJWriteDataOutput(nUnitID, 0, textXJcarStation.Text.Trim(), textXJcarPass.Text.Trim());
                        if (checkCar2.Checked)//龟兹地磅
                            nState = carxjClass.CarQCWriteDataOutput(nUnitID, 0, textXJcarStation.Text.Trim(), textXJcarPass.Text.Trim());
                        if (checkCar3.Checked)//众泰地磅
                            nState = carxjClass.CarZTWriteDataOutput(nUnitID, 0, textXJcarStation.Text.Trim(), textXJcarPass.Text.Trim());
                        if (checkCar4.Checked)//音西地磅
                            nState = carxjClass.CarYXWriteDataOutput(nUnitID, 0, textXJcarStation.Text.Trim(), textXJcarPass.Text.Trim());
                        if (checkCar5.Checked)//众维地磅
                            nState = carxjClass.CarZWWriteDataOutput(nUnitID, 0, textXJcarStation.Text.Trim(), textXJcarPass.Text.Trim());
                        if (checkCar6.Checked)//恒泰地磅
                            nState = carxjClass.CarHTWriteDataOutput(nUnitID, 0, textXJcarStation.Text.Trim(), textXJcarPass.Text.Trim());
                        carxjClass.WriteLog("新疆地磅正在发送数据 ID--" + nState.ToString());
                        strMessage = "新疆地磅正在发送数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));

                    }

                    if (checkHNUP.Checked)//淮南煤质
                    {
                        hnClass.WriteLog("开始执行HNUP" + checkHAUp.Checked.ToString() + "      2");
                       // nState = hnClass.HNWriteDataOutput();
                        nState = hnClass.HNSQLWriteDataOutput();
                        hnClass.WriteLog("淮南煤质系统正在发送数据 ID--" + nState.ToString());
                        strMessage = "淮南煤质系统正在发送数据 ID--" + nState.ToString() + " " + DateTime.Now.ToString();
                        lbShowInfo.Invoke(new EventHandler(WorkList_Update));
                    }
                    DateTime newDT = DateTime.Now.AddDays(-10);
                    string strLogName = newDT.ToString("yyyy-MM-dd");
                    string strPath = @"c:\scalelog\" + "log" + strLogName + ".txt";
                    if (System.IO.File.Exists(strPath))
                        System.IO.File.Delete(strPath);
                }
                finally
                {
                    //foreach (string s in strObjArray)
                    //{
                    //    System.Threading.Monitor.Exit(s);
                    //}
                }
                //NetState();//测试网络状态564ngb 1383


                if (checkHNUP.Checked)//淮南煤质不需要更新s_statehis
                {

                    return;
                }
                if (dataOp.GetConnection() == null)
                    bNetState = false;

                if (!bNetState)
                {
                    int tempState = dataOp.WriteNetState(nUnitID, "正常", "上传软件正在运行中");
                    if (tempState == 0)
                        bNetState = true;
                }
            }

        }

    }
}