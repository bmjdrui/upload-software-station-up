using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;
using System.IO;
namespace StationUp
{
    /// <summary>
    /// 合肥海明核子秤类
    /// </summary>
    class DataHMClass : DataOption
    {

        #region 数据初始化
        string strAddress = null, strPass = null;
        string[] strDevName = new string[20];//秤名
        int nDevCount = 0;//秤总数
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        /// <summary>
        /// 合肥海明核子秤数据初始化
        /// </summary>
        /// <param name="nUnitID">单位ID</param>
        /// <param name="strName">单位名称</param>
        /// <param name="strCheckDev">备用</param>
        /// <returns>单位ID</returns>
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int hzcDevScale = 0, nTest = 0;
            string hzcDevName = null, strComm = null;
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);//以UnitID为准
            if (nUnitID < 0)
            {
                this.WriteLog("合肥海明核子秤GetInitPara函数错误号为：101");
                return -1;
            }
            try
            {
                string [] strHMInfo= GetHMDynData(AddressMachine, PassMachine);
                if (strHMInfo == null)
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                for (int i = 0; i<nDevCount;i++ )
                {

                    hzcDevName = strDevName[i+1];//秤名
                    hzcDevScale = i+1;//秤号
                    hzcDevScale = nUnitID * 100 + hzcDevScale;//计算编号
                    nTest = SelectData(2, nUnitID, strName, hzcDevScale, hzcDevName);//第三个参数暂时不用 通过秤名字和矿区ID号判断
                    if (nTest == -1)//
                    {
                        sqlComm.Dispose();
                        if (sqlComm.Connection.State == ConnectionState.Open)
                            sqlComm.Connection.Close();
                        sqlComm.Connection.Dispose();
                        return -3;
                    }
                    if (nTest == 1)//有数据
                    {
                        strComm = "update  s_device set DeviceName='" + hzcDevName + "' where DeviceID=" + hzcDevScale + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }

                    if (nTest == 0)//无数据
                    {

                        strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + hzcDevScale + ",'" + hzcDevName + "'," + nUnitID + ",1,1,1,2)";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }
                    //***************************************
                    
                }
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DataHMClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
        #endregion

        #region 从access中获取核子秤的动态与班产量信息
        /// <summary>
        /// 得到合肥海明核子秤数据库的连接
        /// </summary>
        /// <param name="strPass">核子秤密码 默认：qqq</param>
        /// <returns></returns>
        private OleDbConnection GetHMConnection(string strAddress, string strPass)
        {
            string strPath = "\\\\";
            if (strAddress != "127.0.0.1")
            {
                strPath = strAddress;
                strPath += "\\mh.mdb";
            }
            else
            {
                strPath = @"D:\HDS产量监控系统软件\data\mh.mdb";
            }
            string strHzcConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";
            try
            {
                OleDbConnection hzcConn = new OleDbConnection(strHzcConn);
                if (hzcConn.State ==ConnectionState.Closed)
                    hzcConn.Open();
                return hzcConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHMConnection 函数 " + ex.ToString());
                return null;
            }
        }

        
        /// <summary>
        /// 得到合肥海明核子秤的实时数据
        /// </summary>
        /// <param name="strStation">中心站</param>
        /// <param name="startTime">密码 默认：qqq</param>
        /// <returns></returns>
        private string[] GetHMDynData(string strStation,string strPass)
        {
            string strLine;
            string[] strArray = null;
            char[] charArray = new Char[] { '\t' };
            string strPath = null;
            if (strStation != "127.0.0.1")
            {
                strPath = strAddress;
                strPath += "\\rt.txt";
            }
            else
            {
                strPath = @"D:\HDS产量监控系统软件\data\rt.txt";
            }

          
            this.WriteLog("GetHMDynData 函数" + strPath);
            
            string[] strTemp=new string[200];
            int nCount = 0;
            try
            {
                int nCurNode = 0;
                FileStream aFile = new FileStream(strPath, FileMode.Open);
                if (aFile == null)
                    return null;
                StreamReader sr = new StreamReader(aFile,Encoding.GetEncoding("gb2312"));

                strLine = sr.ReadLine();
                nDevCount = 0;
                while (strLine != null)
                {
                    strArray = null;
                    strArray = strLine.Split(charArray);
                    if ((!this.IsNumber(strArray[0].Trim())) || (strArray[0]=="-"))
                    {
                        strLine = sr.ReadLine();
                        continue;
                    }
                    nCurNode = Convert.ToInt32(strArray[0]);
                    strDevName[nCurNode] = strArray[1];//保存秤名
                   // if (nCurNode == nNode)
                   //     break;
                    for (int x = 0; x <= strArray.GetUpperBound(0); x++)
                    {
                        strTemp[nCount] = strArray[x].Trim();
                        nCount++;
                    }
                    nDevCount++;//秤总数
                    strLine = sr.ReadLine();
                }
                sr.Close();
               
                return strTemp;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHMDynData 函数" + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 得到合肥海明核子秤的班产量信息
        /// </summary>
        /// <param name="strOldTime"></param>
        /// <returns></returns>
        private OleDbDataReader GetHMHzcOutput(string strOldTime, string hzcAddress, string hzcPass,string strTable)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                DateTime newDT = DateTime.Now.AddDays(1);
                string strComm = "SELECT * FROM "+strTable+" WHERE 日期 > #" + oldDT.ToString("yyyy-MM-dd") + "# order by 日期";
                //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                OleDbCommand hzcComm = new OleDbCommand(strComm, GetHMConnection(hzcAddress, hzcPass));
                OleDbDataReader hzcReader = hzcComm.ExecuteReader();
                hzcComm.Dispose();
                return hzcReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHMHzcOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 合肥海明核子秤数据写入到服务器
        /// <summary>
        /// 向服务器中写合肥海明动态数据
        /// </summary>
        /// <param name="hzcID">矿区编号</param>
        /// <returns>错误编号</returns>
        public int HMWriteDataDyn(int nUnitID, string strAddress, string hzcPass, string strCheckDev)
        {
            try
            {
                string strComm = null, hzcDevName = null, hzcState = null; ;
                int hzcDevScale = 0;
                float hzcSpeed = 0, hzcBurthen = 0, hzcFlow = 0, hzcFta = 0, hzcFma = 0,hzcFday=0, hzcSignel = 0;
                float hzcChangeValue = 0;
                hzcChangeValue = this.GetChangeValue();
                string [] strHMInfo = GetHMDynData(strAddress, hzcPass);
                OleDbDataReader hzcReader = null;
                if (strHMInfo == null)
                    return 801004;
                //得到秤总数
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                for (int i = 0; i < nDevCount;i++ )
                {
                    hzcDevScale = Convert.ToInt32(strHMInfo[i*10+0]);//秤号
                   
                    hzcDevName =strHMInfo[i * 10 + 1].ToString().Trim();//秤名
                    hzcSignel = Convert.ToSingle(strHMInfo[i * 10 + 2]);//速度信号
                    string strTemp = strHMInfo[i * 10 + 3];//速度状态
                    hzcState = strTemp;//增加核子秤状态
                    if (IsNumber(strTemp))
                        hzcSpeed = Convert.ToSingle(strTemp);
                    else
                        hzcSpeed = 0;
                    hzcBurthen = Convert.ToSingle(strHMInfo[i * 10 + 4]);
                    hzcFlow = Convert.ToSingle(strHMInfo[i * 10 + 5]);
                                     
                    hzcFta = Convert.ToSingle(strHMInfo[i * 10 + 6]);//系数
                    hzcFday = Convert.ToSingle(strHMInfo[i * 10] + 7);
                    hzcFma = Convert.ToSingle(strHMInfo[i * 10 + 8]);//系数
                    

                    //**************************更新dyn表****************************
                    hzcDevScale = nUnitID * 100 + hzcDevScale;//核子秤的编号
                    int nTest = SelectData(0, nUnitID, null, hzcDevScale, hzcDevName);//    
                    if (nTest == -1)
                        return 801005;
                    if (nTest == 1)
                        strComm = "update s_JLDyn set CurDT='" + DateTime.Now.ToString() + "',Speed=" + hzcSpeed + ",Burthen=" + hzcBurthen + ",Flow=" + hzcFlow + ",ClassOP=" + hzcFta + ",DayOP="+hzcFday+",MonthOP=" + hzcFma + ",Remark='" + hzcState + "' where DeviceID=" + hzcDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_JLDyn (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + hzcDevScale + ",'" + DateTime.Now.ToString() + "'," + hzcSignel + " ," + hzcSpeed + "," + hzcBurthen + "," + hzcFlow + ",0,0,0," + hzcFta + ","+hzcFday+"," + hzcFma + ",0,'" + hzcState + "') ";
                    this.WriteLog("HZCWriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    //*************************添加到历史表****************************************
                    strComm = "insert into s_JLHis (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + hzcDevScale + ",'" + DateTime.Now.ToString() + "'," + hzcSignel + " ," + hzcSpeed + "," + hzcBurthen + "," + hzcFlow + ",0,0,0," + hzcFta + "," + hzcFday + "," + hzcFma + ",0,'" + hzcState + "') ";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("HMWriteDataDyn 函数 " + ex.ToString());
                return 801006;
            }

        }
        /// <summary>
        /// 向服务器写合肥海明核子秤的班产量
        /// </summary>
        /// <param name="hzcID">矿区编号</param>
        /// <returns></returns>
        public int HMWriteDataOutput(int nUnitID, string strAddress, string strPass, int nHour, int nMin, string strCheckDev, System.Windows.Forms.ListBox lb)
        {
            try
            {
                string strComm = null;
                float hzcOut1 = 0, hzcOut2 = 0, hzcOut3 = 0, hzcOut4 = 0, hzcTotal = 0;
                int hzcDevScale = 0, nTestData = 0;

                string strCurDT = null;
                string[] strInfo = new string[2];
                float hzcChangeValue = 0;

                hzcChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(6);//得到时间 以及是否上传过

                bool bHzcHour = (DateTime.Now.Hour == nHour);
                bool bHzcMin = (DateTime.Now.Minute == nMin);
                //新桥的产量

                //if (bHzcHour && bHzcMin)
                //{
                this.WriteLog("HMCWriteDataOutput 函数 正在读取数据--");
                OleDbDataReader hzcReader=null;
                SqlConnection conn=null;
                SqlCommand sqlComm = null;
                conn = GetConnection();
                sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                for (int i = 0; i < nDevCount; i++)
                {
                    hzcReader = GetHMHzcOutput(strInfo[0], strAddress, strPass, strDevName[i+1]);
                   
                    while (hzcReader.Read())
                    {

                        this.WriteLog("HMCWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                        hzcDevScale = i+1;//秤号
                        
                        hzcDevScale = nUnitID * 100 + hzcDevScale;
                        strCurDT = hzcReader["日期"].ToString();
                        nTestData = this.SelectData(6, nUnitID, null, hzcDevScale, strCurDT);//监测数据库中是否有数据

                        if (nTestData == 1)
                            continue;
                        if (hzcReader["第1班"].GetType().Name == "DBNull")
                            hzcOut1 = 0;
                        else
                        {
                            hzcOut1 = Convert.ToSingle(hzcReader["第1班"]);
                        }
                        if (hzcReader["第2班"].GetType().Name == "DBNull")
                            hzcOut2 = 0;
                        else
                        {
                            hzcOut2 = Convert.ToSingle(hzcReader["第2班"]);
                        }
                        if (hzcReader["第3班"].GetType().Name == "DBNull")
                            hzcOut3 = 0;
                        else
                        {
                            hzcOut3 = Convert.ToSingle(hzcReader["第3班"]);
                        }
                       
                        hzcTotal = hzcOut1 + hzcOut2 + hzcOut3;
                        strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + hzcDevScale + ",'" + strCurDT + "'," + hzcOut1 + "," + hzcOut2 + "," + hzcOut3 + "," + hzcOut4 + "," + hzcTotal + ") ";

                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.SetBanDataInfo(0, strCurDT);//保存时间到注册表
                    }
                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                hzcReader.Close();
                hzcReader.Dispose();
                lb.Items.Add("海明皮带秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                this.WriteLog("海明皮带秤正在发送班产量数据 ");
                // }

                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("HMWriteDataOutput 函数 " + ex.ToString());
                return 801007;
            }
        }
        #endregion



    }
}
