﻿namespace StationUp
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxUser = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkAlertSound = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkDev11 = new System.Windows.Forms.CheckBox();
            this.checkDev10 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.btnHzcChange = new System.Windows.Forms.Button();
            this.checkDev9 = new System.Windows.Forms.CheckBox();
            this.checkDev8 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkDev7 = new System.Windows.Forms.CheckBox();
            this.checkDev6 = new System.Windows.Forms.CheckBox();
            this.checkDev5 = new System.Windows.Forms.CheckBox();
            this.checkDev4 = new System.Windows.Forms.CheckBox();
            this.checkDev3 = new System.Windows.Forms.CheckBox();
            this.checkDev2 = new System.Windows.Forms.CheckBox();
            this.checkDev1 = new System.Windows.Forms.CheckBox();
            this.checkDev0 = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textHZClass = new System.Windows.Forms.TextBox();
            this.textHZMin = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textHZHour = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textHZPass = new System.Windows.Forms.TextBox();
            this.textHZUser = new System.Windows.Forms.TextBox();
            this.textHZStation = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.checkHZUp = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textHZDataPass = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textDZClass = new System.Windows.Forms.TextBox();
            this.textDZMin = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textDZHour = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textDZPass = new System.Windows.Forms.TextBox();
            this.textDZUser = new System.Windows.Forms.TextBox();
            this.textDZStation = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.checkDZUp = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textDZDataPass = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.checkBoxSclVer = new System.Windows.Forms.CheckBox();
            this.btnSclChange = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textSCLClass = new System.Windows.Forms.TextBox();
            this.textSCLMin = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textSCLHour = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.textSCLPass = new System.Windows.Forms.TextBox();
            this.textSCLUser = new System.Windows.Forms.TextBox();
            this.textSCLStation = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.checkSCLUp = new System.Windows.Forms.CheckBox();
            this.label43 = new System.Windows.Forms.Label();
            this.textSCLDataPass = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textHfyFB100Class = new System.Windows.Forms.TextBox();
            this.textHfyFB100Min = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.textHfyFB100Hour = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.textHfyFB100Pass = new System.Windows.Forms.TextBox();
            this.textHfyFB100User = new System.Windows.Forms.TextBox();
            this.textHfyFB100Station = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.checkHfyFB100Up = new System.Windows.Forms.CheckBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textHfyFB100DataPass = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textHAClass = new System.Windows.Forms.TextBox();
            this.textHAMin = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textHAHour = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textHAPass = new System.Windows.Forms.TextBox();
            this.textHAUser = new System.Windows.Forms.TextBox();
            this.textHAStation = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.checkHAUp = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textHADataPass = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label102 = new System.Windows.Forms.Label();
            this.checkHCCheck0 = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textHCClass = new System.Windows.Forms.TextBox();
            this.textHCMin = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.textHCHour = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.textHCPass = new System.Windows.Forms.TextBox();
            this.textHCUser = new System.Windows.Forms.TextBox();
            this.textHCStation = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.checkHCUp = new System.Windows.Forms.CheckBox();
            this.label40 = new System.Windows.Forms.Label();
            this.textHCDataPass = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label51 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textHMClass = new System.Windows.Forms.TextBox();
            this.textHMMin = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textHMHour = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.textHMPass = new System.Windows.Forms.TextBox();
            this.textHMUser = new System.Windows.Forms.TextBox();
            this.textHMStation = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.checkHMUp = new System.Windows.Forms.CheckBox();
            this.label58 = new System.Windows.Forms.Label();
            this.textHMDataPass = new System.Windows.Forms.TextBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label59 = new System.Windows.Forms.Label();
            this.textSFYBan = new System.Windows.Forms.TextBox();
            this.textSFYMin = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.textSFYHour = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.textSHYPassWord = new System.Windows.Forms.TextBox();
            this.textSFYUser = new System.Windows.Forms.TextBox();
            this.textSFYStation = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.checkSFYUP = new System.Windows.Forms.CheckBox();
            this.label65 = new System.Windows.Forms.Label();
            this.textSFYPass = new System.Windows.Forms.TextBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.label101 = new System.Windows.Forms.Label();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.textLMQBan = new System.Windows.Forms.TextBox();
            this.textLMQMin = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.textLMQHour = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.textLMQPass = new System.Windows.Forms.TextBox();
            this.textLMQUser = new System.Windows.Forms.TextBox();
            this.textLMQStation = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.checkLMQUP = new System.Windows.Forms.CheckBox();
            this.label72 = new System.Windows.Forms.Label();
            this.textLMQDataPass = new System.Windows.Forms.TextBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.label73 = new System.Windows.Forms.Label();
            this.textQhfyBan = new System.Windows.Forms.TextBox();
            this.textQhfyMin = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.textQhfyHour = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.textQhfyPass = new System.Windows.Forms.TextBox();
            this.textQhfyUser = new System.Windows.Forms.TextBox();
            this.textQhfyStation = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.checkQhfyUP = new System.Windows.Forms.CheckBox();
            this.label79 = new System.Windows.Forms.Label();
            this.textQhfyDataPass = new System.Windows.Forms.TextBox();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this.textQsfyBan = new System.Windows.Forms.TextBox();
            this.textQsfyMin = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.textQsfyHour = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.textQsfyPass = new System.Windows.Forms.TextBox();
            this.textQsfyUser = new System.Windows.Forms.TextBox();
            this.textQsfyStation = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.checkQsfyUP = new System.Windows.Forms.CheckBox();
            this.label86 = new System.Windows.Forms.Label();
            this.textQsfyDataPass = new System.Windows.Forms.TextBox();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.label87 = new System.Windows.Forms.Label();
            this.textQjgcBan = new System.Windows.Forms.TextBox();
            this.textQjgcMin = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.textQjgcHour = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.textQjgcPass = new System.Windows.Forms.TextBox();
            this.textQjgcUser = new System.Windows.Forms.TextBox();
            this.textQjgcStation = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.checkQjgcUP = new System.Windows.Forms.CheckBox();
            this.label93 = new System.Windows.Forms.Label();
            this.textQjgcDataPass = new System.Windows.Forms.TextBox();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.textKFwhUser = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.label94 = new System.Windows.Forms.Label();
            this.textKFwhBan = new System.Windows.Forms.TextBox();
            this.textKFwhMin = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.textKFwhHour = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.textKFwhPass = new System.Windows.Forms.TextBox();
            this.textKFwhStation = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.checkKfwhUP = new System.Windows.Forms.CheckBox();
            this.label100 = new System.Windows.Forms.Label();
            this.textKFwhDataPass = new System.Windows.Forms.TextBox();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.label111 = new System.Windows.Forms.Label();
            this.checkCar8 = new System.Windows.Forms.CheckBox();
            this.checkCar7 = new System.Windows.Forms.CheckBox();
            this.checkCar6 = new System.Windows.Forms.CheckBox();
            this.checkCar5 = new System.Windows.Forms.CheckBox();
            this.checkCar4 = new System.Windows.Forms.CheckBox();
            this.checkCar3 = new System.Windows.Forms.CheckBox();
            this.checkCar2 = new System.Windows.Forms.CheckBox();
            this.checkCar1 = new System.Windows.Forms.CheckBox();
            this.textXJcarUser = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.label110 = new System.Windows.Forms.Label();
            this.textXJcarBan = new System.Windows.Forms.TextBox();
            this.textXJcarMin = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.textXJcarHour = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.textXJcarPass = new System.Windows.Forms.TextBox();
            this.textXJcarStation = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.checkXJcarUP = new System.Windows.Forms.CheckBox();
            this.label116 = new System.Windows.Forms.Label();
            this.textXJcarDataPass = new System.Windows.Forms.TextBox();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.textHNUser = new System.Windows.Forms.TextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.label124 = new System.Windows.Forms.Label();
            this.textHNDay = new System.Windows.Forms.TextBox();
            this.textHNMin = new System.Windows.Forms.TextBox();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.textHNHour = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.textHNPass = new System.Windows.Forms.TextBox();
            this.textHNStation = new System.Windows.Forms.TextBox();
            this.label129 = new System.Windows.Forms.Label();
            this.checkHNUP = new System.Windows.Forms.CheckBox();
            this.label130 = new System.Windows.Forms.Label();
            this.textHNDataPass = new System.Windows.Forms.TextBox();
            this.lbShowInfo = new System.Windows.Forms.ListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnModfiy = new System.Windows.Forms.Button();
            this.timerWrite = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label103 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.label109 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.label117 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label122 = new System.Windows.Forms.Label();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.label123 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxPass);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxUser);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxAddress);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 127);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "服务器设置";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "密码:";
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(91, 89);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.PasswordChar = '*';
            this.textBoxPass.Size = new System.Drawing.Size(100, 21);
            this.textBoxPass.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "用户名:";
            // 
            // textBoxUser
            // 
            this.textBoxUser.Location = new System.Drawing.Point(91, 57);
            this.textBoxUser.Name = "textBoxUser";
            this.textBoxUser.PasswordChar = '*';
            this.textBoxUser.Size = new System.Drawing.Size(100, 21);
            this.textBoxUser.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "数据库地址:";
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(91, 22);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(100, 21);
            this.textBoxAddress.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkAlertSound);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBoxID);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBoxName);
            this.groupBox2.Location = new System.Drawing.Point(226, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(158, 127);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "属性设置";
            // 
            // checkAlertSound
            // 
            this.checkAlertSound.AutoSize = true;
            this.checkAlertSound.Location = new System.Drawing.Point(13, 89);
            this.checkAlertSound.Name = "checkAlertSound";
            this.checkAlertSound.Size = new System.Drawing.Size(102, 16);
            this.checkAlertSound.TabIndex = 26;
            this.checkAlertSound.Text = "关闭声音报警 ";
            this.checkAlertSound.UseVisualStyleBackColor = true;
            this.checkAlertSound.CheckedChanged += new System.EventHandler(this.checkAlertSound_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "编号:";
            // 
            // textBoxID
            // 
            this.textBoxID.Enabled = false;
            this.textBoxID.Location = new System.Drawing.Point(48, 57);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(100, 21);
            this.textBoxID.TabIndex = 4;
            this.textBoxID.Text = "-1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "矿名:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(47, 22);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 21);
            this.textBoxName.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Location = new System.Drawing.Point(6, 20);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(355, 284);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(347, 258);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "核子秤";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkDev11);
            this.groupBox3.Controls.Add(this.checkDev10);
            this.groupBox3.Controls.Add(this.checkBox21);
            this.groupBox3.Controls.Add(this.btnHzcChange);
            this.groupBox3.Controls.Add(this.checkDev9);
            this.groupBox3.Controls.Add(this.checkDev8);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.checkDev7);
            this.groupBox3.Controls.Add(this.checkDev6);
            this.groupBox3.Controls.Add(this.checkDev5);
            this.groupBox3.Controls.Add(this.checkDev4);
            this.groupBox3.Controls.Add(this.checkDev3);
            this.groupBox3.Controls.Add(this.checkDev2);
            this.groupBox3.Controls.Add(this.checkDev1);
            this.groupBox3.Controls.Add(this.checkDev0);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textHZClass);
            this.groupBox3.Controls.Add(this.textHZMin);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textHZHour);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textHZPass);
            this.groupBox3.Controls.Add(this.textHZUser);
            this.groupBox3.Controls.Add(this.textHZStation);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.checkHZUp);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.textHZDataPass);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(338, 230);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            // 
            // checkDev11
            // 
            this.checkDev11.AutoSize = true;
            this.checkDev11.Location = new System.Drawing.Point(263, 108);
            this.checkDev11.Name = "checkDev11";
            this.checkDev11.Size = new System.Drawing.Size(48, 16);
            this.checkDev11.TabIndex = 29;
            this.checkDev11.Text = "备用";
            this.checkDev11.UseVisualStyleBackColor = true;
            // 
            // checkDev10
            // 
            this.checkDev10.AutoSize = true;
            this.checkDev10.Location = new System.Drawing.Point(263, 86);
            this.checkDev10.Name = "checkDev10";
            this.checkDev10.Size = new System.Drawing.Size(78, 16);
            this.checkDev10.TabIndex = 27;
            this.checkDev10.Text = "日产为ftd";
            this.checkDev10.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Checked = true;
            this.checkBox21.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox21.Location = new System.Drawing.Point(263, 131);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(72, 16);
            this.checkBox21.TabIndex = 28;
            this.checkBox21.Text = "混淆加密";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // btnHzcChange
            // 
            this.btnHzcChange.Location = new System.Drawing.Point(189, 133);
            this.btnHzcChange.Name = "btnHzcChange";
            this.btnHzcChange.Size = new System.Drawing.Size(23, 23);
            this.btnHzcChange.TabIndex = 27;
            this.btnHzcChange.Text = "...";
            this.btnHzcChange.UseVisualStyleBackColor = true;
            this.btnHzcChange.Click += new System.EventHandler(this.btnHzcChange_Click);
            // 
            // checkDev9
            // 
            this.checkDev9.AutoSize = true;
            this.checkDev9.Location = new System.Drawing.Point(263, 66);
            this.checkDev9.Name = "checkDev9";
            this.checkDev9.Size = new System.Drawing.Size(60, 16);
            this.checkDev9.TabIndex = 26;
            this.checkDev9.Text = "BRatio";
            this.checkDev9.UseVisualStyleBackColor = true;
            // 
            // checkDev8
            // 
            this.checkDev8.AutoSize = true;
            this.checkDev8.Location = new System.Drawing.Point(263, 44);
            this.checkDev8.Name = "checkDev8";
            this.checkDev8.Size = new System.Drawing.Size(72, 16);
            this.checkDev8.TabIndex = 25;
            this.checkDev8.Text = "DynRatio";
            this.checkDev8.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(196, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 12);
            this.label8.TabIndex = 24;
            this.label8.Text = "以下设备禁止上传(秤号)";
            // 
            // checkDev7
            // 
            this.checkDev7.AutoSize = true;
            this.checkDev7.Location = new System.Drawing.Point(227, 196);
            this.checkDev7.Name = "checkDev7";
            this.checkDev7.Size = new System.Drawing.Size(30, 16);
            this.checkDev7.TabIndex = 23;
            this.checkDev7.Text = "7";
            this.checkDev7.UseVisualStyleBackColor = true;
            // 
            // checkDev6
            // 
            this.checkDev6.AutoSize = true;
            this.checkDev6.Location = new System.Drawing.Point(227, 174);
            this.checkDev6.Name = "checkDev6";
            this.checkDev6.Size = new System.Drawing.Size(30, 16);
            this.checkDev6.TabIndex = 22;
            this.checkDev6.Text = "6";
            this.checkDev6.UseVisualStyleBackColor = true;
            // 
            // checkDev5
            // 
            this.checkDev5.AutoSize = true;
            this.checkDev5.Location = new System.Drawing.Point(227, 152);
            this.checkDev5.Name = "checkDev5";
            this.checkDev5.Size = new System.Drawing.Size(30, 16);
            this.checkDev5.TabIndex = 21;
            this.checkDev5.Text = "5";
            this.checkDev5.UseVisualStyleBackColor = true;
            // 
            // checkDev4
            // 
            this.checkDev4.AutoSize = true;
            this.checkDev4.Location = new System.Drawing.Point(227, 130);
            this.checkDev4.Name = "checkDev4";
            this.checkDev4.Size = new System.Drawing.Size(30, 16);
            this.checkDev4.TabIndex = 20;
            this.checkDev4.Text = "4";
            this.checkDev4.UseVisualStyleBackColor = true;
            // 
            // checkDev3
            // 
            this.checkDev3.AutoSize = true;
            this.checkDev3.Location = new System.Drawing.Point(227, 108);
            this.checkDev3.Name = "checkDev3";
            this.checkDev3.Size = new System.Drawing.Size(30, 16);
            this.checkDev3.TabIndex = 19;
            this.checkDev3.Text = "3";
            this.checkDev3.UseVisualStyleBackColor = true;
            // 
            // checkDev2
            // 
            this.checkDev2.AutoSize = true;
            this.checkDev2.Location = new System.Drawing.Point(227, 86);
            this.checkDev2.Name = "checkDev2";
            this.checkDev2.Size = new System.Drawing.Size(30, 16);
            this.checkDev2.TabIndex = 18;
            this.checkDev2.Text = "2";
            this.checkDev2.UseVisualStyleBackColor = true;
            // 
            // checkDev1
            // 
            this.checkDev1.AutoSize = true;
            this.checkDev1.Location = new System.Drawing.Point(227, 64);
            this.checkDev1.Name = "checkDev1";
            this.checkDev1.Size = new System.Drawing.Size(30, 16);
            this.checkDev1.TabIndex = 17;
            this.checkDev1.Text = "1";
            this.checkDev1.UseVisualStyleBackColor = true;
            // 
            // checkDev0
            // 
            this.checkDev0.AutoSize = true;
            this.checkDev0.Location = new System.Drawing.Point(227, 42);
            this.checkDev0.Name = "checkDev0";
            this.checkDev0.Size = new System.Drawing.Size(30, 16);
            this.checkDev0.TabIndex = 16;
            this.checkDev0.Text = "0";
            this.checkDev0.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 104);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 15;
            this.label13.Text = "班制";
            // 
            // textHZClass
            // 
            this.textHZClass.Location = new System.Drawing.Point(84, 100);
            this.textHZClass.Name = "textHZClass";
            this.textHZClass.Size = new System.Drawing.Size(26, 21);
            this.textHZClass.TabIndex = 14;
            this.textHZClass.Text = "3";
            // 
            // textHZMin
            // 
            this.textHZMin.Location = new System.Drawing.Point(138, 73);
            this.textHZMin.Name = "textHZMin";
            this.textHZMin.Size = new System.Drawing.Size(26, 21);
            this.textHZMin.TabIndex = 15;
            this.textHZMin.Text = "59";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(115, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 14;
            this.label14.Text = "：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 12);
            this.label15.TabIndex = 13;
            this.label15.Text = "换天点:";
            // 
            // textHZHour
            // 
            this.textHZHour.Location = new System.Drawing.Point(83, 73);
            this.textHZHour.Name = "textHZHour";
            this.textHZHour.Size = new System.Drawing.Size(26, 21);
            this.textHZHour.TabIndex = 12;
            this.textHZHour.Text = "11";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 200);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 12);
            this.label16.TabIndex = 11;
            this.label16.Text = "密码:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 168);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 12);
            this.label17.TabIndex = 9;
            this.label17.Text = "用户名:";
            // 
            // textHZPass
            // 
            this.textHZPass.Location = new System.Drawing.Point(82, 197);
            this.textHZPass.Name = "textHZPass";
            this.textHZPass.PasswordChar = '*';
            this.textHZPass.Size = new System.Drawing.Size(73, 21);
            this.textHZPass.TabIndex = 10;
            this.textHZPass.Text = "scale";
            // 
            // textHZUser
            // 
            this.textHZUser.Location = new System.Drawing.Point(83, 165);
            this.textHZUser.Name = "textHZUser";
            this.textHZUser.Size = new System.Drawing.Size(74, 21);
            this.textHZUser.TabIndex = 10;
            this.textHZUser.Text = "scale";
            // 
            // textHZStation
            // 
            this.textHZStation.Location = new System.Drawing.Point(83, 134);
            this.textHZStation.Name = "textHZStation";
            this.textHZStation.Size = new System.Drawing.Size(100, 21);
            this.textHZStation.TabIndex = 9;
            this.textHZStation.Text = "127.0.0.1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 137);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 12);
            this.label18.TabIndex = 8;
            this.label18.Text = "中心站:";
            // 
            // checkHZUp
            // 
            this.checkHZUp.AutoSize = true;
            this.checkHZUp.Location = new System.Drawing.Point(12, 20);
            this.checkHZUp.Name = "checkHZUp";
            this.checkHZUp.Size = new System.Drawing.Size(72, 16);
            this.checkHZUp.TabIndex = 6;
            this.checkHZUp.Text = "是否上传";
            this.checkHZUp.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 12);
            this.label19.TabIndex = 5;
            this.label19.Text = "数据库密码:";
            // 
            // textHZDataPass
            // 
            this.textHZDataPass.Location = new System.Drawing.Point(83, 42);
            this.textHZDataPass.Name = "textHZDataPass";
            this.textHZDataPass.PasswordChar = '*';
            this.textHZDataPass.Size = new System.Drawing.Size(100, 21);
            this.textHZDataPass.TabIndex = 4;
            this.textHZDataPass.Text = "scale";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(347, 258);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "KJD-N电子秤";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.textDZClass);
            this.groupBox4.Controls.Add(this.textDZMin);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.textDZHour);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.textDZPass);
            this.groupBox4.Controls.Add(this.textDZUser);
            this.groupBox4.Controls.Add(this.textDZStation);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.checkDZUp);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.textDZDataPass);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(283, 226);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 104);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 12);
            this.label20.TabIndex = 15;
            this.label20.Text = "班制";
            // 
            // textDZClass
            // 
            this.textDZClass.Location = new System.Drawing.Point(84, 100);
            this.textDZClass.Name = "textDZClass";
            this.textDZClass.Size = new System.Drawing.Size(26, 21);
            this.textDZClass.TabIndex = 14;
            this.textDZClass.Text = "3";
            // 
            // textDZMin
            // 
            this.textDZMin.Location = new System.Drawing.Point(138, 73);
            this.textDZMin.Name = "textDZMin";
            this.textDZMin.Size = new System.Drawing.Size(26, 21);
            this.textDZMin.TabIndex = 15;
            this.textDZMin.Text = "59";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(115, 78);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 14;
            this.label21.Text = "：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 77);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 12);
            this.label22.TabIndex = 13;
            this.label22.Text = "换天点:";
            // 
            // textDZHour
            // 
            this.textDZHour.Location = new System.Drawing.Point(83, 73);
            this.textDZHour.Name = "textDZHour";
            this.textDZHour.Size = new System.Drawing.Size(26, 21);
            this.textDZHour.TabIndex = 12;
            this.textDZHour.Text = "11";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 200);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 12);
            this.label23.TabIndex = 11;
            this.label23.Text = "密码:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 168);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 12);
            this.label24.TabIndex = 9;
            this.label24.Text = "用户名:";
            // 
            // textDZPass
            // 
            this.textDZPass.Location = new System.Drawing.Point(82, 197);
            this.textDZPass.Name = "textDZPass";
            this.textDZPass.PasswordChar = '*';
            this.textDZPass.Size = new System.Drawing.Size(73, 21);
            this.textDZPass.TabIndex = 10;
            this.textDZPass.Text = "scale";
            // 
            // textDZUser
            // 
            this.textDZUser.Location = new System.Drawing.Point(83, 165);
            this.textDZUser.Name = "textDZUser";
            this.textDZUser.Size = new System.Drawing.Size(74, 21);
            this.textDZUser.TabIndex = 10;
            this.textDZUser.Text = "scale";
            // 
            // textDZStation
            // 
            this.textDZStation.Location = new System.Drawing.Point(83, 134);
            this.textDZStation.Name = "textDZStation";
            this.textDZStation.Size = new System.Drawing.Size(100, 21);
            this.textDZStation.TabIndex = 9;
            this.textDZStation.Text = "127.0.0.1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(11, 137);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 12);
            this.label25.TabIndex = 8;
            this.label25.Text = "中心站:";
            // 
            // checkDZUp
            // 
            this.checkDZUp.AutoSize = true;
            this.checkDZUp.Location = new System.Drawing.Point(12, 20);
            this.checkDZUp.Name = "checkDZUp";
            this.checkDZUp.Size = new System.Drawing.Size(72, 16);
            this.checkDZUp.TabIndex = 6;
            this.checkDZUp.Text = "是否上传";
            this.checkDZUp.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(11, 46);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 12);
            this.label26.TabIndex = 5;
            this.label26.Text = "数据库密码:";
            // 
            // textDZDataPass
            // 
            this.textDZDataPass.Location = new System.Drawing.Point(83, 42);
            this.textDZDataPass.Name = "textDZDataPass";
            this.textDZDataPass.PasswordChar = '*';
            this.textDZDataPass.Size = new System.Drawing.Size(100, 21);
            this.textDZDataPass.TabIndex = 4;
            this.textDZDataPass.Text = "scale";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox9);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(347, 258);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "SCL监控平台";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.checkBoxSclVer);
            this.groupBox9.Controls.Add(this.btnSclChange);
            this.groupBox9.Controls.Add(this.label9);
            this.groupBox9.Controls.Add(this.textSCLClass);
            this.groupBox9.Controls.Add(this.textSCLMin);
            this.groupBox9.Controls.Add(this.label10);
            this.groupBox9.Controls.Add(this.label11);
            this.groupBox9.Controls.Add(this.textSCLHour);
            this.groupBox9.Controls.Add(this.label12);
            this.groupBox9.Controls.Add(this.label41);
            this.groupBox9.Controls.Add(this.textSCLPass);
            this.groupBox9.Controls.Add(this.textSCLUser);
            this.groupBox9.Controls.Add(this.textSCLStation);
            this.groupBox9.Controls.Add(this.label42);
            this.groupBox9.Controls.Add(this.checkSCLUp);
            this.groupBox9.Controls.Add(this.label43);
            this.groupBox9.Controls.Add(this.textSCLDataPass);
            this.groupBox9.Location = new System.Drawing.Point(4, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(340, 253);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            // 
            // checkBoxSclVer
            // 
            this.checkBoxSclVer.AutoSize = true;
            this.checkBoxSclVer.Location = new System.Drawing.Point(237, 20);
            this.checkBoxSclVer.Name = "checkBoxSclVer";
            this.checkBoxSclVer.Size = new System.Drawing.Size(72, 16);
            this.checkBoxSclVer.TabIndex = 29;
            this.checkBoxSclVer.Text = "早期版本";
            this.checkBoxSclVer.UseVisualStyleBackColor = true;
            // 
            // btnSclChange
            // 
            this.btnSclChange.Location = new System.Drawing.Point(189, 132);
            this.btnSclChange.Name = "btnSclChange";
            this.btnSclChange.Size = new System.Drawing.Size(23, 23);
            this.btnSclChange.TabIndex = 28;
            this.btnSclChange.Text = "...";
            this.btnSclChange.UseVisualStyleBackColor = true;
            this.btnSclChange.Click += new System.EventHandler(this.btnSclChange_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 15;
            this.label9.Text = "班制";
            // 
            // textSCLClass
            // 
            this.textSCLClass.Location = new System.Drawing.Point(84, 100);
            this.textSCLClass.Name = "textSCLClass";
            this.textSCLClass.Size = new System.Drawing.Size(26, 21);
            this.textSCLClass.TabIndex = 14;
            this.textSCLClass.Text = "3";
            // 
            // textSCLMin
            // 
            this.textSCLMin.Location = new System.Drawing.Point(138, 73);
            this.textSCLMin.Name = "textSCLMin";
            this.textSCLMin.Size = new System.Drawing.Size(26, 21);
            this.textSCLMin.TabIndex = 15;
            this.textSCLMin.Text = "59";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(115, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 77);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 13;
            this.label11.Text = "换天点:";
            // 
            // textSCLHour
            // 
            this.textSCLHour.Location = new System.Drawing.Point(83, 73);
            this.textSCLHour.Name = "textSCLHour";
            this.textSCLHour.Size = new System.Drawing.Size(26, 21);
            this.textSCLHour.TabIndex = 12;
            this.textSCLHour.Text = "11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 200);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 12);
            this.label12.TabIndex = 11;
            this.label12.Text = "密码:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(12, 168);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(47, 12);
            this.label41.TabIndex = 9;
            this.label41.Text = "用户名:";
            // 
            // textSCLPass
            // 
            this.textSCLPass.Location = new System.Drawing.Point(82, 197);
            this.textSCLPass.Name = "textSCLPass";
            this.textSCLPass.PasswordChar = '*';
            this.textSCLPass.Size = new System.Drawing.Size(73, 21);
            this.textSCLPass.TabIndex = 10;
            this.textSCLPass.Text = "scale";
            // 
            // textSCLUser
            // 
            this.textSCLUser.Location = new System.Drawing.Point(83, 165);
            this.textSCLUser.Name = "textSCLUser";
            this.textSCLUser.Size = new System.Drawing.Size(74, 21);
            this.textSCLUser.TabIndex = 10;
            this.textSCLUser.Text = "scale";
            // 
            // textSCLStation
            // 
            this.textSCLStation.Location = new System.Drawing.Point(83, 134);
            this.textSCLStation.Name = "textSCLStation";
            this.textSCLStation.Size = new System.Drawing.Size(100, 21);
            this.textSCLStation.TabIndex = 9;
            this.textSCLStation.Text = "127.0.0.1";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(11, 137);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(47, 12);
            this.label42.TabIndex = 8;
            this.label42.Text = "中心站:";
            // 
            // checkSCLUp
            // 
            this.checkSCLUp.AutoSize = true;
            this.checkSCLUp.Location = new System.Drawing.Point(12, 20);
            this.checkSCLUp.Name = "checkSCLUp";
            this.checkSCLUp.Size = new System.Drawing.Size(72, 16);
            this.checkSCLUp.TabIndex = 6;
            this.checkSCLUp.Text = "是否上传";
            this.checkSCLUp.UseVisualStyleBackColor = true;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(11, 46);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(71, 12);
            this.label43.TabIndex = 5;
            this.label43.Text = "数据库密码:";
            // 
            // textSCLDataPass
            // 
            this.textSCLDataPass.Location = new System.Drawing.Point(83, 42);
            this.textSCLDataPass.Name = "textSCLDataPass";
            this.textSCLDataPass.PasswordChar = '*';
            this.textSCLDataPass.Size = new System.Drawing.Size(100, 21);
            this.textSCLDataPass.TabIndex = 4;
            this.textSCLDataPass.Text = "scale";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox10);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(347, 258);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "防爆灰分仪";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label44);
            this.groupBox10.Controls.Add(this.textHfyFB100Class);
            this.groupBox10.Controls.Add(this.textHfyFB100Min);
            this.groupBox10.Controls.Add(this.label45);
            this.groupBox10.Controls.Add(this.label46);
            this.groupBox10.Controls.Add(this.textHfyFB100Hour);
            this.groupBox10.Controls.Add(this.label47);
            this.groupBox10.Controls.Add(this.label48);
            this.groupBox10.Controls.Add(this.textHfyFB100Pass);
            this.groupBox10.Controls.Add(this.textHfyFB100User);
            this.groupBox10.Controls.Add(this.textHfyFB100Station);
            this.groupBox10.Controls.Add(this.label49);
            this.groupBox10.Controls.Add(this.checkHfyFB100Up);
            this.groupBox10.Controls.Add(this.label50);
            this.groupBox10.Controls.Add(this.textHfyFB100DataPass);
            this.groupBox10.Location = new System.Drawing.Point(4, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(340, 253);
            this.groupBox10.TabIndex = 8;
            this.groupBox10.TabStop = false;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(12, 104);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(29, 12);
            this.label44.TabIndex = 15;
            this.label44.Text = "班制";
            // 
            // textHfyFB100Class
            // 
            this.textHfyFB100Class.Location = new System.Drawing.Point(84, 100);
            this.textHfyFB100Class.Name = "textHfyFB100Class";
            this.textHfyFB100Class.Size = new System.Drawing.Size(26, 21);
            this.textHfyFB100Class.TabIndex = 14;
            this.textHfyFB100Class.Text = "3";
            // 
            // textHfyFB100Min
            // 
            this.textHfyFB100Min.Location = new System.Drawing.Point(138, 73);
            this.textHfyFB100Min.Name = "textHfyFB100Min";
            this.textHfyFB100Min.Size = new System.Drawing.Size(26, 21);
            this.textHfyFB100Min.TabIndex = 15;
            this.textHfyFB100Min.Text = "59";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(115, 78);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(17, 12);
            this.label45.TabIndex = 14;
            this.label45.Text = "：";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(11, 77);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(47, 12);
            this.label46.TabIndex = 13;
            this.label46.Text = "换天点:";
            // 
            // textHfyFB100Hour
            // 
            this.textHfyFB100Hour.Location = new System.Drawing.Point(83, 73);
            this.textHfyFB100Hour.Name = "textHfyFB100Hour";
            this.textHfyFB100Hour.Size = new System.Drawing.Size(26, 21);
            this.textHfyFB100Hour.TabIndex = 12;
            this.textHfyFB100Hour.Text = "11";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(12, 200);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(35, 12);
            this.label47.TabIndex = 11;
            this.label47.Text = "密码:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 168);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(47, 12);
            this.label48.TabIndex = 9;
            this.label48.Text = "用户名:";
            // 
            // textHfyFB100Pass
            // 
            this.textHfyFB100Pass.Location = new System.Drawing.Point(82, 197);
            this.textHfyFB100Pass.Name = "textHfyFB100Pass";
            this.textHfyFB100Pass.PasswordChar = '*';
            this.textHfyFB100Pass.Size = new System.Drawing.Size(73, 21);
            this.textHfyFB100Pass.TabIndex = 10;
            this.textHfyFB100Pass.Text = "scale";
            // 
            // textHfyFB100User
            // 
            this.textHfyFB100User.Location = new System.Drawing.Point(83, 165);
            this.textHfyFB100User.Name = "textHfyFB100User";
            this.textHfyFB100User.Size = new System.Drawing.Size(74, 21);
            this.textHfyFB100User.TabIndex = 10;
            this.textHfyFB100User.Text = "scale";
            // 
            // textHfyFB100Station
            // 
            this.textHfyFB100Station.Location = new System.Drawing.Point(83, 134);
            this.textHfyFB100Station.Name = "textHfyFB100Station";
            this.textHfyFB100Station.Size = new System.Drawing.Size(100, 21);
            this.textHfyFB100Station.TabIndex = 9;
            this.textHfyFB100Station.Text = "127.0.0.1";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 137);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(47, 12);
            this.label49.TabIndex = 8;
            this.label49.Text = "中心站:";
            // 
            // checkHfyFB100Up
            // 
            this.checkHfyFB100Up.AutoSize = true;
            this.checkHfyFB100Up.Location = new System.Drawing.Point(12, 20);
            this.checkHfyFB100Up.Name = "checkHfyFB100Up";
            this.checkHfyFB100Up.Size = new System.Drawing.Size(72, 16);
            this.checkHfyFB100Up.TabIndex = 6;
            this.checkHfyFB100Up.Text = "是否上传";
            this.checkHfyFB100Up.UseVisualStyleBackColor = true;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(11, 46);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(71, 12);
            this.label50.TabIndex = 5;
            this.label50.Text = "数据库密码:";
            // 
            // textHfyFB100DataPass
            // 
            this.textHfyFB100DataPass.Location = new System.Drawing.Point(83, 42);
            this.textHfyFB100DataPass.Name = "textHfyFB100DataPass";
            this.textHfyFB100DataPass.PasswordChar = '*';
            this.textHfyFB100DataPass.Size = new System.Drawing.Size(100, 21);
            this.textHfyFB100DataPass.TabIndex = 4;
            this.textHfyFB100DataPass.Text = "scale";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(347, 258);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "SCL-2000A";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.textHAClass);
            this.groupBox5.Controls.Add(this.textHAMin);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.textHAHour);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.textHAPass);
            this.groupBox5.Controls.Add(this.textHAUser);
            this.groupBox5.Controls.Add(this.textHAStation);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.checkHAUp);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.textHADataPass);
            this.groupBox5.Location = new System.Drawing.Point(3, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(288, 226);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 104);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 12);
            this.label27.TabIndex = 15;
            this.label27.Text = "班制";
            // 
            // textHAClass
            // 
            this.textHAClass.Location = new System.Drawing.Point(84, 100);
            this.textHAClass.Name = "textHAClass";
            this.textHAClass.Size = new System.Drawing.Size(26, 21);
            this.textHAClass.TabIndex = 14;
            this.textHAClass.Text = "3";
            // 
            // textHAMin
            // 
            this.textHAMin.Location = new System.Drawing.Point(138, 73);
            this.textHAMin.Name = "textHAMin";
            this.textHAMin.Size = new System.Drawing.Size(26, 21);
            this.textHAMin.TabIndex = 15;
            this.textHAMin.Text = "59";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(115, 78);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 12);
            this.label28.TabIndex = 14;
            this.label28.Text = "：";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(11, 77);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 12);
            this.label29.TabIndex = 13;
            this.label29.Text = "换天点:";
            // 
            // textHAHour
            // 
            this.textHAHour.Location = new System.Drawing.Point(83, 73);
            this.textHAHour.Name = "textHAHour";
            this.textHAHour.Size = new System.Drawing.Size(26, 21);
            this.textHAHour.TabIndex = 12;
            this.textHAHour.Text = "11";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 200);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 12);
            this.label30.TabIndex = 11;
            this.label30.Text = "密码:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 168);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(47, 12);
            this.label31.TabIndex = 9;
            this.label31.Text = "用户名:";
            // 
            // textHAPass
            // 
            this.textHAPass.Location = new System.Drawing.Point(82, 197);
            this.textHAPass.Name = "textHAPass";
            this.textHAPass.PasswordChar = '*';
            this.textHAPass.Size = new System.Drawing.Size(73, 21);
            this.textHAPass.TabIndex = 10;
            this.textHAPass.Text = "scale";
            // 
            // textHAUser
            // 
            this.textHAUser.Location = new System.Drawing.Point(83, 165);
            this.textHAUser.Name = "textHAUser";
            this.textHAUser.Size = new System.Drawing.Size(74, 21);
            this.textHAUser.TabIndex = 10;
            this.textHAUser.Text = "scale";
            // 
            // textHAStation
            // 
            this.textHAStation.Location = new System.Drawing.Point(83, 134);
            this.textHAStation.Name = "textHAStation";
            this.textHAStation.Size = new System.Drawing.Size(100, 21);
            this.textHAStation.TabIndex = 9;
            this.textHAStation.Text = "127.0.0.1";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(11, 137);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 12);
            this.label32.TabIndex = 8;
            this.label32.Text = "中心站:";
            // 
            // checkHAUp
            // 
            this.checkHAUp.AutoSize = true;
            this.checkHAUp.Location = new System.Drawing.Point(12, 20);
            this.checkHAUp.Name = "checkHAUp";
            this.checkHAUp.Size = new System.Drawing.Size(72, 16);
            this.checkHAUp.TabIndex = 6;
            this.checkHAUp.Text = "是否上传";
            this.checkHAUp.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(11, 46);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(71, 12);
            this.label33.TabIndex = 5;
            this.label33.Text = "数据库密码:";
            // 
            // textHADataPass
            // 
            this.textHADataPass.Location = new System.Drawing.Point(83, 42);
            this.textHADataPass.Name = "textHADataPass";
            this.textHADataPass.PasswordChar = '*';
            this.textHADataPass.Size = new System.Drawing.Size(100, 21);
            this.textHADataPass.TabIndex = 4;
            this.textHADataPass.Text = "scale";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(347, 258);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "SCL-2000C";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label102);
            this.groupBox7.Controls.Add(this.checkHCCheck0);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.textHCClass);
            this.groupBox7.Controls.Add(this.textHCMin);
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Controls.Add(this.label36);
            this.groupBox7.Controls.Add(this.textHCHour);
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.label38);
            this.groupBox7.Controls.Add(this.textHCPass);
            this.groupBox7.Controls.Add(this.textHCUser);
            this.groupBox7.Controls.Add(this.textHCStation);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Controls.Add(this.checkHCUp);
            this.groupBox7.Controls.Add(this.label40);
            this.groupBox7.Controls.Add(this.textHCDataPass);
            this.groupBox7.Location = new System.Drawing.Point(3, 5);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(301, 226);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(82, 21);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(89, 12);
            this.label102.TabIndex = 27;
            this.label102.Text = "(默认为25字段)";
            // 
            // checkHCCheck0
            // 
            this.checkHCCheck0.AutoSize = true;
            this.checkHCCheck0.Location = new System.Drawing.Point(201, 20);
            this.checkHCCheck0.Name = "checkHCCheck0";
            this.checkHCCheck0.Size = new System.Drawing.Size(84, 16);
            this.checkHCCheck0.TabIndex = 26;
            this.checkHCCheck0.Text = "28字段版本";
            this.checkHCCheck0.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(12, 104);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 15;
            this.label34.Text = "班制";
            // 
            // textHCClass
            // 
            this.textHCClass.Location = new System.Drawing.Point(84, 100);
            this.textHCClass.Name = "textHCClass";
            this.textHCClass.Size = new System.Drawing.Size(26, 21);
            this.textHCClass.TabIndex = 14;
            this.textHCClass.Text = "3";
            // 
            // textHCMin
            // 
            this.textHCMin.Location = new System.Drawing.Point(138, 73);
            this.textHCMin.Name = "textHCMin";
            this.textHCMin.Size = new System.Drawing.Size(26, 21);
            this.textHCMin.TabIndex = 15;
            this.textHCMin.Text = "59";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(115, 78);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(17, 12);
            this.label35.TabIndex = 14;
            this.label35.Text = "：";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(11, 77);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 12);
            this.label36.TabIndex = 13;
            this.label36.Text = "换天点:";
            // 
            // textHCHour
            // 
            this.textHCHour.Location = new System.Drawing.Point(83, 73);
            this.textHCHour.Name = "textHCHour";
            this.textHCHour.Size = new System.Drawing.Size(26, 21);
            this.textHCHour.TabIndex = 12;
            this.textHCHour.Text = "11";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(12, 200);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 12);
            this.label37.TabIndex = 11;
            this.label37.Text = "密码:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(12, 168);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(47, 12);
            this.label38.TabIndex = 9;
            this.label38.Text = "用户名:";
            // 
            // textHCPass
            // 
            this.textHCPass.Location = new System.Drawing.Point(82, 197);
            this.textHCPass.Name = "textHCPass";
            this.textHCPass.PasswordChar = '*';
            this.textHCPass.Size = new System.Drawing.Size(73, 21);
            this.textHCPass.TabIndex = 10;
            this.textHCPass.Text = "scale";
            // 
            // textHCUser
            // 
            this.textHCUser.Location = new System.Drawing.Point(83, 165);
            this.textHCUser.Name = "textHCUser";
            this.textHCUser.Size = new System.Drawing.Size(74, 21);
            this.textHCUser.TabIndex = 10;
            this.textHCUser.Text = "scale";
            // 
            // textHCStation
            // 
            this.textHCStation.Location = new System.Drawing.Point(83, 134);
            this.textHCStation.Name = "textHCStation";
            this.textHCStation.Size = new System.Drawing.Size(100, 21);
            this.textHCStation.TabIndex = 9;
            this.textHCStation.Text = "d:\\ash";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(11, 137);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(47, 12);
            this.label39.TabIndex = 8;
            this.label39.Text = "中心站:";
            // 
            // checkHCUp
            // 
            this.checkHCUp.AutoSize = true;
            this.checkHCUp.Location = new System.Drawing.Point(12, 20);
            this.checkHCUp.Name = "checkHCUp";
            this.checkHCUp.Size = new System.Drawing.Size(72, 16);
            this.checkHCUp.TabIndex = 6;
            this.checkHCUp.Text = "是否上传";
            this.checkHCUp.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(11, 46);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(71, 12);
            this.label40.TabIndex = 5;
            this.label40.Text = "数据库密码:";
            // 
            // textHCDataPass
            // 
            this.textHCDataPass.Location = new System.Drawing.Point(83, 42);
            this.textHCDataPass.Name = "textHCDataPass";
            this.textHCDataPass.PasswordChar = '*';
            this.textHCDataPass.Size = new System.Drawing.Size(100, 21);
            this.textHCDataPass.TabIndex = 4;
            this.textHCDataPass.Text = "scale";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox11);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(347, 258);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "合肥海明核子秤";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button1);
            this.groupBox11.Controls.Add(this.checkBox1);
            this.groupBox11.Controls.Add(this.checkBox2);
            this.groupBox11.Controls.Add(this.label51);
            this.groupBox11.Controls.Add(this.checkBox3);
            this.groupBox11.Controls.Add(this.checkBox4);
            this.groupBox11.Controls.Add(this.checkBox5);
            this.groupBox11.Controls.Add(this.checkBox6);
            this.groupBox11.Controls.Add(this.checkBox7);
            this.groupBox11.Controls.Add(this.checkBox8);
            this.groupBox11.Controls.Add(this.checkBox9);
            this.groupBox11.Controls.Add(this.checkBox10);
            this.groupBox11.Controls.Add(this.label52);
            this.groupBox11.Controls.Add(this.textHMClass);
            this.groupBox11.Controls.Add(this.textHMMin);
            this.groupBox11.Controls.Add(this.label53);
            this.groupBox11.Controls.Add(this.label54);
            this.groupBox11.Controls.Add(this.textHMHour);
            this.groupBox11.Controls.Add(this.label55);
            this.groupBox11.Controls.Add(this.label56);
            this.groupBox11.Controls.Add(this.textHMPass);
            this.groupBox11.Controls.Add(this.textHMUser);
            this.groupBox11.Controls.Add(this.textHMStation);
            this.groupBox11.Controls.Add(this.label57);
            this.groupBox11.Controls.Add(this.checkHMUp);
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this.textHMDataPass);
            this.groupBox11.Location = new System.Drawing.Point(4, 14);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(338, 230);
            this.groupBox11.TabIndex = 8;
            this.groupBox11.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(189, 133);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(23, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(263, 66);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 16);
            this.checkBox1.TabIndex = 26;
            this.checkBox1.Text = "班产系数";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(263, 44);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(72, 16);
            this.checkBox2.TabIndex = 25;
            this.checkBox2.Text = "动态系数";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(196, 24);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(137, 12);
            this.label51.TabIndex = 24;
            this.label51.Text = "以下设备禁止上传(秤号)";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(227, 196);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(30, 16);
            this.checkBox3.TabIndex = 23;
            this.checkBox3.Text = "7";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(227, 174);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(30, 16);
            this.checkBox4.TabIndex = 22;
            this.checkBox4.Text = "6";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(227, 152);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(30, 16);
            this.checkBox5.TabIndex = 21;
            this.checkBox5.Text = "5";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(227, 130);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(30, 16);
            this.checkBox6.TabIndex = 20;
            this.checkBox6.Text = "4";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(227, 108);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(30, 16);
            this.checkBox7.TabIndex = 19;
            this.checkBox7.Text = "3";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(227, 86);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(30, 16);
            this.checkBox8.TabIndex = 18;
            this.checkBox8.Text = "2";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(227, 64);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(30, 16);
            this.checkBox9.TabIndex = 17;
            this.checkBox9.Text = "1";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(227, 42);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(30, 16);
            this.checkBox10.TabIndex = 16;
            this.checkBox10.Text = "0";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(12, 104);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(29, 12);
            this.label52.TabIndex = 15;
            this.label52.Text = "班制";
            // 
            // textHMClass
            // 
            this.textHMClass.Location = new System.Drawing.Point(84, 100);
            this.textHMClass.Name = "textHMClass";
            this.textHMClass.Size = new System.Drawing.Size(26, 21);
            this.textHMClass.TabIndex = 14;
            this.textHMClass.Text = "3";
            // 
            // textHMMin
            // 
            this.textHMMin.Location = new System.Drawing.Point(138, 73);
            this.textHMMin.Name = "textHMMin";
            this.textHMMin.Size = new System.Drawing.Size(26, 21);
            this.textHMMin.TabIndex = 15;
            this.textHMMin.Text = "59";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(115, 78);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(17, 12);
            this.label53.TabIndex = 14;
            this.label53.Text = "：";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(11, 77);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(47, 12);
            this.label54.TabIndex = 13;
            this.label54.Text = "换天点:";
            // 
            // textHMHour
            // 
            this.textHMHour.Location = new System.Drawing.Point(83, 73);
            this.textHMHour.Name = "textHMHour";
            this.textHMHour.Size = new System.Drawing.Size(26, 21);
            this.textHMHour.TabIndex = 12;
            this.textHMHour.Text = "11";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(12, 200);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(35, 12);
            this.label55.TabIndex = 11;
            this.label55.Text = "密码:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(12, 168);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(47, 12);
            this.label56.TabIndex = 9;
            this.label56.Text = "用户名:";
            // 
            // textHMPass
            // 
            this.textHMPass.Location = new System.Drawing.Point(82, 197);
            this.textHMPass.Name = "textHMPass";
            this.textHMPass.PasswordChar = '*';
            this.textHMPass.Size = new System.Drawing.Size(73, 21);
            this.textHMPass.TabIndex = 10;
            this.textHMPass.Text = "scale";
            // 
            // textHMUser
            // 
            this.textHMUser.Location = new System.Drawing.Point(83, 165);
            this.textHMUser.Name = "textHMUser";
            this.textHMUser.Size = new System.Drawing.Size(74, 21);
            this.textHMUser.TabIndex = 10;
            this.textHMUser.Text = "scale";
            // 
            // textHMStation
            // 
            this.textHMStation.Location = new System.Drawing.Point(83, 134);
            this.textHMStation.Name = "textHMStation";
            this.textHMStation.Size = new System.Drawing.Size(100, 21);
            this.textHMStation.TabIndex = 9;
            this.textHMStation.Text = "127.0.0.1";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(11, 137);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(47, 12);
            this.label57.TabIndex = 8;
            this.label57.Text = "中心站:";
            // 
            // checkHMUp
            // 
            this.checkHMUp.AutoSize = true;
            this.checkHMUp.Location = new System.Drawing.Point(12, 20);
            this.checkHMUp.Name = "checkHMUp";
            this.checkHMUp.Size = new System.Drawing.Size(72, 16);
            this.checkHMUp.TabIndex = 6;
            this.checkHMUp.Text = "是否上传";
            this.checkHMUp.UseVisualStyleBackColor = true;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(11, 46);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(71, 12);
            this.label58.TabIndex = 5;
            this.label58.Text = "数据库密码:";
            // 
            // textHMDataPass
            // 
            this.textHMDataPass.Location = new System.Drawing.Point(83, 42);
            this.textHMDataPass.Name = "textHMDataPass";
            this.textHMDataPass.PasswordChar = '*';
            this.textHMDataPass.Size = new System.Drawing.Size(100, 21);
            this.textHMDataPass.TabIndex = 4;
            this.textHMDataPass.Text = "scale";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.label59);
            this.tabPage8.Controls.Add(this.textSFYBan);
            this.tabPage8.Controls.Add(this.textSFYMin);
            this.tabPage8.Controls.Add(this.label60);
            this.tabPage8.Controls.Add(this.label61);
            this.tabPage8.Controls.Add(this.textSFYHour);
            this.tabPage8.Controls.Add(this.label62);
            this.tabPage8.Controls.Add(this.label63);
            this.tabPage8.Controls.Add(this.textSHYPassWord);
            this.tabPage8.Controls.Add(this.textSFYUser);
            this.tabPage8.Controls.Add(this.textSFYStation);
            this.tabPage8.Controls.Add(this.label64);
            this.tabPage8.Controls.Add(this.checkSFYUP);
            this.tabPage8.Controls.Add(this.label65);
            this.tabPage8.Controls.Add(this.textSFYPass);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(347, 258);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "水分仪(东方)";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(15, 106);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(29, 12);
            this.label59.TabIndex = 29;
            this.label59.Text = "班制";
            // 
            // textSFYBan
            // 
            this.textSFYBan.Location = new System.Drawing.Point(87, 102);
            this.textSFYBan.Name = "textSFYBan";
            this.textSFYBan.Size = new System.Drawing.Size(26, 21);
            this.textSFYBan.TabIndex = 27;
            this.textSFYBan.Text = "3";
            // 
            // textSFYMin
            // 
            this.textSFYMin.Location = new System.Drawing.Point(141, 75);
            this.textSFYMin.Name = "textSFYMin";
            this.textSFYMin.Size = new System.Drawing.Size(26, 21);
            this.textSFYMin.TabIndex = 30;
            this.textSFYMin.Text = "59";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(118, 80);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(17, 12);
            this.label60.TabIndex = 28;
            this.label60.Text = "：";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(14, 79);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(47, 12);
            this.label61.TabIndex = 26;
            this.label61.Text = "换天点:";
            // 
            // textSFYHour
            // 
            this.textSFYHour.Location = new System.Drawing.Point(86, 75);
            this.textSFYHour.Name = "textSFYHour";
            this.textSFYHour.Size = new System.Drawing.Size(26, 21);
            this.textSFYHour.TabIndex = 25;
            this.textSFYHour.Text = "11";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(15, 202);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(35, 12);
            this.label62.TabIndex = 24;
            this.label62.Text = "密码:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(15, 170);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(47, 12);
            this.label63.TabIndex = 20;
            this.label63.Text = "用户名:";
            // 
            // textSHYPassWord
            // 
            this.textSHYPassWord.Location = new System.Drawing.Point(85, 199);
            this.textSHYPassWord.Name = "textSHYPassWord";
            this.textSHYPassWord.PasswordChar = '*';
            this.textSHYPassWord.Size = new System.Drawing.Size(73, 21);
            this.textSHYPassWord.TabIndex = 22;
            this.textSHYPassWord.Text = "scale";
            // 
            // textSFYUser
            // 
            this.textSFYUser.Location = new System.Drawing.Point(86, 167);
            this.textSFYUser.Name = "textSFYUser";
            this.textSFYUser.Size = new System.Drawing.Size(74, 21);
            this.textSFYUser.TabIndex = 23;
            this.textSFYUser.Text = "scale";
            // 
            // textSFYStation
            // 
            this.textSFYStation.Location = new System.Drawing.Point(86, 136);
            this.textSFYStation.Name = "textSFYStation";
            this.textSFYStation.Size = new System.Drawing.Size(100, 21);
            this.textSFYStation.TabIndex = 21;
            this.textSFYStation.Text = "127.0.0.1";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(14, 139);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(47, 12);
            this.label64.TabIndex = 19;
            this.label64.Text = "中心站:";
            // 
            // checkSFYUP
            // 
            this.checkSFYUP.AutoSize = true;
            this.checkSFYUP.Location = new System.Drawing.Point(15, 22);
            this.checkSFYUP.Name = "checkSFYUP";
            this.checkSFYUP.Size = new System.Drawing.Size(72, 16);
            this.checkSFYUP.TabIndex = 18;
            this.checkSFYUP.Text = "是否上传";
            this.checkSFYUP.UseVisualStyleBackColor = true;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(14, 48);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(71, 12);
            this.label65.TabIndex = 17;
            this.label65.Text = "数据库密码:";
            // 
            // textSFYPass
            // 
            this.textSFYPass.Location = new System.Drawing.Point(86, 44);
            this.textSFYPass.Name = "textSFYPass";
            this.textSFYPass.PasswordChar = '*';
            this.textSFYPass.Size = new System.Drawing.Size(100, 21);
            this.textSFYPass.TabIndex = 16;
            this.textSFYPass.Text = "scale";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.label101);
            this.tabPage9.Controls.Add(this.checkBox11);
            this.tabPage9.Controls.Add(this.checkBox12);
            this.tabPage9.Controls.Add(this.checkBox13);
            this.tabPage9.Controls.Add(this.checkBox14);
            this.tabPage9.Controls.Add(this.checkBox15);
            this.tabPage9.Controls.Add(this.checkBox16);
            this.tabPage9.Controls.Add(this.checkBox17);
            this.tabPage9.Controls.Add(this.checkBox18);
            this.tabPage9.Controls.Add(this.button2);
            this.tabPage9.Controls.Add(this.label66);
            this.tabPage9.Controls.Add(this.textLMQBan);
            this.tabPage9.Controls.Add(this.textLMQMin);
            this.tabPage9.Controls.Add(this.label67);
            this.tabPage9.Controls.Add(this.label68);
            this.tabPage9.Controls.Add(this.textLMQHour);
            this.tabPage9.Controls.Add(this.label69);
            this.tabPage9.Controls.Add(this.label70);
            this.tabPage9.Controls.Add(this.textLMQPass);
            this.tabPage9.Controls.Add(this.textLMQUser);
            this.tabPage9.Controls.Add(this.textLMQStation);
            this.tabPage9.Controls.Add(this.label71);
            this.tabPage9.Controls.Add(this.checkLMQUP);
            this.tabPage9.Controls.Add(this.label72);
            this.tabPage9.Controls.Add(this.textLMQDataPass);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(347, 258);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "拉姆齐(MySql)";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(236, 22);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(29, 12);
            this.label101.TabIndex = 53;
            this.label101.Text = "秤体";
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(242, 196);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(60, 16);
            this.checkBox11.TabIndex = 52;
            this.checkBox11.Text = "第八台";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(242, 174);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(60, 16);
            this.checkBox12.TabIndex = 51;
            this.checkBox12.Text = "第七台";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(242, 152);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(60, 16);
            this.checkBox13.TabIndex = 50;
            this.checkBox13.Text = "第六台";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(242, 130);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(60, 16);
            this.checkBox14.TabIndex = 49;
            this.checkBox14.Text = "第五台";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(242, 108);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(60, 16);
            this.checkBox15.TabIndex = 48;
            this.checkBox15.Text = "第四台";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(242, 86);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(60, 16);
            this.checkBox16.TabIndex = 47;
            this.checkBox16.Text = "第三台";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(242, 64);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(60, 16);
            this.checkBox17.TabIndex = 46;
            this.checkBox17.Text = "第二台";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(242, 42);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(60, 16);
            this.checkBox18.TabIndex = 45;
            this.checkBox18.Text = "第一台";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(193, 130);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(23, 23);
            this.button2.TabIndex = 44;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(16, 102);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(29, 12);
            this.label66.TabIndex = 42;
            this.label66.Text = "班制";
            // 
            // textLMQBan
            // 
            this.textLMQBan.Location = new System.Drawing.Point(88, 98);
            this.textLMQBan.Name = "textLMQBan";
            this.textLMQBan.Size = new System.Drawing.Size(26, 21);
            this.textLMQBan.TabIndex = 40;
            this.textLMQBan.Text = "3";
            // 
            // textLMQMin
            // 
            this.textLMQMin.Location = new System.Drawing.Point(142, 71);
            this.textLMQMin.Name = "textLMQMin";
            this.textLMQMin.Size = new System.Drawing.Size(26, 21);
            this.textLMQMin.TabIndex = 43;
            this.textLMQMin.Text = "59";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(119, 76);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(17, 12);
            this.label67.TabIndex = 41;
            this.label67.Text = "：";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(15, 75);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(47, 12);
            this.label68.TabIndex = 39;
            this.label68.Text = "换天点:";
            // 
            // textLMQHour
            // 
            this.textLMQHour.Location = new System.Drawing.Point(87, 71);
            this.textLMQHour.Name = "textLMQHour";
            this.textLMQHour.Size = new System.Drawing.Size(26, 21);
            this.textLMQHour.TabIndex = 38;
            this.textLMQHour.Text = "11";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(11, 198);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(65, 12);
            this.label69.TabIndex = 37;
            this.label69.Text = "MySql密码:";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(11, 166);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(77, 12);
            this.label70.TabIndex = 33;
            this.label70.Text = "MySql用户名:";
            // 
            // textLMQPass
            // 
            this.textLMQPass.Location = new System.Drawing.Point(86, 195);
            this.textLMQPass.Name = "textLMQPass";
            this.textLMQPass.PasswordChar = '*';
            this.textLMQPass.Size = new System.Drawing.Size(73, 21);
            this.textLMQPass.TabIndex = 36;
            this.textLMQPass.Text = "scale";
            // 
            // textLMQUser
            // 
            this.textLMQUser.Location = new System.Drawing.Point(89, 163);
            this.textLMQUser.Name = "textLMQUser";
            this.textLMQUser.Size = new System.Drawing.Size(74, 21);
            this.textLMQUser.TabIndex = 35;
            this.textLMQUser.Text = "scale";
            // 
            // textLMQStation
            // 
            this.textLMQStation.Location = new System.Drawing.Point(87, 132);
            this.textLMQStation.Name = "textLMQStation";
            this.textLMQStation.Size = new System.Drawing.Size(100, 21);
            this.textLMQStation.TabIndex = 34;
            this.textLMQStation.Text = "127.0.0.1";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(11, 135);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(65, 12);
            this.label71.TabIndex = 32;
            this.label71.Text = "MySql地址:";
            // 
            // checkLMQUP
            // 
            this.checkLMQUP.AutoSize = true;
            this.checkLMQUP.Location = new System.Drawing.Point(16, 18);
            this.checkLMQUP.Name = "checkLMQUP";
            this.checkLMQUP.Size = new System.Drawing.Size(72, 16);
            this.checkLMQUP.TabIndex = 31;
            this.checkLMQUP.Text = "是否上传";
            this.checkLMQUP.UseVisualStyleBackColor = true;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(15, 44);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(71, 12);
            this.label72.TabIndex = 30;
            this.label72.Text = "数据库密码:";
            // 
            // textLMQDataPass
            // 
            this.textLMQDataPass.Location = new System.Drawing.Point(87, 40);
            this.textLMQDataPass.Name = "textLMQDataPass";
            this.textLMQDataPass.PasswordChar = '*';
            this.textLMQDataPass.Size = new System.Drawing.Size(100, 21);
            this.textLMQDataPass.TabIndex = 29;
            this.textLMQDataPass.Text = "scale";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.button3);
            this.tabPage10.Controls.Add(this.label73);
            this.tabPage10.Controls.Add(this.textQhfyBan);
            this.tabPage10.Controls.Add(this.textQhfyMin);
            this.tabPage10.Controls.Add(this.label74);
            this.tabPage10.Controls.Add(this.label75);
            this.tabPage10.Controls.Add(this.textQhfyHour);
            this.tabPage10.Controls.Add(this.label76);
            this.tabPage10.Controls.Add(this.label77);
            this.tabPage10.Controls.Add(this.textQhfyPass);
            this.tabPage10.Controls.Add(this.textQhfyUser);
            this.tabPage10.Controls.Add(this.textQhfyStation);
            this.tabPage10.Controls.Add(this.label78);
            this.tabPage10.Controls.Add(this.checkQhfyUP);
            this.tabPage10.Controls.Add(this.label79);
            this.tabPage10.Controls.Add(this.textQhfyDataPass);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(347, 258);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "清华灰分仪";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(191, 131);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(23, 23);
            this.button3.TabIndex = 44;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(14, 103);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(29, 12);
            this.label73.TabIndex = 42;
            this.label73.Text = "班制";
            // 
            // textQhfyBan
            // 
            this.textQhfyBan.Location = new System.Drawing.Point(86, 99);
            this.textQhfyBan.Name = "textQhfyBan";
            this.textQhfyBan.Size = new System.Drawing.Size(26, 21);
            this.textQhfyBan.TabIndex = 40;
            this.textQhfyBan.Text = "3";
            // 
            // textQhfyMin
            // 
            this.textQhfyMin.Location = new System.Drawing.Point(140, 72);
            this.textQhfyMin.Name = "textQhfyMin";
            this.textQhfyMin.Size = new System.Drawing.Size(26, 21);
            this.textQhfyMin.TabIndex = 43;
            this.textQhfyMin.Text = "59";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(117, 77);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(17, 12);
            this.label74.TabIndex = 41;
            this.label74.Text = "：";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(13, 76);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(47, 12);
            this.label75.TabIndex = 39;
            this.label75.Text = "换天点:";
            // 
            // textQhfyHour
            // 
            this.textQhfyHour.Location = new System.Drawing.Point(85, 72);
            this.textQhfyHour.Name = "textQhfyHour";
            this.textQhfyHour.Size = new System.Drawing.Size(26, 21);
            this.textQhfyHour.TabIndex = 38;
            this.textQhfyHour.Text = "11";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(14, 199);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 12);
            this.label76.TabIndex = 37;
            this.label76.Text = "密码:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(14, 167);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(47, 12);
            this.label77.TabIndex = 33;
            this.label77.Text = "端口号:";
            // 
            // textQhfyPass
            // 
            this.textQhfyPass.Location = new System.Drawing.Point(84, 196);
            this.textQhfyPass.Name = "textQhfyPass";
            this.textQhfyPass.PasswordChar = '*';
            this.textQhfyPass.Size = new System.Drawing.Size(73, 21);
            this.textQhfyPass.TabIndex = 36;
            this.textQhfyPass.Text = "scale";
            // 
            // textQhfyUser
            // 
            this.textQhfyUser.Location = new System.Drawing.Point(85, 164);
            this.textQhfyUser.Name = "textQhfyUser";
            this.textQhfyUser.Size = new System.Drawing.Size(74, 21);
            this.textQhfyUser.TabIndex = 35;
            this.textQhfyUser.Text = "scale";
            // 
            // textQhfyStation
            // 
            this.textQhfyStation.Location = new System.Drawing.Point(85, 133);
            this.textQhfyStation.Name = "textQhfyStation";
            this.textQhfyStation.Size = new System.Drawing.Size(100, 21);
            this.textQhfyStation.TabIndex = 34;
            this.textQhfyStation.Text = "127.0.0.1";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(13, 136);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(47, 12);
            this.label78.TabIndex = 32;
            this.label78.Text = "中心站:";
            // 
            // checkQhfyUP
            // 
            this.checkQhfyUP.AutoSize = true;
            this.checkQhfyUP.Location = new System.Drawing.Point(14, 19);
            this.checkQhfyUP.Name = "checkQhfyUP";
            this.checkQhfyUP.Size = new System.Drawing.Size(72, 16);
            this.checkQhfyUP.TabIndex = 31;
            this.checkQhfyUP.Text = "是否上传";
            this.checkQhfyUP.UseVisualStyleBackColor = true;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(13, 45);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(71, 12);
            this.label79.TabIndex = 30;
            this.label79.Text = "数据库密码:";
            // 
            // textQhfyDataPass
            // 
            this.textQhfyDataPass.Location = new System.Drawing.Point(85, 41);
            this.textQhfyDataPass.Name = "textQhfyDataPass";
            this.textQhfyDataPass.PasswordChar = '*';
            this.textQhfyDataPass.Size = new System.Drawing.Size(100, 21);
            this.textQhfyDataPass.TabIndex = 29;
            this.textQhfyDataPass.Text = "scale";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.button4);
            this.tabPage11.Controls.Add(this.label80);
            this.tabPage11.Controls.Add(this.textQsfyBan);
            this.tabPage11.Controls.Add(this.textQsfyMin);
            this.tabPage11.Controls.Add(this.label81);
            this.tabPage11.Controls.Add(this.label82);
            this.tabPage11.Controls.Add(this.textQsfyHour);
            this.tabPage11.Controls.Add(this.label83);
            this.tabPage11.Controls.Add(this.label84);
            this.tabPage11.Controls.Add(this.textQsfyPass);
            this.tabPage11.Controls.Add(this.textQsfyUser);
            this.tabPage11.Controls.Add(this.textQsfyStation);
            this.tabPage11.Controls.Add(this.label85);
            this.tabPage11.Controls.Add(this.checkQsfyUP);
            this.tabPage11.Controls.Add(this.label86);
            this.tabPage11.Controls.Add(this.textQsfyDataPass);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(347, 258);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "清华水分仪";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(194, 133);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(23, 23);
            this.button4.TabIndex = 44;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(17, 105);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(29, 12);
            this.label80.TabIndex = 42;
            this.label80.Text = "班制";
            // 
            // textQsfyBan
            // 
            this.textQsfyBan.Location = new System.Drawing.Point(89, 101);
            this.textQsfyBan.Name = "textQsfyBan";
            this.textQsfyBan.Size = new System.Drawing.Size(26, 21);
            this.textQsfyBan.TabIndex = 40;
            this.textQsfyBan.Text = "3";
            // 
            // textQsfyMin
            // 
            this.textQsfyMin.Location = new System.Drawing.Point(143, 74);
            this.textQsfyMin.Name = "textQsfyMin";
            this.textQsfyMin.Size = new System.Drawing.Size(26, 21);
            this.textQsfyMin.TabIndex = 43;
            this.textQsfyMin.Text = "59";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(120, 79);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(17, 12);
            this.label81.TabIndex = 41;
            this.label81.Text = "：";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(16, 78);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(47, 12);
            this.label82.TabIndex = 39;
            this.label82.Text = "换天点:";
            // 
            // textQsfyHour
            // 
            this.textQsfyHour.Location = new System.Drawing.Point(88, 74);
            this.textQsfyHour.Name = "textQsfyHour";
            this.textQsfyHour.Size = new System.Drawing.Size(26, 21);
            this.textQsfyHour.TabIndex = 38;
            this.textQsfyHour.Text = "11";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(17, 201);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 12);
            this.label83.TabIndex = 37;
            this.label83.Text = "密码:";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(17, 169);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(47, 12);
            this.label84.TabIndex = 33;
            this.label84.Text = "用户名:";
            // 
            // textQsfyPass
            // 
            this.textQsfyPass.Location = new System.Drawing.Point(87, 198);
            this.textQsfyPass.Name = "textQsfyPass";
            this.textQsfyPass.PasswordChar = '*';
            this.textQsfyPass.Size = new System.Drawing.Size(73, 21);
            this.textQsfyPass.TabIndex = 36;
            this.textQsfyPass.Text = "scale";
            // 
            // textQsfyUser
            // 
            this.textQsfyUser.Location = new System.Drawing.Point(88, 166);
            this.textQsfyUser.Name = "textQsfyUser";
            this.textQsfyUser.Size = new System.Drawing.Size(74, 21);
            this.textQsfyUser.TabIndex = 35;
            this.textQsfyUser.Text = "scale";
            // 
            // textQsfyStation
            // 
            this.textQsfyStation.Location = new System.Drawing.Point(88, 135);
            this.textQsfyStation.Name = "textQsfyStation";
            this.textQsfyStation.Size = new System.Drawing.Size(100, 21);
            this.textQsfyStation.TabIndex = 34;
            this.textQsfyStation.Text = "127.0.0.1";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(16, 138);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(47, 12);
            this.label85.TabIndex = 32;
            this.label85.Text = "中心站:";
            // 
            // checkQsfyUP
            // 
            this.checkQsfyUP.AutoSize = true;
            this.checkQsfyUP.Location = new System.Drawing.Point(17, 21);
            this.checkQsfyUP.Name = "checkQsfyUP";
            this.checkQsfyUP.Size = new System.Drawing.Size(72, 16);
            this.checkQsfyUP.TabIndex = 31;
            this.checkQsfyUP.Text = "是否上传";
            this.checkQsfyUP.UseVisualStyleBackColor = true;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(16, 47);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(71, 12);
            this.label86.TabIndex = 30;
            this.label86.Text = "数据库密码:";
            // 
            // textQsfyDataPass
            // 
            this.textQsfyDataPass.Location = new System.Drawing.Point(88, 43);
            this.textQsfyDataPass.Name = "textQsfyDataPass";
            this.textQsfyDataPass.PasswordChar = '*';
            this.textQsfyDataPass.Size = new System.Drawing.Size(100, 21);
            this.textQsfyDataPass.TabIndex = 29;
            this.textQsfyDataPass.Text = "scale";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.button5);
            this.tabPage12.Controls.Add(this.label87);
            this.tabPage12.Controls.Add(this.textQjgcBan);
            this.tabPage12.Controls.Add(this.textQjgcMin);
            this.tabPage12.Controls.Add(this.label88);
            this.tabPage12.Controls.Add(this.label89);
            this.tabPage12.Controls.Add(this.textQjgcHour);
            this.tabPage12.Controls.Add(this.label90);
            this.tabPage12.Controls.Add(this.label91);
            this.tabPage12.Controls.Add(this.textQjgcPass);
            this.tabPage12.Controls.Add(this.textQjgcUser);
            this.tabPage12.Controls.Add(this.textQjgcStation);
            this.tabPage12.Controls.Add(this.label92);
            this.tabPage12.Controls.Add(this.checkQjgcUP);
            this.tabPage12.Controls.Add(this.label93);
            this.tabPage12.Controls.Add(this.textQjgcDataPass);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(347, 258);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "赛华皮带秤";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(195, 131);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(23, 23);
            this.button5.TabIndex = 44;
            this.button5.Text = "...";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(18, 103);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(29, 12);
            this.label87.TabIndex = 42;
            this.label87.Text = "班制";
            // 
            // textQjgcBan
            // 
            this.textQjgcBan.Location = new System.Drawing.Point(90, 99);
            this.textQjgcBan.Name = "textQjgcBan";
            this.textQjgcBan.Size = new System.Drawing.Size(26, 21);
            this.textQjgcBan.TabIndex = 40;
            this.textQjgcBan.Text = "3";
            // 
            // textQjgcMin
            // 
            this.textQjgcMin.Location = new System.Drawing.Point(144, 72);
            this.textQjgcMin.Name = "textQjgcMin";
            this.textQjgcMin.Size = new System.Drawing.Size(26, 21);
            this.textQjgcMin.TabIndex = 43;
            this.textQjgcMin.Text = "59";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(121, 77);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(17, 12);
            this.label88.TabIndex = 41;
            this.label88.Text = "：";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(17, 76);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(47, 12);
            this.label89.TabIndex = 39;
            this.label89.Text = "换天点:";
            // 
            // textQjgcHour
            // 
            this.textQjgcHour.Location = new System.Drawing.Point(89, 72);
            this.textQjgcHour.Name = "textQjgcHour";
            this.textQjgcHour.Size = new System.Drawing.Size(26, 21);
            this.textQjgcHour.TabIndex = 38;
            this.textQjgcHour.Text = "11";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(18, 199);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(35, 12);
            this.label90.TabIndex = 37;
            this.label90.Text = "密码:";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(18, 167);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(47, 12);
            this.label91.TabIndex = 33;
            this.label91.Text = "端口号:";
            // 
            // textQjgcPass
            // 
            this.textQjgcPass.Location = new System.Drawing.Point(88, 196);
            this.textQjgcPass.Name = "textQjgcPass";
            this.textQjgcPass.PasswordChar = '*';
            this.textQjgcPass.Size = new System.Drawing.Size(73, 21);
            this.textQjgcPass.TabIndex = 36;
            this.textQjgcPass.Text = "scale";
            // 
            // textQjgcUser
            // 
            this.textQjgcUser.Location = new System.Drawing.Point(89, 164);
            this.textQjgcUser.Name = "textQjgcUser";
            this.textQjgcUser.Size = new System.Drawing.Size(74, 21);
            this.textQjgcUser.TabIndex = 35;
            this.textQjgcUser.Text = "scale";
            // 
            // textQjgcStation
            // 
            this.textQjgcStation.Location = new System.Drawing.Point(89, 133);
            this.textQjgcStation.Name = "textQjgcStation";
            this.textQjgcStation.Size = new System.Drawing.Size(100, 21);
            this.textQjgcStation.TabIndex = 34;
            this.textQjgcStation.Text = "127.0.0.1";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(17, 136);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(47, 12);
            this.label92.TabIndex = 32;
            this.label92.Text = "中心站:";
            // 
            // checkQjgcUP
            // 
            this.checkQjgcUP.AutoSize = true;
            this.checkQjgcUP.Location = new System.Drawing.Point(18, 19);
            this.checkQjgcUP.Name = "checkQjgcUP";
            this.checkQjgcUP.Size = new System.Drawing.Size(72, 16);
            this.checkQjgcUP.TabIndex = 31;
            this.checkQjgcUP.Text = "是否上传";
            this.checkQjgcUP.UseVisualStyleBackColor = true;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(17, 45);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(71, 12);
            this.label93.TabIndex = 30;
            this.label93.Text = "数据库密码:";
            // 
            // textQjgcDataPass
            // 
            this.textQjgcDataPass.Location = new System.Drawing.Point(89, 41);
            this.textQjgcDataPass.Name = "textQjgcDataPass";
            this.textQjgcDataPass.PasswordChar = '*';
            this.textQjgcDataPass.Size = new System.Drawing.Size(100, 21);
            this.textQjgcDataPass.TabIndex = 29;
            this.textQjgcDataPass.Text = "scale";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.textKFwhUser);
            this.tabPage13.Controls.Add(this.button6);
            this.tabPage13.Controls.Add(this.label94);
            this.tabPage13.Controls.Add(this.textKFwhBan);
            this.tabPage13.Controls.Add(this.textKFwhMin);
            this.tabPage13.Controls.Add(this.label95);
            this.tabPage13.Controls.Add(this.label96);
            this.tabPage13.Controls.Add(this.textKFwhHour);
            this.tabPage13.Controls.Add(this.label97);
            this.tabPage13.Controls.Add(this.label98);
            this.tabPage13.Controls.Add(this.textKFwhPass);
            this.tabPage13.Controls.Add(this.textKFwhStation);
            this.tabPage13.Controls.Add(this.label99);
            this.tabPage13.Controls.Add(this.checkKfwhUP);
            this.tabPage13.Controls.Add(this.label100);
            this.tabPage13.Controls.Add(this.textKFwhDataPass);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(347, 258);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "开封万惠/清华激光秤";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // textKFwhUser
            // 
            this.textKFwhUser.Location = new System.Drawing.Point(86, 162);
            this.textKFwhUser.Name = "textKFwhUser";
            this.textKFwhUser.Size = new System.Drawing.Size(73, 21);
            this.textKFwhUser.TabIndex = 45;
            this.textKFwhUser.Text = "scale";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(192, 128);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(23, 23);
            this.button6.TabIndex = 44;
            this.button6.Text = "...";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(15, 100);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(29, 12);
            this.label94.TabIndex = 42;
            this.label94.Text = "班制";
            // 
            // textKFwhBan
            // 
            this.textKFwhBan.Location = new System.Drawing.Point(87, 96);
            this.textKFwhBan.Name = "textKFwhBan";
            this.textKFwhBan.Size = new System.Drawing.Size(26, 21);
            this.textKFwhBan.TabIndex = 40;
            this.textKFwhBan.Text = "3";
            // 
            // textKFwhMin
            // 
            this.textKFwhMin.Location = new System.Drawing.Point(141, 69);
            this.textKFwhMin.Name = "textKFwhMin";
            this.textKFwhMin.Size = new System.Drawing.Size(26, 21);
            this.textKFwhMin.TabIndex = 43;
            this.textKFwhMin.Text = "59";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(118, 74);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(17, 12);
            this.label95.TabIndex = 41;
            this.label95.Text = "：";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(14, 73);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(47, 12);
            this.label96.TabIndex = 39;
            this.label96.Text = "换天点:";
            // 
            // textKFwhHour
            // 
            this.textKFwhHour.Location = new System.Drawing.Point(86, 69);
            this.textKFwhHour.Name = "textKFwhHour";
            this.textKFwhHour.Size = new System.Drawing.Size(26, 21);
            this.textKFwhHour.TabIndex = 38;
            this.textKFwhHour.Text = "11";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(15, 196);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(35, 12);
            this.label97.TabIndex = 37;
            this.label97.Text = "密码:";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(15, 164);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(47, 12);
            this.label98.TabIndex = 33;
            this.label98.Text = "用户名:";
            // 
            // textKFwhPass
            // 
            this.textKFwhPass.Location = new System.Drawing.Point(85, 193);
            this.textKFwhPass.Name = "textKFwhPass";
            this.textKFwhPass.PasswordChar = '*';
            this.textKFwhPass.Size = new System.Drawing.Size(73, 21);
            this.textKFwhPass.TabIndex = 36;
            this.textKFwhPass.Text = "scale";
            // 
            // textKFwhStation
            // 
            this.textKFwhStation.Location = new System.Drawing.Point(86, 130);
            this.textKFwhStation.Name = "textKFwhStation";
            this.textKFwhStation.Size = new System.Drawing.Size(100, 21);
            this.textKFwhStation.TabIndex = 34;
            this.textKFwhStation.Text = "127.0.0.1";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(14, 133);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(47, 12);
            this.label99.TabIndex = 32;
            this.label99.Text = "中心站:";
            // 
            // checkKfwhUP
            // 
            this.checkKfwhUP.AutoSize = true;
            this.checkKfwhUP.Location = new System.Drawing.Point(15, 16);
            this.checkKfwhUP.Name = "checkKfwhUP";
            this.checkKfwhUP.Size = new System.Drawing.Size(72, 16);
            this.checkKfwhUP.TabIndex = 31;
            this.checkKfwhUP.Text = "是否上传";
            this.checkKfwhUP.UseVisualStyleBackColor = true;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(14, 42);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(71, 12);
            this.label100.TabIndex = 30;
            this.label100.Text = "数据库密码:";
            // 
            // textKFwhDataPass
            // 
            this.textKFwhDataPass.Location = new System.Drawing.Point(86, 38);
            this.textKFwhDataPass.Name = "textKFwhDataPass";
            this.textKFwhDataPass.PasswordChar = '*';
            this.textKFwhDataPass.Size = new System.Drawing.Size(100, 21);
            this.textKFwhDataPass.TabIndex = 29;
            this.textKFwhDataPass.Text = "scale";
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.label111);
            this.tabPage14.Controls.Add(this.checkCar8);
            this.tabPage14.Controls.Add(this.checkCar7);
            this.tabPage14.Controls.Add(this.checkCar6);
            this.tabPage14.Controls.Add(this.checkCar5);
            this.tabPage14.Controls.Add(this.checkCar4);
            this.tabPage14.Controls.Add(this.checkCar3);
            this.tabPage14.Controls.Add(this.checkCar2);
            this.tabPage14.Controls.Add(this.checkCar1);
            this.tabPage14.Controls.Add(this.textXJcarUser);
            this.tabPage14.Controls.Add(this.button8);
            this.tabPage14.Controls.Add(this.label110);
            this.tabPage14.Controls.Add(this.textXJcarBan);
            this.tabPage14.Controls.Add(this.textXJcarMin);
            this.tabPage14.Controls.Add(this.label112);
            this.tabPage14.Controls.Add(this.textXJcarHour);
            this.tabPage14.Controls.Add(this.label113);
            this.tabPage14.Controls.Add(this.label114);
            this.tabPage14.Controls.Add(this.textXJcarPass);
            this.tabPage14.Controls.Add(this.textXJcarStation);
            this.tabPage14.Controls.Add(this.label115);
            this.tabPage14.Controls.Add(this.checkXJcarUP);
            this.tabPage14.Controls.Add(this.label116);
            this.tabPage14.Controls.Add(this.textXJcarDataPass);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(347, 258);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "地磅";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(207, 17);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(89, 12);
            this.label111.TabIndex = 70;
            this.label111.Text = "请选择矿区上传";
            // 
            // checkCar8
            // 
            this.checkCar8.AutoSize = true;
            this.checkCar8.Location = new System.Drawing.Point(238, 196);
            this.checkCar8.Name = "checkCar8";
            this.checkCar8.Size = new System.Drawing.Size(54, 16);
            this.checkCar8.TabIndex = 69;
            this.checkCar8.Text = "备用2";
            this.checkCar8.UseVisualStyleBackColor = true;
            // 
            // checkCar7
            // 
            this.checkCar7.AutoSize = true;
            this.checkCar7.Location = new System.Drawing.Point(238, 174);
            this.checkCar7.Name = "checkCar7";
            this.checkCar7.Size = new System.Drawing.Size(54, 16);
            this.checkCar7.TabIndex = 68;
            this.checkCar7.Text = "备用1";
            this.checkCar7.UseVisualStyleBackColor = true;
            // 
            // checkCar6
            // 
            this.checkCar6.AutoSize = true;
            this.checkCar6.Location = new System.Drawing.Point(238, 152);
            this.checkCar6.Name = "checkCar6";
            this.checkCar6.Size = new System.Drawing.Size(48, 16);
            this.checkCar6.TabIndex = 67;
            this.checkCar6.Text = "恒泰";
            this.checkCar6.UseVisualStyleBackColor = true;
            // 
            // checkCar5
            // 
            this.checkCar5.AutoSize = true;
            this.checkCar5.Location = new System.Drawing.Point(238, 130);
            this.checkCar5.Name = "checkCar5";
            this.checkCar5.Size = new System.Drawing.Size(48, 16);
            this.checkCar5.TabIndex = 66;
            this.checkCar5.Text = "众维";
            this.checkCar5.UseVisualStyleBackColor = true;
            // 
            // checkCar4
            // 
            this.checkCar4.AutoSize = true;
            this.checkCar4.Location = new System.Drawing.Point(238, 108);
            this.checkCar4.Name = "checkCar4";
            this.checkCar4.Size = new System.Drawing.Size(48, 16);
            this.checkCar4.TabIndex = 65;
            this.checkCar4.Text = "音西";
            this.checkCar4.UseVisualStyleBackColor = true;
            // 
            // checkCar3
            // 
            this.checkCar3.AutoSize = true;
            this.checkCar3.Location = new System.Drawing.Point(238, 86);
            this.checkCar3.Name = "checkCar3";
            this.checkCar3.Size = new System.Drawing.Size(48, 16);
            this.checkCar3.TabIndex = 64;
            this.checkCar3.Text = "众泰";
            this.checkCar3.UseVisualStyleBackColor = true;
            // 
            // checkCar2
            // 
            this.checkCar2.AutoSize = true;
            this.checkCar2.Location = new System.Drawing.Point(238, 64);
            this.checkCar2.Name = "checkCar2";
            this.checkCar2.Size = new System.Drawing.Size(60, 16);
            this.checkCar2.TabIndex = 63;
            this.checkCar2.Text = "龟兹矿";
            this.checkCar2.UseVisualStyleBackColor = true;
            // 
            // checkCar1
            // 
            this.checkCar1.AutoSize = true;
            this.checkCar1.Location = new System.Drawing.Point(238, 42);
            this.checkCar1.Name = "checkCar1";
            this.checkCar1.Size = new System.Drawing.Size(60, 16);
            this.checkCar1.TabIndex = 62;
            this.checkCar1.Text = "潘津矿";
            this.checkCar1.UseVisualStyleBackColor = true;
            // 
            // textXJcarUser
            // 
            this.textXJcarUser.Location = new System.Drawing.Point(89, 161);
            this.textXJcarUser.Name = "textXJcarUser";
            this.textXJcarUser.Size = new System.Drawing.Size(73, 21);
            this.textXJcarUser.TabIndex = 61;
            this.textXJcarUser.Text = "scale";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(195, 127);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(23, 23);
            this.button8.TabIndex = 60;
            this.button8.Text = "...";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(18, 99);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(59, 12);
            this.label110.TabIndex = 58;
            this.label110.Text = "开始编号:";
            // 
            // textXJcarBan
            // 
            this.textXJcarBan.Location = new System.Drawing.Point(90, 95);
            this.textXJcarBan.Name = "textXJcarBan";
            this.textXJcarBan.Size = new System.Drawing.Size(26, 21);
            this.textXJcarBan.TabIndex = 56;
            this.textXJcarBan.Text = "0";
            // 
            // textXJcarMin
            // 
            this.textXJcarMin.Location = new System.Drawing.Point(87, 219);
            this.textXJcarMin.Name = "textXJcarMin";
            this.textXJcarMin.Size = new System.Drawing.Size(26, 21);
            this.textXJcarMin.TabIndex = 59;
            this.textXJcarMin.Text = "59";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(17, 72);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(59, 12);
            this.label112.TabIndex = 55;
            this.label112.Text = "地磅名称:";
            // 
            // textXJcarHour
            // 
            this.textXJcarHour.Location = new System.Drawing.Point(89, 68);
            this.textXJcarHour.Name = "textXJcarHour";
            this.textXJcarHour.Size = new System.Drawing.Size(100, 21);
            this.textXJcarHour.TabIndex = 54;
            this.textXJcarHour.Text = "11";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(18, 195);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(35, 12);
            this.label113.TabIndex = 53;
            this.label113.Text = "密码:";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(18, 163);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(47, 12);
            this.label114.TabIndex = 50;
            this.label114.Text = "用户名:";
            // 
            // textXJcarPass
            // 
            this.textXJcarPass.Location = new System.Drawing.Point(88, 192);
            this.textXJcarPass.Name = "textXJcarPass";
            this.textXJcarPass.PasswordChar = '*';
            this.textXJcarPass.Size = new System.Drawing.Size(73, 21);
            this.textXJcarPass.TabIndex = 52;
            this.textXJcarPass.Text = "scale";
            // 
            // textXJcarStation
            // 
            this.textXJcarStation.Location = new System.Drawing.Point(89, 129);
            this.textXJcarStation.Name = "textXJcarStation";
            this.textXJcarStation.Size = new System.Drawing.Size(100, 21);
            this.textXJcarStation.TabIndex = 51;
            this.textXJcarStation.Text = "127.0.0.1";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(17, 132);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(47, 12);
            this.label115.TabIndex = 49;
            this.label115.Text = "中心站:";
            // 
            // checkXJcarUP
            // 
            this.checkXJcarUP.AutoSize = true;
            this.checkXJcarUP.Location = new System.Drawing.Point(18, 15);
            this.checkXJcarUP.Name = "checkXJcarUP";
            this.checkXJcarUP.Size = new System.Drawing.Size(72, 16);
            this.checkXJcarUP.TabIndex = 48;
            this.checkXJcarUP.Text = "是否上传";
            this.checkXJcarUP.UseVisualStyleBackColor = true;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(17, 41);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(71, 12);
            this.label116.TabIndex = 47;
            this.label116.Text = "数据库密码:";
            // 
            // textXJcarDataPass
            // 
            this.textXJcarDataPass.Location = new System.Drawing.Point(89, 37);
            this.textXJcarDataPass.Name = "textXJcarDataPass";
            this.textXJcarDataPass.PasswordChar = '*';
            this.textXJcarDataPass.Size = new System.Drawing.Size(100, 21);
            this.textXJcarDataPass.TabIndex = 46;
            this.textXJcarDataPass.Text = "scale";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.textHNUser);
            this.tabPage15.Controls.Add(this.button10);
            this.tabPage15.Controls.Add(this.label124);
            this.tabPage15.Controls.Add(this.textHNDay);
            this.tabPage15.Controls.Add(this.textHNMin);
            this.tabPage15.Controls.Add(this.label125);
            this.tabPage15.Controls.Add(this.label126);
            this.tabPage15.Controls.Add(this.textHNHour);
            this.tabPage15.Controls.Add(this.label127);
            this.tabPage15.Controls.Add(this.label128);
            this.tabPage15.Controls.Add(this.textHNPass);
            this.tabPage15.Controls.Add(this.textHNStation);
            this.tabPage15.Controls.Add(this.label129);
            this.tabPage15.Controls.Add(this.checkHNUP);
            this.tabPage15.Controls.Add(this.label130);
            this.tabPage15.Controls.Add(this.textHNDataPass);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(347, 258);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "淮南煤质";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // textHNUser
            // 
            this.textHNUser.Location = new System.Drawing.Point(86, 165);
            this.textHNUser.Name = "textHNUser";
            this.textHNUser.Size = new System.Drawing.Size(73, 21);
            this.textHNUser.TabIndex = 61;
            this.textHNUser.Text = "scale";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(192, 131);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(23, 23);
            this.button10.TabIndex = 60;
            this.button10.Text = "...";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(15, 103);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(59, 12);
            this.label124.TabIndex = 58;
            this.label124.Text = "时间范围:";
            // 
            // textHNDay
            // 
            this.textHNDay.Location = new System.Drawing.Point(87, 99);
            this.textHNDay.Name = "textHNDay";
            this.textHNDay.Size = new System.Drawing.Size(26, 21);
            this.textHNDay.TabIndex = 56;
            this.textHNDay.Text = "3";
            // 
            // textHNMin
            // 
            this.textHNMin.Location = new System.Drawing.Point(141, 72);
            this.textHNMin.Name = "textHNMin";
            this.textHNMin.Size = new System.Drawing.Size(26, 21);
            this.textHNMin.TabIndex = 59;
            this.textHNMin.Text = "59";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(118, 77);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(17, 12);
            this.label125.TabIndex = 57;
            this.label125.Text = "：";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(14, 76);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(47, 12);
            this.label126.TabIndex = 55;
            this.label126.Text = "换天点:";
            // 
            // textHNHour
            // 
            this.textHNHour.Location = new System.Drawing.Point(86, 72);
            this.textHNHour.Name = "textHNHour";
            this.textHNHour.Size = new System.Drawing.Size(26, 21);
            this.textHNHour.TabIndex = 54;
            this.textHNHour.Text = "11";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(15, 199);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(35, 12);
            this.label127.TabIndex = 53;
            this.label127.Text = "密码:";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(15, 167);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(47, 12);
            this.label128.TabIndex = 50;
            this.label128.Text = "用户名:";
            // 
            // textHNPass
            // 
            this.textHNPass.Location = new System.Drawing.Point(85, 196);
            this.textHNPass.Name = "textHNPass";
            this.textHNPass.PasswordChar = '*';
            this.textHNPass.Size = new System.Drawing.Size(73, 21);
            this.textHNPass.TabIndex = 52;
            this.textHNPass.Text = "scale";
            // 
            // textHNStation
            // 
            this.textHNStation.Location = new System.Drawing.Point(86, 133);
            this.textHNStation.Name = "textHNStation";
            this.textHNStation.Size = new System.Drawing.Size(100, 21);
            this.textHNStation.TabIndex = 51;
            this.textHNStation.Text = "127.0.0.1";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(14, 136);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(47, 12);
            this.label129.TabIndex = 49;
            this.label129.Text = "中心站:";
            // 
            // checkHNUP
            // 
            this.checkHNUP.AutoSize = true;
            this.checkHNUP.Location = new System.Drawing.Point(15, 19);
            this.checkHNUP.Name = "checkHNUP";
            this.checkHNUP.Size = new System.Drawing.Size(72, 16);
            this.checkHNUP.TabIndex = 48;
            this.checkHNUP.Text = "是否上传";
            this.checkHNUP.UseVisualStyleBackColor = true;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(14, 45);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(71, 12);
            this.label130.TabIndex = 47;
            this.label130.Text = "数据库密码:";
            // 
            // textHNDataPass
            // 
            this.textHNDataPass.Location = new System.Drawing.Point(86, 41);
            this.textHNDataPass.Name = "textHNDataPass";
            this.textHNDataPass.PasswordChar = '*';
            this.textHNDataPass.Size = new System.Drawing.Size(100, 21);
            this.textHNDataPass.TabIndex = 46;
            this.textHNDataPass.Text = "scale";
            // 
            // lbShowInfo
            // 
            this.lbShowInfo.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbShowInfo.FormattingEnabled = true;
            this.lbShowInfo.HorizontalScrollbar = true;
            this.lbShowInfo.ItemHeight = 16;
            this.lbShowInfo.Location = new System.Drawing.Point(7, 17);
            this.lbShowInfo.Name = "lbShowInfo";
            this.lbShowInfo.Size = new System.Drawing.Size(457, 340);
            this.lbShowInfo.TabIndex = 7;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lbShowInfo);
            this.groupBox6.Location = new System.Drawing.Point(399, 17);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(470, 367);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "状态信息";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(433, 433);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "北京市煤炭矿用机电设备技术开发有限公司";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(667, 433);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 12);
            this.label7.TabIndex = 10;
            this.label7.Text = "版权所有 ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tabControl1);
            this.groupBox8.Location = new System.Drawing.Point(12, 145);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(372, 310);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "设备设置";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(490, 390);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 28);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnModfiy
            // 
            this.btnModfiy.Location = new System.Drawing.Point(650, 390);
            this.btnModfiy.Name = "btnModfiy";
            this.btnModfiy.Size = new System.Drawing.Size(65, 28);
            this.btnModfiy.TabIndex = 13;
            this.btnModfiy.Text = "修改";
            this.btnModfiy.UseVisualStyleBackColor = true;
            this.btnModfiy.Click += new System.EventHandler(this.btnModfiy_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(86, 162);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(73, 21);
            this.textBox1.TabIndex = 45;
            this.textBox1.Text = "scale";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(192, 128);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(23, 23);
            this.button7.TabIndex = 44;
            this.button7.Text = "...";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(15, 100);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(29, 12);
            this.label103.TabIndex = 42;
            this.label103.Text = "班制";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(87, 96);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(26, 21);
            this.textBox2.TabIndex = 40;
            this.textBox2.Text = "3";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(141, 69);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(26, 21);
            this.textBox3.TabIndex = 43;
            this.textBox3.Text = "59";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(118, 74);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(17, 12);
            this.label104.TabIndex = 41;
            this.label104.Text = "：";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(14, 73);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(47, 12);
            this.label105.TabIndex = 39;
            this.label105.Text = "换天点:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(86, 69);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(26, 21);
            this.textBox4.TabIndex = 38;
            this.textBox4.Text = "11";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(15, 196);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(35, 12);
            this.label106.TabIndex = 37;
            this.label106.Text = "密码:";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(15, 164);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(47, 12);
            this.label107.TabIndex = 33;
            this.label107.Text = "用户名:";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(85, 193);
            this.textBox5.Name = "textBox5";
            this.textBox5.PasswordChar = '*';
            this.textBox5.Size = new System.Drawing.Size(73, 21);
            this.textBox5.TabIndex = 36;
            this.textBox5.Text = "scale";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(86, 130);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 21);
            this.textBox6.TabIndex = 34;
            this.textBox6.Text = "127.0.0.1";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(14, 133);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(47, 12);
            this.label108.TabIndex = 32;
            this.label108.Text = "中心站:";
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(15, 16);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(72, 16);
            this.checkBox19.TabIndex = 31;
            this.checkBox19.Text = "是否上传";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(14, 42);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(71, 12);
            this.label109.TabIndex = 30;
            this.label109.Text = "数据库密码:";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(86, 38);
            this.textBox7.Name = "textBox7";
            this.textBox7.PasswordChar = '*';
            this.textBox7.Size = new System.Drawing.Size(100, 21);
            this.textBox7.TabIndex = 29;
            this.textBox7.Text = "scale";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(86, 162);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(73, 21);
            this.textBox8.TabIndex = 45;
            this.textBox8.Text = "scale";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(192, 128);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(23, 23);
            this.button9.TabIndex = 44;
            this.button9.Text = "...";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(15, 100);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(29, 12);
            this.label117.TabIndex = 42;
            this.label117.Text = "班制";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(87, 96);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(26, 21);
            this.textBox9.TabIndex = 40;
            this.textBox9.Text = "3";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(141, 69);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(26, 21);
            this.textBox10.TabIndex = 43;
            this.textBox10.Text = "59";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(118, 74);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(17, 12);
            this.label118.TabIndex = 41;
            this.label118.Text = "：";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(14, 73);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(47, 12);
            this.label119.TabIndex = 39;
            this.label119.Text = "换天点:";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(86, 69);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(26, 21);
            this.textBox11.TabIndex = 38;
            this.textBox11.Text = "11";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(15, 196);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(35, 12);
            this.label120.TabIndex = 37;
            this.label120.Text = "密码:";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(15, 164);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(47, 12);
            this.label121.TabIndex = 33;
            this.label121.Text = "用户名:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(85, 193);
            this.textBox12.Name = "textBox12";
            this.textBox12.PasswordChar = '*';
            this.textBox12.Size = new System.Drawing.Size(73, 21);
            this.textBox12.TabIndex = 36;
            this.textBox12.Text = "scale";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(86, 130);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 21);
            this.textBox13.TabIndex = 34;
            this.textBox13.Text = "127.0.0.1";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(14, 133);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(47, 12);
            this.label122.TabIndex = 32;
            this.label122.Text = "中心站:";
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(15, 16);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(72, 16);
            this.checkBox20.TabIndex = 31;
            this.checkBox20.Text = "是否上传";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(14, 42);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(71, 12);
            this.label123.TabIndex = 30;
            this.label123.Text = "数据库密码:";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(86, 38);
            this.textBox14.Name = "textBox14";
            this.textBox14.PasswordChar = '*';
            this.textBox14.Size = new System.Drawing.Size(100, 21);
            this.textBox14.TabIndex = 29;
            this.textBox14.Text = "scale";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 467);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.btnModfiy);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "中心站数据上传软件  版本v4.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textHZClass;
        private System.Windows.Forms.TextBox textHZMin;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textHZHour;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textHZPass;
        private System.Windows.Forms.TextBox textHZUser;
        private System.Windows.Forms.TextBox textHZStation;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox checkHZUp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textHZDataPass;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textDZClass;
        private System.Windows.Forms.TextBox textDZMin;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textDZHour;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textDZPass;
        private System.Windows.Forms.TextBox textDZUser;
        private System.Windows.Forms.TextBox textDZStation;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox checkDZUp;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textDZDataPass;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textHAClass;
        private System.Windows.Forms.TextBox textHAMin;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textHAHour;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textHAPass;
        private System.Windows.Forms.TextBox textHAUser;
        private System.Windows.Forms.TextBox textHAStation;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox checkHAUp;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textHADataPass;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textHCClass;
        private System.Windows.Forms.TextBox textHCMin;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textHCHour;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textHCPass;
        private System.Windows.Forms.TextBox textHCUser;
        private System.Windows.Forms.TextBox textHCStation;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.CheckBox checkHCUp;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textHCDataPass;
        private System.Windows.Forms.ListBox lbShowInfo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnModfiy;
        private System.Windows.Forms.Timer timerWrite;
        private System.Windows.Forms.CheckBox checkDev3;
        private System.Windows.Forms.CheckBox checkDev2;
        private System.Windows.Forms.CheckBox checkDev1;
        private System.Windows.Forms.CheckBox checkDev0;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkDev7;
        private System.Windows.Forms.CheckBox checkDev6;
        private System.Windows.Forms.CheckBox checkDev5;
        private System.Windows.Forms.CheckBox checkDev4;
        private System.Windows.Forms.CheckBox checkDev9;
        private System.Windows.Forms.CheckBox checkDev8;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textSCLClass;
        private System.Windows.Forms.TextBox textSCLMin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textSCLHour;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textSCLPass;
        private System.Windows.Forms.TextBox textSCLUser;
        private System.Windows.Forms.TextBox textSCLStation;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox checkSCLUp;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textSCLDataPass;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textHfyFB100Class;
        private System.Windows.Forms.TextBox textHfyFB100Min;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textHfyFB100Hour;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textHfyFB100Pass;
        private System.Windows.Forms.TextBox textHfyFB100User;
        private System.Windows.Forms.TextBox textHfyFB100Station;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.CheckBox checkHfyFB100Up;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textHfyFB100DataPass;
        private System.Windows.Forms.Button btnHzcChange;
        private System.Windows.Forms.Button btnSclChange;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textHMClass;
        private System.Windows.Forms.TextBox textHMMin;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textHMHour;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textHMPass;
        private System.Windows.Forms.TextBox textHMUser;
        private System.Windows.Forms.TextBox textHMStation;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.CheckBox checkHMUp;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textHMDataPass;
        private System.Windows.Forms.CheckBox checkHCCheck0;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textSFYBan;
        private System.Windows.Forms.TextBox textSFYMin;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textSFYHour;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textSHYPassWord;
        private System.Windows.Forms.TextBox textSFYUser;
        private System.Windows.Forms.TextBox textSFYStation;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.CheckBox checkSFYUP;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textSFYPass;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textLMQBan;
        private System.Windows.Forms.TextBox textLMQMin;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox textLMQHour;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox textLMQPass;
        private System.Windows.Forms.TextBox textLMQUser;
        private System.Windows.Forms.TextBox textLMQStation;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.CheckBox checkLMQUP;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textLMQDataPass;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox textQhfyBan;
        private System.Windows.Forms.TextBox textQhfyMin;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox textQhfyHour;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox textQhfyPass;
        private System.Windows.Forms.TextBox textQhfyUser;
        private System.Windows.Forms.TextBox textQhfyStation;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.CheckBox checkQhfyUP;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox textQhfyDataPass;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox textQsfyBan;
        private System.Windows.Forms.TextBox textQsfyMin;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox textQsfyHour;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox textQsfyPass;
        private System.Windows.Forms.TextBox textQsfyUser;
        private System.Windows.Forms.TextBox textQsfyStation;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.CheckBox checkQsfyUP;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox textQsfyDataPass;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox textQjgcBan;
        private System.Windows.Forms.TextBox textQjgcMin;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox textQjgcHour;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox textQjgcPass;
        private System.Windows.Forms.TextBox textQjgcUser;
        private System.Windows.Forms.TextBox textQjgcStation;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.CheckBox checkQjgcUP;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox textQjgcDataPass;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox textKFwhBan;
        private System.Windows.Forms.TextBox textKFwhMin;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox textKFwhHour;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox textKFwhPass;
        private System.Windows.Forms.TextBox textKFwhStation;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.CheckBox checkKfwhUP;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox textKFwhDataPass;
        private System.Windows.Forms.TextBox textKFwhUser;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.CheckBox checkBoxSclVer;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textXJcarUser;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox textXJcarBan;
        private System.Windows.Forms.TextBox textXJcarMin;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox textXJcarHour;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox textXJcarPass;
        private System.Windows.Forms.TextBox textXJcarStation;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.CheckBox checkXJcarUP;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox textXJcarDataPass;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.CheckBox checkCar8;
        private System.Windows.Forms.CheckBox checkCar7;
        private System.Windows.Forms.CheckBox checkCar6;
        private System.Windows.Forms.CheckBox checkCar5;
        private System.Windows.Forms.CheckBox checkCar4;
        private System.Windows.Forms.CheckBox checkCar3;
        private System.Windows.Forms.CheckBox checkCar2;
        private System.Windows.Forms.CheckBox checkCar1;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TextBox textHNUser;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.TextBox textHNDay;
        private System.Windows.Forms.TextBox textHNMin;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.TextBox textHNHour;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.TextBox textHNPass;
        private System.Windows.Forms.TextBox textHNStation;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.CheckBox checkHNUP;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TextBox textHNDataPass;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkAlertSound;
        private System.Windows.Forms.CheckBox checkDev11;
        private System.Windows.Forms.CheckBox checkDev10;

    }
}

