﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
namespace StationUp
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool bCreate;
            Mutex mu = new Mutex(true, "stationup.exe", out bCreate);//防止程序重复运行
            if (bCreate)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FormMain());
                mu.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("程序已启动!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
    }
}