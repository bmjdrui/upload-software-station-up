using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
namespace StationUp
{
    class DataDzcClass:DataOption
    {
        #region 数据初始化
        string strAddress = null, strPass = null;
        /// <summary>
        /// 电子秤数据库地址
        /// </summary>
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        /// <summary>
        /// 电子秤密码
        /// </summary>
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        /// <summary>
        /// 电子称初始化
        /// </summary>
        /// <param name="nUnitID">编号</param>
        /// <param name="strName"></param>
        /// <param name="strCheckDev"></param>
        /// <returns></returns>
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            
            int dzcDevScale = 0,nTest=0;
            string dzcDevName = null,strComm=null;
            nUnitID=base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("电子秤GetInitPara函数错误号为： 80090");
                return -1;
            }
            try
            {
                OleDbDataReader dzcReader = GetDzcDynData(AddressMachine, PassMachine);
                if (dzcReader == null)
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                while (dzcReader.Read())
                {

                    dzcDevName = Convert.ToString(dzcReader["秤名"]);
                    dzcDevScale = Convert.ToInt32(dzcReader["秤号"]);
                    if (strCheckDev != null)
                    {
                        if (strCheckDev.Substring(dzcDevScale, 1) == "1")//选中的不传输
                            continue;
                    }
                    dzcDevScale = nUnitID * 100 + dzcDevScale;//计算编号
                    nTest = SelectData(2, nUnitID, strName, dzcDevScale, dzcDevName);//第三个参数暂时不用 通过秤名字和矿区ID号判断
                    if (nTest == -1)//
                    {
                        this.WriteLog("读取SQL SERVER数据库错误 错误号:92");
                        return -3;
                    }
                    
                    if (nTest == 1)//有数据
                    {
                        strComm = "update  s_device set DeviceName='" + dzcDevName + "' where DeviceID=" + dzcDevScale + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }

                    if (nTest == 0)//无数据
                    {
                        
                        strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + dzcDevScale + ",'" + dzcDevName + "'," + nUnitID + ",1,1,1,2)";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }
                    //***************************************
                }
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DataDzcClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
#endregion

        #region 电子称数据初始化
        /// <summary>
        /// 得到电子称数据库的链接
        /// </summary>
        /// <param name="strAddressMachine">地址</param>
        /// <param name="strPassMachine">密码</param>
        /// <returns>链接对象</returns>
        private OleDbConnection GetDzcDynConnection(string strAddressMachine, string strPassMachine)
        {
            string strPath = "\\\\";
            strPath += strAddress;
            strPath += "\\mdb";
            strPath += "\\netdata.mdb";
            strPath = @"C:\KJD-N型电子秤计量监控系统软件\mdb\netdata.mdb";
            string strDzcConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";
            try
            {
                OleDbConnection dzcConn = new OleDbConnection(strDzcConn);
                if (dzcConn.State == ConnectionState.Closed)
                    dzcConn.Open();
                return dzcConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHzcConnection 函数 " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 得到电子称班产数据库的链接
        /// </summary>
        /// <param name="strAddressMachine"></param>
        /// <param name="strPassMachine"></param>
        /// <returns></returns>
        private OleDbConnection GetDzcBanConnection(string strAddressMachine, string strPassMachine)
        {
            string strPath = "\\\\";
            strPath += strAddress;
            strPath += "\\mdb";
            strPath += "\\para.mdb";
            strPath = @"C:\KJD-N型电子秤计量监控系统软件\mdb\para.mdb";
            string strDzcConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";
            try
            {
                OleDbConnection dzcConn = new OleDbConnection(strDzcConn);
                if (dzcConn.State == ConnectionState.Closed)
                    dzcConn.Open();
                return dzcConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHzcConnection 函数 " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 获取电子称的数据
        /// </summary>
        /// <param name="strAddressMachine">地址</param>
        /// <param name="strPassMachine">密码</param>
        /// <returns>数据对象</returns>
        private OleDbDataReader GetDzcDynData(string strAddressMachine, string strPassMachine)
        {
            try
            {
                string strComm = "select * from tblnet order by 秤号";
                OleDbCommand dzcComm = new OleDbCommand(strComm, GetDzcDynConnection(strAddressMachine, strAddressMachine));
                OleDbDataReader dzcReader = dzcComm.ExecuteReader();
                return dzcReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetDzcDynData 函数 " + ex.ToString());
                return null;
            }
        }
        // <summary>
        /// 得到电子秤的班产量信息
        /// </summary>
        /// <param name="strOldTime">时间</param>
        /// <returns>OleDbDataReader数据集</returns>
        private OleDbDataReader GetDzcOutput(string strOldTime, string dzcAddress, string dzcPass)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                DateTime newDT = DateTime.Now.AddDays(1);
                string strComm = "SELECT * FROM tblban WHERE 日期 >= #" + oldDT.ToString("yyyy-MM-dd") + "# order by 日期";
                //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                OleDbCommand dzcComm = new OleDbCommand(strComm, GetDzcBanConnection(dzcAddress, dzcPass));
                OleDbDataReader dzcReader = dzcComm.ExecuteReader();
                dzcComm.Dispose();
                return dzcReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetDzcOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region 电子秤数据写入到服务器
        /// <summary>
        /// 向服务器中写动态数据
        /// </summary>
        /// <param name="dzcID">矿区编号</param>
        /// <returns>错误编号</returns>
        public int DzcWriteDataDyn(int nUnitID, string dzcAddress, string dzcPass, string strCheckDev)
        {
            try
            {
                string strComm = null, dzcDevName = null, dzcState = null; ;
                int dzcDevScale = 0;
                float dzcSpeed = 0, dzcBurthen = 0, dzcFlow = 0, dzcFta = 0, dzcDay=0,dzcFma = 0, dzcSignel = 0;
                float dzcChangeValue = 0;
                dzcChangeValue = 1f;//this.GetChangeValue();
                OleDbDataReader dzcReader = GetDzcDynData(dzcAddress, dzcPass);
                if (dzcReader == null)
                {
                    this.WriteLog("读取电子秤数据库错误 错误号:94");
                    return 80094;
                }
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                while (dzcReader.Read())
                {
                    dzcDevScale = Convert.ToInt32(dzcReader["秤号"]);
                    if (strCheckDev != null)
                    {
                        if (strCheckDev.Substring(dzcDevScale, 1) == "1")//选中的不传输
                            continue;
                    }
                    dzcDevName = Convert.ToString(dzcReader["秤名"]);
                    dzcSignel =0;//信号
                    float fSpeed = Convert.ToSingle(dzcReader["速度"]);
                    if (fSpeed>0)//状态
                    {
                        dzcSpeed = fSpeed;
                        dzcState = "开机";
                    }
                    else
                    {
                        dzcSpeed = 0;
                        dzcState = "停机";
                    }
                    dzcBurthen = 0;//负荷
                    dzcFlow = Convert.ToSingle(dzcReader["流量"]);
                    if (strCheckDev != null)
                    {
                        if (strCheckDev.Substring(8, 1) == "1")
                        {
                            dzcFta = Convert.ToSingle(dzcReader["班累计"]) * dzcChangeValue;//系数
                            dzcFma = Convert.ToSingle(dzcReader["月累计"]) * dzcChangeValue;//系数
                            dzcDay = Convert.ToSingle(dzcReader["日累计"]) * dzcChangeValue;//系数
                        }
                        else
                        {
                            dzcFta = Convert.ToSingle(dzcReader["班累计"]);//系数
                            dzcFma = Convert.ToSingle(dzcReader["月累计"]);//系数
                            dzcDay = Convert.ToSingle(dzcReader["日累计"]);//系数

                        }
                    }
                    else
                    {
                        dzcFta = Convert.ToSingle(dzcReader["班累计"]);//系数
                        dzcFma = Convert.ToSingle(dzcReader["月累计"]);//系数
                        dzcDay = Convert.ToSingle(dzcReader["日累计"]);//系数
                    }
                                     

                    //**************************更新dyn表****************************
                    dzcDevScale = nUnitID * 100 + dzcDevScale;//电子秤的编号
                    int nTest = SelectData(0, nUnitID, null, dzcDevScale, dzcDevName);//    
                    if (nTest == -1)
                        return 80095;
                    if (nTest == 1)
                        strComm = "update s_JLDyn set CurDT='" + DateTime.Now.ToString() + "',Speed=" + dzcSpeed + ",Burthen=" + dzcBurthen + ",Flow=" + dzcFlow + ",ClassOP=" + dzcFta + ",DayOP="+dzcDay+",MonthOP=" + dzcFma + ",Remark='" + dzcState + "' where DeviceID=" + dzcDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_JLDyn (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + dzcDevScale + ",'" + DateTime.Now.ToString() + "'," + dzcSignel + " ," + dzcSpeed + "," + dzcBurthen + "," + dzcFlow + ",0,0,0," + dzcFta + ","+dzcDay+"," + dzcFma + ",0,'" + dzcState + "') ";
                    this.WriteLog("dzcWriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    //*************************添加到历史表****************************************
                    strComm = "insert into s_JLHis (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Remark) Values(" + nUnitID + "," + dzcDevScale + ",'" + DateTime.Now.ToString() + "'," + dzcSignel + " ," + dzcSpeed + "," + dzcBurthen + "," + dzcFlow + ",0,0,0," + dzcFta + "," + dzcDay + "," + dzcFma + ",0,'" + dzcState + "') ";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                if(conn.State==ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                sqlComm.Dispose();
                dzcReader.Close();
                dzcReader.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("dzcWriteDataDyn 函数 " + ex.ToString());
                return 80096;
            }

        }
        /// <summary>
        /// 向服务器写电子秤的班产量
        /// </summary>
        /// <param name="dzcID">矿区编号</param>
        /// <returns></returns>
        public int DzcWriteDataOutput(int nUnitID, string strAddress, string strPass, int nHour, int nMin, string strCheckDev, System.Windows.Forms.ListBox lb)
        {
            try
            {
                string strComm = null;
                float dzcOut1 = 0,  dzcTotal = 0;
                int dzcDevScale = 0, nTestData = 0;
                float[,] dzcOutList = new float[8, 5];//班产
                int[] nScaleList = new int[8];//秤号
                string [] dtList = new string[8];//日期
                int nScale = 0, nBanID=0;//秤号 班次

                string strCurDT = null;
                string[] strInfo = new string[2];
                float dzcChangeValue = 0;

                dzcChangeValue = 1f;// this.GetChangeValue();
                strInfo = this.GetBanDataInfo(3);//得到时间 以及是否上传过
                this.WriteLog("dzcDT" + strInfo[0]);
                this.WriteLog(strInfo[1]);

                //bool bdzcHour = (DateTime.Now.Hour == nHour);
                //bool bdzcMin = (DateTime.Now.Minute == nMin);
                //if (!(bdzcHour && bdzcMin))
                //    return 0;


                OleDbDataReader dzcReader = GetDzcOutput(strInfo[0], strAddress, strPass);
                if (dzcReader == null)//有错误
                {
                    this.WriteLog("读取电子秤数据库错误 错误号:97");
                    return 80097;
                }
                if (!dzcReader.HasRows)//无数据
                    return 0;
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                while (dzcReader.Read())
                {

                    this.WriteLog("dzcWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                    nScale = Convert.ToInt32(dzcReader["秤号"]);
                    nScaleList[nScale] = nScale;//秤号数组赋值
                    if (strCheckDev != null)
                    {
                        if (strCheckDev.Substring(dzcDevScale, 1) == "1")//选中的不传输
                            continue;
                    }
                    dzcDevScale = nUnitID * 100 + nScale;
                    if(strCurDT!=dzcReader["日期"].ToString())
                    {
                       
                        for (int i = 0; i < 8; i++)
                        {
                            for(int m=0;m<5;m++)
                                dzcOutList[i,m]=0;
                        }
                    }
                    strCurDT = dzcReader["日期"].ToString();
                    dtList[nScale] = strCurDT;
                    nBanID = Convert.ToInt32(dzcReader["班次"].ToString());
                    
                    //**************************************
                    //统一格式化
                    
                    //**************************************
                    
                    if (dzcReader["班产量"].GetType().Name == "DBNull")
                        dzcOut1 = 0f;
                    else
                    {
                        if (strCheckDev != null)
                        {
                            if (strCheckDev.Substring(9, 1) == "1")//若果选中乘系数
                                dzcOut1 = Convert.ToSingle(dzcReader["班产量"]) * dzcChangeValue;
                            else
                                dzcOut1 = Convert.ToSingle(dzcReader["班产量"]);
                        }
                        else
                        {
                                dzcOut1 = Convert.ToSingle(dzcReader["班产量"]);
                        }
                    }
                    dzcOutList[nScale,nBanID] = dzcOut1;//赋值班产量给数组
                    //***************************************
                    nTestData = this.SelectData(6, nUnitID, null, dzcDevScale, strCurDT);//监测数据库中是否有数据
                    if (nTestData == 1)//更新数据
                    {
                        dzcTotal = dzcOutList[nScale, 1] + dzcOutList[nScale, 2] + dzcOutList[nScale, 3] + dzcOutList[nScale, 4];
                        strComm = "update s_JLOUT set Class1OP=" + dzcOutList[nScale, 1] + ",Class2OP =" + dzcOutList[nScale, 2] + ",Class3OP=" + dzcOutList[nScale, 3] + ",Class4OP=" + dzcOutList[nScale, 4] + ",Total=" + dzcTotal + " where DeviceID=" + dzcDevScale + " and curDt='" + strCurDT + "'";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.SetBanDataInfo(3, dtList[nScale]);//保存时间到注册表

                    }
                    else//插入数据
                    {
                        dzcTotal = dzcOutList[nScale, 1] + dzcOutList[nScale, 2] + dzcOutList[nScale, 3] + dzcOutList[nScale, 4];
                        strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + dzcDevScale + ",'" + dtList[nScale] + "'," + dzcOutList[nScale, 1] + "," + dzcOutList[nScale, 2] + "," + dzcOutList[nScale, 3] + "," + dzcOutList[nScale, 4] + "," + dzcTotal + ") ";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.SetBanDataInfo(3, dtList[nScale]);//保存时间到注册表
                    }
                        

                    //***************************************
                   
                }
                //写入中心数据库
                /*for (int i = 0; i >= 7; i++)
                {
                    if (dtList != null)
                    {
                        dzcTotal = dzcOutList[i,1] + dzcOutList[i,2] + dzcOutList[i,3] + dzcOutList[i,4];
                        strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + nScaleList[i] + ",'" + dtList[i] + "'," + dzcOutList[i,1] + "," + dzcOutList[i,2] + "," + dzcOutList[i,3] + "," + dzcOutList[i,4] + "," + dzcTotal + ") ";

                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.SetBanDataInfo(0, dtList[i]);//保存时间到注册表
                    }
                    
                }*/


                    //****************************************************
                sqlComm.Dispose();
                if(conn.State==ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                dzcReader.Close();
                dzcReader.Dispose();
                lb.Items.Add("电子秤数据正在发送班产量数据 ");
                this.WriteLog("电子秤数据正在发送班产量数据 " + " " + DateTime.Now.ToString());
                // }

                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("错误 函数 " + ex.Message);

                this.WriteLog("DZCWriteDataOutput 函数 " + ex.ToString());
                return 80098;
            }
        }
        #endregion

    }

}
