using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.Win32;
namespace StationUp
{
    /// <summary>
    /// 2000A型
    /// </summary>
    class DataHfyAClass : DataOption
    {
        #region 数据初始化
        string strAddress = null, strPass = null;

        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="strName">矿区名称</param>
        /// <param name="strCheckDev">是否传输</param>
        /// <returns>矿区最新编号</returns>
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int nTest = 0,nCurUnitID=0;
            string strComm = null,strhfyName=null;

            nUnitID=base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("2000A灰分仪GetInitPara函数错误号为：401");
                return -1;
            }
            try
            {
                strhfyName = strName + "灰分仪";
                nTest = SelectData(4, nUnitID, null, nUnitID*100+50, null);//第三个参数暂时不用 通过秤nUnitID*100+50判断
                if (nTest == -1)//
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                nCurUnitID = nUnitID * 100 + 50;
                if (nTest == 1)//有数据
                {
                    strComm = "update  s_device set DeviceName='" + strhfyName + "' where DeviceID=" + nCurUnitID + "";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();   
                }

                if (nTest == 0)//无数据
                {
                    strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + nCurUnitID + ",'" + strhfyName + "'," + nUnitID + ",2,1,1,2)";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DataHfyAClass下的GetInitPara函数" + ex.Message);
                return -4;
            }
        }
        #endregion

        #region 将2000A数据写入到sql server服务器
        /// <summary>
        /// 将2000A数据写入数据库
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="hzcAddress">中心站</param>
        /// <param name="hzcPass">数据库密码</param>
        /// <returns>正常为0 不正常为5开头的序列</returns>
        public int HFYWriteDataDyn(int nUnitID, string hzcAddress, string hzcPass)
        {
            int nTest = 0;
            string strComm = null;
            string [] strHfyDataAvg=new string[9];
            float hfyMinAvg1 = 0, hfyMinAvg2 = 0, hfyHourAvg1 = 0, hfyHourAvg2 = 0, hfyDayAvg1 = 0, hfyDayAvg2 = 0, hfyMin1 = 0, hfyMin2 = 0;
            try
            {
                strHfyDataAvg = GetHfyDynAvgData(null,hzcAddress, hzcPass);
                if (strHfyDataAvg == null)
                    return 800404;
                DateTime curHfyDT = Convert.ToDateTime(strHfyDataAvg[0]);
                string strCurDT = curHfyDT.ToString("yyyy-MM-dd HH:mm:ss");
                hfyMin1 = Convert.ToSingle(strHfyDataAvg[1]);
                hfyMin2 = Convert.ToSingle(strHfyDataAvg[2]);
                hfyMinAvg1 = Convert.ToSingle(strHfyDataAvg[3]);
                hfyMinAvg2 = Convert.ToSingle(strHfyDataAvg[4]);
                hfyHourAvg1 = Convert.ToSingle(strHfyDataAvg[5]);
                hfyHourAvg2 = Convert.ToSingle(strHfyDataAvg[6]);
                hfyDayAvg1 = Convert.ToSingle(strHfyDataAvg[7]);
                hfyDayAvg2 = Convert.ToSingle(strHfyDataAvg[8]);
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                //查看是否有记录灰分以用矿区编号*100 直接对应设备编号
                nTest = this.SelectData(3, nUnitID, null, nUnitID * 100+50, null);
                int nCurUnitID = nUnitID * 100 + 50;
                if (nTest == -1)
                    return 800405;
                if (nTest == 0)
                {
                    strComm = "insert into S_HFYDyn (UnitID,DeviceID,CurDT,CsSignal,AmSignal,LNR,Cs,Am,Slope,Intercept,hui,minutehui,Tenhui,hourhui,banhui,dayhui) Values(" + nUnitID + "," + nCurUnitID + ",'" + strCurDT + "',0,0,0,0,0,0,0," + hfyMin1 + "," + hfyMin1 + " ," + hfyMinAvg1 + "," + hfyHourAvg1 + ",0," + hfyDayAvg1 + ") ";
                }
                if (nTest == 1)
                {
                    strComm = "update S_HFYDyn set CurDT='" + strHfyDataAvg[0] + "',CsSignal=0,AmSignal=0,LNR=0,Cs=0,Am=0,Slope=0,Intercept=0,hui=" + hfyMin1 + ",minutehui=" + hfyMin1+ ",Tenhui=" + hfyMinAvg1 + ",hourhui=" + hfyHourAvg1 + ",Dayhui=" + hfyDayAvg1 + " where DeviceID=" + nCurUnitID + "";
                  
                }
                this.WriteLog("HFYWriteDataDyn 函数--"+strComm);
                sqlComm.CommandText = strComm;
                sqlComm.ExecuteNonQuery();
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("HFYWriteDataDyn 函数 " + ex.ToString());
                return 800406;
            }
        }
        /// <summary>
        /// 向sql server 数据库中写入历史数据（断点续传）
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="hfyAddress">中心站</param>
        /// <param name="hfyPass">密码</param>
        /// <returns></returns>
        public int HFYWriteDataHis(int nUnitID, string hfyAddress, string hfyPass)
        {
            try
            {
                int nTestData = 0;
                DateTime curDt = DateTime.Now;
                float hfyMin = 0;
                string[] strInfo = new string[2];
                string strComm = null;
                strInfo = this.GetBanDataInfo(1);//得到时间 以及是否上传过 参数1 为灰分仪
                DateTime saveDT = Convert.ToDateTime(strInfo[0]);
                DateTime nowDt=DateTime.Now;
                int nOne = Convert.ToInt32(nowDt.ToString("dd"));
                int nTwo = Convert.ToInt32(saveDT.ToString("dd"));
                int ndays=nOne - nTwo+1;
                int nSubDays = 0;
                while (ndays!=nSubDays)//当网络连接跨天时，在不同数据库中获取数据
                {
                    string strSaveDT=null;
                    if (nSubDays > 0)//如果跨天time从0点开始计
                    {
                        strSaveDT = saveDT.AddDays(nSubDays).ToString("yyyy-MM-dd 00:00:01");// saveDT.ToString("yyyy-MM-dd 00:00:01");
                    }
                    else
                    {
                        strSaveDT = saveDT.AddDays(nSubDays).ToString();
                    }
                    OleDbDataReader hfyReader = GetHfyHisData(strSaveDT, hfyAddress, hfyPass);
                    if (hfyReader == null)
                        return 407;
                    SqlConnection sqlConn = new SqlConnection();
                    sqlConn = GetConnection();
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.Connection = sqlConn;
                    int nCurUnitID = nUnitID * 100 + 50;
                    while (hfyReader.Read())
                    {
                        if (hfyReader["灰1"].GetType().Name == "DBNull")
                            hfyMin = 0;
                        else
                            hfyMin = Convert.ToSingle(hfyReader["灰1"]);
                        curDt = Convert.ToDateTime(hfyReader["日期时间"]);
                        nTestData = SelectData(5, nUnitID, null, nCurUnitID, curDt.ToString());//测试与数据库是否匹配
                        this.WriteLog("HFYWriteDataHis 函数--测试数据库服务器是否有当前数据" + nTestData.ToString());
                        if (nTestData == 1)
                            continue;
                        strComm = "insert into s_HFYOut (UnitID,DeviceID,CurDT,hui,minutehui,Tenhui,hourhui,banhui,dayhui,Measure,DTBetween) Values(" + nUnitID + "," + nCurUnitID + ",'" + curDt + "'," + hfyMin + "," + hfyMin + " ,0,0,0,0,0,0) ";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.SetBanDataInfo(1,curDt.ToString());//保存时间到注册表2000A型
                    }
                    this.WriteLog("HFYWriteDataHis 函数--注册表保存时间hfyADT"+curDt.ToString());
                    sqlComm.Dispose();
                    hfyReader.Close();
                    hfyReader.Dispose();
                    sqlConn.Close();
                    sqlConn.Dispose();
                    nSubDays++;//增加天数累计
                    
                }
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("HFYWriteDataHis 函数 "+ex.ToString());
                return 800408;
            }

        }
        #endregion

        #region 获取获取2000A数据
        /// <summary>
        /// 得到2000A型数据库的连接
        /// </summary>
        /// <param name="strPass">密码</param>
        /// <returns></returns>
        private OleDbConnection GetHfyConnection(string strAddress, string strPass,DateTime dtName)
        {
            //string strPath = "\\\\";
            //strPath += strAddress;
            //strPath += "\\转换";
            string strPath = @"C:\数据存储3\转换";
            strPath += "\\"+"sd"+dtName.ToString("yyyyMMdd")+".mdb";
            string strHzcConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";
            try
            {
                OleDbConnection hzcConn = new OleDbConnection(strHzcConn);
                if (hzcConn.State == ConnectionState.Closed)
                    hzcConn.Open();
                return hzcConn;
            }
            catch(Exception ex)
            {
                this.WriteLog("GetHfyConnection 函数 " + ex.ToString());
                return null;
            }
        }

         /// <summary>
        /// 得到2000A的灰分数据
        /// </summary>
        /// <param name="strOldTime"></param>
        /// <returns></returns>
        private OleDbDataReader GetHfyHisData(string strOldTime, string hfyAddress, string hfyPass)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                DateTime newDT = DateTime.Now;

                //string strComm = "SELECT 日期时间,灰1 FROM 动态 WHERE 日期时间 between #" + oldDT.ToString() + "# and #" + newDT.ToString() + "# order by 日期时间";
                string strComm = "SELECT 日期时间,灰1 FROM 动态 WHERE 日期时间 > #" + oldDT.ToString() + "# order by 日期时间";
                //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                OleDbCommand hzcComm = new OleDbCommand(strComm, GetHfyConnection(hfyAddress, hfyPass, oldDT));
                OleDbDataReader hzcReader = hzcComm.ExecuteReader();
                hzcComm.Dispose();
                return hzcReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHfyHisData 函数 " + ex.ToString());
                return null;
            }

        }
        /// <summary>
        /// 获取2000a平均数据信息
        /// </summary>
        /// <param name="strOldTime">日期</param>
        /// <param name="hfyAddress">数据库地址</param>
        /// <param name="hfyPass">密码</param>
        /// <returns>分钟灰分1 2，十分钟灰分1 2，小时灰分1 2，天灰分1 2</returns>
        private string[] GetHfyDynAvgData(string strOldTime, string hfyAddress, string hfyPass)
        {
            float hfyTenMinAvg1 = 0, hfyTenMinAvg2 = 0, hfyHourAvg1 = 0, hfyHourAvg2 = 0, hfyDayAvg1 = 0, hfyDayAvg2 = 0, hfyMin1 = 0, hfyMin2 = 0,nAvg1 = 0, nAvg2 = 0, nAvgSum1 = 0;
            string [] strhfyAvg=new string[9];
            string strCurDt = null;
            int nCount = 0;
            try
            {
                OleDbConnection hfyConn = GetHfyConnection(hfyAddress, hfyPass, DateTime.Now);
                if (hfyConn == null)
                {
                    this.WriteLog("GetHfyConnection函数返回为空");
                    return null;
                }
                OleDbCommand hfyComm = new OleDbCommand();
                hfyComm.Connection = hfyConn;
                DateTime nowDT = DateTime.Now;
                //**************分钟灰分**************************************
                string strComm = "select top 1 * from 动态 order by 日期时间 desc";//倒序
                hfyComm.CommandText = strComm;
                OleDbDataReader hfyMinReader = hfyComm.ExecuteReader();
                while (hfyMinReader.Read())
                {
                    strCurDt = Convert.ToString(hfyMinReader["日期时间"]);
                    if (hfyMinReader["灰1"].GetType().Name == "DBNull")
                        hfyMin1 = 0F;
                    else
                        hfyMin1 = Convert.ToSingle(hfyMinReader["灰1"]);
                    if (hfyMinReader["灰2"].GetType().Name == "DBNull")
                        hfyMin2 = 0F;
                    else
                        hfyMin2 = Convert.ToSingle(hfyMinReader["灰2"]);
                }
                hfyMinReader.Close();
                hfyMinReader.Dispose();
                hfyConn.Close();
                //**************十分钟灰分*************************************
                DateTime startMinDT;
                //DateTime nowDT = DateTime.Now;
               
                startMinDT = nowDT.AddMinutes(-10);
                string strStart = startMinDT.ToString();
                string strNow = DateTime.Now.ToString();
                //string startMinDT = null;
                //string strTemp = startMinDT.ToString("yyyy-MM-dd HH:MM:ss");
                strComm = "SELECT 灰1,灰2 FROM 动态 WHERE 日期时间 between #" + strStart + "# and #" + strNow + "#";
                if (hfyConn.State == ConnectionState.Closed)
                    hfyConn.Open();
                hfyComm.CommandText = strComm;
                OleDbDataReader hfyMinAvgReader = hfyComm.ExecuteReader();
                while (hfyMinAvgReader.Read())
                {
                    if (hfyMinAvgReader[0].GetType().Name == "DBNull")
                        nAvg1 = 0F;
                    else
                        nAvg1 = Convert.ToSingle(hfyMinAvgReader[0]);
                    if (hfyMinAvgReader[1].GetType().Name == "DBNull")
                        nAvg2 = 0F;
                    else
                        nAvg2 = Convert.ToSingle(hfyMinAvgReader[1]);
                    if (nAvg1 >0)
                    {
                        nAvgSum1 += nAvg1;
                        nCount=nCount+1;
                    }
                    if (nCount == 0)
                        hfyTenMinAvg1 = 0;
                    else
                        hfyTenMinAvg1 = nAvgSum1 / nCount;

                }

                hfyMinAvgReader.Close();
                hfyMinAvgReader.Dispose();
                hfyConn.Close();
                this.WriteLog("GetHfyDynAvgData 函数 hfyMinAvg1--" + hfyTenMinAvg1.ToString());
                //****************小时灰分************************************
                DateTime startHourDt = nowDT.AddHours(-1);//小时灰分
                strComm = "SELECT 灰1,灰2 FROM 动态 WHERE 日期时间 between #" + nowDT.ToString("yyyy-MM-dd HH:mm:ss") + "# and #" + startHourDt.ToString("yyyy-MM-dd HH:mm:ss") + "#";
                if (hfyConn.State == ConnectionState.Closed)
                    hfyConn.Open();
                hfyComm.CommandText = strComm;
                OleDbDataReader hfyHourAvgReader = hfyComm.ExecuteReader();
                nAvgSum1 = 0;
                nCount = 0;
                while (hfyHourAvgReader.Read())
                {
                    if (hfyHourAvgReader[0].GetType().Name == "DBNull")
                        nAvg1 = 0F;
                    else
                        nAvg1 = Convert.ToSingle(hfyHourAvgReader[0]);
                    if (hfyHourAvgReader[1].GetType().Name == "DBNull")
                        nAvg2 = 0F;
                    else
                        nAvg2 = Convert.ToSingle(hfyHourAvgReader[1]);

                    if (nAvg1 > 0)
                    {
                        nAvgSum1 += nAvg1;
                        nCount = nCount + 1;
                    }
                    if (nCount == 0)
                        hfyHourAvg1 = 0;
                    else
                        hfyHourAvg1 = nAvgSum1 / nCount;//把0删除掉再平均
                }
                hfyHourAvgReader.Close();
                hfyHourAvgReader.Dispose();
                hfyConn.Close();
                this.WriteLog("GetHfyDynAvgData 函数 hfyHourAvg1--" + hfyTenMinAvg1.ToString());
                //***************天灰分*************************
                strComm = "SELECT 灰1,灰2 FROM 动态";
                if (hfyConn.State == ConnectionState.Closed)
                    hfyConn.Open();
                hfyComm.CommandText = strComm;
                OleDbDataReader hfyDayAvgReader = hfyComm.ExecuteReader();
                nAvgSum1 = 0;
                nCount = 0;
                while (hfyDayAvgReader.Read())
                {
                    if (hfyDayAvgReader[0].GetType().Name == "DBNull")
                        nAvg1 = 0F;
                    else
                        nAvg1 = Convert.ToSingle(hfyDayAvgReader[0]);
                    if (hfyDayAvgReader[1].GetType().Name == "DBNull")
                        nAvg2 = 0F;
                    else
                        nAvg2 = Convert.ToSingle(hfyDayAvgReader[1]);
                    if (nAvg1 > 0)
                    {
                        nAvgSum1 += nAvg1;
                        nCount = nCount + 1;
                    }
                    if (nCount == 0)
                        hfyDayAvg1 = 0;
                    else
                        hfyDayAvg1 = nAvgSum1 / nCount;//把0删除掉再平均
                }
                
                hfyDayAvgReader.Close();
                hfyDayAvgReader.Dispose();
                hfyConn.Close();
                strhfyAvg[0] = strCurDt;
                strhfyAvg[1] = hfyMin1.ToString();
                strhfyAvg[2] = hfyMin2.ToString();
                strhfyAvg[3] = hfyTenMinAvg1.ToString();
                strhfyAvg[4] = hfyTenMinAvg2.ToString();
                strhfyAvg[5] = hfyHourAvg1.ToString();
                strhfyAvg[6] = hfyHourAvg2.ToString();
                strhfyAvg[7] = hfyDayAvg1.ToString();
                strhfyAvg[8] = hfyDayAvg2.ToString();
                return strhfyAvg;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetHfyDynAvgData 函数 " + ex.ToString());
                return null;
            }

        }
        #endregion

        
    }
}
