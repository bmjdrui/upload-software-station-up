using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
namespace StationUp
{
    /// <summary>
    /// 防爆HFYFB100灰分仪
    /// </summary>
    class DataHfyFB100Class:DataOption
    {
        string strPathFB100= @"C:\Program Files\北京市煤炭矿用机电设备技术开发公司\斯凯尔-灰分仪监测系统平台\HFYData\";
        
       
        string[] m_strName = new string[10];
        float [] m_iHFUp = new float[10];
        float[] m_iHFDown = new float[10];
        int[] m_iDevID = new int[10];
        #region 数据初始化
        string strAddress = null, strPass = null;

        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        /// <summary>
        /// 初始化数据
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="strName">矿区名称</param>
        /// <param name="strCheckDev">是否传输</param>
        /// <returns>矿区最新编号</returns>
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int nTest = 0, nCurUnitID = 0, iDevID=0;
            string strComm = null, strhfyName = null;
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("2000A灰分仪GetInitPara函数错误号为：701");
                return -1;
            }
            try
            {
                OleDbCommand oleComm = new OleDbCommand();
                oleComm.CommandText = "select A.devname,A.isusing,A.devid,B.hfup,B.hfdown from h_device as A,h_para as B where A.devid=B.devid order by A.devid";
                OleDbConnection oleConn = this.GetHFYFB100ParaConnection(null, null, DateTime.Now, 0);//获取灰分仪名字 上下限
                oleComm.Connection = oleConn;
                OleDbDataReader srDev = oleComm.ExecuteReader();
                if (srDev == null)
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                while (srDev.Read())
                {
                    if (!Convert.ToBoolean(srDev["isusing"]))
                        continue;
                    iDevID = Convert.ToInt32(srDev["devid"]);
                    strhfyName = srDev["devname"].ToString();
                    m_iHFUp[iDevID] = Convert.ToSingle(srDev["hfUp"]);
                    m_iHFDown[iDevID] = Convert.ToSingle(srDev["hfDown"]);
                    nCurUnitID = nUnitID * 100 + 50 + m_iDevID[iDevID];
                    nTest = SelectData(4, nUnitID, null, nCurUnitID, null);//                    if (nTest == -1)//
                    if (nTest == -1)//
                    {
                        sqlComm.Dispose();
                        if (sqlComm.Connection.State == ConnectionState.Open)
                            sqlComm.Connection.Close();
                        sqlComm.Connection.Dispose();
                        return -3;
                    }
                   
                    if (nTest == 1)//有数据
                    {
                        strComm = "update  s_device set DeviceName='" + strhfyName + "' where DeviceID=" + nCurUnitID + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }

                    if (nTest == 0)//无数据
                    {
                        strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + nCurUnitID + ",'" + strhfyName + "'," + nUnitID + ",2,1,1,2)";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }
                   
                }
                sqlComm.Dispose();
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                srDev.Close();
                srDev.Dispose();
                oleComm.Dispose();
                oleConn.Close();
                oleConn.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("DataHfyAClass下的GetInitPara函数" + ex.Message);
                return -4;
            }
        }
        #endregion

        #region 获取防爆灰分仪数据
        /// <summary>
        /// 得到防爆灰分仪型数据库的连接
        /// </summary>
        /// <param name="strAddress">本地数据库地址</param>
        /// <param name="strPass">密码</param>
        /// <param name="dtName">时间</param>
        /// <param name="devID">灰分仪编号</param>
        /// <returns>数据连接</returns>
        private OleDbConnection GetHfyFB100Connection(string strAddress, string strPass, DateTime dtName,int devID)
        {

            string strPath = null;
            string strHfyFBConn = null;
            
            if (strAddress != "127.0.0.1")
            {
                strPath = strAddress;
                strPath += "\\"+"HFYMiddleData_" + DateTime.Now.ToString("yyyy") + "年" + DateTime.Now.ToString("MM") + "月" + ".mdb";;
            }
            else
            {
                strPath = strPathFB100 + "MiddleData\\HFYMiddleData_" + DateTime.Now.ToString("yyyy") + "年" + DateTime.Now.ToString("MM") + "月" + ".mdb";
            }
            try
            {
                //strPath = strPathFB100 + @"MiddleData\HFYMiddleData_" + devID.ToString() + ".mdb";
                
                strHfyFBConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";
                OleDbConnection hfyFB100Conn = new OleDbConnection(strHfyFBConn);
                if (hfyFB100Conn.State == ConnectionState.Closed)
                    hfyFB100Conn.Open();
                return hfyFB100Conn;
             }
            catch (Exception ex)
            {
                if(devID==0)
                    this.WriteLog("GetHfyConnection 函数 " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 获取参数数据库的链接
        /// </summary>
        /// <returns></returns>
        private OleDbConnection GetHFYFB100ParaConnection(string strAddress, string strPass, DateTime dtName, int devID)
        {
            string strPath = strPathFB100 + "HFYMonitorPlatform.mdb";
            string strHfyFBConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=null";
            OleDbConnection hfyFB100Conn = new OleDbConnection(strHfyFBConn);
            if (hfyFB100Conn.State == ConnectionState.Closed)
                hfyFB100Conn.Open();
            return hfyFB100Conn;
 
        }
        
        /// <summary>
        /// 得到防爆灰分仪的灰分数据
        /// </summary>
        /// <param name="strOldTime">时间</param>
        /// <param name="hfyAddress">地址</param>
        /// <param name="hfyPass">密码</param>
        /// <returns>本地数据集</returns>
        private OleDbDataReader GetHfyFB100HisData(string strOldTime, string hfyAddress, string hfyPass, int devID)
        {
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                DateTime newDT = DateTime.Now.AddMinutes(-2);

                string strComm = "SELECT RecordTime,MinuteAsh FROM HFYMiddleData WHERE (devid="+devID+")and (RecordTime between #" + oldDT.ToString() + "# and #" + newDT.ToString() + "#) order by RecordTime";
                //string strComm = "SELECT RecordTime,MinuteAsh FROM HFYMiddleData WHERE RecordTime > #" + oldDT.ToString() + "# order by RecordTime";
                //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                OleDbCommand hfyFB100Comm = new OleDbCommand(strComm, GetHfyFB100Connection(hfyAddress, hfyPass, oldDT,devID));
                OleDbDataReader hfyFB100Reader = hfyFB100Comm.ExecuteReader();
                hfyFB100Comm.Dispose();
                return hfyFB100Reader;
            }
            catch (Exception ex)
            {
                //this.WriteLog("DatahfyFB100Class下的GetHfyHisData 函数出现以下错误 " + ex.ToString());
                return null;
            }

        }
        
        #endregion

        #region 将防爆灰分仪写入到sql server服务器
        /// <summary>
        /// 将防爆灰分仪数据写入数据库
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="hzcAddress">中心站</param>
        /// <param name="hzcPass">数据库密码</param>
        /// <returns>正常为0 不正常为7开头的序列</returns>
        public int HFYFB100WriteDataDyn(int nUnitID, string hzcAddress, string hzcPass)
        {
            int nTest = 0,iDevID=0;
            bool bConn = false;
            string strComm = null;
            string[] strHfyDataAvg = new string[9];
            float  hfyMin1 = 0,hfyTenAvg1 = 0, hfyHourAvg1 = 0, hfyDayAvg1 = 0, hfyClassAvg1 = 0, hfyMonthAvg1 = 0;
            OleDbConnection hfyConn = GetHFYFB100ParaConnection(null , null, DateTime.Now, 0);
            if (hfyConn == null)
            {
                this.WriteLog("GetHfyFB100DynAvgData函数返回为空");
                return 800704;
            }
            OleDbCommand hfyComm = new OleDbCommand();
            hfyComm.Connection = hfyConn;
            DateTime nowDT = DateTime.Now;
            strComm = "select a.* ,b.isusing from h_dyndata as A,h_device as B where A.devid=B.devid";
            hfyComm.CommandText = strComm;
            OleDbDataReader hfyReader = hfyComm.ExecuteReader();
            try
            {
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
               while(hfyReader.Read())
                {
                    if (!Convert.ToBoolean(hfyReader["isusing"]))
                        continue;
                    //*******************************************************
                    DateTime curHfyDT = Convert.ToDateTime(hfyReader["recordtime"]);
                    string strCurDT = curHfyDT.ToString("yyyy-MM-dd HH:mm:ss");
                    iDevID = Convert.ToInt32(hfyReader["devid"]);
                    if (hfyReader["MinuteAsh"].GetType().Name == "DBNull")
                        hfyMin1 = 0F;
                    else
                        hfyMin1 = Convert.ToSingle(hfyReader["MinuteAsh"]);
                    if (hfyReader["TenMinuteAsh"].GetType().Name == "DBNull")
                        hfyTenAvg1 = 0F;
                    else
                        hfyTenAvg1 = Convert.ToSingle(hfyReader["TenMinuteAsh"]);

                    if (hfyReader["HourAsh"].GetType().Name == "DBNull")
                        hfyHourAvg1 = 0F;
                    else
                        hfyHourAvg1 = Convert.ToSingle(hfyReader["HourAsh"]);

                    if (hfyReader["ClassAsh"].GetType().Name == "DBNull")
                        hfyClassAvg1 = 0F;
                    else
                        hfyClassAvg1 = Convert.ToSingle(hfyReader["ClassAsh"]);

                    if (hfyReader["DayAsh"].GetType().Name == "DBNull")
                        hfyDayAvg1 = 0F;
                    else
                        hfyDayAvg1 = Convert.ToSingle(hfyReader["DayAsh"]);
                    if (hfyReader["MonthAsh"].GetType().Name == "DBNull")
                        hfyMonthAvg1 = 0F;
                    else
                        hfyMonthAvg1 = Convert.ToSingle(hfyReader["MonthAsh"]);
                    //*******************************************************
                    
                    //查看是否有记录灰分以用矿区编号*100 直接对应设备编号
                    int nCurUnitID = nUnitID * 100 + 50 + iDevID;
                    nTest = this.SelectData(3, nUnitID, null, nCurUnitID, null);

                    if (nTest == -1)
                    {
                        sqlComm.Dispose();
                        if (conn.State == ConnectionState.Open)
                            conn.Close();
                        conn.Dispose();
                        hfyReader.Close();
                        hfyReader.Dispose();
                        hfyComm.Dispose();
                        hfyConn.Close();
                        hfyConn.Dispose();
                        return 800705;
                    }
                    if (nTest == 0)
                    {
                        strComm = "insert into S_HFYDyn (UnitID,DeviceID,CurDT,CsSignal,AmSignal,LNR,Cs,Am,Slope,Intercept,hui,minutehui,Tenhui,hourhui,banhui,dayhui) Values(" + nUnitID + "," + nCurUnitID + ",'" + strCurDT + "',0,0,0,0,0,0,0," + hfyMin1 + "," + hfyMin1 + " ," + hfyTenAvg1 + "," + hfyHourAvg1 + ","+hfyClassAvg1+"," + hfyDayAvg1 + ") ";
                    }
                    if (nTest == 1)
                    {
                        strComm = "update S_HFYDyn set CurDT='" + strCurDT + "',CsSignal=0,AmSignal=0,LNR=0,Cs=0,Am=0,Slope=0,Intercept=0,hui=" + hfyMin1 + ",minutehui=" + hfyMin1 + ",Tenhui=" + hfyTenAvg1 + ",hourhui=" + hfyHourAvg1 + ",banhui=" + hfyClassAvg1 + ",Dayhui=" + hfyDayAvg1 + " where DeviceID=" + nCurUnitID + "";

                    }
                    this.WriteLog("HFYFB100WriteDataDyn 函数--" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    bConn = true;
                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                hfyReader.Close();
                hfyReader.Dispose();
                hfyComm.Dispose();
                hfyConn.Close();
                hfyConn.Dispose();
                if (bConn)//是否连接上本地数据库
                    return 0;
                else
                    return 800706;
            }
            catch (Exception ex)
            {
                this.WriteLog("HFYFB100WriteDataDyn 函数 " + ex.ToString());
                return 800707;
            }
        }
        /// <summary>
        /// 向sql server 数据库中写入历史数据（断点续传）
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="hfyAddress">中心站</param>
        /// <param name="hfyPass">密码</param>
        /// <returns></returns>
        public int HFYFB100WriteDataHis(int nUnitID, string hfyAddress, string hfyPass)
        {
            try
            {
                int nTestData = 0;
                bool bConn = false;
                DateTime curDt = DateTime.Now;
                float hfyMin = 0;
                string[] strInfo = new string[2];
                string strComm = null;
                strInfo = this.GetBanDataInfo(5);//得到时间 以及是否上传过 参数5 为防爆灰分仪
                DateTime saveDT = Convert.ToDateTime(strInfo[0]);
                DateTime nowDt = DateTime.Now;
                SqlConnection sqlConn = new SqlConnection();
                sqlConn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = sqlConn;
                for (int i = 0;i<8; i++)//在不同数据库中获取数据
                {
                    OleDbDataReader hfyReader = GetHfyFB100HisData(strInfo[0], hfyAddress, hfyPass,i);
                    if (hfyReader == null)
                        continue;
                    
                    int nCurUnitID = nUnitID * 100 + 50+i;//设备编号
                    while (hfyReader.Read())
                    {
                        if (hfyReader["MinuteAsh"].GetType().Name == "DBNull")
                            hfyMin = 0;
                        else
                            hfyMin = Convert.ToSingle(hfyReader["MinuteAsh"]);
                        curDt = Convert.ToDateTime(hfyReader["RecordTime"]);
                        nTestData = SelectData(5, nUnitID, null, nCurUnitID, curDt.ToString());//测试与数据库是否匹配
                        //this.WriteLog("HFYFB100WriteDataHis 函数--测试数据库服务器是否有当前数据" + nTestData.ToString());
                        if (nTestData == 1)
                            continue;
                        if ((hfyMin >= m_iHFUp[i]) || (hfyMin <= m_iHFDown[i]))//判断灰分仪的上下限 超过 不上传
                        {
                            this.WriteLog("第"+nCurUnitID.ToString()+"台灰分仪"+curDt.ToString() + "分钟灰分没有上传  该值为：" + hfyMin.ToString()+"上限为："+m_iHFUp[i]+"下限为："+m_iHFDown[i]);
                            this.SetBanDataInfo(5, curDt.ToString());//保存时间到注册表HFYFB100型
                            continue;
                        }
                        strComm = "insert into s_HFYOut (UnitID,DeviceID,CurDT,hui,minutehui,Tenhui,hourhui,banhui,dayhui,Measure,DTBetween) Values(" + nUnitID + "," + nCurUnitID + ",'" + curDt + "'," + hfyMin + "," + hfyMin + " ,0,0,0,0,0,0) ";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        this.SetBanDataInfo(5, curDt.ToString());//保存时间到注册表HFYFB100型
                    }
                    this.WriteLog("HFYFB100WriteDataHis 函数--注册表保存时间hfyADT" + curDt.ToString());
                    
                    hfyReader.Close();
                    hfyReader.Dispose();
                    bConn = true;
                  }
                  sqlComm.Dispose();
                  if (sqlConn.State == ConnectionState.Open)
                      sqlConn.Close();
                  sqlConn.Dispose();
                  if (bConn)//是否能连接上本地数据库
                      return 0;
                  else
                      return 800708;
            }
            catch (Exception ex)
            {
                this.WriteLog("HFYFB100WriteDataHis 函数 " + ex.ToString());
                return 800709;
            }

        }
        #endregion


    }
}
