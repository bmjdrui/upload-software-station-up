using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
namespace StationUp
{
    /// <summary>
    /// SCL监控系统平台
    /// </summary>
    class DataSCLClass:DataOption
    {
        #region 数据初始化
        string strAddress = null, strPass = null;
        bool bVer = true;//版本选择默认为最新的版本
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        public override int GetInitPara(int nUnitID, string strName, string strCheckDev)
        {
            int sclDevScale = 0, nTest = 0;
            string sclDevName = null, strComm = null;
            if (strCheckDev != null)
            {
                if (strCheckDev.Substring(0, 1) == "1")//是否为旧版本
                    bVer=false;
            }
            nUnitID = base.GetInitPara(nUnitID, strName, strCheckDev);
            if (nUnitID < 0)
            {
                this.WriteLog("SCL监控系统GetInitPara函数错误号为：101");
                return -1;
            }
            try
            {
                OleDbDataReader sclReader = GetSclDynData(AddressMachine, PassMachine,strCheckDev);
                if (sclReader == null)
                    return -2;
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = GetConnection();
                while (sclReader.Read())
                {
                    if (bVer)
                    {   //新版本
                        sclDevName = Convert.ToString(sclReader["DevName"]);
                        sclDevScale = Convert.ToInt32(sclReader["DevID"]);
                    }
                    else
                    {   //旧版本
                        sclDevName = Convert.ToString(sclReader["BalName"]);
                        sclDevScale = Convert.ToInt32(sclReader["BalNumber"]);
                    }
                    sclDevScale = nUnitID * 100 + sclDevScale;//计算编号
                    nTest = SelectData(2, nUnitID, strName, sclDevScale, sclDevName);//第三个参数暂时不用 通过秤名字和矿区ID号判断
                    if (nTest == -1)//
                        return -3;
                    if (nTest == 1)//有数据
                    {
                        strComm = "update  s_device set DeviceName='" + sclDevName + "' where DeviceID=" + sclDevScale + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }

                    if (nTest == 0)//无数据
                    {

                        //strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + sclDevScale + ",'" + sclDevName + "'," + nUnitID + ",1,1,1,2)";
                        strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,JLTypeID)values(" + sclDevScale + ",'" + sclDevName + "'," + nUnitID + ",1,1)";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }
                    //***************************************
                    
                }
                if (sqlComm.Connection.State == ConnectionState.Open)
                    sqlComm.Connection.Close();
                sqlComm.Connection.Dispose();
                sqlComm.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                WriteLog("SCL监控系统下的DataSclClass下GetInitPara 函数" + ex.ToString());
                return -4;
            }
        }
        #endregion

        #region 从access中获取核子秤的动态与班产量信息
        /// <summary>
        /// 得到SCL监控系统数据库DevData的连接
        /// </summary>
        /// <param name="strPass"></param>
        /// <returns></returns>
        private OleDbConnection GetSclOutConnection(string strAddress, string strPass,string strCheckDev)
        {
            string strPath = null;
            if (strCheckDev != null)
            {
                if (strCheckDev.Substring(0, 1) == "1")//是否为旧版本
                    bVer = false;
            }
            if (bVer)
            {   //新版本
                if (strAddress.Trim() != "127.0.0.1")
                {
                    strPath = strAddress;
                    strPath += "\\PDCData\\PDCBan.mdb";
                }
                else
                {
                    strPath = @"C:\Program Files\北京市煤炭矿用机电设备技术开发公司\SCL 生产监控综合管理系统\PDCData\PDCBan.mdb";
                }
            }
            else
            {
                //旧版本
                if (strAddress.Trim() != "127.0.0.1")
                {
                    strPath = strAddress;
                    strPath += "\\DevData.mdb";
                }
                else
                {
                    strPath = @"C:\Program Files\北京市煤炭矿用机电设备技术开发公司\SCL 生产监控综合管理系统\Data\DevData.mdb";
                }
            }
            this.WriteLog("数据库路径"+strPath);
            string strHzcConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";
            try
            {
                OleDbConnection sclConn = new OleDbConnection(strHzcConn);
                if (sclConn.State == ConnectionState.Closed)
                    sclConn.Open();
                return sclConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("SCL监控系统下的GetSclConnection 函数 " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        ///  得到SCL监控系统数据库netdata的连接
        /// </summary>
        /// <param name="strAddress">地址</param>
        /// <param name="strPass">密码</param>
        /// <returns>netdata数据库的连接</returns>
        private OleDbConnection GetSclDynConnection(string strAddress, string strPass,string strCheckDev)
        {
            string strPath = null;
            if (strCheckDev != null)
            {
                if (strCheckDev.Substring(0, 1) == "1")//是否为旧版本
                    bVer = false;
            }
            if (bVer)
            {  //新版本
                if (strAddress.Trim() != "127.0.0.1")
                {
                    strPath = strAddress;
                    strPath += "\\dyndata.mdb";
                }
                else
                {
                    strPath = @"C:\Program Files\北京市煤炭矿用机电设备技术开发公司\SCL 生产监控综合管理系统\DB\dyndata.mdb";
                }
            }
            else
            {   //旧版本
                if (strAddress.Trim() != "127.0.0.1")
                {
                    strPath = strAddress;
                    strPath += "\\netdata.mdb";
                }
                else
                {
                    strPath = @"C:\Program Files\北京市煤炭矿用机电设备技术开发公司\SCL 生产监控综合管理系统\data\netdata.mdb";
                }
                
            }
            this.WriteLog("数据库路径" + strPath);
                string strHzcConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Persist Security Info=False;Jet OLEDB:Database Password=" + strPass + "";
            try
            {
                OleDbConnection sclConn = new OleDbConnection(strHzcConn);
                if (sclConn.State == ConnectionState.Closed)
                    sclConn.Open();
                return sclConn;
            }
            catch (Exception ex)
            {
                this.WriteLog("SCL监控系统下的GetSclConnection 函数 " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 得到SCL监控系统的动态数据
        /// </summary>
        /// <param name="sclAddress">核子秤地址</param>
        /// <param name="sclPass">密码</param>
        /// <returns>数据集</returns>
        private OleDbDataReader GetSclDynData(string sclAddress, string sclPass,string strCheckDev)
        {
            if (strCheckDev != null)
            {
                if (strCheckDev.Substring(0, 1) == "1")//是否为旧版本
                    bVer = false;
            }
            string strComm = null;
            try
            {
                if (bVer)
                {
                    //新版本
                    strComm = "select * from pdcnetdata";
                }
                else
                {// 旧版本
                    strComm = "select * from tbltemp";
                }
                
                OleDbCommand sclComm = new OleDbCommand(strComm, GetSclDynConnection(sclAddress, sclPass,strCheckDev));
                OleDbDataReader sclReader = sclComm.ExecuteReader();
                return sclReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("Scl监控系统下的GetSclDynData 函数 " + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 得到SCL监控系统的班产量信息
        /// </summary>
        /// <param name="strOldTime"></param>
        /// <returns></returns>
        private OleDbDataReader GetSclOutput(string strOldTime, string sclAddress, string sclPass,string strCheckDev)
        {
            string strComm = null;
            if (strCheckDev != null)
            {
                if (strCheckDev.Substring(0, 1) == "1")//是否为旧版本
                    bVer = false;
            }
            try
            {
                DateTime oldDT = Convert.ToDateTime(strOldTime);
                //DateTime newDT = DateTime.Now.AddDays(1);
                if (bVer)
                {
                    //新版本
                    strComm = "SELECT * FROM Bantotal WHERE curdt > #" + oldDT.ToString("yyyy-MM-dd") + "# order by curdt";
                }
                else
                {
                    //旧版本
                    strComm = "SELECT * FROM tblBan WHERE dt > #" + oldDT.ToString("yyyy-MM-dd") + "# order by dt";
                }
                //  strComm = "select * from dataout where date between #2009-02-01# and #2009-10-01#";
                OleDbCommand sclComm = new OleDbCommand(strComm, GetSclOutConnection(sclAddress, sclPass,strCheckDev));
                OleDbDataReader sclReader = sclComm.ExecuteReader();
                sclComm.Dispose();
                return sclReader;
            }
            catch (Exception ex)
            {
                this.WriteLog("Scl监控系统下的GetSclOutput 函数" + ex.ToString());
                return null;
            }

        }
        #endregion

        #region SCL监控系统写入到服务器
        /// <summary>
        /// SCL监控系统向服务器中写动态数据
        /// </summary>
        /// <param name="sclID">矿区编号</param>
        /// <returns>错误编号</returns>
        public int SCLWriteDataDyn(int nUnitID, string sclAddress, string sclPass, string strCheckDev)
        {
            try
            {
                string strComm = null, sclDevName = null, sclState = null;
                string strMid=null;
                int sclDevScale = 0;
                float sclSpeed = 0, sclBurthen = 0, sclFlow = 0, sclFta = 0, sclDay=0,sclFma = 0, sclSignel = 0, sclFYa=0;
                float sclChangeValue = 0;
                float fSpeed = 0;
                float strTotal = 0;
                string strAler = null, strShellState = null, strPowerState = null;
                string strCommState = null;
                sclChangeValue = this.GetChangeValue();
                OleDbDataReader sclReader = GetSclDynData(sclAddress, sclPass,strCheckDev);
                if (sclReader == null)
                    return 804;
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                if (strCheckDev != null)
                {
                    if (strCheckDev.Substring(0, 1) == "1")//是否为旧版本
                        bVer = false;
                }
                while (sclReader.Read())
                {
                    if (bVer)
                    {   //新版本
                        sclDevScale = Convert.ToInt32(sclReader["DevID"]);
                        sclDevName = Convert.ToString(sclReader["DevName"]);
                        sclSignel = Convert.ToSingle(sclReader["signal"]);
                        fSpeed = Convert.ToSingle(sclReader["speed"]);
                        strShellState = sclReader["shellState"].ToString();
                        strPowerState = sclReader["powerState"].ToString();
                        strTotal= Convert.ToSingle(sclReader["Total"]);
                        strAler = "正常";// Convert.ToInt16(sclReader["parameterstate"]);//信号与零点电压差值

                        if (strShellState == "Close")
                        {
                            strShellState = "正常";
                        }
                        else
                        {
                            strShellState = "不正常";
                        }
                        if (strPowerState == "Normal")
                        {
                            strPowerState = "正常";
                        }
                        else
                        {
                            strPowerState = "欠压";
                        }
                        //增加核子秤状态
                        if (fSpeed > 0)
                        {
                            sclSpeed = fSpeed;
                            sclState = "开机";
                        }
                        else
                        {
                            sclSpeed = 0;
                            sclState = "停机";
                        }
                        sclBurthen = 0;//负荷
                        sclFlow = Convert.ToSingle(sclReader["flow"]);
                        strCommState = Convert.ToString(sclReader["CommState"]);//通讯状态
                        if (strCommState == "CommState")
                        {
                            strCommState = "通讯正常";
                        }
                        else
                        {
                            strCommState = "通讯中断";
                        }
                    }
                    else
                    {
                        //旧版本
                        sclDevScale = Convert.ToInt32(sclReader["BalNumber"]);
                        sclDevName = Convert.ToString(sclReader["BalName"]);
                        sclSignel = 0;
                        sclFlow = Convert.ToSingle(sclReader["flow1"]);
                        strMid = Convert.ToString(sclReader["statedesc"]);
                        
                        if (strMid == "设备停止")
                            sclState = "停机";
                        else
                            sclState = "开机";

                    }
                    if (bVer)
                    {
                       //新版本
                       sclFta = Convert.ToSingle(sclReader["BanTotal"]);//系数
                       sclFma = Convert.ToSingle(sclReader["MonthTotal"]);//系数
                       sclDay = Convert.ToSingle(sclReader["DayTotal"]);//系数
                       sclFYa = Convert.ToSingle(sclReader["YearTotal"]);//系数
                        
                    }
                    else
                    {   //旧版本
                        sclFta = Convert.ToSingle(sclReader["BanT"]);//系数
                        sclFma = Convert.ToSingle(sclReader["MonthT"]);//系数
                        sclFYa = Convert.ToSingle(sclReader["YearT"]);//系数
                        sclDay = Convert.ToSingle(sclReader["DayT"]);//系数

                    }
                    //**************************更新dyn表****************************
                    sclDevScale = nUnitID * 100 + sclDevScale;//SCL的编号
                    int nTest = SelectData(0, nUnitID, null, sclDevScale, sclDevName);//    
                    if (nTest == -1)
                        return 105;
                    if (nTest == 1)
                        strComm = "update s_JLDyn set CurDT='" + DateTime.Now.ToString() + "',Speed=" + sclSpeed + ",Burthen=" + sclBurthen + ",Flow=" + sclFlow + ",ClassOP=" + sclFta + ",DayOP=" + sclDay + ",MonthOP=" + sclFma + ",YearOP="+sclFYa+",Remark='" + sclState + "'where DeviceID=" + sclDevScale + "";
                    if (nTest == 0)
                        strComm = "insert into s_JLDyn (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Total,IOState,Remark) Values(" + nUnitID + "," + sclDevScale + ",'" + DateTime.Now.ToString() + "'," + sclSignel + " ," + sclSpeed + "," + sclBurthen + "," + sclFlow + ",0,0,0," + sclFta + "," + sclDay + "," + sclFma + "," + sclFYa + ",'"+strTotal+"','"+strCommState+"','" + sclState + "') ";
                    this.WriteLog("SCLWriteDataDyn 函数" + strComm);
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    //*************************添加到历史表****************************************
                    strComm = "insert into s_JLHis (UnitID,DeviceID,CurDT,signal,Speed,Burthen,Flow,zero,interval,hourop,classop,DayOP,MonthOP,YearOP,Total,IOState,Remark) Values(" + nUnitID + "," + sclDevScale + ",'" + DateTime.Now.ToString() + "'," + sclSignel + " ," + sclSpeed + "," + sclBurthen + "," + sclFlow + ",0,0,0," + sclFta + "," + sclDay + "," + sclFma + "," + sclFYa + ",'" + strTotal + "','" + strCommState + "','" + sclState + "') ";
                     sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                }
                if(conn.State==ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                sqlComm.Dispose();
                sclReader.Close();
                sclReader.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("SCLWriteDataDyn 函数 " + ex.ToString());
                return 805;
            }

        }
        /// <summary>
        /// SCL监控系统写入数据库
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="strAddress">地址</param>
        /// <param name="strPass">密码</param>
        /// <param name="nHour">时</param>
        /// <param name="nMin">分</param>
        /// <param name="strCheckDev">是否上传</param>
        /// <param name="lb">listbox显示列表</param>
        /// <returns>错误编号 正确=0</returns>
        public int SCLWriteDataOutput(int nUnitID, string strAddress, string strPass, int nHour, int nMin, string strCheckDev, System.Windows.Forms.ListBox lb)
        {
            try
            {
                string strComm = null;
                float sclOut1 = 0, sclOut2 = 0, sclOut3 = 0, sclOut4 = 0, sclTotal = 0;
                int sclDevScale = 0, nTestData = 0;

                string strCurDT = null;
                string[] strInfo = new string[2];
                float sclChangeValue = 0;

                sclChangeValue = this.GetChangeValue();
                strInfo = this.GetBanDataInfo(4);//得到时间 以及是否上传过

                bool bSclHour = (DateTime.Now.Hour == nHour);
                bool bSclMin = (DateTime.Now.Minute == nMin);
                
                this.WriteLog("SCLWriteDataOutput 函数 正在读取数据--");
                OleDbDataReader sclReader = GetSclOutput(strInfo[0], strAddress, strPass,strCheckDev);
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                if (strCheckDev != null)
                {
                    if (strCheckDev.Substring(0, 1) == "1")//选中的不传输
                        bVer=false;
                }
                while (sclReader.Read())
                {

                    this.WriteLog("SCLWriteDataOutput 函数 正在读取数据" + strInfo[0].ToString());
                    
                    if (bVer)
                    {
                        strCurDT = sclReader["curdt"].ToString();
                        sclDevScale = Convert.ToInt32(sclReader["devid"]);//秤号

                        
                        if (sclReader["BanTotal1"].GetType().Name == "DBNull")
                            sclOut1 = 0;
                        else
                        {
                            sclOut1 = Convert.ToSingle(sclReader["BanTotal1"]);

                        }

                        if (sclReader["BanTotal2"].GetType().Name == "DBNull")
                            sclOut2 = 0;
                        else
                        {
                            sclOut2 = Convert.ToSingle(sclReader["BanTotal2"]);

                        }

                        if (sclReader["BanTotal3"].GetType().Name == "DBNull")
                            sclOut3 = 0;
                        else
                        {
                            sclOut3 = Convert.ToSingle(sclReader["BanTotal3"]);

                        }

                        if (sclReader["BanTotal4"].GetType().Name == "DBNull")
                            sclOut4 = 0;
                        else
                        {
                            sclOut4 = Convert.ToSingle(sclReader["BanTotal4"]);

                        }
                    }
                    else
                    {
                        //旧版本
                        sclDevScale = Convert.ToInt32(sclReader["BalNumber"]);//秤号
                        strCurDT = sclReader["dt"].ToString();

                        if (sclReader["BanT1"].GetType().Name == "DBNull")
                            sclOut1 = 0;
                        else
                        {
                            sclOut1 = Convert.ToSingle(sclReader["BanT1"]);

                        }

                        if (sclReader["BanT2"].GetType().Name == "DBNull")
                            sclOut2 = 0;
                        else
                        {
                            sclOut2 = Convert.ToSingle(sclReader["BanT2"]);

                        }

                        if (sclReader["BanT3"].GetType().Name == "DBNull")
                            sclOut3 = 0;
                        else
                        {
                            sclOut3 = Convert.ToSingle(sclReader["BanT3"]);

                        }

                        if (sclReader["BanT4"].GetType().Name == "DBNull")
                            sclOut4 = 0;
                        else
                        {
                            sclOut4 = Convert.ToSingle(sclReader["BanT4"]);

                        }
                    }
                    sclDevScale = nUnitID * 100 + sclDevScale;
                    sclTotal = sclOut1 + sclOut2 + sclOut3 + sclOut4;
                    nTestData = this.SelectData(6, nUnitID, null, sclDevScale, strCurDT);//监测数据库中是否有数据
                    //如果有数据就更新没有数据就插入 实时执行
                    if (nTestData == 1)
                    {
                        strComm = "update s_JLOUT set Class1OP=" + sclOut1 + ",Class2OP=" + sclOut2 + ",Class3OP=" + sclOut3 + ",Class4OP=" + sclOut4 + ",Total=" + sclTotal + " where DeviceID=" + sclDevScale + " and CurDt='" + strCurDT + "'";
                    }
                    else
                    {
                        strComm = "insert into s_JLOUT (UnitID,DeviceID,CurDT,Class1OP,Class2OP,Class3OP,Class4OP,Total) Values(" + nUnitID + "," + sclDevScale + ",'" + strCurDT + "'," + sclOut1 + "," + sclOut2 + "," + sclOut3 + "," + sclOut4 + "," + sclTotal + ") ";
                    }
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    DateTime dtFormat = DateTime.Parse(strCurDT);
                    lb.Items.Add("SCL监控系统数据正在更新"+sclDevScale.ToString()+"号秤班产量数据");
                    this.WriteLog("SCL监控系统数据正在发送班产量数据最新的班产信息为 " + strCurDT);
                    strCurDT = dtFormat.AddDays(-1).ToString("yyyy-MM-dd");
                    this.SetBanDataInfo(4, strCurDT);//保存时间到注册表
                    
                }
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                sqlComm.Dispose();
                sclReader.Close();
                sclReader.Dispose();
                
                // }

                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("SCLWriteDataOutput 函数 " + ex.ToString());
                return 806;
            }
        }
        #endregion
    }
}
