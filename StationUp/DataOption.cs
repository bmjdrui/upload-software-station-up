using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Win32;
using System.Data.OleDb;
//using System.Management;
namespace StationUp
{
    class DataOption
    {
        #region 与数据库链接
        string strSqlAddress = "127.0.0.1";
        string strSqlName = "sa";
        string strSqlPass = "sss";
        static string strClientName = "测试";
        string strStation = "127.0.0.1";
        
        /// <summary>
        /// 数据库地址
        /// </summary>
        public string ServerAddress
        {
            set
            {
                strSqlAddress = value;
            }
        }
        /// <summary>
        /// 数据库用户名
        /// </summary>
        public string SqlName
        {
            set { strSqlName = value; }
        }
        /// <summary>
        /// 数据库密码
        /// </summary>
        public string SqlPass
        {
            set { strSqlPass = value; }
        }
        /// <summary>
        /// 中心站IP
        /// </summary>
        public string Station
        {
            set {
                strStation = value;
            }
            get { return strStation; }
        }
        /// <summary>
        /// 矿井名称
        /// </summary>
        public static string UnitName
        {
            set { strClientName = value; }
            get { return strClientName; }
        }
        /// <summary>
        /// sql server 服务器连接
        /// </summary>
        /// <param name="strAddress">地址</param>
        /// <param name="strName">用户</param>
        /// <param name="strPass">密码</param>
        /// <returns></returns>
        public SqlConnection GetConnection(System.Windows.Forms.ListBox lb)
        {

            string strConnSql = "data source=" + strSqlAddress + ";initial catalog=ScalePubFlat;user id=" + strSqlName + ";password=" + strSqlPass.Substring(0, 8) + ";max pool size=512";
            try
            {
                SqlConnection sqlConn = new SqlConnection(strConnSql);
                if (sqlConn.State == ConnectionState.Closed)
                    sqlConn.Open();
                return sqlConn;
            }
            catch (SqlException ex)
            {
                //lb.Items.Add(" + strSqlAddress + " + "  Sql Server服务器通信不正常,错误代码:" + ex.Class.ToString());
                this.WriteLog("DataOption下的GetConnection 函数" + ex.ToString());
                AlertSound.Alert(true);
                return null;
            }
        }
        public SqlConnection GetConnection()
        {

            string strConnSql = "data source=" + strSqlAddress + ";initial catalog=ScalePubFlat;user id=" + strSqlName + ";password=" + strSqlPass + ";max pool size=512";
            try
            {
                SqlConnection sqlConn = new SqlConnection(strConnSql);
                if (sqlConn.State == ConnectionState.Closed)
                    sqlConn.Open();
                return sqlConn;
            }
            catch (SqlException ex)
            {
                this.WriteLog("DataOption下的GetConnection 函数" + ex.ToString());
                return null;
            }
        }
        #endregion

        #region 辅助函数
        private string GetDataTime()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\KJH-UP", true);

            string strDT = key.GetValue("hzcDT", (object)DateTime.Now.ToString()).ToString();
            return strDT;
        }
        /// <summary>
        /// 判断动态数据表中是否有数据
        /// </summary>
        /// <param name="nTable"></param>
        /// <param name="nUnitID">矿号</param>
        /// <param name="strUnitName">矿名</param>
        /// <param name="nDevID">设备编号</param>
        /// <param name="strDev">设备名称</param>
        /// <returns>0：无数据 1： 有数据 其他：错误 </returns>
        public int SelectData(int nTable, int nUnitID, string strUnitName, int nDevID, string strDev)
        {
            try
            {
                string strComm = null;
                int nReturn = 0;
                SqlConnection conn = this.GetConnection(); 
                SqlCommand comm = new SqlCommand();
                switch (nTable)
                {
                    case 0://s_JLDyn
                        {
                            strComm = "select * from s_JLDyn where DeviceID=" + nDevID + "";
                            break;
                        }
                    case 1://unit
                        {
                            strComm = "select * from s_unit where UnitID=" + nUnitID + "";
                            break;
                        }
                    case 2://device 计量设备
                        {
                            strComm = "select * from s_device where DeviceID=" + nDevID + " and unitID=" + nUnitID + "";
                            break;
                        }
                    case 3://s_hfyDyn
                        {
                            strComm = "select * from S_HFYDyn where DeviceID=" + nDevID + "";
                            break;
                        }
                    case 4://device 灰分仪设备
                        {
                            //int nCurUnitID = nUnitID * 100 + 50;
                            strComm = "select * from s_device where DeviceID=" + nDevID + "";
                            break;
                        }
                    case 5://监测灰分仪、水分仪hfyout表中是否有数据
                        {
                            strComm = "select * from s_hfyOut where DeviceID=" + nDevID + " and curDT='" + strDev + "'";
                            break;
                        }
                    case 6://监测皮带秤jlout表中是否有当前的班产数据
                        {
                            strComm = "select * from s_JLOut where DeviceID=" + nDevID + " and curDT='" + strDev + "'";
                            break;
                        }
                    case 7://监测地磅s_carsHis是否有数据
                        {
                            strComm = "select * from s_carsHis where deviceid=" + nDevID + " and curDT='" + strDev + "'";
                            break;
                        }
                    case 8://监测地磅s_carsdyn是否有数据
                        {
                            strComm = "select * from s_carsdyn where deviceid=" + nDevID + "";
                            break;
                        }
                    case 9://淮南煤质unit表 比较特别nUnitID传过来的为MKDM
                        {
                            strComm = "select * from s_unit where mkdm=" + nUnitID + "";
                            break;
                        }
                }
               // this.WriteLog("select输出为："+strComm.ToString());
                comm.CommandText = strComm;
                comm.Connection = conn;
                SqlDataReader sr = comm.ExecuteReader();
                if (sr.HasRows)
                {
                    nReturn = 1;
                }

                sr.Close();
                sr.Dispose();
                comm.Dispose();
                if(conn.State==ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                return nReturn;
            }
            catch (Exception ex)
            {
                this.WriteLog("SelectData 函数 " + ex.ToString());
                return -1;
            }

        }
        /// <summary>
        /// 初始化设备参数
        /// </summary>
        /// <param name="nUnitID">矿区编号</param>
        /// <param name="strName">矿区名称</param>
        /// <param name="strCheckDev">要传输的秤</param>
        /// <returns>-1：错误 </returns>
        public virtual int GetInitPara(int nUnitID, string strName,string strCheckDev)
        {
            try
            {
                string  strComm = null;
                int nTest = -1;
                string[] strInfo = new string[2];//GetIPAndGate();//得到ip和gateip
                strInfo[0] = " ";
                strInfo[1] = " ";
                //****************更新unit表************
                nTest = SelectData(1, nUnitID,strName, 0, null);//以UnitID为准

                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                if (nTest == -1)//
                    return -1;
                if (nTest == 1)//有数据
                {
                    strComm = "select * from s_unit where UnitID=" + nUnitID + "";
                    sqlComm.CommandText = strComm;
                    SqlDataReader sr=sqlComm.ExecuteReader();
                    while (sr.Read())
                    {
                       UnitName = Convert.ToString(sr["UnitName"]);
                       nUnitID = Convert.ToInt32(sr["unitid"]);
                       this.WriteLog("nTest=1  得到的nUnitid为" + nUnitID.ToString());
                    }
                    sr.Close();
                    sr.Dispose();
                    //更新本地名称
                    /*if (strName != null)
                    {
                        strComm = "update s_unit set UnitName='" + strName + "' ,hostip='" + strInfo[0] + "',gateip='" + strInfo[1] + "' where UnitID=" + nUnitID + "";
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                    }*/
                    //更新ip
                    strComm = "update s_unit set UnitName='" + strName + "',hostip='" + strInfo[0] + "',gateip='" + strInfo[1] + "' where UnitID=" + nUnitID + "";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    if (UnitName == "")
                        UnitName = strName;
                    
                }

               // if (nUnitID == -1)//无数据
                if (nTest == 0)//无数据
                {
                 
                    this.WriteLog("nTest=0  无法从数据库获得nUnitid,请联系系统管理员");
                    nUnitID = -1;

                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                this.WriteLog("GetInitPara 函数" + ex.ToString());
                return -1;
            }
        }
        /// <summary>
        /// 保存换班信息
        /// </summary>
        /// <param name="nDev">0: 核子秤 1:2000A灰分仪 2:2000C灰分仪 3:电子秤4:SCL监控系统5:防爆灰分仪6:备用</param>
        /// <param name="strSaveDT">日期</param>
        /// <returns>无</returns>
        public string SetBanDataInfo(int nDev, string strSaveDT)
        {
            //RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\KJH-UP", true);
            //const string userRoot = "HKEY_LOCAL_MACHINE";
            string userRoot = Registry.CurrentUser.ToString();

            string subkey = "SOFTWARE\\KJH-UP";
            string keyName = userRoot + "\\" + subkey;
            switch (nDev)
            {

                case 0://核子秤
                    {
                        Registry.SetValue(keyName, "hzcDT", strSaveDT);
                        //key.SetValue("hzcDT", strSaveDT);
                        break;
                    }
                case 1://2000A型灰分仪
                    {
                        Registry.SetValue(keyName, "hfyADT", strSaveDT);
                        //key.SetValue("hfyADT", strSaveDT);
                        break;
                    }
                case 2://2000C型灰分仪
                    {
                        Registry.SetValue(keyName, "hfyCDT", strSaveDT);
                        // key.SetValue("hfyCDT",strSaveDT);
                        break;
                    }
                case 3://电子秤
                    {
                        Registry.SetValue(keyName, "dzcDT", strSaveDT);
                        break;
                    }
                case 4://SCL监控系统
                    {
                        Registry.SetValue(keyName, "sclDT", strSaveDT);
                        break;
                    }
                case 5://防爆灰分仪
                    {
                        Registry.SetValue(keyName, "hfyFB100DT", strSaveDT);
                        break;
                    }
                case 6://海明核子秤
                    {
                        Registry.SetValue(keyName, "HMDT", strSaveDT);
                        break;
                    }
                case 7://东方测控水分仪
                    {
                        Registry.SetValue(keyName, "SFY", strSaveDT);
                        break;
                    }
                case 8://拉姆齐电子秤
                    {
                        Registry.SetValue(keyName, "LMQdzcDT", strSaveDT);
                        break;
                    }
                case 9://清华灰分仪
                    {
                        Registry.SetValue(keyName, "QHhfy", strSaveDT);
                        break;
                    }
                case 10://开封万惠/清华激光秤
                    {
                        Registry.SetValue(keyName, "kfwhDT", strSaveDT);
                        break;
                    }
                case 11://清华激光秤
                    {
                        Registry.SetValue(keyName, "QHjgcDT", strSaveDT);
                        break;
                    }
                case 12://新疆地磅
                    {
                        Registry.SetValue(keyName,"XJcarDT",strSaveDT);
                        break;
                    }
                case 13://郑州地磅
                    {
                        Registry.SetValue(keyName, "ZZcarDT", strSaveDT);
                        break;
                    }
                case 14://其他地磅
                    {
                        Registry.SetValue(keyName, "CarDT", strSaveDT);
                        break;
                    }
            }
            return null;
        }
        /// <summary>
        /// 获取换班信息
        /// </summary>
        /// <param name="nDev">0: 核子秤 1:2000A灰分仪 2:2000C灰分仪 3:电子秤4:SCL监控系统5:防爆灰分仪6:备用</param>
        /// <returns>换班信息</returns>
        public string[] GetBanDataInfo(int nDev)
        {
            string[] strInfo = new string[2];
            //RegistryKey pRegKey = Registry.LocalMachine;
            //pRegKey = pRegKey.OpenSubKey("software\\KJH-UP");
            //const string userRoot = "HKEY_LOCAL_MACHINE";
            string userRoot = Registry.CurrentUser.ToString();

             string subkey = "SOFTWARE\\KJH-UP";
             string keyName = userRoot + "\\" + subkey;
            
            switch (nDev)
            {
                case 0://核子秤
                    {
                        //strInfo[0] = Registry.GetValue(keyName, "hzcDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();

                        strInfo[0] = Registry.GetValue(keyName, "hzcDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        strInfo[1] = Registry.GetValue(keyName, "hzcBan", (object)0).ToString();
                        //strInfo[0]= pRegKey.GetValue("hzcDT",(object)DateTime.Now.AddDays(-1).ToString()).ToString();
                        break;
                    }
                case 1://2000A灰分仪
                    {
                        strInfo[0] = Registry.GetValue(keyName, "hfyADT", (object)DateTime.Now.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        // strInfo[0] = pRegKey.GetValue("hfyADT", (object)DateTime.Now.ToString()).ToString();
                        break;
                    }
                case 2://2000c灰分仪
                    {
                        strInfo[0] = Registry.GetValue(keyName, "hfyCDT", (object)DateTime.Now.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        //strInfo[0] = pRegKey.GetValue("hfyCDT", (object)DateTime.Now.ToString()).ToString();
                        break;
                    }
                case 3://电子秤
                    {
                        strInfo[0] = Registry.GetValue(keyName, "dzcDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        strInfo[1] = Registry.GetValue(keyName, "hzcBan", (object)0).ToString();
                        break;
                    }
                case 4://SCL监控系统
                    {
                        strInfo[0] = Registry.GetValue(keyName, "sclDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        strInfo[1] = Registry.GetValue(keyName, "sclBan", (object)0).ToString();
                        break;
                    }
                case 5://防爆灰分仪
                    {
                        strInfo[0] = Registry.GetValue(keyName, "hfyFB100DT", (object)DateTime.Now.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        //strInfo[0] = pRegKey.GetValue("hfyCDT", (object)DateTime.Now.ToString()).ToString();
                        break;
                    }
                case 6://
                    {
                        strInfo[0] = Registry.GetValue(keyName, "HMDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        strInfo[1] = Registry.GetValue(keyName, "HMBan", (object)0).ToString();
                        break;
                    }
                case 7://东方测控水分仪
                    {
                        strInfo[0] = strInfo[0] = Registry.GetValue(keyName, "SFY", (object)DateTime.Now.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        break;
                    }
                case 8://拉姆齐电子秤
                    {
                         strInfo[0] = Registry.GetValue(keyName, "LMQdzcDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        strInfo[1] = Registry.GetValue(keyName, "LMQdzcBan", (object)0).ToString();
                        break;
                    }
                case 9://清华灰分仪
                    {
                        strInfo[0] = strInfo[0] = Registry.GetValue(keyName, "QHhfy", (object)DateTime.Now.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        break;
                    }
                case 10://开封万惠/清华激光秤核子秤
                    {
                        strInfo[0] = Registry.GetValue(keyName, "kfwhDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        strInfo[1] = Registry.GetValue(keyName, "kfwhBan", (object)0).ToString();
                        break;
                    }
                case 11://清华激光秤
                    {
                        strInfo[0] = Registry.GetValue(keyName, "QHjgcDT", (object)DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        break;
                    }
                case 12://新疆地磅
                    {
                        strInfo[0] = Registry.GetValue(keyName, "XJcarDT", (object)DateTime.Now.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:00")).ToString();
                        break;
                    }
                case 13://音西地磅 保存序号 
                    {
                        strInfo[0] = Registry.GetValue(keyName, "XJcarDT", (object)1).ToString();
                        break;
                    }

            }
            return strInfo;
        }
        /// <summary>
        /// 得到核子秤系数
        /// </summary>
        /// <returns></returns>
        public float GetChangeValue()
        {
            try
            {
                RegistryKey pRegKey = Registry.CurrentUser;
                pRegKey = pRegKey.OpenSubKey(@"Software\KJH_N\KJH-N核子秤动态计量控制系统\\SYS");
                float nChange = Convert.ToSingle(pRegKey.GetValue("ChangeValue", 100));
                float nReturn = nChange / 100;
                this.WriteLog("GetChangeValue 函数     " + nReturn.ToString());
                return nReturn;
            }
            catch (Exception ex)
            {
                //this.WriteLog("GetChangeValue" + ex.ToString());
                return 800102;
            }
        }
        /// <summary>
        /// 判断是否是数字
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool IsNumber(string s)
        {
            int Flag = 0;
            char[] str = s.ToCharArray();
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != '.')
                {
                    if (Char.IsNumber(str[i]))
                    {
                        Flag++;
                    }
                    else
                    {
                        Flag = -1;
                        break;
                    }
                }
            }
            if (Flag > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 记录错误信息
        /// </summary>
        /// <param name="strInfo">信息内容</param>
        public void WriteLog(string strInfo)
        {
            try
            {


                if (!System.IO.Directory.Exists(@"c:\scalelog"))
                {
                    System.IO.Directory.CreateDirectory(@"c:\scalelog");
                }
                string strLogName = DateTime.Now.ToString("yyyy-MM-dd");
                string strPath = @"c:\scalelog\" + "log" + strLogName + ".txt";
                // FileStream aFile = new FileStream(strPath, FileMode.penOrCreate);
                StreamWriter sw = new StreamWriter(strPath, true);
                sw.WriteLine(DateTime.Now.ToString() + "--" + strInfo);
                sw.Close();

            }
            catch (IOException e)
            {
                return;
            }

        }
        /// <summary>
        /// 获取IP和网关
        /// </summary>
        /// <returns></returns>
      /*  public string [] GetIPAndGate()
        {

            string [] strInfo=new string[2];
            strInfo[0] = "0.0.0.0";
            strInfo[1] = "0.0.0.0";
            try
            {
                ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection nics = mc.GetInstances();
                foreach (ManagementObject nic in nics)
                {
                    if (Convert.ToBoolean(nic["ipEnabled"]) == true)
                    {
                        
                        if ((nic["IPAddress"] as String[]).Length > 0 && strInfo[0] == "0.0.0.0")
                        {
                            strInfo[0] = (nic["IPAddress"] as String[])[0];
                        }
                        if (nic["DefaultIPGateway"] == null)
                            continue;
                        if ((nic["DefaultIPGateway"] as String[]).Length > 0 && strInfo[1] == "0.0.0.0")
                        {
                            strInfo[1] = (nic["DefaultIPGateway"] as String[])[0];
                        }

                    }
                }
                return strInfo;
            }
                 
            catch (Exception ex)
            {
                return strInfo;
            }
           
        }*/
        /// <summary>
        /// 网络状态信息写s_statehis数据表
        /// </summary>
        /// <param name="nUnitID"></param>
        /// <param name="strState">状态信息(正常 ,断开)</param>
        /// <param name="strRemark"></param>
        /// <returns></returns>
        public int WriteNetState(int nUnitID,string strState,string strRemark)
        {
            try
            {
                SqlConnection conn = GetConnection();
                string strComm = "Insert into s_stateHis (unitid,curdt,netstate,remark) values(" + nUnitID + ",'" + DateTime.Now.ToString() + "', '" + strState + "','" + strRemark + "')";
                SqlCommand sqlComm = new SqlCommand(strComm, conn);
                sqlComm.ExecuteNonQuery();
                sqlComm.Dispose();
                conn.Close();
                conn.Dispose();
                return 0;
                
            }
            catch (Exception ex)
            {
                return 1;
            }
                       
        }
        #endregion

     
    }
}
