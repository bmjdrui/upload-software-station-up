﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using Microsoft.Win32;
using System.Security.Permissions;
using System.IO;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Globalization;
using System.Diagnostics;
namespace StationUp
{
    /// <summary>
    /// 获取淮南煤质类
    /// </summary>
    class DataHNClass : DataOption
    {

        #region 数据初始化
        string strAddress = null, strPass = null, strUser = null;
        int nDay = -5;
        int tempStateClose = 1;//是否写oracle网络断开状态数据
        int tempStateOpen = 1;//是否写oracle网络正常状态数据
        SqlConnection hnConn = new SqlConnection();
        OracleConnection oracleConn = null;
        string strDT1 = null;
        string strDT2 = null;
        public string AddressMachine
        {
            get { return strAddress; }
            set { strAddress = value; }
        }
        public string PassMachine
        {
            get { return strPass; }
            set { strPass = value; }
        }
        public string UserMaching
        {
            get { return strUser; }
            set { strUser = value; }
        }
        public int Day
        {
            get { return nDay; }
            set { nDay = value; }
        }
        #endregion
               
        #region 获取煤质信息
        /// <summary>
        /// 得到煤质oracle数据库的连接
        /// </summary>
        /// <returns>oracal连接</returns>
        private OracleConnection GethnConnection()
        {
            string strConnOracle = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" + AddressMachine + ")(PORT=1521))" + "(CONNECT_DATA=(SID=hnmis)));User Id=" + UserMaching + ";Password="+PassMachine+";Pooling=False";
            try
            {
                oracleConn = new OracleConnection(strConnOracle);
                if (oracleConn.State == ConnectionState.Closed)
                    oracleConn.Open();
                Debug.Print("oracleConn OK");
                WriteOracleStateUnit("正常");//网络状态写到服务器中的s_unit
                return oracleConn;
            }
            catch (OracleException ex)
            {
                Debug.Print("oracleConn Error");
                this.WriteLog("淮南煤质DataOption下的GethnConnection 函数" + ex.ToString());
                WriteOracleStateUnit("断开");//网络状态写到服务器中的s_unit
                return null;
            }
        }
        /// <summary>
        /// 得到煤质信息 SQL SERVER
        /// 1014511581 888pcr
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetHNSQLConnection()
        {

            string strConnSql = "data source=" + AddressMachine + ";initial catalog=hnmtmy;user id=" + UserMaching + ";password=" + PassMachine + "";
            try
            {
                SqlConnection sqlConn = new SqlConnection(strConnSql);
                if (sqlConn.State == ConnectionState.Closed)
                    sqlConn.Open();
                WriteOracleStateUnit("正常");//网络状态写到服务器中的s_unit
                return sqlConn;
            }
            catch (SqlException ex)
            {
                this.WriteLog("DataOption下的GetConnection 函数" + ex.ToString());
                WriteOracleStateUnit("断开");//网络状态写到服务器中的s_unit
                return null;
            }
        }
        /// <summary>
        /// 得到煤质量历史信息
        /// </summary>
        /// <returns>oracle数据集</returns>
        private OracleDataReader GetHNOutput()
        {
            OracleCommand hnComm = null;
            OracleDataReader hnReader = null;
            try
            {
                this.WriteLog("GetHNOutput函数执行正常1");
                DateTime nowDT = DateTime.Now.AddDays(1);
                DateTime newDT = DateTime.Now.AddDays(Day * -1);

                strDT1 = newDT.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo);
                strDT2 = nowDT.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo);
                // string strComm = "SELECT * FROM v_zxl_cxhy where  (xslx!=1 or xslx is null) and tjsj between to_date('" + strDT1 + "','yyyy-mm-dd') and to_date('" + strDT2 + "','yyyy-mm-dd')";
                string strComm = "SELECT * FROM v_zxl_cxhy where tjsj between to_date('" + strDT1 + "','yyyy-mm-dd') and to_date('" + strDT2 + "','yyyy-mm-dd')";
               
                //'XSLX', 销售类型(1正常销售2委托销售3电厂自用4重复运量5原煤入洗)
                this.WriteLog("GetHNOutput函数执行" + strComm);
                Debug.Print("GetHNOutput函数执行" + strComm);
                hnComm = new OracleCommand(strComm, GethnConnection());
                hnReader = hnComm.ExecuteReader();
                hnComm.Dispose();
                this.WriteLog("GetHNOutput函数执行正常2");
                return hnReader;
              
            }
            catch (OracleException ex)
            {
                Debug.Print("GetHNOutput函数执行 Error");
                this.WriteLog("GetHNOutput 函数" + ex.ToString());
                return null;
            }
         
        }
        private SqlDataReader GetHNSqlOutput()
        {
            SqlCommand hnComm = null;
            SqlDataReader hnReader = null;
            try
            {
                this.WriteLog("GetHNOutput函数执行正常1");
                DateTime nowDT = DateTime.Now.AddDays(1);
                DateTime newDT = DateTime.Now.AddDays(Day * -1);

                strDT1 = newDT.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo);
                strDT2 = nowDT.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo);
                // string strComm = "SELECT * FROM v_zxl_cxhy where  (xslx!=1 or xslx is null) and tjsj between to_date('" + strDT1 + "','yyyy-mm-dd') and to_date('" + strDT2 + "','yyyy-mm-dd')";
                //string strComm = "SELECT * FROM v_zxl_cxhy where tjsj between to_date('" + strDT1 + "','yyyy-mm-dd') and to_date('" + strDT2 + "','yyyy-mm-dd')";
                string strComm="select * from v_jzjl where rq between '" + strDT1 + "' and '" + strDT2 + "'";

                //'XSLX', 销售类型(1正常销售2委托销售3电厂自用4重复运量5原煤入洗)
                this.WriteLog("GetHNSQLOutput函数执行" + strComm);
               
                hnComm = new SqlCommand(strComm, GetHNSQLConnection());
                hnReader = hnComm.ExecuteReader(CommandBehavior.CloseConnection);
                hnComm.Dispose();
                this.WriteLog("GetHNSQLOutput函数执行正常2");
                return hnReader;

            }
            catch (OracleException ex)
            {
                Debug.Print("GetHNSQLOutput函数执行 Error");
                this.WriteLog("GetHNSQLOutput 函数" + ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 得到矿名
        /// </summary>
        /// <param name="mkdm"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        private string GetName(int mkdm)
        {
            string strComm = "select mc from v_zzd where id=" + mkdm + " and lb='P'";
            //this.WriteLog("GetName函数执行" + strComm);
            OracleCommand hnComm = new OracleCommand(strComm,oracleConn);
            string strName=hnComm.ExecuteScalar().ToString();
            hnComm.Dispose();
            Debug.Print("GetName函数执行正常");
            return strName;
        }
        #endregion

        #region 煤质数据写入到服务器
         /// <summary>
        /// 向服务器写入历史信息
        /// </summary>
        /// <returns>0 正常  其他  代表错误</returns>
        public int HNWriteDataOutput()
        {
            OracleDataReader hnReader = null;
            SqlConnection conn = null;
            SqlCommand sqlComm=null;
            int nReturn = 0;
            try
            {
                string strComm = null;
                ArrayList arHydm = new ArrayList();
                int hnDevScale = 0, nUnitID = 0, MKDM = 0;
                string PC = null, Number = null, CaiYangDT = null;
                float ZongYunDun = 0, Mt = 0, Mad = 0, Ad = 0, Aar = 0, var = 0, CRC = 0, Vdaf = 0, Std = 0, Had = 0, Qbad = 0, QnetarMJ = 0, QnetarCal = 0;
                float KouShui = 0;
                string Level = null, HuaYanRen = null, ShenHeRen = null, ZhuShenRen = null, BaoGaoDT = null, XiuGaiDT = null;
                int ShenHe = 1, XiaoShouType = 0, LiuChengType = 0, PinMing = 0;
                string HYDM = null,SHRMC=null,YSFS=null;
                int SHRDM = 0;
                float Fyl = 0, Fpdj = 0, Jsl = 0;//发运量，发票单价，折合量
                string strCurDT = null, strShenHe = null;
                string[] strInfo = new string[2];
                int count = 0;
                int coalSign = 0;//标示原煤入洗 1，外销 2


                hnReader = GetHNOutput();
                Debug.Print("Programe 1");
                //判断网络通断
                if ((hnReader == null) && (tempStateClose == 1))
                {
                    tempStateClose = WriteOracleStateHis("断开", "煤质服务器断开");
                    tempStateOpen = 1;
                }
                if (hnReader == null)//网络未通返回
                {
                    return 801500;
                }
                tempStateClose = 1;
                if ((hnReader != null) && (tempStateOpen == 1))
                {
                    tempStateOpen = WriteOracleStateHis("正常", "煤质服务器正常");
                }
                /////////////////////////////////////////

                conn = GetConnection();
                sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                arHydm.Clear();
                Debug.Print("Prgrame 2");
                while (hnReader.Read())
                {
                    
                    //煤质数据
                    MKDM = hnReader["MKDM"].GetType().Name == "DBNull" ? -1 : Convert.ToInt32(hnReader["MKDM"]);
                    if (MKDM == -1)
                        continue;

                    //处理一些单位与设备编码的函数
                    nUnitID = InsertUnit(MKDM);
                    int n = InsertDeviceID(nUnitID);//s_device表
                    if (n < 0)//更新设备表失败
                        return 801504;
                    hnDevScale = nUnitID * 100 + 70;//煤质化验设备

                    HYDM = hnReader["HYDM"].ToString();
                    string s = hnReader["xslx"].ToString();
                    XiaoShouType = hnReader["xslx"].GetType().Name == "DBNull" ? 0 : Convert.ToInt32(hnReader["xslx"].ToString());//销售类型

                    PC = hnReader["HYPC"].ToString();//化验批次
                    Number = hnReader["HYXH"].ToString();//化验序号
                    CaiYangDT = hnReader["TJSJ"].ToString();//采样时间
                    LiuChengType = Convert.ToInt32(hnReader["FLOW_TYPE"].ToString());//流程类型

                    PinMing = hnReader["PPID"].GetType().Name == "DBNull" ? 0 : Convert.ToInt32(hnReader["PPID"].ToString());//品种类型
                    ZongYunDun = hnReader["ZYD"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["ZYD"].ToString());//总云吨

                    Mt = hnReader["SF"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["SF"].ToString());//全水
                    Mad = hnReader["SF1"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["SF1"].ToString());//空气干燥基水分
                    Ad = hnReader["HF"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["HF"].ToString());//灰分
                    Aar = hnReader["HF1"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["HF1"].ToString());//收到基灰分
                    var = hnReader["HFF1"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["HFF1"].ToString());//收到基挥发分
                    CRC = hnReader["YZ"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["YZ"].ToString());//焦渣特性
                    Vdaf = hnReader["HFF"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["HFF"].ToString());//挥发份
                    Std = hnReader["LF"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["LF"].ToString());//干燥基全硫
                    Qbad = hnReader["DDRZ"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["DDRZ"].ToString());//弹筒热值
                    // Had=Convert.ToSingle(hnReader[" "].ToString());//空气干燥基氢
                    QnetarMJ = hnReader["RZ1"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["RZ1"].ToString());//mj热值
                    QnetarCal = hnReader["RZ"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["RZ"].ToString());//cal热值
                    KouShui = hnReader["KS"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["KS"].ToString());//扣水

                    Level = hnReader["JB"].ToString();//级别
                    HuaYanRen = hnReader["HYY"].ToString();//化验人员
                    ShenHeRen = hnReader["SHR"].ToString();//审核人员
                    ZhuShenRen = hnReader["ZGR"].ToString();//主管人员
                    //BaoGaoDT=hnReader[" "].ToString();//报告日期
                    XiuGaiDT = hnReader["ZHXGSJ"].GetType().Name == "DBNull" ? null : hnReader["ZHXGSJ"].ToString();//最后修改该日期
                    if (hnReader["SHRDM"].GetType().Name == "DBNull")
                    {
                        SHRDM = 0;
                    }
                    else
                    {
                        SHRDM = Convert.ToInt32(hnReader["SHRDM"]);
                    }
                    if (hnReader["SHRMC"].GetType().Name == "DBNull")
                    {
                        SHRMC = null;
                    }
                    else
                    {
                        SHRMC =hnReader["SHRMC"].ToString();
                    }
                     if (hnReader["YSFS"].GetType().Name == "DBNull")
                    {
                        YSFS =null;
                    }
                    else
                    {
                        YSFS =hnReader["YSFS"].ToString();
                    }
                    // 审核状态 （已审  未审）
                    if (hnReader["BILL_STS"].GetType().Name == "DBNull")
                        ShenHe = 2;
                    else
                    {
                        strShenHe = hnReader["BILL_STS"].ToString();
                        if (strShenHe == "0" || strShenHe == "1")
                            ShenHe = 1;
                    }
                    string ysdm = null;
                    if (hnReader["ysdm"].GetType().Name == "DBNull")
                    {
                        ysdm = null;
                    }
                    else
                    {
                        ysdm = hnReader["ysdm"].ToString();
                    }

                    float [] ysfy=AddJiaGe(ysdm);//获取 发运量 折合量 发票单价
                    if (ysfy != null)
                    {
                        Fyl = ysfy[0];
                        Jsl = ysfy[1];
                        Fpdj = ysfy[2];
                    }
                    arHydm.Add(HYDM);//增加hydm到数据列表
                    //筛选数据，筛选规则见相关文档
                    int coalTemp = SelectCoal(MKDM, LiuChengType, PinMing, XiaoShouType);
                    if (coalTemp == 1)
                        coalSign = 1;//外销 块煤 筛末 煤块 煤矸 掘进煤 基建煤 
                    if (coalTemp == 0)
                        coalSign = 0;//不添加该数据
                    if (XiaoShouType == 5)
                        coalSign = 2;//原煤入洗
                    if (XiaoShouType != 5)
                     {
                        //if ((PinMing != 5) && (PinMing != 7))
                        //块煤 筛末 煤块 煤矸 掘进煤 基建煤
                         if ((PinMing != 5) && (PinMing != 7) &&(PinMing==21)&&(PinMing==8)&&(PinMing==15)&&(PinMing==17))
                        {
                            if(MKDM!=1 && MKDM!=2 && MKDM!=6)//新庄子 谢一矿  李嘴子按照车水地算外销
                                coalSign = 3;//商品煤
                            if((SHRDM==27803290|| SHRDM==27804703||SHRDM==27803290)&&(YSFS==null))//入洗
                            {
                                PinMing = 30;

                            }
                        }
                    }
                   
                    //////////////////////////////////////////////
                    //插入数据
                    string strCommStart1 = "insert into s_coalQuality (DeviceID,UnitID,LiuChengType,XiaoShouType,PC,Number,CaiYangDT,PinMing,ZongYunDun,";
                    string strCommStart2 = "Mt,Mad,Ad,Aar,var,CRC,Vdaf,Std,Had,Qbad,QnetarMJ,QnetarCal,KouShui,";
                    string strCommStart3 = "Level,HuaYanRen,ShenHeRen,ZhuShenRen,BaoGaoDT,XiuGaiDT,ShenHe,HYDM,SHRMC,SHRDM,CoalSign,FYL,JSL,FPDJ)";
                    string strCommEnd1 = "Values(" + hnDevScale + "," + nUnitID + "," + LiuChengType + "," + XiaoShouType + ",'" + PC + "','" + Number + "','" + CaiYangDT + "'," + PinMing + "," + ZongYunDun + ", ";
                    string strCommEnd2 = "" + Mt + "," + Mad + "," + Ad + "," + Aar + "," + var + "," + CRC + "," + Vdaf + "," + Std + "," + Had + "," + Qbad + "," + QnetarMJ + "," + QnetarCal + "," + KouShui + ",";
                    string strCommEnd3 = "'" + Level + "','" + HuaYanRen + "','" + ShenHeRen + "','" + ZhuShenRen + "','" + BaoGaoDT + "','" + XiuGaiDT + "'," + ShenHe + ",'" + HYDM + "','"+SHRMC+"',"+SHRDM+"," + coalSign + ","+Fyl+","+Jsl+","+Fpdj+")";
                    //更新
                    string strCommUpDate1 = "update s_coalQuality set UnitID=" + nUnitID + ",liuChengType=" + LiuChengType + ",XiaoShouType=" + XiaoShouType + ",PC='" + PC + "',Number='" + Number + "',CaiYangDT='" + CaiYangDT + "',PinMing=" + PinMing + ",ZongYunDun=" + ZongYunDun + ",";
                    string strCommUpDate2 = "Mt=" + Mt + ",Mad=" + Mad + ",Ad=" + Ad + ",var=" + var + ",CRC=" + CRC + ",Vdaf=" + Vdaf + ",Std=" + Std + ",Had=" + Had + ",Qbad=" + Qbad + ",QnetarMJ=" + QnetarMJ + ",QnetarCal=" + QnetarCal + ",KouShui=" + KouShui + ",";
                    string strCommUpDate3 = "Level='" + Level + "',HuaYanRen='" + HuaYanRen + "',ShenHeRen='" + ShenHeRen + "',ZhuShenRen='" + ZhuShenRen + "',BaoGaoDT='" + BaoGaoDT + "',XiuGaiDT='" + XiuGaiDT + "',ShenHe=" + ShenHe + ",SHRMC='"+SHRMC+"',SHRDM="+SHRDM+",CoalSign=" + coalSign + " ,FYL="+Fyl+",JSL="+Jsl+",FPDJ="+Fpdj+" where HYDM='" + HYDM + "'";


                    int exist = DataExist(HYDM, PC, CaiYangDT, nUnitID);
                    if (exist == -1)
                        return 801502;
                    if (exist == 0)//判断数据是否存在
                        strComm = strCommStart1 + strCommStart2 + strCommStart3 + strCommEnd1 + strCommEnd2 + strCommEnd3;//无数据  插入
                    if (exist == 1)
                        strComm = strCommUpDate1 + strCommUpDate2 + strCommUpDate3;//有数据更新

                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    count++;
                    Debug.Print("淮南煤质数据正在更新" + count.ToString()+" "+CaiYangDT + "矿区代码(" + MKDM.ToString() + "):化验代码:" + HYDM + "  传输时间" + DateTime.Now.ToString());
                    //lb.Items.Add("淮南煤质数据正在更新" + CaiYangDT + "矿区代码(" +MKDM.ToString()+ "):化验代码:"+HYDM+"  传输时间"+DateTime.Now.ToString());
                    this.WriteLog("淮南煤质数据正在更新" + count.ToString()+"  "+CaiYangDT + "矿区代码(" + MKDM.ToString() + "):化验代码:" + HYDM + "  传输时间" + DateTime.Now.ToString());

                }

                sqlComm.Dispose();
                //看看sql server数据库中是否有多余的数据（oracle 删除过的，但已经同步到sql server,该函数删除掉这些数据）
                SqlDataDelete(arHydm);
                this.WriteLog("HNWriteDataOutput执行正常");
                nReturn = 0;
                return nReturn;
               
            }
            catch (Exception ex)
            {
                this.WriteLog("HNWriteDataOutput 函数 " + ex.ToString());
                nReturn=801503;
            }
            finally
            {
                if (conn != null)
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                }
                if (oracleConn != null)
                {
                    if (oracleConn.State == ConnectionState.Open)
                    {
                        oracleConn.Close();
                    }
                    oracleConn.Dispose();;
                   
                }
                
                if (hnReader != null)
                {
                    hnReader.Close();
                    hnReader.Dispose();
                }
               
            }
            return nReturn;
        }
        /// <summary>
        /// 煤质处在2019-1-1日启用新的煤质系统，该函数适配于该需求的变动
        /// </summary>
        /// <returns></returns>
        public int HNSQLWriteDataOutput()
        {
            SqlDataReader hnReader = null;
            SqlConnection conn = null;
            SqlCommand sqlComm = null;
            int nReturn = 0;
            try
            {
                string strComm = null;
                ArrayList arHydm = new ArrayList();
                int hnDevScale = 0, nUnitID = 0, MKDM = 0;
                string PC = null, Number = null, CaiYangDT = null;
                float ZongYunDun = 0, Mt = 0, Mad = 0, Ad = 0, Aar = 0, var = 0, CRC = 0, Vdaf = 0, Std = 0, Had = 0, Qbad = 0, QnetarMJ = 0, QnetarCal = 0;
                float KouShui = 0;
                string Level = null, HuaYanRen = null, ShenHeRen = null, ZhuShenRen = null, BaoGaoDT = null, XiuGaiDT = null;
                int ShenHe = 1, XiaoShouType = 0, LiuChengType = 0, PinMing = 0;
                string HYDM = null, SHRMC = null, YSFS = null;
                int SHRDM = 0;
                float Fyl = 0, Fpdj = 0, Jsl = 0;//发运量，发票单价，折合量
                string strCurDT = null, strShenHe = null;
                string[] strInfo = new string[2];
                int count = 0;
                int coalSign = 0;//标示原煤入洗 1，外销 2


                hnReader = GetHNSqlOutput();
                Debug.Print("Programe 1");
                //判断网络通断
                if ((hnReader == null) && (tempStateClose == 1))
                {
                    tempStateClose = WriteOracleStateHis("断开", "煤质服务器断开");
                    tempStateOpen = 1;
                }
                if (hnReader == null)//网络未通返回
                {
                    return 801500;
                }
                tempStateClose = 1;
                if ((hnReader != null) && (tempStateOpen == 1))
                {
                    tempStateOpen = WriteOracleStateHis("正常", "煤质服务器正常");
                }
                /////////////////////////////////////////

                conn = GetConnection();
                sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                arHydm.Clear();
                Debug.Print("Prgrame 2");
                while (hnReader.Read())
                {

                    //煤质数据
                    string companyname = hnReader["gsmc"].ToString().Trim();
                   
                    //处理一些单位与设备编码的函数
                    nUnitID=InsertUnitSQL(companyname);

                    if (nUnitID < 0)//依靠煤质量数据库gsmc字段名字获取unitid编号失败
                    {
                        this.WriteLog("依靠煤质量数据库gsmc字段名字获取unitid编号失败,单位名称:"+companyname);
                        continue;
                    }
                       
                    hnDevScale = nUnitID * 100 + 70;//煤质化验设备
                    if (BoDevice(hnDevScale))//判断煤质设备是否存在
                    {
                        //this.WriteLog("执行了新改Deviceid="+ hnDevScale);
                        if (hnReader["qnetar"].GetType().Name == "DBNull")
                        {
                            this.WriteLog("煤质数据库中qnetar字段为NULL，矿区名称:" + companyname + " 日期:" + hnReader["rq"].ToString());

                            continue;
                        }
                        string ymwy = hnReader["ymwy"].ToString().Trim();//外运  原煤
                        if (ymwy == "原煤")
                        {
                            coalSign = 2;//
                        }
                        if (ymwy == "外运")
                        {
                            coalSign = 1;//
                        }
                        //xsfs--销售方式，主要分为车运、地销、水运、皮带销售和原煤入洗，这里的原煤入洗是指通过火车或汽车发往选煤厂的

                        string xsfs = hnReader["xsfs"].ToString();
                        XiaoShouType = SelectXSFS(xsfs);
                        CaiYangDT = Convert.ToDateTime(hnReader["rq"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");//采样时间
                                                                                                                  //PinMing = hnReader["PPID"].GetType().Name == "DBNull" ? 0 : Convert.ToInt32(hnReader["PPID"].ToString());//品种类型
                        ZongYunDun = hnReader["sl"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["sl"].ToString());//总云吨

                        Mt = hnReader["mt"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["mt"].ToString());//全水
                       // v-jljz没有硫分 Std = hnReader["LF"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["LF"].ToString());//干燥基全硫
                        Ad = hnReader["ad"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["ad"].ToString());//灰分
                        QnetarCal = hnReader["qnetar"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["qnetar"].ToString());//cal热值
                        string mztype = hnReader["cpmc"].ToString().Trim();
                        PinMing = SelectMZType(mztype);
                        HYDM = hnReader["ID"].ToString();

                        arHydm.Add(HYDM);

                        Fyl = hnReader["fyl"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["fyl"].ToString());//发运量
                        Jsl = hnReader["jssl"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["jssl"].ToString());//折合量
                        Fpdj = hnReader["jsdj"].GetType().Name == "DBNull" ? 0 : Convert.ToSingle(hnReader["jsdj"].ToString());//发票单价


                        //////////////////////////////////////////////
                        //插入数据
                        string strCommStart1 = "insert into s_coalQuality (MzDeviceID,UnitID,LiuChengType,XiaoShouType,PC,Number,CaiYangDT,PinMing,ZongYunDun,";
                        string strCommStart2 = "Mt,Mad,Ad,Aar,var,CRC,Vdaf,Std,Had,Qbad,QnetarMJ,QnetarCal,KouShui,";
                        string strCommStart3 = "Level,HuaYanRen,ShenHeRen,ZhuShenRen,BaoGaoDT,XiuGaiDT,ShenHe,HYDM,SHRMC,SHRDM,CoalSign,FYL,JSL,FPDJ)";
                        string strCommEnd1 = "Values(" + hnDevScale + "," + nUnitID + "," + LiuChengType + "," + XiaoShouType + ",'" + PC + "','" + Number + "','" + CaiYangDT + "'," + PinMing + "," + ZongYunDun + ", ";
                        string strCommEnd2 = "" + Mt + "," + Mad + "," + Ad + "," + Aar + "," + var + "," + CRC + "," + Vdaf + "," + Std + "," + Had + "," + Qbad + "," + QnetarMJ + "," + QnetarCal + "," + KouShui + ",";
                        string strCommEnd3 = "'" + Level + "','" + HuaYanRen + "','" + ShenHeRen + "','" + ZhuShenRen + "','" + BaoGaoDT + "','" + XiuGaiDT + "'," + ShenHe + ",'" + HYDM + "','" + SHRMC + "'," + SHRDM + "," + coalSign + "," + Fyl + "," + Jsl + "," + Fpdj + ")";
                        //更新
                        string strCommUpDate1 = "update s_coalQuality set UnitID=" + nUnitID + ",liuChengType=" + LiuChengType + ",XiaoShouType=" + XiaoShouType + ",PC='" + PC + "',Number='" + Number + "',CaiYangDT='" + CaiYangDT + "',PinMing=" + PinMing + ",ZongYunDun=" + ZongYunDun + ",";
                        string strCommUpDate2 = "Mt=" + Mt + ",Mad=" + Mad + ",Ad=" + Ad + ",var=" + var + ",CRC=" + CRC + ",Vdaf=" + Vdaf + ",Std=" + Std + ",Had=" + Had + ",Qbad=" + Qbad + ",QnetarMJ=" + QnetarMJ + ",QnetarCal=" + QnetarCal + ",KouShui=" + KouShui + ",";
                        string strCommUpDate3 = "Level='" + Level + "',HuaYanRen='" + HuaYanRen + "',ShenHeRen='" + ShenHeRen + "',ZhuShenRen='" + ZhuShenRen + "',BaoGaoDT='" + BaoGaoDT + "',XiuGaiDT='" + XiuGaiDT + "',ShenHe=" + ShenHe + ",SHRMC='" + SHRMC + "',SHRDM=" + SHRDM + ",CoalSign=" + coalSign + " ,FYL=" + Fyl + ",JSL=" + Jsl + ",FPDJ=" + Fpdj + " where HYDM='" + HYDM + "'";


                        int exist = SqlDataExist(HYDM, PC, CaiYangDT, nUnitID);
                        if (exist == -1)
                            return 801502;
                        if (exist == 0)//判断数据是否存在
                            strComm = strCommStart1 + strCommStart2 + strCommStart3 + strCommEnd1 + strCommEnd2 + strCommEnd3;//无数据  插入
                        if (exist == 1)
                            strComm = strCommUpDate1 + strCommUpDate2 + strCommUpDate3;//有数据更新
                        //this.WriteLog("插入语句=" + strComm);
                        sqlComm.CommandText = strComm;
                        sqlComm.ExecuteNonQuery();
                        
                        count++;
                        Debug.Print("淮南煤质数据正在更新" + count.ToString() + " " + CaiYangDT + "矿区代码(" + MKDM.ToString() + "):化验代码:" + HYDM + "  传输时间" + DateTime.Now.ToString());
                        //lb.Items.Add("淮南煤质数据正在更新" + CaiYangDT + "矿区代码(" +MKDM.ToString()+ "):化验代码:"+HYDM+"  传输时间"+DateTime.Now.ToString());
                        this.WriteLog("淮南煤质数据正在更新" + companyname + "  " + CaiYangDT + " " + ymwy);
                    }
                    else
                    {
                        //不存在不处理
                    }
                   

                }

                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    conn.Dispose();
                }
                //看看sql server数据库中是否有多余的数据（oracle 删除过的，但已经同步到sql server,该函数删除掉这些数据）
                SqlDataDelete(arHydm);
                this.WriteLog("HNSQLWriteDataOutput执行正常");
                nReturn = 0;
                return nReturn;
                
            }
            catch (Exception ex)
            {
                this.WriteLog("HNSQLWriteDataOutput 函数 " + ex.ToString());
                nReturn = 801503;
            }
            finally
            {
                if (conn != null)
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                }
                if (oracleConn != null)
                {
                    if (oracleConn.State == ConnectionState.Open)
                    {
                        oracleConn.Close();
                    }
                    oracleConn.Dispose(); ;

                }

                if (hnReader != null)
                {
                    hnReader.Close();
                    hnReader.Dispose();
                }

            }
            return nReturn;
        }
        //-销售方式，主要分为车运、地销、水运、皮带销售和原煤入洗，这里的原煤入洗是指通过火车或汽车发往选煤厂的
        private int SelectXSFS(string xsfs)
        {
            int result = 0;
            if (xsfs == "车运")
            {
                result = 1;
            }
            if (xsfs == "地销")
            {
                result = 2;
            }
            if (xsfs == "水运")
            {
                result = 3;
            }
            if (xsfs == "皮带销售")
            {
                result = 4;
            }
            if (xsfs == "原煤入洗")
            {
                result = 5;
            }
            return result;


        }
        private int SelectMZType(string mztype)
        {
            int result = 0;
            if (mztype == "筛末煤")
            {
                result = 7;
            }
            if (mztype == "块煤矸")
            {
                result = 8;
            }
            if (mztype == "脏杂煤")
            {
                result = 21;
            }
            if (mztype == "掘进煤")
            {
                result= 15;
            }
            if (mztype == "基建煤")
            {
                result = 17;
            }
            if (mztype == "块煤")
            {
                result = 5;
            }
            return result;
        }
        /// <summary>
        /// 判断是否存在数据
        /// </summary>
        /// <param name="strhydm">每条煤质数据代码</param>
        /// <returns>是否有数据</returns>
        private int DataExist(string strhydm,string pc,string dt,int id)
        {
           int nReturn = -1;
            try
            {
                
                SqlCommand sqlComm1 = null;
                SqlConnection conn = GetConnection();
                string strComm1 = "select count(hydm) from s_coalQuality where hydm='"+ strhydm +"'";
                sqlComm1 = new SqlCommand(strComm1, conn);
                int row= Convert.ToInt32(sqlComm1.ExecuteScalar());
                
                if (row <= 0)
                    nReturn= 0;
                else
                    nReturn= 1;
                sqlComm1.Dispose();
                return nReturn;
            }
            catch (OracleException ex)
            {
                this.WriteLog("DataExist 函数 " + ex.ToString());
                return -1;
            }
        }

        private int SqlDataExist(string strhydm, string pc, string dt, int id)
        {
            int nReturn = -1;
            try
            {

                SqlCommand sqlComm1 = null;
                SqlConnection conn = GetConnection();
                string strComm1 = "select count(hydm) from s_coalQuality where hydm='" + strhydm + "'";
                sqlComm1 = new SqlCommand(strComm1, conn);
                int row = Convert.ToInt32(sqlComm1.ExecuteScalar());

                if (row <= 0)
                    nReturn = 0;
                else
                    nReturn = 1;
                sqlComm1.Dispose();
                conn.Close();
                conn.Dispose();
                return nReturn;
            }
            catch (OracleException ex)
            {
                this.WriteLog("DataExist 函数 " + ex.ToString());
                return -1;
            }
        }
        /// <summary>
        /// 更新s_unit表
        /// </summary>
        /// <param name="MKDM">煤矿代码</param>
        /// <returns>单位编号</returns>
        private int InsertUnit(int MKDM)
        {
            try
            {
                string strComm = null,strName="测试";
                int nTest = -1, nUnitID=-1;
                string[] strInfo = new string[2]; //base.GetIPAndGate();//得到ip和gateip
                strInfo[0] = "137.12.9.101";
                strInfo[1] = "137.12.8.254";
                //****************更新unit表************
                nTest = SelectData(9, MKDM, strName, 0, null);
                //依据MKDM得到名称
                strName = GetName(MKDM);
                strName = strName + "[质]";
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                if (nTest == -1)//
                    return -1;
                if (nTest == 1)//有数据
                {
                    strComm = "select unitid from s_unit where MKDM=" + MKDM + "";
                    sqlComm.CommandText = strComm;
                    object obj1 = sqlComm.ExecuteScalar();
                    nUnitID = Convert.ToInt32(obj1);
                                    
                    //更新ip
                    strComm = "update s_unit set UnitName='" + strName + "',hostip='" + strInfo[0] + "',gateip='" + strInfo[1] + "',unittypeid=1,userGroupID=19 where MKDM=" + MKDM + "";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                 }          
                if (nTest == 0)//无数据
                {
                    strComm = "insert into s_unit (mkdm,unitname,hostip,gateip,unittypeid,userGroupID) values(" + MKDM + ",'"+strName+"','" + strInfo[0] + "','" + strInfo[1] + "',1,19);select @@identity";
                    sqlComm.CommandText = strComm;
                    object obj=sqlComm.ExecuteScalar();
                    nUnitID = Convert.ToInt32(obj);

                }
                sqlComm.Dispose();
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                this.WriteLog("淮南煤质InsertUnit 函数" + ex.ToString());
                return -1;
            }
        }
        /// <summary>
        /// 判断传入的设备id在MZdevice表中是否存在
        /// </summary>
        /// <param name="mzdevice"></param>
        /// <returns></returns>
        private bool BoDevice(int mzdevice)
        {
            try
            {
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
                string strComm = "select MzDeviceID from S_MzDevice where MzDeviceID=" + mzdevice + "";
                sqlComm.CommandText = strComm;
                object obj1 = sqlComm.ExecuteScalar();
                conn.Close();
                conn.Dispose();
                if (obj1 != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        private int InsertUnitSQL(string companyname)
        {
            try
            {
                string strComm = null, strName = "测试";
                int nUnitID = -1;

                //****************更新unit表************

                if (companyname == "谢桥矿老井")
                {
                    companyname = "谢桥老井";
                }
                if (companyname == "谢桥矿箕斗井")
                {
                    companyname = "谢桥箕斗井";
                }
                if (companyname == "朱集东矿")
                {
                    companyname = "朱集矿";
                }
                if (companyname == "潘一东矿")
                {
                    companyname = "潘一矿东井";
                }
                //if(companyname== "潘一矿选煤厂")
                //{
                //    companyname = "潘一选煤厂";
                //}
                strName = companyname + "[质]";
                //strName = companyname ;
                SqlConnection conn = GetConnection();
                SqlCommand sqlComm = new SqlCommand();
                sqlComm.Connection = conn;
               
                strComm = "select MzUnitID from S_MzUnit where UnitName='" + strName + "'";
                sqlComm.CommandText = strComm;
                object obj1 = sqlComm.ExecuteScalar();
                if (obj1 != null)
                {
                    nUnitID = Convert.ToInt32(obj1);
                }
                else
                {
                    nUnitID = -1;
                }
                sqlComm.Dispose();
               
                    conn.Close();
                conn.Dispose();
                return nUnitID;
            }
            catch (Exception ex)
            {
                this.WriteLog("淮南煤质InsertUnit 函数" + ex.ToString());
                return -1;
            }
        }
        /// <summary>
        /// 在s_deviceid中插入更新设备序号
        /// </summary>
        /// <param name="MKDM">在oracle中的煤矿代码</param>
        private int InsertDeviceID(int nUnitID)
        {
            int nTest = 0, nCurUnitID = 0, nMKDM = 0;
            string strComm = null, strhfyName = null;
            string[] strMKMC = new string[] { "谢一矿", "zi", };
            try
            {
                strhfyName = "煤质设备";
                nTest = SelectData(4, nUnitID, null, nUnitID * 100 + 70, null);//第三个参数暂时不用 通过秤nUnitID*100+50判断
                //this.WriteLog("淮南煤质InsertDeviceID函数是否具有设备ID");
                if (nTest == -1)
                    return -1;
                if (nTest == 1)//有数据
                { }

                if (nTest == 0)//无数据   HFYFactoryID=3
                {
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.Connection = GetConnection();
                    nCurUnitID = nUnitID * 100 + 70;
                    strComm = "insert into s_device (DeviceID,DeviceName,UnitID,DeviceTypeID,HFYFactoryID,HFYTypeID,JLTypeID)values(" + nCurUnitID + ",'" + strhfyName + "'," + nUnitID + ",2,3,1,2)";
                    sqlComm.CommandText = strComm;
                    sqlComm.ExecuteNonQuery();
                    if (sqlComm.Connection.State == ConnectionState.Open)
                        sqlComm.Connection.Close();
                    sqlComm.Connection.Dispose();
                }
                return 0;
            }
            catch (Exception ex)
            {
                WriteLog("InsertDeviceID下的GetInitPara函数" + ex.Message);
                return -2;
            }
        }
        /// <summary>
        /// 选取合适的数据
        /// </summary>
        /// <param name="MKDM">煤矿代码</param>
        /// <param name="flow_type">流程类型</param>
        /// <param name="pp">煤类型</param>
        /// <returns>是否更新插入数据库</returns>
        private int SelectCoal(int MKDM,int flow_type,int pp,int xstype)
        {
            //流程类型(1车2水3地4专运线5皮带)' 
            //外销 块煤 筛末 煤块 煤矸 掘进煤 基建煤 
            int nReturn = 0;
         
            switch(MKDM)
            {
                case 0:
                    break;
                case 1://新庄子矿
                    {
                        if ((flow_type == 1) || (flow_type == 2) || (flow_type == 3))
                            nReturn=1;
                        break;
                    }
                    
                case 2://谢一矿
                    {
                        if ((flow_type == 1) || (flow_type == 2) || (flow_type == 3))
                            nReturn = 1;
                        break;
                    }
                case 6://李嘴孜矿[质]
                    {
                        if ((flow_type == 1) || (flow_type == 2) || (flow_type == 3))
                            nReturn = 1;
                        break;
                    }
                case 7://潘一一厂[质]
                    {

                        if (pp == 5 || pp == 7|| pp == 21 || pp == 8 || pp == 15|| pp == 17)
                            nReturn = 1;
                        break;
                    }
                case 8://潘二分厂[质]
                    {
                        break;
                    }
                case 9://潘三分厂[质]
                    {
                        if (pp == 5 || pp == 7|| pp == 21 || pp == 8 || pp == 15|| pp == 17)
                            nReturn = 1;
                        break;
                    }
                case 10://谢桥分厂[质]
                    {
                        if (pp == 5 || pp == 7 || pp == 21 || pp == 8 || pp == 15 || pp == 17)
                            nReturn = 1;
                        break;
                    }
                case 11://张集一厂[质]
                    {
                        if (pp == 5 || pp == 7 || pp == 21 || pp == 8 || pp == 15 || pp == 17)
                            nReturn = 1;
                        break;
                    }
                case 12://望选厂[质]
                    {
                        break;
                    }
                case 13://张集二厂[质]
                    {
                        break;
                    }
                case 15://新庄孜选煤厂[质]
                    {
                        break;
                    }
                 case 16://顾桥分厂[质]
                    {
                        break;
                    }
                 case 17://丁集矿[质][质]
                    {
                        if (pp == 5 || pp == 7 || pp == 21 || pp == 8 || pp == 15 || pp == 17)
                            nReturn = 1;
                        break;
                    }
                 case 18://顾北矿[质]
                    {
                        break;
                    }
                 case 19://潘北分厂[质
                    {
                        break;
                    }
                 case 21://朱集分厂[质]
                    {
                        break;
                    }
                 case 22://潘一东分厂[质]
                    {
                        break;
                    }
                default:
                    break;
            }
            return nReturn;
         }
        /// <summary>
        /// 删除sql Server 数据库中多余的数据
        /// </summary>
        /// <param name="arOracle">hydm 集合</param>
        /// <returns></returns>
        private int SqlDataDelete(ArrayList arOracle)
        {
            if((strDT1==null)||(strDT2==null))
            {
                return 0;
            }
            SqlConnection conn = null;
            try
            {
                ArrayList arSql = new ArrayList();
                ArrayList arDelete = new ArrayList();
                string strSqlHYDM = null;
                string strDelete = null;
                arSql.Clear();
                arDelete.Clear();
                 conn = GetConnection();
                SqlCommand sqlCommDel = new SqlCommand();
                sqlCommDel.CommandText = "select * from s_coalquality where caiyangdt between '" + strDT1 + "' and '" + strDT2 + "' order by caiyangdt";
                sqlCommDel.Connection = conn;
                SqlDataReader sqlDR = sqlCommDel.ExecuteReader(CommandBehavior.CloseConnection);
                strDelete = "delete from s_coalquality where hydm in (";
                while (sqlDR.Read())
                {
                    strSqlHYDM = sqlDR["hydm"].ToString();
                    bool bCF = arOracle.Contains(strSqlHYDM);
                    if (!bCF)
                    {
                        arDelete.Add(strSqlHYDM);
                    }
                }
                sqlDR.Close();
                sqlDR.Dispose();
                foreach (string strDel in arDelete)
                {
                   strDelete += "'" + strDel + "'" + ",";
                   this.WriteLog("hydm" + strDel + "  在煤质服务器已经删除，sql Server 准备进行同步删除");
                }
                if (arDelete.Count != 0)//如果没有重复的数据就不删除
                {
                    strDelete = strDelete.Remove(strDelete.Length - 1, 1);
                    strDelete += ")";
                    sqlCommDel.CommandText = strDelete;
                    sqlCommDel.ExecuteNonQuery();
                }
                sqlCommDel.Dispose();
                conn.Close();
                conn.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                this.WriteLog("SqlDataDelete函数错误"+ex.ToString());
                return 1;
            }
            finally
            {
                if (conn != null)
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                }
            }
        }
        /// <summary>
        /// 筛选煤质数据
        /// </summary>
        /// <param name="MKDM">煤矿代码</param>
        /// <param name="flow_type">流程类型</param>
        /// <param name="pp">品种</param>
        /// <param name="xstype">销售类型</param>
        /// <returns>是否插入到数据库</returns>
        private int SelectQuality(int MKDM, int flow_type, int pp, int xstype)
        {
            int nReturn = 0;
            if (flow_type == 0)
                return 0;
            if (xstype == 0)
                return 0;
            switch (MKDM)
            {
                case 0:
                    break;
                case 1://新庄子矿
                    {
                        if ((flow_type == 2) || (flow_type == 3) || (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5)))
                            nReturn = 1;
                        break;
                    }

                case 2://谢一矿
                    {
                        if ((flow_type == 1) || (flow_type == 2) || (flow_type == 3) || (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5)))
                            nReturn = 1;
                        break;
                    }
                case 6://李嘴孜矿[质]
                    {
                        if ((flow_type == 1) || (flow_type == 2) || (flow_type == 3))
                            nReturn = 1;
                        break;
                    }
                case 7://潘一一厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        if ((flow_type == 3 || flow_type == 2) && (pp == 5 || pp == 7))
                            nReturn = 1;
                        break;
                    }
                case 8://潘二分厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        break;
                    }
                case 9://潘三分厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        if ((flow_type == 3 || flow_type == 2) && (pp == 5 || pp == 7))
                            nReturn = 1;
                        break;
                    }
                case 10://谢桥分厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        if ((flow_type == 3 || flow_type == 2) && (pp == 5 || pp == 7))
                            nReturn = 1;
                        break;
                    }
                case 11://张集一厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        if ((flow_type == 3 || flow_type == 2) && (pp == 5 || pp == 7))
                            nReturn = 1;
                        break;
                    }
                case 12://望选厂[质]
                    {

                        nReturn = 1;
                        break;
                    }
                case 13://张集二厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        break;
                    }
                case 15://新庄孜选煤厂[质]
                    {
                        nReturn = 0;
                        break;
                    }
                case 16://顾桥分厂[质]
                    {
                        nReturn = 0;
                        break;
                    }
                case 17://丁集矿[质][质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        break;
                    }
                case 18://顾北矿[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        break;
                    }
                case 19://潘北分厂[质
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        break;
                    }
                case 21://朱集分厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        break; ;
                    }
                case 22://潘一东分厂[质]
                    {
                        if (flow_type == 5 && (xstype == 0 || xstype == 2 || xstype == 3 || xstype == 4 || xstype == 5))
                            nReturn = 1;
                        break;
                    }
                default:
                    break;
            }
            return nReturn;
        }
        #endregion

        #region 辅助处理函数
        /// <summary>
        /// 将煤质服务器的连通状态更新到sql server
        /// </summary>
        /// <param name="strState"></param>
        public void WriteOracleStateUnit(string strState)
        {
            Debug.Print("WriteOracaleSTateUnit Start");
            try
            {
                SqlConnection conn = GetConnection();
                string strComm = "update c SET c.ns='" + strState + "' from  (select A.NetState as ns  from S_Unit as A,S_Device as B where B.DeviceTypeID=2 and A.UnitID=B.UnitID) as c";
                SqlCommand sqlComm = new SqlCommand(strComm,conn);
                sqlComm.ExecuteNonQuery();
                sqlComm.Dispose();
                conn.Close();
                conn.Dispose();
            }
            catch (Exception ex)
            {
                return;
            }
            finally
            {
                Debug.Print("WriteOracaleSTateUnit Start");
            }
            return;
            
        }
        /// <summary>
        /// 将煤质服务器的连通状态写入增加到sql server
        /// </summary>
        /// <param name="strState">状态信息</param>
        /// <param name="strRemark">备注</param>
        /// <returns></returns>
        public int WriteOracleStateHis(string strState,string strRemark)
        {
            SqlConnection conn = null;
            try
            {
                conn = GetConnection();
                int nUnit = 0;
                string strComm = "select  * from S_Unit as A,S_Device as B where B.DeviceTypeID=2 and A.UnitID=B.UnitID";
                SqlCommand sqlComm = new SqlCommand(strComm, conn);
                SqlDataReader dr = sqlComm.ExecuteReader();
                SqlCommand sqlCommInsert = new SqlCommand();
                sqlCommInsert.Connection = conn;
                while (dr.Read())
                {
                    nUnit = Convert.ToInt16(dr["unitid"]);
                    sqlCommInsert.CommandText = "insert into s_statehis (curdt,unitid,netstate,remark) values('" + DateTime.Now.ToString() + "'," + nUnit + ",'" + strState + "','" + strRemark + "')";
                    sqlCommInsert.ExecuteNonQuery();
                }
                sqlCommInsert.Dispose();
                sqlComm.Dispose();
                conn.Close();
                conn.Dispose();
                return 0;
            }
            catch (Exception ex)
            {
                return 1;
            }
 
        }
        /// <summary>
        /// 获取发票单价 发运量 折合量
        /// </summary>
        /// <param name="strYsdm">与zxl_cxhy关联字段</param>
        /// <returns></returns>
        public float [] AddJiaGe(string strYsdm)
        {
            
             OracleDataReader dr=null;
            OracleCommand oracleComm =null;
            float[] fReturn = new float[3]{0,0,0};
            if (strYsdm == null)
            {
                return fReturn;
            }

            try
            {
                string strComm = "select * from v_js_ysfyb where ysdm='" + strYsdm + "'";
                //ysdmConn = GethnConnection();
                oracleComm = new OracleCommand(strComm, oracleConn);
                dr=oracleComm.ExecuteReader();
                while (dr.Read())
                {
                    if (dr["fyl"].GetType().Name == "DBNull")
                    {
                        fReturn[0] = 0f;
                    }
                    else
                    {
                        fReturn[0] = Convert.ToSingle(dr["fyl"].ToString());
                    }
                    if (dr["jsl"].GetType().Name == "DBNull")
                    {
                        fReturn[1] = 0f;
                    }
                    else
                    {
                        fReturn[1] = Convert.ToSingle(dr["jsl"].ToString());
                    }
                    if (dr["FPDJ"].GetType().Name == "DBNull")
                    {
                        fReturn[2] = 0f;
                    }
                    else
                    {
                        fReturn[2] = Convert.ToSingle(dr["FPDJ"].ToString());
                    }


                }
               
              
                this.WriteLog("AddJiaGe函数执行正常2");
                return fReturn;

            }
            catch (Exception ex)
            {
                this.WriteLog("AddJiaGe函数" + ex.ToString());
                fReturn= null;
            }
            finally
            {
                if (oracleComm != null)
                {
                    oracleComm.Dispose();
                }
                if (dr != null)
                {
                    dr.Close();
                    dr.Dispose();
                }
              
           
            }
             return fReturn;

        }
        #endregion
    }
       
}
